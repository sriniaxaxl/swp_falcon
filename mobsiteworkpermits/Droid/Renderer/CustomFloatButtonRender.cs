﻿using System;
using CustomControl;
using Droid.Renderer;
using FB = Android.Support.Design.Widget.FloatingActionButton;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content.Res;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(CusFloatButton), typeof(CustomFloatButtonRender))]
namespace Droid.Renderer
{
    public class CustomFloatButtonRender : Xamarin.Forms.Platform.Android.AppCompat.ViewRenderer<CusFloatButton, FB>
    {
        CusFloatButton Fabbtn;  
        protected override void OnElementChanged(ElementChangedEventArgs<CusFloatButton> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;
            
            Fabbtn = (CusFloatButton)Element;
            if (this.Control == null)
            {
                this.ViewGroup.SetClipChildren(false);
                this.ViewGroup.SetClipToPadding(false);
            }

            var fab = new FB(Context);
            Android.Support.V4.View.ViewCompat.SetBackgroundTintList(fab, ColorStateList.ValueOf(Element.ButtonColor.ToAndroid()));
            fab.UseCompatPadding = false;
            fab.Elevation = 5.0f;
            fab.SetBackgroundColor(Color.Transparent.ToAndroid());      
            //fab.SetImageResource(CustomControl.Droid.Resource.Drawable.ic_add_white);
            //var imageFile = elementImage?.File;
            //if (imageFile != null)
            //{
                
            //    //fab.SetImageDrawable(Context.Resources.GetDrawable(imageFile));
            //}
            fab.Click += Fab_Click;
            SetNativeControl(fab);
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            //Control.BringToFront();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var fab = (FB)Control;
            if (e.PropertyName == nameof(Element.ButtonColor))
            {
                Android.Support.V4.View.ViewCompat.SetBackgroundTintList(fab, ColorStateList.ValueOf(Element.ButtonColor.ToAndroid()));
            }
            //if (e.PropertyName == nameof(Element.Image))
            //{
            //    //var elementImage = Element.Image;
            //    //var imageFile = elementImage?.File;
            //    //if (imageFile != null)
            //    //{
            //    //    fab.SetImageDrawable(Context.Resources.GetDrawable(imageFile));
            //    //}
            //}
            base.OnElementPropertyChanged(sender, e);
        }


        private void Fab_Click(object sender, EventArgs e)
        {
            Fabbtn.Clicked.Invoke(Fabbtn, e);
        }
    }
}
