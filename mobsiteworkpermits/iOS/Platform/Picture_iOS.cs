﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using SiteWorkPermits.iOS.Platform;
using SiteWorkPermits.Platform;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(Picture_iOS))]
namespace SiteWorkPermits.iOS.Platform
{
    public class Picture_iOS : IPicture
    {
        public async Task<bool> SavePictureToDisk(string filename, byte[] imageData)
        {
            var chartImage = new UIImage(NSData.FromArray(imageData));
            bool isuccess = false;
            chartImage.SaveToPhotosAlbum((image, error) =>
            {
                //you can retrieve the saved UI Image as well if needed using  
                //var i = image as UIImage;  
                isuccess = true;
                if (error != null)
                {
                    Console.WriteLine(error.ToString());
                    isuccess = false;
                }
                else {
                    isuccess = true;
                }
            });
            return isuccess;
        }
    }
}