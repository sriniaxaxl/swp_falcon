﻿using SiteWorkPermits.DAL.HotworksPermitDAL;
using Prism.Navigation;
using Prism.Services;
using Prism.Mvvm;
using System.Linq;
using Prism.Commands;
using System.Collections.ObjectModel;
using SiteWorkPermits.Model;
using System;
using Xamarin.Forms;
using SiteWorkPermits.Model.SyncHotWorks;
using System.Collections.Generic;
using SiteWorkPermits.DAL.RestHelper;
using System.Threading.Tasks;
using CustomControl;
using Plugin.Connectivity;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.Model.SyncImpairements;
using SiteWorkPermits.Platform;
using System.Diagnostics;
using SiteWorkPermits.DAL.sqlIntegration;
using SiteWorkPermits.Resx;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.CommonViews;
using static CustomControl.Validation;
using System.Windows.Input;
using SQLite;

namespace SiteWorkPermits
{
    public class HomePageViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;
        public IPageDialogService _pageDialog;
        IWorkOrdersService _workOrdersService;
        IHotworkPermitService _hotworkPermitService;
        IScopeService _scopeService;
        private IImpairmentClassService _impairmentClassService;
        IAuthorizationService _authorizationService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        IHighHazardAreasService _highHazardAreasService;
        IImpairmentService _impairmentService;
        IIncUniqueid _incUniqueid;
        public event RefreshListAction RefreshListEvent;
        public delegate void RefreshListAction();
        IImpairmentTypeService _impairmentTypeService;
        IShutDownReasonService _shutDownReasonService;
        public DelegateCommand<string> OnNavigateCommand { get; set; }
     //   public DelegateCommand OnSyncCommand { get; set; }
        //public DelegateCommand<string> OnPermitTapComand { get; set; }
        public IList<ModelWorkPermit> Items { get; private set; }

        public DelegateCommand<string> DisplayActiveData { get; set; }

        private ObservableCollection<ModelWorkPermit> _activePermitsNImpairments;

        public ObservableCollection<ModelWorkPermit> ActivePermitsNImpairments
        {
            get { return _activePermitsNImpairments; }
            set { SetProperty(ref _activePermitsNImpairments, value); }
        }

        private Color _ActiveHWColor;
        public Color ActiveHWColor
        {
            get { return _ActiveHWColor; }
            set { SetProperty(ref _ActiveHWColor, value); }
        }

        private int _activeHWCount;
        public int ActiveHWCount
        {
            get { return _activeHWCount; }
            set { SetProperty(ref _activeHWCount, value); }
        }

        private string _activeImpCount;
        public string ActiveImpCount
        {
            get { return _activeImpCount; }
            set { SetProperty(ref _activeImpCount, value); }
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }

         private bool _showLogoutLoader;
        public bool showLogoutLoader
        {
            get { return _showLogoutLoader; }
            set { SetProperty(ref _showLogoutLoader, value); }
        }

        private string _xLMySiteLabel;
        public string XLMySiteLabel
        {
            get { return _xLMySiteLabel; }
            set { SetProperty(ref _xLMySiteLabel, value); }
        }

        private string _companyNameLabel;
        public string CompanyNameLabel
        {
            get { return _companyNameLabel; }
            set { SetProperty(ref _companyNameLabel, value); }
        }

        private string _companyAddressLabel;
        public string CompanyAddressLabel
        {
            get { return _companyAddressLabel; }
            set { SetProperty(ref _companyAddressLabel, value); }
        }

        private string _ImpStartDate;
        public string ImpStartDate
        {
            get { return _ImpStartDate; }
            set { SetProperty(ref _ImpStartDate, value); }
        }

        private bool _showCloseAuthorizerName;
        public bool ShowCloseAuthorizerName
        {
            get { return _showCloseAuthorizerName; }
            set { SetProperty(ref _showCloseAuthorizerName, value); }
        }

        private ObservableCollection<HotworksPermits> _hotworksPermitsList;
        public ObservableCollection<HotworksPermits> HotworksPermitsList
        {
            get { return _hotworksPermitsList; }
            set { SetProperty(ref _hotworksPermitsList, value); }
        }

        private Color _ActiveImpColor;

        public Color ActiveImpColor
        {
            get { return _ActiveImpColor; }
            set { SetProperty(ref _ActiveImpColor, value); }
        }

        public ObservableCollection<ObservableGroupCollection<string, ModelWorkPermit>>
            GroupedData { get; set; }

        private ObservableCollection<ModelWorkPermit> _workPermits;

        public ObservableCollection<ModelWorkPermit> WorkPermits
        {
            get { return _workPermits; }
            set { SetProperty(ref _workPermits, value); }
        }

        private ObservableCollection<ListViewElement> _listViewElements;

        public ObservableCollection<ListViewElement> ListViewElements
        {
            get { return _listViewElements; }
            set { SetProperty(ref _listViewElements, value); }
        }

        private ObservableCollection<ListViewElement> _ImpairmentlistViewElements;

        public ObservableCollection<ListViewElement> ImpairmentListViewElements
        {
            get { return _ImpairmentlistViewElements; }
            set { SetProperty(ref _ImpairmentlistViewElements, value); }
        }

        private ObservableCollection<ModelPermits> _ActivePermitCollection;
        public ObservableCollection<ModelPermits> ActivePermitCollection
        {
            get { return _ActivePermitCollection; }
            set { SetProperty(ref _ActivePermitCollection, value); }
        }
        private ObservableCollection<ModelWorkPermit> _workImpairment;

        public ObservableCollection<ModelWorkPermit> WorkImpairment
        {
            get { return _workImpairment; }
            set { SetProperty(ref _workImpairment, value); }
        }

        private ObservableCollection<ModelPermits> _ActiveImpairmentCollection;
        public ObservableCollection<ModelPermits> ActiveImpairmentCollection
        {
            get { return _ActiveImpairmentCollection; }
            set { SetProperty(ref _ActiveImpairmentCollection, value); }
        }

        private ObservableCollection<ModelPermits> _DraftPermitCollection;
        public ObservableCollection<ModelPermits> DraftPermitCollection
        {
            get { return _DraftPermitCollection; }
            set { SetProperty(ref _DraftPermitCollection, value); }
        }

        private ObservableCollection<ModelPermits> _UpcomingPermitCollection;
        public ObservableCollection<ModelPermits> UpcomingPermitCollection
        {
            get { return _UpcomingPermitCollection; }
            set { SetProperty(ref _UpcomingPermitCollection, value); }
        }

        private ObservableCollection<ModelPermits> _ExpiredPermitCollection;
        public ObservableCollection<ModelPermits> ExpiredPermitCollection
        {
            get { return _ExpiredPermitCollection; }
            set { SetProperty(ref _ExpiredPermitCollection, value); }
        }


        private bool _showClosePopup;
        public bool showClosePopup
        {
            get { return _showClosePopup; }
            set { SetProperty(ref _showClosePopup, value); }
        }

        HotWorkPermit_sql _workPermit_Sql;
        Impairment_Sql _impairment_Sql;
        Scope_sql _workPermit_scope_Sql;

        private string _elementCategory_sql;
        public string ElementCategory_sql
        {
            get { return _elementCategory_sql; }
            set { SetProperty(ref _elementCategory_sql, value); }
        }

        private string _elementDescription_Sql;
        public string ElementDescription_Sql
        {
            get { return _elementDescription_Sql; }
            set { SetProperty(ref _elementDescription_Sql, value); }
        }

        private string _elementStartDate_Sql;
        public string ElementStartDate_Sql
        {
            get { return _elementStartDate_Sql; }
            set { SetProperty(ref _elementStartDate_Sql, value); }
        }

        private string _impReason;
        public string ImpReason
        {
            get { return _impReason; }
            set { SetProperty(ref _impReason, value); }
        }

        private string _PopupMsg;
        public string PopupMsg
        {
            get { return _PopupMsg; }
            set { SetProperty(ref _PopupMsg, value); }
        }

        private int impHandheldId;

        private TimeSpan _selectedTime;
        public TimeSpan SelectedTime
        {
            get { return _selectedTime; }
            set { SetProperty(ref _selectedTime, value); }
        }

        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set { SetProperty(ref _selectedDate, value); }
        }

        private string _impType;
        public string ImpType
        {
            get { return _impType; }
            set { SetProperty(ref _impType, value); }
        }

        private string _impClass;
        public string ImpClass
        {
            get { return _impClass; }
            set { SetProperty(ref _impClass, value); }
        }

        private DateTime _closedDate;
        public DateTime ClosedDate
        {
            get { return _closedDate; }
            set { SetProperty(ref _closedDate, value); }
        }



        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

          private bool _showNoHWLabel;
        public bool showNoHWLabel
        {
            get { return _showNoHWLabel; }
            set { SetProperty(ref _showNoHWLabel, value); }
        }


        private bool _showNoIMPLabel;
        public bool showNoIMPLabel
        {
            get { return _showNoIMPLabel; }
            set { SetProperty(ref _showNoIMPLabel, value); }
        }



        private string _uniqueId;

        private Color _hotworkTabColor;
        public Color HotworkTabColor
        {
            get { return _hotworkTabColor; }
            set { SetProperty(ref _hotworkTabColor, value); }
        }

        private Color _impairmentTabColor;
        public Color ImpairmentTabColor
        {
            get { return _impairmentTabColor; }
            set { SetProperty(ref _impairmentTabColor, value); }
        }

        private bool _showImpListView;
        public bool showImpListView
        {
            get { return _showImpListView; }
            set { SetProperty(ref _showImpListView, value); }
        }


        private bool _showHotworkListView;
        public bool showHotworkListView
        {
            get { return _showHotworkListView; }
            set { SetProperty(ref _showHotworkListView, value); }
        }

        private bool _isCloseDetailVisible;
        public bool isCloseDetailVisible
        {
            get { return _isCloseDetailVisible; }
            set { SetProperty(ref _isCloseDetailVisible, value); }
        }


        private string _ClosureText;
        public string ClosureText
        {
            get { return _ClosureText; }
            set { SetProperty(ref _ClosureText, value); }
        }

        private string _CloseType;
        public string CloseType
        {
            get { return _CloseType; }
            set { SetProperty(ref _CloseType, value); }
        }

        private string _closureTypeBtnText;
        public string ClosureTypeBtnText
        {
            get { return _closureTypeBtnText; }
            set { SetProperty(ref _closureTypeBtnText, value); }
        }

        string _element;

        SQLiteConnection _connection;

        private ObservableCollection<ListViewElement> ListViewElements_Temp;
        private ObservableCollection<ListViewElement> ImpairmentListViewElements_Temp;

        public HomePageViewModel() { }
        public HomePageViewModel(INavigationService navigationService,
                                 IPageDialogService pageDialog,
                                 IIncUniqueid incUniqueid,
                                 IHotworkPermitService hotworkPermitService,
                                 IWorkOrdersService workOrdersService,
                                 IImpairmentClassService impairmentClassService,
                                 IScopeService scopeService, IAuthorizationService authorizationService,
                                 IFirePreventionMeasuresService firePreventionMeasuresService,
                                 IPrecautionsService precautionsService,
                                 IImpairmentService impairmentService,
                                 IShutDownReasonService shutDownReasonService,
                                 IImpairmentTypeService impairmentTypeService,
                                 IFirewatchPersonsService firewatchPersonsService,
                                 IHighHazardAreasService highHazardAreasService)
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            _workOrdersService = workOrdersService;
            _scopeService = scopeService;
            _incUniqueid = incUniqueid;
            _impairmentTypeService = impairmentTypeService;
            _impairmentClassService = impairmentClassService;
            _hotworkPermitService = hotworkPermitService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _scopeService = scopeService;
            _shutDownReasonService = shutDownReasonService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _highHazardAreasService = highHazardAreasService;
            _authorizationService = authorizationService;
            ShowActivityLoader = false;
            _impairmentService = impairmentService;
            OnNavigateCommand = new DelegateCommand<string>(NavigateAsync);

            DisplayActiveData = new DelegateCommand<string>(DisplayActiveDataAsync);
            ClosureText = AppResource.IMPAIRMENT_CLOSE_TEXT;
            ClosureTypeBtnText = AppResource.CLOSE_IMPAIRMENT;
            CloseType = AppResource.CLOSE_IMPAIRMENT;
            isCloseDetailVisible = true;
            ActiveHWColor = Color.FromHex("00afad");
            ShowSyncIcon = true;
         //   OnSyncCommand = new DelegateCommand(async () => await SyncData());
            App.HomeNavigation = navigationService;
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            analyticsService.LogScreen("DASHBOARD");
            ActivePermitsNImpairments = new ObservableCollection<ModelWorkPermit>();
            PopupMsg = AppResource.DASHBOARD_SYNC_MSG;
            SelectedDate = DateTime.Now;
            SelectedTime = SelectedDate.TimeOfDay;
            ShowCloseAuthorizerName = false;
            // CompanyNameLabel = "Global Industries Inc";
            XLMySiteLabel = "AXA XL Site Work Permits";
            App.isImpairementEditorDuplicate = "";
            App.Subscribval++;
            Debug.WriteLine(App.Subscribval);
            HotworkTabColor = Color.FromHex("#00AFAD");
            showHotworkListView = true;
            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "NavigateToPrintPermit", async (sender) =>
            {
                await _navigationService.NavigateAsync("PrintPermitPage", null, null, false);
            });

            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "NavigateToPrintImp", async (sender) =>
            {
                await _navigationService.NavigateAsync("PrintImpairmentPage", null, null, false);
            });

            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "NavigateToArchive", async (sender) =>
            {
                await _navigationService.NavigateAsync("ArchivePage", null, null, false);
            });
        }

        public ICommand OnSyncCommand
        {
            get
            {
                return new Command(async (x) =>
                {
                    try
                    {
                        await SyncData();
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
        }

        private async Task<bool> IsCloseEmailSent(Impairment_Sql impairment)
        {

            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });
                try
                {
                    var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                    analyticsService.LogEvent("", "Imapirment Submit", "Impairment Submit");
                    string impguid = string.Empty;
                    int impserverid = 0;
                    if (impairment.server_id == 0)
                    {
                        var impairements = CustomControl.Validation.GetServerImpaierment(impairment);
                    //    impairements.Result.CloseDateTime = Convert.ToDateTime(impairment.impairmentCloseDateTime);
                        _connection.UpdateWithChildren(impairment);
                        var createImpResponse = await RestApiHelper<CreateIMPResponse>.CreateImp_Post(impairements.Result);

                        if (createImpResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            return false;
                        }
                        else if (createImpResponse.Data != null)
                        {
                            impguid = createImpResponse.Data.GUID;
                            impserverid = createImpResponse.Data.Id;
                        }
                        else if (createImpResponse.Status != null)
                        {
                            if (createImpResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", createImpResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                // Xamarin.Forms.Application.Current.Properties.Clear();

                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                return false;
                            }
                        }

                        impairment.server_id = impserverid;
                        impairment.impairmentGUID = impguid;
                        //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                    //    var connection = hotworkDatabase.GetdbConnection();
                        _connection.UpdateWithChildren(impairment);
                    }
                    else
                    {
                        _connection.UpdateWithChildren(impairment);
                        await IsSyncDone();
                        impguid = impairment.impairmentGUID;
                    }

                    var isEmailSentResponse = await RestApiHelper<EmailHWResponse>.SendEmailImp_Post(impguid, string.Empty, "USLETTER");

                    //bool isEmailSent;
                    if (isEmailSentResponse == null)
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                        return false;
                    }
                    else if (isEmailSentResponse.Status != null)
                    {
                        if (isEmailSentResponse.Status.StatusCode == 401)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", isEmailSentResponse.Status.StatusMessage, AppResource.OK_LABEL);
                            Xamarin.Forms.Application.Current.Properties.Clear();
                            await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                            ShowActivityLoader = false;
                            return false ;
                        }
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    if (isEmailSentResponse.Data.Value)
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.IMP_SUBMITTED_SUCCESS, AppResource.OK_LABEL);
                        // await _navigationService.NavigateAsync("HomePage");
                        return true;
                    }
                    else
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                    }

                }
                catch (Exception e)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    Console.WriteLine("IOException source: {0}", e.Source);
                    return false;
                }
            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                return false;
            }
            return false;
        }

        private async Task<bool> IsHWCloseEmailSent(HotWorkPermit_sql permit_Sql)
        {

            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });
                try
                {
                    var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                    analyticsService.LogEvent("", "Permit Submit", "Permit Submit");
                    string permitguid = string.Empty;
                    int permitserverid = 0;
                    if (permit_Sql.server_id == 0)
                    {
                        var serverHotwork = CustomControl.Validation.GetServerHotwork(permit_Sql);
                        //    impairements.Result.CloseDateTime = Convert.ToDateTime(impairment.impairmentCloseDateTime);
                        _connection.UpdateWithChildren(permit_Sql);
                        var createHWResponse = await RestApiHelper<CreateIMPResponse>.CreateHW_Post(serverHotwork.Result);

                        if (createHWResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            return false;
                        }
                        else if (createHWResponse.Data != null)
                        {
                           permitguid = createHWResponse.Data.GUID;
                            permitserverid = createHWResponse.Data.Id;
                        }
                        else if (createHWResponse.Status != null)
                        {
                            if (createHWResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", createHWResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                // Xamarin.Forms.Application.Current.Properties.Clear();

                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                return false;
                            }
                        }

                        permit_Sql.server_id = permitserverid;
                        permit_Sql.hotworkGUID = permitguid;
                        //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                        //    var connection = hotworkDatabase.GetdbConnection();
                        _connection.UpdateWithChildren(permit_Sql);
                    }
                    else
                    {
                        _connection.UpdateWithChildren(permit_Sql);
                        await IsSyncDone();
                        permitguid = permit_Sql.hotworkGUID;
                    }

                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.PERMIT_CLOSE_SUCCESS, AppResource.OK_LABEL);


                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    return true;
                }
                catch (Exception e)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    Console.WriteLine("IOException source: {0}", e.Source);
                    return false;
                }
            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                return false;
            }
        }

        #region close Hotwork/impairment       

        /// <summary>
        /// called when popup opens
        /// </summary>
        public ICommand OnElementCloseCommand
        {
            get
            {
                return new Command(async(x) =>
                {
                    try
                    {
                        var element = (x as CustomControl.Validation.ListViewElement);
                        _element = element.ElementType;
                        SQLiteDatabase elementDatabase = new SQLiteDatabase();
                        _connection = elementDatabase.GetdbConnection();

                        if (element.ElementType == "HOTWORKS")
                        {
                            //SelectedDate = DateTime.Now;
                            //_workPermit_Sql = _connection.GetWithChildren<HotWorkPermit_sql>(element.ElementId, true);
                            //_workPermit_scope_Sql = _connection.GetWithChildren<Scope_sql>(_workPermit_Sql.scope_Sql.id, recursive: true);
                            //ElementCategory_sql = string.Join(", ", _workPermit_scope_Sql.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName));
                            //ElementDescription_Sql = _workPermit_scope_Sql.WorkLocation;
                            //ElementStartDate_Sql = _workPermit_scope_Sql.StartDateTime;
                            App.isDuplicate = true;
                            App.isImpairementEditorDuplicate = "Duplicate";
                            var hotwork_sql = EditPermitByID((x as ListViewElement).ElementId);
                            var navigationParams = new NavigationParameters();
                            navigationParams.Add("hotworkPermit_sql", hotwork_sql);
                            await _navigationService.NavigateAsync(new Uri("NavigationPage/NewHotWorksPage", UriKind.Relative) , navigationParams, null, false);


                        }
                        else
                        {
                            _impairment_Sql = _connection.GetWithChildren<Impairment_Sql>(element.ElementId, true);
                            var impairmentType = _connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == _impairment_Sql.impairmentTypeId).FirstOrDefault().TypeDescription;
                            var impairmentClass = _connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == _impairment_Sql.impairmentClassId).FirstOrDefault().ClassDescription;
                            var impairmentShutdownReason = _connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == _impairment_Sql.shutDownReasonId).FirstOrDefault().ClassDescription;
                            ElementCategory_sql = impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason;
                            ElementDescription_Sql = _impairment_Sql.impairmentDescription;
                            ElementStartDate_Sql = _impairment_Sql.startDateTime;
                        }
                        showClosePopup = true;
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
        }

        /// <summary>
        /// called when entry is closed and update
        /// </summary>
        public ICommand CloseHotworkCommand
        {
            get
            {
                return new Command(async (x) =>
                {
                    try
                    {
                        if (_element == "HOTWORKS")
                        {
                            _workPermit_Sql.isEdited = true;
                            _workPermit_Sql.hotWorkCloseDate = SelectedDate.Add(SelectedTime).ToString();
                            _workPermit_Sql.workFlowStatus = "CLOSED";
                            _connection.UpdateWithChildren(_workPermit_Sql);
                            await IsHWCloseEmailSent(_workPermit_Sql);
                        }
                        else
                        {
                            _impairment_Sql.isEdited = true;
                            _impairment_Sql.impairmentCloseDateTime = SelectedDate.Add(SelectedTime).ToString();
                            _impairment_Sql.workFlowStatus = "CLOSED";
                            _connection.UpdateWithChildren(_impairment_Sql);
                            //TODO: sent email for clousure

                            var isMailSuccess = await IsCloseEmailSent(_impairment_Sql);
                        }

                        showClosePopup = false;
                        SetDashboardData();
                    }
                    catch (Exception ex) {
                        showClosePopup = false;
                    }
                });
            }
        }

        public ICommand CancelHotworkCommand
        {
            get
            {
                return new Command((x) =>
                {
                    try
                    {
                        showClosePopup = false;
                    }
                    catch (Exception ex) { }
                });
            }
        }
        #endregion

        #region edit Hotwork/Impairment

        #region edit hotwork

        public ICommand OnElementSelectEdit
        {
            get
            {
                return new Command(async (x) =>
                {
                    App.isImpairementEditorDuplicate = "Edit";
                    var permit = CustomControl.Validation.EditPermitByID((x as ListViewElement).ElementId);
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("hotworkPermit_sql", permit);
                    await _navigationService.NavigateAsync(new Uri("NavigationPage/NewHotWorksPage", UriKind.Relative), navigationParams, null, false);
                });
            }
        }

        #endregion

        #region edit impairment

        public ICommand OnImpElementSelectEdit
        {
            get
            {
                return new Command(async (x) =>
                {
                    App.isImpairementEditorDuplicate = "Edit";
                    var impairment = EditImpairmentByID((x as ListViewElement).ElementId);
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("impairmentModel", impairment);
                    await _navigationService.NavigateAsync(new Uri("NavigationPage/NewImpairmentPage", UriKind.Relative), navigationParams, null, false);
                });
            }
        }

        #endregion

        #endregion

        #region set dashboard data

        private void DisplayActiveDataAsync(string page)
        {
            if (page == "HW")
            {
                ActivePermitsNImpairments = WorkPermits;
                ActiveHWColor = Color.FromHex("#00afad");
                ActiveImpColor = Color.Transparent;
            }
            else
            {
                ActivePermitsNImpairments = WorkImpairment;
                ActiveImpColor = Color.FromHex("#00afad");
                ActiveHWColor = Color.Transparent;
            }
        }

        private void ChangeTabColor(string x)
        {
            switch (x.ToLower())
            {
                case "hotwork":
                    HotworkTabColor = Color.FromHex("#00AFAD");
                    ImpairmentTabColor = Color.Transparent;
                    break;
                case "impairment":
                    HotworkTabColor = Color.Transparent;
                    ImpairmentTabColor = Color.FromHex("#00AFAD");
                    break;

            }
        }

        public ICommand OnTabChangedCommand
        {
            get
            {
                return new Command<string>((x) =>
                {
                    showNoIMPLabel = false;
                    showNoHWLabel = false;
                    ChangeTabColor(x);
                    if (x.ToLower() == "hotwork")
                    {
                        ListViewElements = ListViewElements_Temp;
                        showHotworkListView = true;
                        showImpListView = false;


                        if (ListViewElements.Count() == 0) {
                            showNoIMPLabel = false;
                            showNoHWLabel = true;
                        }

                        //var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals(x.ToLower()));
                        //ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                    }
                    else
                    {
                        ImpairmentListViewElements = ImpairmentListViewElements_Temp;
                        showHotworkListView = false;
                        showImpListView = true;

                        if (ImpairmentListViewElements.Count() == 0)
                        {
                            showNoIMPLabel = true;
                            showNoHWLabel = false;
                        }
                        //var result = ImpairmentListViewElements.Where(w => w.ElementId!=null);
                        //ImpairmentListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                    }
                });
            }
        }

        private void SetDashboardData()
        {
            try
            {
                if (Application.Current.Properties.ContainsKey("CompanyName"))
                {
                    CompanyNameLabel = Application.Current.Properties["CompanyName"] as string;
                }
                if (Application.Current.Properties.ContainsKey("CompanyAddress"))
                {
                    CompanyAddressLabel = Application.Current.Properties["CompanyAddress"] as string;
                }
                if (Application.Current.Properties.ContainsKey("siteId"))
                {
                    App.SiteId = Application.Current.Properties["siteId"] as string;
                }

                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();

                var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                var impairment_sql_List = connection.GetAllWithChildren<Impairment_Sql>().ToList();

                WorkPermits = new ObservableCollection<ModelWorkPermit>();
                WorkImpairment = new ObservableCollection<ModelWorkPermit>();


                ImpairmentListViewElements = new ObservableCollection<ListViewElement>();
                ListViewElements = new ObservableCollection<ListViewElement>();
                  ImpairmentListViewElements_Temp = new ObservableCollection<ListViewElement>();
                ListViewElements_Temp = new ObservableCollection<ListViewElement>();

                string currentDateTime = DateTime.Now.ToString("M/d/yyyy h:mm:ss tt");

                if (hotwork_sql_List.Count > 0)
                {
                    foreach (HotWorkPermit_sql hotWorkPermit_Sql in hotwork_sql_List)
                    {
                        if (!hotWorkPermit_Sql.isDeleted && hotWorkPermit_Sql.workFlowStatus.ToLower() == "active")
                        {
                            Scope_sql scope = new Scope_sql();
                            try
                            {
                                scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql.id, recursive: true);

                            }
                            catch (Exception ex)
                            {
                                scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql_id, recursive: true);
                            }



                            // * Active records only
                            // * Permit Status -> EXPIRED / UPCOMING
                            // * DateTime EX. 3/7/2021 9:35:00 PM
                            int permitUpComingDateTimeCompareResult = DateTime.Compare(DateTime.Parse(hotWorkPermit_Sql.scope_Sql.StartDateTime), DateTime.Parse(currentDateTime));
                            int permitDateTimeCompareResult = DateTime.Compare(DateTime.Parse(hotWorkPermit_Sql.scope_Sql.EndDateTime), DateTime.Parse(currentDateTime));


                            if (permitUpComingDateTimeCompareResult > 0)
                                hotWorkPermit_Sql.workFlowStatus = "UPCOMING";
                            else
                            {
                                if (permitDateTimeCompareResult < 0)
                                    hotWorkPermit_Sql.workFlowStatus = "EXPIRED";
                                else
                                    hotWorkPermit_Sql.workFlowStatus = "ACTIVE";
                            }
                            connection.UpdateWithChildren(hotWorkPermit_Sql);


                            // var scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql.id, recursive: true);
                            ListViewElements.Add(new ListViewElement
                            {
                                ElementId = hotWorkPermit_Sql.id,
                                ElementCategory = hotWorkPermit_Sql.server_id == 0 ? string.Join(", ", scope.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName)) : "#" + hotWorkPermit_Sql.server_id + "\n" + string.Join(", ", string.Join(", ", scope.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName))),
                                ElementDescription = hotWorkPermit_Sql.scope_Sql.WorkLocation,
                                ElementEndDate = hotWorkPermit_Sql.scope_Sql.EndDateTime,
                                ElementStartDate = hotWorkPermit_Sql.scope_Sql.StartDateTime,
                                ElementStatus = hotWorkPermit_Sql.workFlowStatus,
                                ElementEvent = AppResource.DUPLICATE_PERMIT,
                                ElementType = "HOTWORKS"
                            });
                            ListViewElements_Temp = ListViewElements;
                        }
                    }
                }

                if (impairment_sql_List.Count > 0)
                {
                    foreach (Impairment_Sql impairment_Sql in impairment_sql_List)
                    {
                        var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == impairment_Sql.impairmentTypeId).FirstOrDefault().TypeDescription;
                        var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == impairment_Sql.impairmentClassId).FirstOrDefault().ClassDescription;
                        var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == impairment_Sql.shutDownReasonId).FirstOrDefault().ClassDescription;


                        if (!impairment_Sql.isDeleted && impairment_Sql.workFlowStatus == "ACTIVE")
                        {
                            // * Active records only
                            // * Permit Status -> EXPIRED / UPCOMING
                            // * DateTime EX. 3/7/2021 9:35:00 PM
                            int impairmentUpComingDateTimeCompareResult = DateTime.Compare(DateTime.Parse(impairment_Sql.startDateTime), DateTime.Parse(currentDateTime));
                            int impairmentDateTimeCompareResult = DateTime.Compare(DateTime.Parse(impairment_Sql.endDateTime), DateTime.Parse(currentDateTime));

                            if (impairmentUpComingDateTimeCompareResult > 0)
                                impairment_Sql.workFlowStatus = "UPCOMING";
                            else
                            {
                                if (impairmentDateTimeCompareResult < 0)
                                    impairment_Sql.workFlowStatus = "EXPIRED";
                                else
                                    impairment_Sql.workFlowStatus = "ACTIVE";
                            }
                            connection.UpdateWithChildren(impairment_Sql);


                            //try
                            //{
                            //    var reporterDetail = connection.GetWithChildren<ReporterDetail_sql>(impairment_Sql.reporterDetail_sql_id, recursive: true);
                            //}
                            //catch (Exception ex) {
                            //    var reporterDetail = connection.GetWithChildren<ReporterDetail_sql>(impairment_Sql.reporterDetail_Sql.id, recursive: true);
                            //}


                            ImpairmentListViewElements.Add(new ListViewElement
                            {
                                ElementId = impairment_Sql.id,
                                ElementCategory = impairment_Sql.server_id == 0 ? impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason : "#" + impairment_Sql.server_id + "\n" + impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason,
                                ElementDescription = impairment_Sql.impairmentDescription,
                                ElementEndDate = impairment_Sql.endDateTime,
                                ElementStartDate = impairment_Sql.startDateTime,
                                ElementStatus = impairment_Sql.workFlowStatus,
                                ElementEvent = CustomControl.Validation.GetElementEventName(impairment_Sql.workFlowStatus),
                                ElementType = "IMPAIRMENTS"
                            });
                            ImpairmentListViewElements_Temp = ImpairmentListViewElements;
                        }
                    }
                }

                ActiveImpCount = string.Format(AppResource.DASH_HW_IMP_INFO_LABEL, ListViewElements.Count(), ImpairmentListViewElements.Count());

                //ActiveImpCount = "You have " + ListViewElements.Count() + " Hot Works planned for today, and " +
                //    ImpairmentListViewElements.Count() + " raised Impairment.";

                if (ListViewElements.Count() == 0) {
                    showNoIMPLabel = false;
                    showNoHWLabel = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
        }

        #endregion
                
        private async Task SyncData()
        {
            PopupMsg = AppResource.DASHBOARD_SYNC_MSG;
            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });

              
                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();
                var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                //todo: store scanned permit in temp list
                App.scannedPermitModels = new List<ScannedPermitModel>();
                foreach (var hw in hotwork_sql_List) {

                    if (hw.ScannedPaperPermit != null && hw.ScannedPaperPermitGUID != null) {
                        hw.IsScannedPermits = true;
                        connection.UpdateWithChildren(hw);
                        App.scannedPermitModels.Add(new ScannedPermitModel
                        {
                            ScannedPaperPermit = hw.ScannedPaperPermit,
                            ScannedPaperPermitGUID = hw.ScannedPaperPermitGUID
                        });
                    }
                }

                ShowActivityLoader = true;
               bool isSyncDone = await CustomControl.Validation.IsSyncDone();

                if (isSyncDone) {
                    SetUnSyncAlarm();
                    //TODO: sync paper permit
                    //PopupMsg = "Submitting scanned paper permit. Please wait..";
                    
                    //var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                    ScanPermitRootObject syncPermitResponse = new ScanPermitRootObject();

                    if (App.scannedPermitModels.Count() > 0) {
                         syncPermitResponse = await RestApiHelper<ScanPermitRootObject>.
                            SyncMultipartData(App.scannedPermitModels);
                    }

                    if (syncPermitResponse != null) {

                        if (string.IsNullOrEmpty(syncPermitResponse.Data))
                        {

                        }
                        else {

                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync(AppResource.SCAN_COPY_LABEL, AppResource.SCANNED_PAPER_SUBMIT_SUCCESS, AppResource.OK_LABEL);

                        }

                    }
                }


                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = false;
                    SetDashboardData();
                    ChangeTabColor("hotwork");
                });

            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
            }
        }

        private async void NavigateAsync(string page)
        {
            var navigationParams = new NavigationParameters();

            //if (page.Equals("HotWorksPage")) {
            //    navigationParams.Add("navigationParameter", "HotWorksPage");
            //}
            //else if(page.Equals("Impairments"))
            //{
            //    navigationParams.Add("navigationParameter", "Impairments");
            //}
            //else if (page.Equals("Impairments"))
            //{
            //    navigationParams.Add("navigationParameter", "Impairments");
            //}





            if (page.Equals("logout"))
            {

                await Task.Yield();
                var answer = await _pageDialog.DisplayAlertAsync(AppResource.ALERT_MSG, AppResource.DASHBOARD_LOGOUT_MSG, AppResource.OK_LABEL, AppResource.CANCEL_LABEL);
                if (answer)
                {
                    // drop all data 

                    string siteID = string.Empty;
                    string accountID = string.Empty;

                    if (Application.Current.Properties.ContainsKey("siteId") && Application.Current.Properties.ContainsKey("accountId"))
                    {
                        siteID = Application.Current.Properties["siteId"] as string;
                        accountID = Application.Current.Properties["accountId"] as string;
                        // do something with id
                    }

                    showLogoutLoader = true;
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "HideHambergerMenu");
                    var userLogout = await RestApiHelper<UserLogout>.LogoutUser(siteID, accountID);

                    if (userLogout != null) {
                        if (userLogout.Status.StatusCode == 0)
                        {
                            _scopeService.RemoveAllData();
                            _firewatchPersonsService.RemoveAllData();
                            _firePreventionMeasuresService.RemoveAllData();
                            _authorizationService.RemoveAllData();
                            _hotworkPermitService.RemoveAllData();
                            // drop all data 
                            _impairmentService.RemoveAllData();

                            var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                            // Xamarin.Forms.Application.Current.Properties.Clear();

                            foreach (string key in keyList.ToList())
                            {
                                if (!key.Equals("WelcomeShow"))
                                {
                                    Application.Current.Properties.Remove(key);
                                }
                            }

                            await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                        }
                        else
                        {
                            await _pageDialog.DisplayAlertAsync(AppResource.ALERT_MSG, userLogout.Data, AppResource.OK_LABEL, AppResource.CANCEL_LABEL);

                        }


                    }
                    showLogoutLoader = false;
                }
            }
            else
            {
                await _navigationService.NavigateAsync(new Uri(page, UriKind.Relative), null, null, false);
            }



        }

        

        public void OnNavigatedFrom(NavigationParameters parameters) { }

        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            App.FromHwToNewHw = false;
            App.FromHwToArchive = false;
            App.FromImpToArchive = false;
            App.FromImpToNewImp = false;
            if (parameters.Count > 0)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (parameters.ContainsKey("syncEnable"))
                    {
                        var isSync = (bool)parameters["syncEnable"];
                        if (isSync)
                        {
                            await SyncData();
                        }
                    }
                }
                else
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                }
            }
            SetDashboardData();
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}


public class ViewInfo
{
    public bool FromArchieve { get; set; }
    public int Id { get; set; }
}

