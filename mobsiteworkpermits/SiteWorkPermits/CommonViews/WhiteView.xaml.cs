﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using SiteWorkPermits.Model;
using Prism.Navigation;
using Prism.Common;
using SiteWorkPermits.ViewPage;

namespace SiteWorkPermits
{
    public partial class WhiteView : ContentView
    {
        public EventHandler<HotWorksArgs> Checked_Changed;
        INavigationService navigationService;
        public WhiteView(bool draft)
        {
            InitializeComponent();
            BindingContext = this;
            // Gdduplicate.IsVisible = !draft;
            //  Gddelete.IsVisible = true;
            Btnduplicate.IsVisible = !draft;
        }

        private void HotWorksDuplicate_Tapped(object sender, System.EventArgs e)
        {
            App.isImpairementEditorDuplicate = "Duplicate";
            if (InfoType == "Impairment")
            {                
                if (HWStatus == HWStatus.CLOSED || HWStatus == HWStatus.DRAFT)
                {                  
                    App.ImpairementEditableId = PermitId;
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId;
                    if (FromArchieve)
                    {
                        viewInfo.FromArchieve = true;
                    }
                    else
                    {
                        viewInfo.FromArchieve = false;
                    }
                    App.isDuplicate = true;
                    MessagingCenter.Send(Application.Current, "EditImparement",viewInfo);
                }
                else
                {
                    App.ImpairementEditableId = PermitId;
                    var Curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);

                    if (Curpage is ImpairmentsPage)
                    {
                        MessagingCenter.Send(Xamarin.Forms.Application.Current, "RestorePopup");
                    }
                    else
                    {
                        MessagingCenter.Send(Xamarin.Forms.Application.Current, "DashRestorePopup");
                    }
                }
            }
            else
            {              
                if (HWStatus == HWStatus.EXPIRED)
                {
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId; 
                    App.PermitEditableId = PermitId;
                    if (FromArchieve)
                    {
                        viewInfo.FromArchieve = true;                       
                    }
                    else
                    {
                        viewInfo.FromArchieve = false;
                    }
                    App.isDuplicate = true;
                    MessagingCenter.Send(Application.Current, "EditPermit", viewInfo);                                     
                }
                else
                {
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId;
                    viewInfo.FromArchieve = false;
                    App.PermitEditableId = PermitId;
                    App.isDuplicate = true;
                    MessagingCenter.Send(Application.Current, "EditPermit",viewInfo);
                }
            }
        }

        void EditPermit_Tapped(object sender, System.EventArgs e)
        {
            if (InfoType == "Impairment")
            {
                App.ImpairementEditableId = PermitId;
                if (HWStatus == HWStatus.CLOSED)
                {
                    App.isImpairementEditorDuplicate = "Archieve";   
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId;                   
                    if (FromArchieve)
                    {
                        viewInfo.FromArchieve = true;                                          
                    }
                    else
                    {
                        viewInfo.FromArchieve = false;                       
                    }
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "EditImparement", viewInfo);
                }
                else
                {
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId;
                    viewInfo.FromArchieve = false;
                    App.isImpairementEditorDuplicate = "Edit";                   
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "EditImparement",viewInfo);
                }
            }
            else
            {
                App.PermitEditableId = PermitId;
                if (HWStatus == HWStatus.EXPIRED)
                {
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId;  
                    App.isImpairementEditorDuplicate = "Archieve";  
                    if (FromArchieve)
                    {
                        viewInfo.FromArchieve = true;                       
                    }
                    else
                    {
                        viewInfo.FromArchieve = false; 
                    }
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "EditPermit",viewInfo);                                      
                }
                else
                {
                    ViewInfo viewInfo = new ViewInfo();
                    viewInfo.Id = PermitId;
                    viewInfo.FromArchieve = false;
                    App.isImpairementEditorDuplicate = "Edit";   
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "EditPermit",viewInfo);                                                            
                }
            }
        }


        void DeletePermit_Tapped(object sender, System.EventArgs e)
        {
            App.PermitEditableId = PermitId;
            MessagingCenter.Send(Application.Current, "DeletePermit");
            //var hotWorksView =(HotWorksView)this.ParentView Parent;
            //var hotWorkslistView = (HotWorksListview)hotWorksView.Parent;
            //var HomeViewModel = (HomePageViewModel)hotWorkslistView.Parent.BindingContext;
            //HomeViewModel.OnPermitTapComand.Execute(PermitId+"");
        }

        public static BindableProperty InfoTypeProperty = BindableProperty.Create<WhiteView, string>(o => o.InfoType, string.Empty, BindingMode.TwoWay, null, null);

        public string InfoType
        {
            get { return (string)GetValue(InfoTypeProperty); }
            set { SetValue(InfoTypeProperty, value); }
        }

        public static BindableProperty CategoryNameProperty = BindableProperty.Create<WhiteView, string>(o => o.CategoryName, string.Empty, BindingMode.TwoWay, null, null);

        public string CategoryName
        {
            get { return (string)GetValue(CategoryNameProperty); }
            set { SetValue(CategoryNameProperty, value); }
        }


        public static BindableProperty ActionTitleProperty = BindableProperty.Create<WhiteView, string>(o => o.ActionTitle, string.Empty, BindingMode.TwoWay, null, null);

        public string ActionTitle
        {
            get { return (string)GetValue(ActionTitleProperty); }
            set { SetValue(ActionTitleProperty, value); }
        }

        public static BindableProperty PermitIdProperty = BindableProperty.Create<WhiteView, int>(o => o.PermitId, 0, BindingMode.TwoWay, null, null);

        public int PermitId
        {
            get { return (int)GetValue(PermitIdProperty); }
            set { SetValue(PermitIdProperty, value); }
        }


        public static BindableProperty FromArchieveProperty = BindableProperty.Create<WhiteView, bool>(o => o.FromArchieve, false, BindingMode.TwoWay, null, null);

        public bool FromArchieve
        {
            get { return (bool)GetValue(FromArchieveProperty); }
            set { SetValue(FromArchieveProperty, value); }
        }

        public static BindableProperty HWStatusProperty = BindableProperty.Create<WhiteView, HWStatus>(o => o.HWStatus, 0, BindingMode.TwoWay, null, null);

        public HWStatus HWStatus
        {
            get { return (HWStatus)GetValue(HWStatusProperty); }
            set { SetValue(HWStatusProperty, value); }
        }

        public static BindableProperty DescriptionProperty = BindableProperty.Create<WhiteView, string>(o => o.Description, string.Empty, BindingMode.TwoWay, null, null);

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static BindableProperty ExpiryDateProperty = BindableProperty.Create<WhiteView, string>(o => o.ExpiryDate, string.Empty, BindingMode.TwoWay, null, null);

        public string ExpiryDate
        {
            get { return (string)GetValue(ExpiryDateProperty); }
            set { SetValue(ExpiryDateProperty, value); }
        }
    }


    public class HotWorksArgs
    {
        public ModelWorkPermit HotWorkDetail { get; set; }
    }
}