﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CustomControl;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using System.Runtime.CompilerServices;
using SiteWorkPermits.DAL.ImpairmentDAL;
using System.Diagnostics;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLite;
using SiteWorkPermits.DAL.sqlIntegration;
using System.Threading.Tasks;
using Plugin.Connectivity;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public partial class ArchivePage : ContentPage
    {
        ObservableCollection<TabInfo> TabCollection = new ObservableCollection<TabInfo>();
        ObservableCollection<ModelPermits> PermitCollection = new ObservableCollection<ModelPermits>();
        ArchieveViewModel Model;
        SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
        SQLiteConnection connection;
        private string _selectedPermitDuration = string.Empty;
        public string offimg = "radio_off.png";
        public string onimg = "radio_on.png";
        public ArchivePage()
        {
            InitializeComponent();
            pageScroll.Scrolled += OnScrolled;
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            TabCollection = new ObservableCollection<TabInfo>();

            //HotWorkFilterInitial();

            #region HotWorkFilter Filter
            ChkThrowing.Checked_Changed += HotWorksChecked_Changed;
            ChkBrazing.Checked_Changed += HotWorksChecked_Changed;
            ChkCutting.Checked_Changed += HotWorksChecked_Changed;
            ChkWelding.Checked_Changed += HotWorksChecked_Changed;
            ChkGrinding.Checked_Changed += HotWorksChecked_Changed;
            ChkOther.Checked_Changed += HotWorksChecked_Changed;
            #endregion


            #region Impairment Filter

            //Impairment Type
            ChkPlanned.Checked_Changed += ImpairmentType_Cheked;
            ChkEmergency.Checked_Changed += ImpairmentType_Cheked;
            ChkHidden.Checked_Changed += ImpairmentType_Cheked;
            ChkLongterm.Checked_Changed += ImpairmentType_Cheked;
            Chkongoing.Checked_Changed += ImpairmentType_Cheked;

            //ImpairmentClass Type
            ChkAutoSprinkle.Checked_Changed += ImpairmentClass_Cheked;
            ChkSpecialExtinguish.Checked_Changed += ImpairmentClass_Cheked;
            ChkFirePump.Checked_Changed += ImpairmentClass_Cheked;
            ChkFireTank.Checked_Changed += ImpairmentClass_Cheked;
            ChkpublicWater.Checked_Changed += ImpairmentClass_Cheked;
            ChkFireHydrant.Checked_Changed += ImpairmentClass_Cheked;
            ChkFireAlarm.Checked_Changed += ImpairmentClass_Cheked;
            ChkClassOthers.Checked_Changed += ImpairmentClass_Cheked;


            //ReasonShutDown
            ChkSystemModify.Checked_Changed += ReasonShutDown_Cheked;
            ChkSystemRepairResultLoss.Checked_Changed += ReasonShutDown_Cheked;
            ChkSystemRepairNOResultLoss.Checked_Changed += ReasonShutDown_Cheked;
            ChkRoutinemaint.Checked_Changed += ReasonShutDown_Cheked;
            ChkBuildingrenovate.Checked_Changed += ReasonShutDown_Cheked;
            ChkReasonOther.Checked_Changed += ReasonShutDown_Cheked;
            GdFilter.IsVisible = true;
            #endregion

            Device.BeginInvokeOnMainThread(() =>
            {
                GdFilter.IsVisible = false;
            });
        }

        private void ClearimpairmentFilter()
        {
            //Impairment Type
            ChkPlanned.Checked = false;
            ChkEmergency.Checked = false;
            ChkHidden.Checked = false;
            ChkLongterm.Checked = false;
            Chkongoing.Checked = false;

            //ImpairmentClass Type
            ChkAutoSprinkle.Checked = false;
            ChkSpecialExtinguish.Checked = false;
            ChkFirePump.Checked = false;
            ChkFireTank.Checked = false;
            ChkpublicWater.Checked = false;
            ChkFireHydrant.Checked = false;
            ChkFireAlarm.Checked = false;
            ChkClassOthers.Checked = false;

            //ReasonShutDown
            ChkSystemModify.Checked = false;
            ChkSystemRepairResultLoss.Checked = false;
            ChkSystemRepairNOResultLoss.Checked = false;
            ChkRoutinemaint.Checked = false;
            ChkBuildingrenovate.Checked = false;
            ChkReasonOther.Checked = false;


            ImpairLast3month.Source = offimg;
            ImpairLast6month.Source = offimg;
            ImpairThisYear.Source = offimg;
            ImpairLastyear.Source = offimg;
            ImpairOlder.Source = offimg;
        }

        private void ClearHotWorkFilter()
        {
            ChkThrowing.Checked = false;
            ChkBrazing.Checked = false;
            ChkCutting.Checked = false;
            ChkWelding.Checked = false;
            ChkGrinding.Checked = false;
            ChkOther.Checked = false;
            last3MonthsIcon.Source = offimg;
            last6MonthsIcon.Source = offimg;
            thisYearIcon.Source = offimg;
            lastYearIcon.Source = offimg;
            olderIcon.Source = offimg;
        }

        private void ImapirDateSelection(object sender, System.EventArgs e)
        {
            Image Selimg = (Image)sender;
            CusGrid selgd = (CusGrid)Selimg.Parent;
            var val = ((Label)selgd.Children[0]).Text;
            var HotWorkSel = Model.showHotworkListView;
            switch (val)
            {
                case "Last 3 months":
                    if (HotWorkSel)
                    {
                        Model.selectedPermitDuration = "LastThreeMonths";
                    }
                    else
                    {
                        Model.selectedImpairmentDuration = 0;
                    }
                    break;
                case "Last 6 months":
                    if (HotWorkSel)
                    {
                        Model.selectedPermitDuration = "LastSixMonths";
                    }
                    else
                    {
                        Model.selectedImpairmentDuration = 1;
                    }

                    break;
                case "This Year":

                    if (HotWorkSel)
                    {
                        Model.selectedPermitDuration = "ThisYear";
                    }
                    else
                    {
                        Model.selectedImpairmentDuration = 2;
                    }
                    break;
                case "Last Year":
                    if (HotWorkSel)
                    {
                        Model.selectedPermitDuration = "LastYear";
                    }
                    else
                    {
                        Model.selectedImpairmentDuration = 3;
                    }
                    break;
                case "Older":
                    if (HotWorkSel)
                    {
                        Model.selectedPermitDuration = "Older";
                    }
                    else
                    {
                        Model.selectedImpairmentDuration = 4;
                    }
                    break;
            }
            if (HotWorkSel)
            {
                SelHotworksDate(Selimg);
            }
            else
            {
                SelImpairDate(Selimg);
            }
        }

        private void SelHotworksDate(Image selimg)
        {
            last3MonthsIcon.Source = offimg;
            last6MonthsIcon.Source = offimg;
            thisYearIcon.Source = offimg;
            lastYearIcon.Source = offimg;
            olderIcon.Source = offimg;
            selimg.Source = onimg;
        }

        private void SelImpairDate(Image selimg)
        {
            ImpairLast3month.Source = offimg;
            ImpairLast6month.Source = offimg;
            ImpairThisYear.Source = offimg;
            ImpairLastyear.Source = offimg;
            ImpairOlder.Source = offimg;
            selimg.Source = onimg;
        }

        private async void OnScrolled(object sender, ScrolledEventArgs e)
        {
            pageScroll = sender as Xamarin.Forms.ScrollView;
            double scrollingSpace = pageScroll.ContentSize.Height - pageScroll.Height;

            //if (scrollingSpace <= e.ScrollY)
            //{
            //    if (Model._isScrollingEnable)
            //    {
            //        if (TabCollection.Where(s => s.IsSelected).First().Title == "HOT WORK PERMITS")
            //        {
            //            Model.HotWorksPageNumber += 1;
            //            await Model.GetArchivePermits(Model.HotWorksPageNumber);
            //            HotWorkSDataDisplay();
            //        }
            //        else
            //        {
            //            Model.ImpairmentPageNumber += 1;
            //            await Model.GetArchieveImpairments(Model.ImpairmentPageNumber);
            //            ImpairmentDataDisplay();
            //        }
            //    }
            //}
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GdFilter.IsVisible = false;
            App.hwfilter = SpHotworkFilterPanel;
            App.impfilter = SpImpairmentFilterPanel;

            connection = hotworkDatabase.GetdbConnection();
            //try
            //{
            //    Device.BeginInvokeOnMainThread(async () =>
            //    {

            Model = (ArchieveViewModel)this.BindingContext;
            Model.ChangeTabEvent += Model_ChangeTabEvent;
            //    //    await Model.GetArchivePermits(Model.HotWorksPageNumber);
            //        if(Model._isLoggedout){
            //            return;
            //        }
            //        await Model.GetArchieveImpairments(Model.ImpairmentPageNumber);
            //    //    Model.HotWorksPermitCollection.ToList().ForEach(s => s.WorkCollection.ToList().ForEach(p => p.FromArchieve = true));
            //     //   Model.ImpairmentCollection.ToList().ForEach(s => s.WorkCollection.ToList().ForEach(p => p.FromArchieve = true));

            //        //Model.RefreshArchiveListEvent += ViewModel_RefreshListEvent;
            //        TabCollection.Add(new TabInfo
            //        {
            //            Title = "HOT WORK PERMITS",
            //            IsSelected = true,
            //            view = new HotWorksListview(Model.HotWorksPermitCollection,
            //                                        false)
            //        });
            //        TabCollection.Add(new TabInfo
            //        {
            //            Title = "RAISED IMPAIRMENTS",
            //            IsSelected = false,
            //            view = new ImpairmentsListView(Model.ImpairmentCollection, false)
            //        });
            //        foreach (var d in TabCollection)
            //        {
            //            TabView tab = new TabView();
            //            tab.Title = d.Title;
            //            tab.TabColor = d.IsSelected ? Color.FromHex("#00AFAD") : Color.Transparent;
            //            tab.GestureRecognizers.Add(new TapGestureRecognizer(HandleAction));
            //            tab.BindingContext = d.Collection;
            //            Spworktabs.Children.Add(tab);
            //        }
            //       //// Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
            //       // Pageview.BindingContext = TabCollection.Where(s => s.IsSelected == true).First().Collection;
            //    });
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("IOException source: {0}", ex.Source);
            //}
        }

        private void Model_ChangeTabEvent()
        {
            ClearimpairmentFilter();
            ClearHotWorkFilter();
        }

        #region Filter Section

        private async void Tagview_DeleteEvent(object sender, TagArgs e)
        {
            try
            {
                Tagview gd = (Tagview)sender;
                Label lbltag = (Label)((CusGrid)gd.Content).Children[0];
                SpHotworkFilterPanel.Children.Remove(gd);
                Model.HotworksFilterColletion.Remove(lbltag.Text);

                if (Model.HotworksFilterColletion.Count() == 0) {
                    ClearHotWorkFilter();
                }

                Model.HotWorksPageNumber = 1;
                await Model.GetArchivePermits(Model.HotWorksPageNumber);
                HotWorkSDataDisplay();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void TagImpairmentview_DeleteEvent(object sender, TagArgs e)
        {
            try
            {
                Tagview gd = (Tagview)sender;
                Label lbltag = (Label)((CusGrid)gd.Content).Children[0];
                SpImpairmentFilterPanel.Children.Remove(gd);
                var val = lbltag.Text;





                //var impairmentType = connection.Table<ImpairmentType_Sql>().ToList();
                //var impairmentClass = connection.Table<ImpairmentClassType_Sql>();
                //var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList();

                string[] impairmentType =new string[] { AppResource.IMP_TYPE_1_PLACEHOLDER,
                    AppResource.IMP_TYPE_2_PLACEHOLDER,
                AppResource.IMP_TYPE_3_PLACEHOLDER,
                AppResource.IMP_TYPE_4_PLACEHOLDER,
                AppResource.IMP_TYPE_5_PLACEHOLDER};
                string[] impairmentClass = new string[] { AppResource.IMP_CLASS_1_PLACEHOLDER,
        AppResource.IMP_CLASS_2_PLACEHOLDER,
        AppResource.IMP_CLASS_3_PLACEHOLDER,
        AppResource.IMP_CLASS_4_PLACEHOLDER,
        AppResource.IMP_CLASS_5_PLACEHOLDER,
        AppResource.IMP_CLASS_6_PLACEHOLDER,
        AppResource.IMP_CLASS_7_PLACEHOLDER,
        AppResource.IMP_CLASS_8_PLACEHOLDER};
                string[] impairmentShutdownReason = new string[] { AppResource.IMP_SHUTDOWN_REASON_1_PLACEHOLDER ,
        AppResource.IMP_SHUTDOWN_REASON_2_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_3_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_4_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_5_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_6_PLACEHOLDER};

                string ImpTyperes = string.Empty;
                string ImpClassres = string.Empty;
                string ImpReasonres = string.Empty;
                foreach (var d in Model.ImpairmentTypeFilterColletion)
                {
                     ImpTyperes = impairmentType.Where(s => s.ToLower() == val.ToLower()).FirstOrDefault();

                   

                    // Tagview tagview = new Tagview() { Tagname = ImpTyperes.TypeDescription };
                    // tagview.DeleteEvent += TagImpairmentview_DeleteEvent;
                    // SpImpairmentFilterPanel.Children.Add(tagview);
                }
                Model.ImpairmentTypeFilterColletion.Remove(Array.IndexOf(impairmentType, ImpTyperes)+1);
                foreach (var d in Model.ImpairmentClassFilterColletion)
                {
                     ImpClassres = impairmentClass.Where(s => s.ToLower() == val.ToLower()).FirstOrDefault();
                    //Tagview tagview = new Tagview() { Tagname = ImpClassres.ClassDescription };
                    //tagview.DeleteEvent += TagImpairmentview_DeleteEvent;
                    //SpImpairmentFilterPanel.Children.Add(tagview);
                }
                Model.ImpairmentClassFilterColletion.Remove(Array.IndexOf(impairmentClass, ImpClassres) + 1);

                foreach (var d in Model.ImpairmentReasonFilterColletion)
                {
                     ImpReasonres = impairmentShutdownReason.Where(s => s.ToLower() == val.ToLower()).FirstOrDefault();
                    //Tagview tagview = new Tagview() { Tagname = ImpReasonres.ClassDescription };
                    //tagview.DeleteEvent += TagImpairmentview_DeleteEvent;
                    //SpImpairmentFilterPanel.Children.Add(tagview);
                }

                Model.ImpairmentReasonFilterColletion.Remove(Array.IndexOf(impairmentShutdownReason, ImpReasonres) + 1);


                Model.ImpairmentPageNumber = 1;
                
                await Model.GetArchieveImpairments(Model.ImpairmentPageNumber);
                ImpairmentDataDisplay();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void ApplyFilter_Tapped(object sender, System.EventArgs e)
        {

            if (CrossConnectivity.Current.IsConnected)
            {

                //   var selitem = TabCollection.Where(s => s.IsSelected == true).FirstOrDefault();
                if (Model.showHotworkListView)
                {
                    SpHotworkFilterPanel.Children.Clear();
                    SpImpairmentFilterPanel.Children.Clear();
                    foreach (var d in Model.HotworksFilterColletion)
                    {
                        Tagview tagview = new Tagview() { Tagname = d };
                        tagview.DeleteEvent += Tagview_DeleteEvent;
                        SpHotworkFilterPanel.Children.Add(tagview);
                    }
                    //ClearHotWorkFilter();
                    GdFilter.IsVisible = false;
                    Model.HotWorksPageNumber = 1;
                    App.WorkPermits = new List<ModelWorkPermit>();
                    App.HotworksArchieves = new List<Model.SyncHotWorks.HotWork>();
                    Model.ListViewElements.Clear();
                    await Model.GetArchivePermits(Model.HotWorksPageNumber);
                    //  HotWorkSDataDisplay();
                }
                else
                {
                    SpImpairmentFilterPanel.Children.Clear();
                    SpHotworkFilterPanel.Children.Clear();
                    //var impairmentType = connection.Table<ImpairmentType_Sql>().ToList();
                    //var impairmentClass = connection.Table<ImpairmentClassType_Sql>();
                    //var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList();


                    string[] impairmentType = new string[] { AppResource.IMP_TYPE_1_PLACEHOLDER,
                    AppResource.IMP_TYPE_2_PLACEHOLDER,
                AppResource.IMP_TYPE_3_PLACEHOLDER,
                AppResource.IMP_TYPE_4_PLACEHOLDER,
                AppResource.IMP_TYPE_5_PLACEHOLDER};
                    string[] impairmentClass = new string[] { AppResource.IMP_CLASS_1_PLACEHOLDER,
        AppResource.IMP_CLASS_2_PLACEHOLDER,
        AppResource.IMP_CLASS_3_PLACEHOLDER,
        AppResource.IMP_CLASS_4_PLACEHOLDER,
        AppResource.IMP_CLASS_5_PLACEHOLDER,
        AppResource.IMP_CLASS_6_PLACEHOLDER,
        AppResource.IMP_CLASS_7_PLACEHOLDER,
        AppResource.IMP_CLASS_8_PLACEHOLDER};
                    string[] impairmentShutdownReason = new string[] { AppResource.IMP_SHUTDOWN_REASON_1_PLACEHOLDER ,
        AppResource.IMP_SHUTDOWN_REASON_2_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_3_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_4_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_5_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_6_PLACEHOLDER};

                    //string ImpTyperes = string.Empty;
                    //string ImpClassres = string.Empty;
                    //string ImpReasonres = string.Empty;


                    await Task.Yield();
                    foreach (var d in Model.ImpairmentTypeFilterColletion)
                    {
                        var ImpTyperes = impairmentType.Where(s => (Array.IndexOf(impairmentType, s) +1) == d).FirstOrDefault();
                        Tagview tagview = new Tagview() { Tagname = ImpTyperes };
                        tagview.DeleteEvent += TagImpairmentview_DeleteEvent;
                        SpImpairmentFilterPanel.Children.Add(tagview);
                    }
                    foreach (var d in Model.ImpairmentClassFilterColletion)
                    {
                        var ImpClassres = impairmentClass.Where(s => (Array.IndexOf(impairmentClass, s) + 1) == d).FirstOrDefault();
                        Tagview tagview = new Tagview() { Tagname = ImpClassres };
                        tagview.DeleteEvent += TagImpairmentview_DeleteEvent;
                        SpImpairmentFilterPanel.Children.Add(tagview);
                    }
                    foreach (var d in Model.ImpairmentReasonFilterColletion)
                    {
                        var ImpReasonres = impairmentShutdownReason.Where(s => (Array.IndexOf(impairmentShutdownReason, s) + 1) == d).FirstOrDefault();
                        Tagview tagview = new Tagview() { Tagname = ImpReasonres };
                        tagview.DeleteEvent += TagImpairmentview_DeleteEvent;
                        SpImpairmentFilterPanel.Children.Add(tagview);
                    }

                    GdFilter.IsVisible = false;
                    Model.ImpairmentPageNumber = 1;
                    //    App.Impairments = new List<ModelWorkPermit>();
                    App.ImpairMentArchieves = new List<Model.SyncImpairements.SyncImpairements>();
                    Model.ImpairmentListViewElements.Clear();
                    await Model.GetArchieveImpairments(Model.HotWorksPageNumber);
                    //  ImpairmentDataDisplay();
                }

            }
            else {
                await Task.Yield();
                await Model._pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
            }
        }

        private async void ClearFilter_Tapped(object sender, System.EventArgs e)
        {
            SpImpairmentFilterPanel.Children.Clear();
            SpHotworkFilterPanel.Children.Clear();
            //var HotworkSel = TabCollection.Where(s => s.Title == "HOT WORK PERMITS").First().IsSelected;
            if (Model.showHotworkListView)
            {
                Model.HotworksFilterColletion = new ObservableCollection<string>();
                Model.selectedPermitDuration = string.Empty;
                ClearHotWorkFilter();
                Model.HotWorksPageNumber = 1;
                await Model.GetArchivePermits(Model.HotWorksPageNumber);
                HotWorkSDataDisplay();
            }
            else
            {
                Model.ImpairmentTypeFilterColletion = new ObservableCollection<int>();
                Model.ImpairmentClassFilterColletion = new ObservableCollection<int>();
                Model.ImpairmentReasonFilterColletion = new ObservableCollection<int>();
                Model.selectedImpairmentDuration = 0;
                ClearimpairmentFilter();
                Model.ImpairmentPageNumber = 1;
                await Model.GetArchieveImpairments(Model.ImpairmentPageNumber);
                ImpairmentDataDisplay();
            }
            GdFilter.IsVisible = false;
        }

        private void HotWorkSDataDisplay()
        {
            //  Model.HotWorksPermitCollection.ToList().ForEach(s => s.WorkCollection.ToList().ForEach(p => p.FromArchieve = true));
            //TabCollection.Where(s => s.Title == "HOT WORK PERMITS").First().view = new HotWorksListview(Model.HotWorksPermitCollection, false);
            // TabCollection.Where(s => s.Title == "HOT WORK PERMITS").First().Collection = Model.HotWorksPermitCollection;
            ///   Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
            //   Pageview.BindingContext = TabCollection.Where(s => s.IsSelected == true).First().Collection;
        }

        private void ImpairmentDataDisplay()
        {
            //Model.ImpairmentCollection.ToList().ForEach(s => s.WorkCollection.ToList().ForEach(p => p.FromArchieve = true));
            // TabCollection.Where(s => s.Title == "RAISED IMPAIRMENTS").First().view = new ImpairmentsListView(Model.ImpairmentCollection, false);
            //TabCollection.Where(s => s.Title == "RAISED IMPAIRMENTS").First().Collection = Model.ImpairmentCollection;
            //    Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
            //  Pageview.BindingContext = TabCollection.Where(s => s.IsSelected == true).First().Collection;
        }

        private void Filter_Tapped(object sender, System.EventArgs e)
        {



            if (Model.showHotworkListView)
            {
                HotWorkpanel.IsVisible = true;
                ImpairmaentPanel.IsVisible = false;
            }
            else
            {
                HotWorkpanel.IsVisible = false;
                ImpairmaentPanel.IsVisible = true;
            }
            GdFilter.IsVisible = true;

        }

        private void FilterClose_Tapped(object sender, System.EventArgs e)
        {
            GdFilter.IsVisible = false;
        }

        private void ClearAll_Tapped(object sender, System.EventArgs e)
        {
            //var selitem = TabCollection.Where(s => s.IsSelected == true).FirstOrDefault();
            if (Model.showHotworkListView)
            {
                SpHotworkFilterPanel.Children.Clear();
            }
            else
            {
                SpImpairmentFilterPanel.Children.Clear();
            }
        }

        #region Impaitment Filter
        private void ImpairmentClass_Cheked(object sender, CheckedArgs e)
        {


            //  var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.TypeDescription.ToLower() == syncImpairements.ImpairmentTypeId).FirstOrDefault().TypeDescription;
            //   var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == syncImpairements.ShutDownReasonId).FirstOrDefault().ClassDescription;



            var selcontrol = (CustomCheckbox)sender;
            selcontrol.Checked = e.Checked;
            CusGrid grid = (CusGrid)selcontrol.Parent;
            Label labeltitle = (Label)grid.Children[0];
            var val = labeltitle.Text;

            //var shutlst = Model._impairmentClassService.GetImpairmentClassType();
            //var res = shutlst.Where(s => s.ClassDescription == val).FirstOrDefault();

            string[] impairmentClass1 = new string[] { AppResource.IMP_CLASS_1_PLACEHOLDER,
        AppResource.IMP_CLASS_2_PLACEHOLDER,
        AppResource.IMP_CLASS_3_PLACEHOLDER,
        AppResource.IMP_CLASS_4_PLACEHOLDER,
        AppResource.IMP_CLASS_5_PLACEHOLDER,
        AppResource.IMP_CLASS_6_PLACEHOLDER,
        AppResource.IMP_CLASS_7_PLACEHOLDER,
        AppResource.IMP_CLASS_8_PLACEHOLDER};

            int impairmentClass = Array.IndexOf(impairmentClass1, val) + 1;
            // var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.ClassDescription.ToLower() == val.ToLower()).FirstOrDefault();


            if (impairmentClass >0 )
            {

                if (e.Checked)
                {
                    Model.ImpairmentClassFilterColletion.Add(impairmentClass);
                }
                else
                {
                    Model.ImpairmentClassFilterColletion.Remove(impairmentClass);
                }
            }
        }

        private void ImpairmentType_Cheked(object sender, CheckedArgs e)
        {


            //var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentTypeId).FirstOrDefault().TypeDescription;
            //var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentClassId).FirstOrDefault().ClassDescription;
            //var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == syncImpairements.ShutDownReasonId).FirstOrDefault().ClassDescription;


            var selcontrol = (CustomCheckbox)sender;
            selcontrol.Checked = e.Checked;
            CusGrid grid = (CusGrid)selcontrol.Parent;
            Label labeltitle = (Label)grid.Children[0];
            var val = labeltitle.Text;
            //var shutlst = Model._impairmentTypeService.GetImpairmentType();
            //var res = shutlst.Where(s => s.TypeDescription == val).FirstOrDefault();

           // var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.TypeDescription.ToLower() == val.ToLower()).FirstOrDefault();
            //  var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentClassId).FirstOrDefault().ClassDescription;
            //   var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == syncImpairements.ShutDownReasonId).FirstOrDefault().ClassDescription;


            string[] impairmentType1 = new string[] { AppResource.IMP_TYPE_1_PLACEHOLDER,
                    AppResource.IMP_TYPE_2_PLACEHOLDER,
                AppResource.IMP_TYPE_3_PLACEHOLDER,
                AppResource.IMP_TYPE_4_PLACEHOLDER,
                AppResource.IMP_TYPE_5_PLACEHOLDER};


            int impairmentType = Array.IndexOf(impairmentType1, val) + 1;

            if (impairmentType >0)
            {
                if (e.Checked)
                {
                    Model.ImpairmentTypeFilterColletion.Add(impairmentType);
                }
                else
                {
                    Model.ImpairmentTypeFilterColletion.Remove(impairmentType);
                }
            }
        }

        private void ReasonShutDown_Cheked(object sender, CheckedArgs e)
        {

            //var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentTypeId).FirstOrDefault().TypeDescription;
            //var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentClassId).FirstOrDefault().ClassDescription;
            //var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == syncImpairements.ShutDownReasonId).FirstOrDefault().ClassDescription;


            var selcontrol = (CustomCheckbox)sender;
            selcontrol.Checked = e.Checked;
            CusGrid grid = (CusGrid)selcontrol.Parent;
            Label labeltitle = (Label)grid.Children[0];
            var val = labeltitle.Text;
            //var shutlst = Model._shutDownReasonService.GetShutDownReason();
            //var res = shutlst.Where(s => s.ClassDescription == val).FirstOrDefault();


            string[] impairmentShutdownReason1 = new string[] { AppResource.IMP_SHUTDOWN_REASON_1_PLACEHOLDER ,
        AppResource.IMP_SHUTDOWN_REASON_2_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_3_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_4_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_5_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_6_PLACEHOLDER};


            int impairmentShutdownReason = Array.IndexOf(impairmentShutdownReason1, val) + 1;


            if (impairmentShutdownReason >0)
            {
                if (e.Checked)
                {
                    Model.ImpairmentReasonFilterColletion.Add(impairmentShutdownReason);
                }
                else
                {
                    Model.ImpairmentReasonFilterColletion.Remove(impairmentShutdownReason);
                }
            }
        }
        #endregion

        #region HotWorks Filter
        private void HotWorksChecked_Changed(object sender, CustomControl.CheckedArgs e)
        {
            var selcontrol = (CustomCheckbox)sender;
            selcontrol.Checked = e.Checked;
            CusGrid grid = (CusGrid)selcontrol.Parent;
            Label labeltitle = (Label)grid.Children[0];
            var val = labeltitle.Text;
            if (e.Checked)
            {
                Model.HotworksFilterColletion.Add(val);
            }
            else
            {
                Model.HotworksFilterColletion.Remove(val);
            }
        }
        #endregion
        #endregion

        #region Tab Selection
        private void HandleAction(View arg1, object arg2)
        {
            Deselectview();
            TabView selv = (TabView)arg1;
            Selectview(selv);
        }

        private void Deselectview()
        {
            //var presel = TabCollection.Where(p => p.IsSelected).First();
            //var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).First());
            //TabView tab = (TabView)Spworktabs.Children[Selind];
            //tab.TabColor = Color.Transparent;
            //TabCollection.Where(p => p.IsSelected).First().IsSelected = false;
        }

        private void Selectview(TabView v)
        {
            TabView selv = v;
            var seltext = selv.Title;
            selv.TabColor = Color.FromHex("#00AFAD");
            var seltab = TabCollection.Where(s => s.Title == seltext).First();
            seltab.IsSelected = true;
            // Pageview.Content = TabCollection.Where(s => s.IsSelected).First().view.Content;
            var selitem = TabCollection.Where(s => s.IsSelected == true).FirstOrDefault();
            if (selitem.Title == "HOT WORK PERMITS")
            {
                SpHotworkFilterPanel.IsVisible = true;
                SpImpairmentFilterPanel.IsVisible = false;
            }
            else
            {
                SpHotworkFilterPanel.IsVisible = false;
                SpImpairmentFilterPanel.IsVisible = true;
            }
        }
        #endregion

        protected override void OnDisappearing()
        {
            App.isArchivePage = false;
            base.OnDisappearing();
        }
    }
}
