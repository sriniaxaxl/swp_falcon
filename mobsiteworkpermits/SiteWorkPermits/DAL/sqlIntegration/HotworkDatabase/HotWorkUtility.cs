﻿using SiteWorkPermits.Model;
using SiteWorkPermits.ViewModel;
using CustomControl;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase
{
    public static class HotWorkUtility
    {
        public static HotWorkPermit_sql CreateHotWorkInstanceSQL(
         bool isDeleted,
         bool isParent,
         bool isNewlyCreated,
         bool isEdited,
         string workFlowStatus,
         string account_number,
         string location_number,
         int server_id,
         int fireHoseCount,
         int handHeldId,
         int fireExtinguisherCount,
         Scope_sql scope_Sql, FirePrevention_sql firePrevention_Sql, 
         List<PrecautionMaster_sql> precautionMaster_Sql,
         Authorization_sql authorization_Sql, SQLiteConnection _connection) {

            try
            {
                HotWorkPermit_sql hotWorkPermit_Sql = new HotWorkPermit_sql()
                {
                    isDeleted = isDeleted,
                    isParent = isParent,
                    isNewlyCreated = isNewlyCreated,
                    isEdited = isEdited,
                    workFlowStatus = workFlowStatus,
                    account_number = account_number,
                    location_number = location_number,
                    server_id = server_id,
                    fireHoseCount = fireHoseCount,
                    handHeldId = handHeldId,
                    hotworkGUID = "00000000-0000-0000-0000-000000000000",
                    fireExtinguisherCount = fireExtinguisherCount,
                    ScannedPaperPermitGUID = Guid.NewGuid().ToString()
                };

                _connection.Insert(hotWorkPermit_Sql);

                hotWorkPermit_Sql.scope_Sql = scope_Sql;
                hotWorkPermit_Sql.firePrevention_sql = firePrevention_Sql;
                hotWorkPermit_Sql.authorization_sql = authorization_Sql;
                hotWorkPermit_Sql.precautionMaster_Sqls = precautionMaster_Sql;
                _connection.UpdateWithChildren(hotWorkPermit_Sql);

                return hotWorkPermit_Sql;
            }
            catch (Exception ex) {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
            return null;
            }

        public static Scope_sql CreateScopeInstanceSQL(
            string WorkDetail,
            string WorkLocation,
            string CompanyName,
            string EquipmentDescription,
            string StartDateTime,
            string EndDateTime,
            List<ScopePerson_sql> scopePerson_Sqls, 
            List<WorkOrderMaster_sql> workOrderMaster_Sqls, SQLiteConnection _connection)
        {
            try { 
            Scope_sql scope_Sql = new Scope_sql() {             
             WorkDetail = WorkDetail,
             WorkLocation = WorkLocation,
             CompanyName = CompanyName,
             EquipmentDescription = EquipmentDescription,
             StartDateTime = StartDateTime,
             EndDateTime = EndDateTime,
            };

            _connection.InsertAll(scopePerson_Sqls);
            _connection.Insert(scope_Sql);
            scope_Sql.scopePerson_Sqls = scopePerson_Sqls;
            scope_Sql.workOrderMaster_Sqls = workOrderMaster_Sqls;
            _connection.UpdateWithChildren(scope_Sql);
            return scope_Sql;
            }
            catch (Exception ex) {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
            return null;
        }

        public static Authorization_sql CreateAuthorizationInstanceSQL(
            string AuthorizerName,
            string Department,
            string Location,
            string AuthorizationDateTime,
            bool IsHighHazardArea,
            string AreaSupervisorName,
            string AreaSupervisorDepartment, SQLiteConnection _connection)
        {
            try
            {
                Authorization_sql authorization_Sql = new Authorization_sql()
                {
                    AuthorizerName = AuthorizerName == null?"":AuthorizerName,
                    Department = Department==null?"": Department,
                    Location = Location==null?"": Location,
                    AuthorizationDateTime = AuthorizationDateTime,
                    IsHighHazardArea = IsHighHazardArea,
                    AreaSupervisorName = AreaSupervisorName==null?"":AreaSupervisorName,
                    AreaSupervisorDepartment = AreaSupervisorDepartment==null?"": AreaSupervisorDepartment
                };
                _connection.Insert(authorization_Sql);

                return authorization_Sql;
            }
            catch (Exception ex) {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
            return null;
        }

        public static FirePrevention_sql CreateFirePreventionInstanceSQL(
            string NearestPhone,
            string NearestFireAlarmLocation,
            List<FireWatcherPrevention_sql> fireWatcherPrevention_Sqls, SQLiteConnection _connection) {
            
            try {
                FirePrevention_sql firePrevention_Sql = new FirePrevention_sql()
                {
                    NearestPhone = NearestPhone,
                    NearestFireAlarmLocation = NearestFireAlarmLocation
                };
                _connection.Insert(firePrevention_Sql);

                _connection.InsertAll(fireWatcherPrevention_Sqls);
                firePrevention_Sql.fireWatcherPrevention_Sqls = fireWatcherPrevention_Sqls;
                _connection.UpdateWithChildren(firePrevention_Sql);
                return firePrevention_Sql;

            } catch (Exception ex) {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }

            return null;
        }


        public static HWStatus GetWorkFlowStatus(string workFlow)
        {
            switch (workFlow)
            {
                case "ACTIVE":
                    return HWStatus.ACTIVE;
                case "UPCOMING":
                    return HWStatus.UPCOMING;
                case "DRAFT":
                    return HWStatus.DRAFT;
                case "EXPIRED":
                    return HWStatus.EXPIRED;
            }
            return HWStatus.ACTIVE;
        }

        public static KeyValuePair<bool,string> IsHotworkMandatoryDataValid(
            DateTime StartHotWork, 
            DateTime EndHotWork,
            ObservableCollection<WorkOrderMasterLocal> WorkOrders,
            string LocationWork)
        {
            string dateTimeValidation;
            string locationValidation;
            string WorkOrderValidation;

            dateTimeValidation = (StartHotWork == DateTime.MinValue || EndHotWork == DateTime.MinValue) ? "• Start Date and End Date" : "";
            WorkOrderValidation = (WorkOrders.ToList().Where(p => p.isSelected == true).ToList().Count <= 0) ? "• Work Order" : "";
            locationValidation = (string.IsNullOrEmpty(LocationWork)) ? "• Work Location" : "";
            KeyValuePair<bool, string> keyValuePair;

            if ((WorkOrderValidation + locationValidation + dateTimeValidation).Length != 0)
            {
                keyValuePair = new KeyValuePair<bool, string>(false, CustomControl.Validation.MANDATORY_FIELDS + "\n" + WorkOrderValidation + "\n" +
                                                        locationValidation + "\n" + dateTimeValidation);
            }
            else
            {
                keyValuePair = new KeyValuePair<bool, string>(true, "");
            }

            return keyValuePair;

        }
    }
}
