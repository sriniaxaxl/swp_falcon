﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.iOS.Platform;
using SiteWorkPermits.Platform;
using Firebase.Analytics;
using Foundation;
using Xamarin.Forms;

[assembly: Dependency(typeof(AnalyticsServiceIOS))]
namespace SiteWorkPermits.iOS.Platform
{
    public class AnalyticsServiceIOS: IAnalyticsService
    {

        public void LogEvent(string eventId)
        {
            //LogEvent(eventId, (IDictionary<string, string>)null);
        }

        public void LogEvent(string eventId, string paramName, string value)
        {
            /*LogEvent(eventId, new Dictionary<string, string>
            {
                {paramName, value}
            });*/
                       
        }
         
        public void LogEvent(string eventId, IDictionary<string, string> parameters)
        {

            /*if (parameters == null)
            {
                // Analytics.LogEvent(eventId, null);
                return;
            }*/

            //var keys = new List<NSString>();
            //var values = new List<NSString>();
            //foreach (var item in parameters)
            //{
            //    keys.Add(new NSString("iOS - "+item.Key));
            //    values.Add(new NSString("iOS - "+item.Value));
            //}



            /*var parameters1 = new Dictionary<object, object> {
                { ParameterNamesConstants.ItemName, parameters.FirstOrDefault().Key } ,
                { ParameterNamesConstants.ItemId, parameters.FirstOrDefault().Key }
              };

            var parametersDictionary = NSDictionary<NSString, NSObject>.FromObjectsAndKeys(parameters1.Values.ToArray(), parameters1.Keys.ToArray());
            Analytics.LogEvent(Firebase.Analytics.EventNamesConstants.SelectContent, parametersDictionary);*/



            //Analytics.LogEvent(Firebase.Analytics.EventNamesConstants.SelectContent, parameters1: [
            //                AnalyticsParameterItemID: "",
            //AnalyticsParameterItemName: ""]);
        }

        public void LogScreen(string screenName)
        {           
            // Analytics.SetScreenNameAndClass(screenName, null);

            //var parametersDictionary = NSDictionary<NSString, NSObject>.FromObjectsAndKeys(parameters.Values.ToArray(), parameters.Keys.ToArray());
            //Analytics.LogEvent(Firebase.Analytics.EventNamesConstants.SelectContent,parametersDictionary);



            /*var parameters = new Dictionary<object, object> {
                { "screen_name", screenName }
              };

            var parametersDictionary = NSDictionary<NSString, NSObject>.FromObjectsAndKeys(parameters.Values.ToArray(), parameters.Keys.ToArray());
            Analytics.LogEvent("screenview", parametersDictionary);*/
        }
    }
}
