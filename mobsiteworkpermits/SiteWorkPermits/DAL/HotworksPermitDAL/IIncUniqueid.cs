﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IIncUniqueid
    {
        IEnumerable<IncUniqueId> GetUniqueIds();
        bool AddIncUniqueId(IncUniqueId incUniqueId);
    }
}
