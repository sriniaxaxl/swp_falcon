﻿using SiteWorkPermits.Resx;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits
{
    public partial class HotWorksPage : ContentPage
    {
        ObservableCollection<TabInfo> TabCollection = new ObservableCollection<TabInfo>();
        ObservableCollection<ModelPermits> PermitCollection = new ObservableCollection<ModelPermits>();
        public HotWorksPage()
        {
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            InitializeComponent();
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_ACTIVE, IsSelected = true });
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_DRAFT, IsSelected = false });
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_UPCOMING, IsSelected = false });
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_EXPIRED, IsSelected = false });
            //Spworktabs.WidthRequest = App.ScreenWidth;
            foreach (var d in TabCollection)
            {
                TabView tab = new TabView();
                tab.HorizontalOptions = LayoutOptions.FillAndExpand;
                //tab.WidthRequest = App.ScreenWidth/TabCollection.Count();
                tab.Title = d.Title;
                tab.TabColor = d.IsSelected ? Color.FromHex("#00AFAD") : Color.Transparent;
                tab.GestureRecognizers.Add(new TapGestureRecognizer(HandleAction));
                tab.BindingContext = d.Collection;
               // Spworktabs.Children.Add(tab);
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();          
            try
            {
                var Model = (HotWorksPageViewModel)this.BindingContext;
                Model.RefreshListHotWorksEvent += ViewModel_RefreshListEvent;
                Model.SetHotworksData();
                Model.ChangeTabColor("active");
                //Activeview.Content = new ContentView { Content = new HotWorksListview(Model.ActivePermitCollection, false) };
                //DraftPageview.Content = new ContentView { Content = new HotWorksListview(Model.DraftPermitCollection, false) };
                //UpComingPageview.Content = new ContentView { Content = new HotWorksListview(Model.UpcomingPermitCollection, false) };
                //ExpiredPageview.Content = new ContentView { Content = new HotWorksListview(Model.ExpiredPermitCollection, false) };
                //Activeview.IsVisible = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }


        void ViewModel_RefreshListEvent()
        {
            var Model = (HotWorksPageViewModel)this.BindingContext;
          //  Model.SetHotworksData();
            //Activeview.Content = new ContentView { Content = new HotWorksListview(Model.ActivePermitCollection, false) };
            //DraftPageview.Content = new ContentView { Content = new HotWorksListview(Model.DraftPermitCollection, false) };
            //UpComingPageview.Content = new ContentView { Content = new HotWorksListview(Model.UpcomingPermitCollection, false) };
            //ExpiredPageview.Content = new ContentView { Content = new HotWorksListview(Model.ExpiredPermitCollection, false) };
            //Activeview.IsVisible = true;
        }

        private void CreateHotWorks_Tapped(object sender, System.EventArgs e)
        {
            App.curhomepage.Detail = new NewHotWorksPage();
        }

        private void HandleAction(View arg1, object arg2)
        {
            Deselectview();
            TabView selv = (TabView)arg1;
            Selectview(selv);
        }

        private void Deselectview()
        {
            var presel = TabCollection.Where(p => p.IsSelected).First();
            var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).First());
            //TabView tab = (TabView)Spworktabs.Children[Selind];
            //tab.TabColor = Color.Transparent;
            TabCollection.Where(p => p.IsSelected).First().IsSelected = false;
            //Pageremove();
        }
                      
        private void Selectview(TabView v)
        {
            TabView selv = v;
            var seltext = selv.Title;
            selv.TabColor = Color.FromHex("#00AFAD");
            var seltab = TabCollection.Where(s => s.Title == seltext).First();
            seltab.IsSelected = true;
            var selind = TabCollection.IndexOf(TabCollection.Where(s => s.Title == seltext).First());
            switch (selind)
            {
                case 0:
                    //Activeview.IsVisible = true;
                    //DraftPageview.IsVisible = false;
                    //UpComingPageview.IsVisible = false;
                    //ExpiredPageview.IsVisible = false;
                    break;
                case 1:
                    //Activeview.IsVisible = false;
                    //DraftPageview.IsVisible = true;
                    //UpComingPageview.IsVisible = false;
                    //ExpiredPageview.IsVisible = false;                                      
                    break;
                case 2:
                    //Activeview.IsVisible = false;
                    //DraftPageview.IsVisible = false;
                    //UpComingPageview.IsVisible = true;
                    //ExpiredPageview.IsVisible = false;   
                    break;
                case 3:
                    //Activeview.IsVisible = false;
                    //DraftPageview.IsVisible = false;
                    //UpComingPageview.IsVisible = false;
                    //ExpiredPageview.IsVisible = true;   
                    break;
            }           
        }             
    }
}
