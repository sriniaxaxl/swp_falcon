﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusEntry : Entry
    {
        #region Constructor
        public CusEntry() : base()
        {
            //BorderColor = Color.Black;
            //BorderWidth = 1;
            //BorderRadius = 5;              
            //if (Device.OS == TargetPlatform.iOS)
            //{
            HeightRequest = 30;
            Validation = ValidationType.None;
            //}

        }
        #endregion

        #region Properties   

        public static BindableProperty TextLengthProperty = BindableProperty.Create<CusEntry, int>(o => o.TextLength, 0);
        public int TextLength
        {
            get { return (int)GetValue(TextLengthProperty); }
            set { SetValue(TextLengthProperty, value); }
        }

        public static BindableProperty LeftImageProperty = BindableProperty.Create<CusEntry, string>(o => o.LeftImage, null);
        public string LeftImage
        {
            get { return (string)GetValue(LeftImageProperty); }
            set { SetValue(LeftImageProperty, value); }
        }

        public static BindableProperty RightImageProperty = BindableProperty.Create<CusEntry, string>(o => o.RightImage, null);
        public string RightImage
        {
            get { return (string)GetValue(RightImageProperty); }
            set { SetValue(RightImageProperty, value); }
        }



        public static BindableProperty PaddingProperty = BindableProperty.Create<CusEntry, Thickness>(o => o.Padding, new Thickness(0));
        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusEntry, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }


        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusEntry, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusEntry, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusEntry, float>(o => o.BorderRadius, 0);

        public float BorderRadius
        {
            get { return (float)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        public static BindableProperty VerticaltextAlignmentProperty = BindableProperty.Create<CusEntry, TextAlignment>(o => o.VerticaltextAlignment, TextAlignment.Center);

        public TextAlignment VerticaltextAlignment
        {
            get { return (TextAlignment)GetValue(VerticaltextAlignmentProperty); }
            set { SetValue(VerticaltextAlignmentProperty, value); }
        }

        public static BindableProperty IsMultilineProperty = BindableProperty.Create<CusEntry, bool>(o => o.IsMultiline, false);

        public bool IsMultiline
        {
            get { return (bool)GetValue(IsMultilineProperty); }
            set { SetValue(IsMultilineProperty, value); }
        }

        public static BindableProperty ValidationProperty = BindableProperty.Create<CusEntry, ValidationType>(o => o.Validation, ValidationType.None);

        public ValidationType Validation
        {
            get { return (ValidationType)GetValue(ValidationProperty); }
            set { SetValue(ValidationProperty, value); }
        }

        public static BindableProperty ValidationMessageProperty = BindableProperty.Create<CusEntry, string>(o => o.ValidationMessage, null);
        public string ValidationMessage
        {
            get { return (string)GetValue(ValidationMessageProperty); }
            set { SetValue(ValidationMessageProperty, value); }
        }

        public enum ValidationType
        {
            Email,
            Numeric,
            MobileNumber,
            Length,
            Password,
            None
        }

        #endregion
    }
}
