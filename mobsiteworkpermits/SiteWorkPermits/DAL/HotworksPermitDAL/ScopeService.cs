﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using System.Diagnostics;
using System.Linq;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class ScopeService : IScopeService
    {
        private readonly DatabaseContext _databaseContext;

        public ScopeService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddScopeAsync(Scopes scope)
        {
            try
            {
                var scopeStatus = _databaseContext.Scopes.Add(scope);
                _databaseContext.SaveChanges();
                var isAdded = scopeStatus.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Scopes> GetScopeAsync()
        {
            try
            {
                var scopes = _databaseContext.Scopes.Include(bc => bc.ScopeWorkOrders).ToList();
                return scopes;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Scopes GetScopeByIdAsync(int id)
        {
            try
            {
                var scope = _databaseContext.Scopes.Where(p=>p.ScopeId==id).Include(bc => bc.ScopeWorkOrders);
                return scope.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {

            var itemsToDelete1 = _databaseContext.Set<ScopeWorkOrder>();
            _databaseContext.ScopeWorkOrders.RemoveRange(itemsToDelete1);

            var itemsToDelete = _databaseContext.Set<Scopes>();
            _databaseContext.Scopes.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }

        public bool RemoveScopeAsync(int id)
        {
            try
            {
                var scope = _databaseContext.Scopes.Find(id);
                var scopeStatus = _databaseContext.Scopes.Remove(scope);
                _databaseContext.SaveChanges();
                var isDeleted = scopeStatus.State == EntityState.Deleted;
                return isDeleted;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateScopeAsync(Scopes scope)
        {
            try
            {
                var scopeStatus = _databaseContext.Scopes.Update(scope);
                _databaseContext.SaveChanges();
                var isUpdated = scopeStatus.State == EntityState.Unchanged;
                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
