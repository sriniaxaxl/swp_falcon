using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using CustomControl;
using Android.Renderer.CommanRender;
using System.ComponentModel;
using Android.Renderer;
using System.Globalization;

[assembly: ExportRenderer(typeof(CusTimePicker), typeof(CustomTimePickerRenderer))]
namespace Android.Renderer
{
    public class CustomTimePickerRenderer : EntryRenderer
    {
        CusTimePicker FormTime;
        private BorderRenderer _renderer;
        private const GravityFlags DefaultGravity = GravityFlags.CenterVertical;
        TimePickerDialog dialog;
       
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            FormTime = Element as CusTimePicker;
            Control.Gravity = DefaultGravity;
            UpdateBackground(FormTime);
            UpdatePadding(FormTime);
            Control.Focusable = false;
            DateTime today = DateTime.Now;
            dialog = new TimePickerDialog(Forms.Context, OnTimeSet, today.Hour, today.Minute, true);
            Control.Click += (sender, er) =>
            {
                dialog.Show();                            
            };
        }

        void OnTimeSet(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            TimeChangeArgs args = new TimeChangeArgs();
            args.NewTime = DateTime.ParseExact(e.HourOfDay.ToString("00") + ":" + e.Minute.ToString("00"), "HH:mm", CultureInfo.InvariantCulture);
            //FormTime.Timeselected.Invoke(FormTime, args);
            if (FormTime.Timeselected != null)
            {
                FormTime.Timeselected.Invoke(FormTime, args);
            } 
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;

            var entryEx = Element as CusTimePicker;
            if (e.PropertyName == CusTimePicker.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusTimePicker.BorderColorProperty.PropertyName ||
                e.PropertyName == CusTimePicker.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusTimePicker.BackgroundColorProperty.PropertyName)
            {
                UpdateBackground(entryEx);
            }
            else if (e.PropertyName == CusTimePicker.TextProperty.PropertyName)
            {
                Control.Text = FormTime.Text;

            }
            else if (e.PropertyName == CusTimePicker.PaddingProperty.PropertyName)
            {
                UpdatePadding(entryEx);
            }
            else if (e.PropertyName == CusTimePicker.IsOpenProperty.PropertyName)
            {
                if (FormTime.IsOpen)
                {
                    dialog.Show();
                    FormTime.IsOpen = false;
                }
            }
            //else if (e.PropertyName == CusTimePicker.UpdateTimeProperty.PropertyName)
            //{
            //    var h = entryEx.UpdateTime.Hour;
            //    var m =  entryEx.UpdateTime.Minute;
            //    dialog.UpdateTime(h,m);     
            //}  
            else if (e.PropertyName == CusTimePicker.IsvalidProperty.PropertyName)
            {
                if (!entryEx.Isvalid)
                {
                    dialog.UpdateTime(DateTime.Now.Hour, DateTime.Now.Minute);
                    entryEx.Isvalid = true;
                    TimeChangeArgs args = new TimeChangeArgs();
                    args.NewTime = DateTime.ParseExact(DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00"), "HH:mm", CultureInfo.InvariantCulture);
                    FormTime.Timeselected.Invoke(FormTime, args); ;
                }
            }
        }


        #region Utility methods

        private void UpdateBackground(CusTimePicker entryEx)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }
            _renderer = new BorderRenderer();
            if (entryEx.InnerBackground != null)
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.InnerBackground, entryEx.BorderWidth, entryEx.BorderRadius);
            }
            else
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.BackgroundColor, entryEx.BorderWidth, entryEx.BorderRadius);
            }

        }

        private void UpdatePadding(CusTimePicker entryEx)
        {
            Control.SetPadding((int)Forms.Context.ToPixels(entryEx.Padding.Left), (int)Forms.Context.ToPixels(entryEx.Padding.Top), (int)Forms.Context.ToPixels(entryEx.Padding.Right), (int)Forms.Context.ToPixels(entryEx.Padding.Bottom));
        }
        #endregion
    }
}