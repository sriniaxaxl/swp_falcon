﻿using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Xamarin.Forms;
using Prism.Autofac;
using Prism;
using Autofac;
using Prism.Ioc;
using SiteWorkPermits.ViewModel;
using System.Diagnostics;
using System;
using System.Linq;
using SiteWorkPermits.ViewPage;
using Prism.Navigation;
using SiteWorkPermits.Model.SyncHotWorks;
using System.Collections.Generic;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.DAL;
using System.Collections.ObjectModel;
using SiteWorkPermits.Model.SyncImpairements;
using Prism.Common;
using CustomControl;
using SQLite;
using SiteWorkPermits.Resx;
using SiteWorkPermits.DAL.sqlIntegration;
using System.IO;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.Services;
using System.Threading.Tasks;

namespace SiteWorkPermits
{
    public partial class App : PrismApplication
    {
        string AuthType;
        public static WrapLayout hwfilter { get; set; }
        public static WrapLayout impfilter { get; set; }
        public static bool PermitLoadingDone {get;set;}
        public static MasterDetailPage curhomepage;
        public static string DBPATH = string.Empty;
        //private static Stopwatch stopWatch = new Stopwatch();
        public static SQLiteConnection connection;
        public static bool FromHwToArchive;
        public static bool FromHwToNewHw;
        public static bool _isBiometricPass;
        public static bool FromImpToArchive;
        public static bool FromImpToNewImp;
        public static string HashKey = "#HM#=3#s2ydEC_+";
        public static int notificationId = 0;
        public static DateTime HotWorksStartDate;
        public static int ArchivePageCount = 1;
        public static DateTime HotWorksEndDate;
        public static INavigationService navigation;
        public static int ScreenHeight { get; set; }
        public static int PermitEditableId { get; set; }
        public static int ImpairementEditableId { get; set; }
        public static int ScreenWidth { get; set; }
        public static string BiometricMsg = AppResource.Biometric_msg;
        public static string BiometricAppNameMsg = AppResource.APP_NAME;

        public static bool isDuplicate { get; set; }
        private const int defaultTimespan = 1;
        public static DateTime? HotEditStartTime;
        public static DateTime? HotEditEndTime;
        public static bool isArchivePage = false;
        public static bool FromScanPermit = false;
        public static string ArchivePermitScanGuid = string.Empty;
        public static string PermitFlow = "new";
        public static List<ScannedPermitModel> scannedPermitModels;

        public static string EditHotworkID = string.Empty;
        public static string SiteId;
        public IContainerProvider containerProvider;
        public static int PrintEditableImpId { get; set; }
        public static List<SyncImpairements> ImpairMentArchieves = new List<SyncImpairements>();
        public static List<HotWork> HotworksArchieves = new List<HotWork>();
        public static List<ModelWorkPermit> WorkPermits = new List<ModelWorkPermit>();
        public static List<ModelWorkPermit> Impairments = new List<ModelWorkPermit>();
        public static string isImpairementEditorDuplicate = string.Empty;
        public static int Subscribval = 0;
        public static INavigationService HomeNavigation;
        public string[] AndroidVersions = new string[] { "1.1.2", "1.1.3", "1.1.4" };
        public string[] IosVersions = new string[] { "1.1.2", "1.1.3", "1.1.4" };
        public StoreType StrorType = StoreType.Public;


        //***********************sql implementaion


        public static int EditablePermitID_SQL;


            //***********************************


        public enum StoreType
        {
            Public,
            MDMU
        }


        public enum AppStatus
        {
            Launch,
            Background
        }

       protected override async void OnInitialized()
        {
            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                AppResource.Culture = ci; // set the RESX for resource localization
                DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
            }

            InitializeComponent();


          //  CheckBiometric();

            navigation = NavigationService;
            containerProvider = Container;
            //Helper.Setval(Helper.WelcomeShow, "");
            SSLValidator.Initialize();
            if (Helper.Getval(Helper.WelcomeShow).ToString() == "true")
            {
                try
                {
                    var key = Helper.Getval("JWT_TOKEN");
                    //var sitekey = Helper.Getval("siteId");
                    if (string.IsNullOrEmpty(key))
                    {
                        await NavigationService.NavigateAsync("NavigationPage/LoginPage", null, null, false);
                    }
                    else
                    {
                        var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                        if (string.IsNullOrEmpty(userToken.ToString()))
                        {
                            await NavigationService.NavigateAsync("NavigationPage/LoginPage", null, null, false);
                        }
                        else
                        {
                            string siteId = string.Empty;
                            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("siteId"))
                            {
                                siteId = Xamarin.Forms.Application.Current.Properties["siteId"].ToString();
                            }

                            if (string.IsNullOrEmpty(siteId))
                            {
                                await NavigationService.NavigateAsync("NavigationPage/LoginPage", null, null, false);
                            }
                            else
                            {
                                //await NavigationService.NavigateAsync("NavigationPage/ArchivePage");
                                try
                                {
                                    bool isSyncEnable = true;
                                    App.SiteId = Xamarin.Forms.Application.Current.Properties["siteId"] as string;
                                    var navigationParams = new NavigationParameters();
                                    navigationParams.Add("syncEnable", isSyncEnable);
                                    await NavigationService.NavigateAsync("NavigationPage/HomePage", navigationParams, null, false);
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.Message);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                finally
                {

                }
            }
            else
            {
                await NavigationService.NavigateAsync("NavigationPage/HelpScreenPageView", null, null, false);
            }

            RegisterMsgcenter();
        }

        void RegisterMsgcenter()
        {

            MessagingCenter.Subscribe<string>("SendUnsyncNotification", "SendUnsyncNotificationn", (sender) =>
            {

               CustomControl.Validation.SetUnSyncAlarm();
            });

            MessagingCenter.Subscribe<Application>(this, "EditPermit_SQL", async (id) =>
            {  

                    SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                    var connection = hotworkDatabase.GetdbConnection();
                    var hotWorkPermit_Sql = connection.GetWithChildren<HotWorkPermit_sql>(App.EditablePermitID_SQL, true);
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("hotworkPermit_sql", hotWorkPermit_Sql);
                    await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
               




                //if (e.FromArchieve)
                //{
                //    var hotworks = App.HotworksArchieves.Where(sr => sr.Id == e.Id).First();
                //    var navigationParams = new NavigationParameters();
                //    navigationParams.Add("archiveModel", hotworks);
                //    await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
                //}
                //else
                //{
                //    EditPermitByID();
                //}
            });




            MessagingCenter.Subscribe<Application, ViewInfo>(this, "EditPermit", async (s, e) =>
            {
                if (e.FromArchieve)
                {
                    var hotworks = App.HotworksArchieves.Where(sr => sr.Id == e.Id).First();
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("archiveModel", hotworks);
                    await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
                }
                else
                {
                    EditPermitByID();
                }
            });

            MessagingCenter.Subscribe<Xamarin.Forms.Application, ViewInfo>(Application.Current, "EditImparement", async (s, e) =>
            {
                if (e.FromArchieve)
                {
                    var rec = App.ImpairMentArchieves.Where(sr => sr.Id == e.Id).First();

                    try
                    {
                        Impairment impair = new Impairment();
                        impair.ImpairmentId = rec.Id;
                        impair.ReporterDetail = new Model.ReporterDetail();
                        impair.ReporterDetail.Name = rec.ReporterDetail.Name;
                        impair.ReporterDetail.Email = rec.ReporterDetail.Email;
                        impair.ReporterDetail.Phone = rec.ReporterDetail.Phone;
                        impair.ImpairmentTypeId = rec.ImpairmentTypeId;
                        impair.ImpairmentClassId = rec.ImpairmentClassId;
                        impair.ShutDownReasonId = rec.ShutDownReasonId;
                        impair.ImpairmentDescription = rec.ImpairmentDescription;
                        impair.StartDateTime = rec.StartDateTime.ToString();
                        impair.EndDateTime = rec.EndDateTime.ToString();
                        impair.ImpairmentImpairmentMeasureMasters = new ObservableCollection<ImpairmentImpairmentMeasureMaster>(from d in rec.ImpairmentMeasureMasterIdList
                                                                                                                                select new ImpairmentImpairmentMeasureMaster
                                                                                                                                {
                                                                                                                                    ImpairmentMeasureMasterId = d
                                                                                                                                });

                        impair.ImpairmentPrecautionTakens = new ObservableCollection<ImpairmentPrecautionTaken>(from d in rec.ImpairmentPrecautionList
                                                                                                                select new ImpairmentPrecautionTaken
                                                                                                                {
                                                                                                                    ImpairmentPrecautionId = d.ImpairmentPrecautionMasterId
                                                                                                                });
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("impairmentArchieveModel", impair);
                        await HomeNavigation.NavigateAsync("NewImpairmentPage", navigationParams, null, false);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }
                else
                {
                    EditImparementByID();
                }
                MessagingCenter.Unsubscribe<Xamarin.Forms.Application>(this, "EditImparement");
            });

            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "ViewPermit", (sender) =>
            {
                ViewPermitByID();
            });

            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "DeletePermit", (sender) =>
            {
                DeletePermitByID();
            });
        }

        public async void EditPermitByID()
        {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();
            var hotWorkPermit_Sql = connection.GetWithChildren<HotWorkPermit_sql>(App.PermitEditableId, true);            
            var navigationParams = new NavigationParameters();
            navigationParams.Add("hotworkPermit_sql", hotWorkPermit_Sql);
            await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
        }

        public async void EditImparementByID()
        {
            //var _impairmentService = Container.Resolve<IImpairmentService>();
            //var impairment = _impairmentService.GetImpairmentById(App.ImpairementEditableId);

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();
            var impairment_Sql = connection.GetWithChildren<Impairment_Sql>(App.ImpairementEditableId, true);

            var navigationParams = new NavigationParameters();
            navigationParams.Add("impairmentModel", impairment_Sql);
            await HomeNavigation.NavigateAsync("NewImpairmentPage", navigationParams, null, false);
        }

        public async void ViewPermitByID()
        {
            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();
            var hotWorkPermit_Sql = connection.GetWithChildren<HotWorkPermit_sql>(App.PermitEditableId, true);

            var navigationParams = new NavigationParameters();
            navigationParams.Add("archiveModel_sql", hotWorkPermit_Sql);
            await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
        }

        public void DeletePermitByID()
        {
          
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<ArchivePage, ArchieveViewModel>();
            containerRegistry.RegisterForNavigation<NewImpairmentPage, NewImpairmentsPageViewModel>();
            containerRegistry.RegisterForNavigation<ImpairmentsPage, ImpairmentsViewModel>();
            containerRegistry.RegisterForNavigation<NewHotWorksPage, NewHotWorksPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<HotWorksPage, HotWorksPageViewModel>();
            containerRegistry.RegisterForNavigation<AboutUsPage, AboutUsPageViewModel>();
            containerRegistry.RegisterForNavigation<TermsConditionLoginPage, TermsAndConditionLoginViewModel>();
            containerRegistry.RegisterForNavigation<TermsAndConditionPage, TermsAndConditionPageViewModel>();
            containerRegistry.RegisterForNavigation<ContactUs, ContactUsViewModel>();
            containerRegistry.RegisterForNavigation<PrintPermitPage, PrintPermitViewModel>();
            containerRegistry.RegisterForNavigation<PrintImpairmentPage, PrintImpairmentViewModel>();
            containerRegistry.RegisterForNavigation<HelpPage, HelpViewModel>();
            containerRegistry.RegisterForNavigation<DownloadPage, DownloadViewModel>();
            containerRegistry.RegisterForNavigation<GuidePage, GuideViewModel>();
            containerRegistry.RegisterForNavigation<HelpScreenPageView, HelpScreenPageViewModel>();
            containerRegistry.Register<INavigationService>();
            containerRegistry.Register<IWorkOrdersService, WorkOrdersService>();
            containerRegistry.Register<IScopeService, ScopeService>();
            containerRegistry.Register<IFirewatchPersonsService, FirewatchPersonsService>();
            containerRegistry.Register<IPrecautionsService, PrecautionsService>();
            containerRegistry.Register<IHighHazardAreasService, HighHazardAreasService>();
            containerRegistry.Register<IFirePreventionMeasuresService, FirePreventionMeasuresService>();
            containerRegistry.Register<IAuthorizationService, AuthorizationService>();
            containerRegistry.Register<IHotworkPermitService, HotworkPermitService>();

            containerRegistry.Register<IIncUniqueid, IncUniqueidService>();

            containerRegistry.Register<IReporterDetailService, ReporterDetailService>();
            containerRegistry.Register<IImpairmentService, ImpairmentService>();
            containerRegistry.Register<IImpairmentTypeService, ImpairmentTypeService>();
            containerRegistry.Register<IImpairmentClassService, ImpairmentClassService>();
            containerRegistry.Register<IShutDownReasonService, ShutDownReasonService>();
            containerRegistry.Register<IPrecautionTakenService, PrecautionTakenService>();
            containerRegistry.Register<IMajorImpairmentsService, MajorImpairmentsService>();

        }

        public App(IPlatformInitializer platformInitializer = null) : base(platformInitializer)
        {

        }

        protected override void OnStart()
        {

            var key = Helper.Getval("JWT_TOKEN");
            //var sitekey = Helper.Getval("siteId");
            if (string.IsNullOrEmpty(key))
            {
            }
            else {
                CheckBiometric();
            }


            //            if (!stopWatch.IsRunning)
            //            {
            //                stopWatch.Start();
            //            }
            //            //UpdateStatus();
            //            Device.StartTimer(TimeSpan.FromMinutes(5), () =>
            //            {
            //                Task.Factory.StartNew(() =>
            //                {
            //                    if (stopWatch.IsRunning && stopWatch.Elapsed.Minutes >= defaultTimespan)
            //                    {
            ////UpdateStatus();
            //                        stopWatch.Restart();
            //                    }
            //                });
            //                return true;
            //            });

            //MamcService(AppStatus.Launch);
        }

        bool Androidpopwork = true;
        #region MamcService
        async void MamcService(AppStatus status)
        {
            var curpage = PageUtilities.GetCurrentPage(Current.MainPage);
            var mamcdata = await RestApiHelper<MamcData>.Get_Mamc();
            try
            {
                if (mamcdata != null)
                {
                    if (status == AppStatus.Launch)
                    {
                        if (mamcdata.DowntimeAlert)
                        {
                            await curpage.DisplayAlert(CustomControl.Validation.DownTimeTitle, mamcdata.ReasonForDowntime, "Ok");
                        }
                    }

                    var Updation = false;

                    string UrlFornavigate;
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        Updation = UpgradeRequired(mamcdata.AndroidMinimumAppVersion, CustomControl.Validation.CurrentAndroidVersion);
                        UrlFornavigate = mamcdata.GooglePlayURL;
                    }
                    else
                    {
                        Updation = UpgradeRequired(mamcdata.iOSMinimumAppVersion, CustomControl.Validation.CurrentIOSVersion);
                        UrlFornavigate = mamcdata.iTunesURL;
                    }

                    if (Updation)
                    {
                        string msg = mamcdata.TypeOfUpgradeAlert == "Optional" ? CustomControl.Validation.UpgradeOptionalMessage : CustomControl.Validation.UpgradeMandatoryMessage;

                        if (mamcdata.TypeOfUpgradeAlert == "Optional")
                        {
                            bool res;
                            if (status == AppStatus.Launch)
                            {
                                res = await curpage.DisplayAlert(CustomControl.Validation.UpgradeTitle, msg, "Upgrade", "Cancel");
                                if (res)
                                {
                                    string url = string.Empty;
                                    if (StrorType == StoreType.Public)
                                    {
                                        url = Device.RuntimePlatform == Device.Android ? mamcdata.GooglePlayURL : mamcdata.iTunesURL;
                                    }
                                    else
                                    {
                                        url = Device.RuntimePlatform == Device.Android ? mamcdata.AndroidMDMURL : mamcdata.iOSMDMURL;
                                    }
                                    Device.OpenUri(new Uri(url));
                                }
                            }
                        }
                        else
                        {
                            if (Device.RuntimePlatform == Device.Android)
                            {
                                Androidpopwork = false;
                                DependencyService.Get<IAlertService>().Valueselected += (sender, e) =>
                                {
                                    string url = string.Empty;
                                    if (StrorType == StoreType.Public)
                                    {
                                        if (StrorType == StoreType.Public)
                                        {
                                            url = Device.RuntimePlatform == Device.Android ? mamcdata.GooglePlayURL : mamcdata.iTunesURL;
                                        }
                                        else
                                        {
                                            url = Device.RuntimePlatform == Device.Android ? mamcdata.AndroidMDMURL : mamcdata.iOSMDMURL;
                                        }
                                        Device.OpenUri(new Uri(url));
                                    }
                                };
                                DependencyService.Get<IAlertService>().DisplayAlert(CustomControl.Validation.UpgradeTitle, msg, "Upgrade", " ");
                                Androidpopwork = true;
                            }
                            else
                            {
                                var res = await curpage.DisplayAlert(CustomControl.Validation.UpgradeTitle, msg, "Upgrade", " ");
                                if (res)
                                {
                                    string url = string.Empty;
                                    if (StrorType == StoreType.Public)
                                    {
                                        if (StrorType == StoreType.Public)
                                        {
                                            url = Device.RuntimePlatform == Device.Android ? mamcdata.GooglePlayURL : mamcdata.iTunesURL;
                                        }
                                        else
                                        {
                                            url = Device.RuntimePlatform == Device.Android ? mamcdata.AndroidMDMURL : mamcdata.iOSMDMURL;
                                        }
                                        Device.OpenUri(new Uri(url));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private bool UpgradeRequired(string Version, string CurrentAppVersion)
        {
            var Digits = GetDigits(Version);
            var CurrentAppDigits = GetDigits(CurrentAppVersion);

            if (Convert.ToInt32(Digits[0]) > Convert.ToInt32(CurrentAppDigits[0]))
            {
                return true;
            }
            else if (Convert.ToInt32(Digits[1]) > Convert.ToInt32(CurrentAppDigits[1]))
            {
                return true;
            }
            else if (Convert.ToInt32(Digits[2]) > Convert.ToInt32(CurrentAppDigits[2]))
            {
                return true;
            }
            else
            {
                return false;
            }



            //if (Convert.ToInt32(CurrentAppDigits[0]) > Convert.ToInt32(Digits[0]))
            //{
            //    return false;
            //}
            //else if (Convert.ToInt32(CurrentAppDigits[1]) > Convert.ToInt32(Digits[1]))
            //{
            //    return false;
            //}
            //else if (Convert.ToInt32(CurrentAppDigits[2]) > Convert.ToInt32(Digits[2]))
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
        }

        string[] GetDigits(string version)
        {
            return version.Split('.');
        }
        #endregion

        protected override void OnSleep()
        {
           // stopWatch.Reset();
        }

        public async void CheckBiometric() {
            App._isBiometricPass = false;
            var navigationParams = new NavigationParameters();
            navigationParams.Add("syncEnable", true);

            if (Device.RuntimePlatform == Device.iOS)
            {
                AuthType = Xamarin.Forms.DependencyService.Get<IBiometricAuthenticateService>().GetAuthenticationType();
                if (!AuthType.Equals("None"))
                {
                    Console.WriteLine("Please use " + AuthType + " to unlock");

                    if (AuthType.Equals("TouchId") || AuthType.Equals("FaceId"))
                    {
                        GetAuthResults();
                    }
                }
                else
                {
                    //await _navigationService.NavigateAsync(page, navigationParams, null, false);
                }
            }
            else
            {
                MessagingCenter.Subscribe<string>("BiometricPrompt", "BiometricResultYes", async (sender) =>
                {
                    //MessagingCenter.Unsubscribe<string>("BiometricPrompt", "BiometricResultYes");
                    // await HomeNavigation.NavigateAsync("HomePage", navigationParams, null, false);
                });

                MessagingCenter.Subscribe<string>("BiometricPrompt", "BiometricResultNo", (sender) =>
                {

                    Xamarin.Forms.DependencyService.Get<IBiometricAuthentication>().isBiometricSuccess();
                    //MessagingCenter.Unsubscribe<string>("BiometricPrompt", "BiometricResultNo");
                });

                if (!App._isBiometricPass)
                {
                    Xamarin.Forms.DependencyService.Get<IBiometricAuthentication>().isBiometricSuccess();
                }

            }
        }

        private async Task GetAuthResults()
        {
            //todo according to Auth type change the authenticationmethod in interface if face id or touch id
            //string AuthType = DependencyService.Get<IFingerprintAuthService>().GetAuthenticationType();
            var result = await Xamarin.Forms.DependencyService.Get<IBiometricAuthenticateService>().AuthenticateUserIDWithTouchID();
            if (result)
            {
                if (AuthType.Equals("TouchId"))
                {
                    Console.WriteLine("TouchID authentication success");
                }
                else if (AuthType.Equals("FaceId"))
                {
                    Console.WriteLine("FaceID authentication success");
                }
                //await _navigationService.NavigateAsync(page, navigationParams, null, false);
            }
            else
            {
                AuthType = Xamarin.Forms.DependencyService.Get<IBiometricAuthenticateService>().GetAuthenticationType();
                if (!AuthType.Equals("None"))
                {
                    Console.WriteLine("Please use " + AuthType + " to unlock");

                    if (AuthType.Equals("TouchId") || AuthType.Equals("FaceId"))
                    {
                        GetAuthResults();
                    }
                }
                //Console.WriteLine("Authentication failed");
            }
        }

        protected override async void OnResume()
        {

            //CheckBiometric();


            //// stopWatch.Start();
            if (Device.RuntimePlatform == Device.iOS)
            {
                //MamcService(AppStatus.Background);
            }
            else
            {
                if (Androidpopwork)
                {
                    //MamcService(AppStatus.Background);
                }
            }
        }
    }
}
