﻿using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.Model;
using CustomControl;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static SiteWorkPermits.ViewModel.NewImpairmentsPageViewModel;

namespace SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase
{
    public static class ImpairmentUtility
    {

        public static KeyValuePair<bool, string> IsImpairmentMandatoryDataValid(
            string reportName, string reportEmail, string reportPhoneNumber,
            string describeImpairment, DateTime date1, DateTime date2,
            Dropitem selImpairmenType, Dropitem selImpairmenClass, Dropitem selReasonForShutDown)
        {
            KeyValuePair<bool, string> keyValuePair;

            if (string.IsNullOrEmpty(reportName) || string.IsNullOrEmpty(reportEmail) ||
                  string.IsNullOrEmpty(reportPhoneNumber) || string.IsNullOrEmpty(describeImpairment))
            {
                keyValuePair = new KeyValuePair<bool, string>(false, "Please fill all mandatory fields");
                return keyValuePair;
            }

            var isEmailValid = CustomControl.Validation.Emailvalid(reportEmail);

            if (!isEmailValid)
            {

                keyValuePair = new KeyValuePair<bool, string>(false, "Enter a valid Email address e.g. yourusername@example.com");
                return keyValuePair;
            }


            var isPhoneValid = CustomControl.Validation.Numericvalid(reportPhoneNumber);

            if (!isPhoneValid)
            {
                keyValuePair = new KeyValuePair<bool, string>(false, "Please enter valid phone number");
                return keyValuePair;
            }

            if (date1.Date == new DateTime(1, 1, 1) || date2.Date == new DateTime(1, 1, 1))
            {
                keyValuePair = new KeyValuePair<bool, string>(false, "Invalid Start/ End time.");
                return keyValuePair;
            }

            if (selImpairmenType.Index == 0)
            {
                keyValuePair = new KeyValuePair<bool, string>(false, "Please select Impairment type.");
                return keyValuePair;
            }


            if (selImpairmenClass.Index == 0)
            {
                keyValuePair = new KeyValuePair<bool, string>(false, "Please select Impairment class.");
                return keyValuePair;
            }


            if (selReasonForShutDown.Index == 0)
            {
                keyValuePair = new KeyValuePair<bool, string>(false, "Please select reason for shutdown.");
                return keyValuePair;
            }

            keyValuePair = new KeyValuePair<bool, string>(true, "");
            return keyValuePair;
        }


        public static ReporterDetail_sql GetReporterDetails(
            string ReportName, string ReportEmail, string ReportPhoneNumber)
        {
            try
            {
                ReporterDetail_sql reporterDetail_Sql = new ReporterDetail_sql
                {
                    ReporterName = ReportName,
                    ReporterEmail = ReportEmail,
                    ReporterPhone = ReportPhoneNumber
                };

                
                return reporterDetail_Sql;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Impairment_Sql GetImpairment(ReporterDetail_sql reporterDetail_Sql,
            int selImpairmenTypeIndex, int selImpairmenClassIndex, int selReasonForShutDownIndex,
            DateTime dateTime1, DateTime dateTime2, string impairmentDescription, string hotworksStatus, int server_id) {

            try
            {

                //int uniqueId = incUniqueid.GetUniqueIds().LastOrDefault().UniqueIncrementalId;
                //var hotWorksHandHeldID = CustomControl.Validation.GetHandHeldId() + "" + (uniqueId + 1).ToString();
                //incUniqueid.AddIncUniqueId(new IncUniqueId { UniqueIncrementalId = uniqueId + 1 });

                Impairment_Sql impairment_Sql = new Impairment_Sql()
                {

                    isParent = true,
                    parentImpairmentId = 0,
                    isDeleted = false,
                    impairmentTypeId = selImpairmenTypeIndex,
                    impairmentClassId = selImpairmenClassIndex,
                    shutDownReasonId = selReasonForShutDownIndex,
                    startDateTime = dateTime1.ToString(),
                    endDateTime = dateTime2.ToString(),
                    reporterDetail_Sql = reporterDetail_Sql,
                    isNewlyCreated = true,
                    isEdited = false,
                    server_id = server_id,
                    impairmentGUID = "00000000-0000-0000-0000-000000000000",
                    impairmentDescription = impairmentDescription,
                    siteId = App.SiteId,
                };

            //    impairment_Sql.impairmentMobileId = Convert.ToInt32(hotWorksHandHeldID);



                if (hotworksStatus.Equals("Draft"))
                {
                    impairment_Sql.workFlowStatus = "DRAFT";
                }
                else
                {
                    if (dateTime1 > DateTime.Now)
                    {
                        impairment_Sql.workFlowStatus = "UPCOMING";
                    }
                    else if (dateTime1.Ticks <= DateTime.Now.Ticks && dateTime2.Ticks >= DateTime.Now.Ticks)
                    {
                        impairment_Sql.workFlowStatus = "ACTIVE";
                    }
                }

                return impairment_Sql;

            }
            catch (Exception ex) {
                return null;
            }
        }
    }
}
