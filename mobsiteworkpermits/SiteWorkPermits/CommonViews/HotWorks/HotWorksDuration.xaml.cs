﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Prism.Common;
using Xamarin.Forms;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public partial class HotWorksDuration : ContentView
    {
        public HotWorksDuration()
        {
            InitializeComponent();
            StartTimePicker.Timeselected += StartTimePicker_Timeselected;
            StartDatePicker.Dateselected += StartDatePicker_Dateselected;
            EndTimePicker.Timeselected += EndTimePicker_Timeselected;
            EndDatePicker.Dateselected += EndDatePicker_Dateselected;
            StartDatePicker.MinimumDate = DateTime.Now;
            StartTimePicker.MinimumDate = DateTime.Now;
            EndDatePicker.MinimumDate = DateTime.Now;
            EndTimePicker.MinimumDate = DateTime.Now;
            EndTimePicker.Text = "";
            EndDatePicker.Text = "";

            if (App.HotEditStartTime != null)
            {
                StartTime = Convert.ToDateTime(App.HotEditStartTime);
                StartDatePicker.Text = CustomControl.Validation.ToCultureDate(StartTime);
                StartTimePicker.Text = Helper.TimeFormatSet(StartTime);
            }
            if (App.HotEditStartTime != null)
            {
                EndTime = Convert.ToDateTime(App.HotWorksEndDate);
                EndDatePicker.Text = CustomControl.Validation.ToCultureDate(EndTime);
                EndTimePicker.Text = Helper.TimeFormatSet(EndTime);
            }
        }

        public static BindableProperty StartTimeProperty = BindableProperty.Create<HotWorksDuration, DateTime>(o => o.StartTime, DateTime.Now, BindingMode.TwoWay, null, StartTimePropertyChanged);

        public DateTime StartTime
        {
            get { return (DateTime)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        static void StartTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var control = (HotWorksDuration)bindable;
            if (newValue.Date == new DateTime(1, 1, 1))
            {
                control.StartDatePicker.Text = String.Empty;
                control.StartTimePicker.Text = String.Empty;
                // Xamarin.Forms.Application.Current.MainPage.DisplayAlert("", "End Time can not be less than Start Time", "Ok");
            }
            else
            {
                control.StartTime = newValue;
                control.StartDatePicker.Text = CustomControl.Validation.ToCultureDate(newValue);
                control.StartTimePicker.Text = Helper.TimeFormatSet(newValue);
            }
        }

        public static BindableProperty EndTimeProperty = BindableProperty.Create<HotWorksDuration, DateTime>(o => o.EndTime, DateTime.Now, BindingMode.TwoWay, null, EndTimePropertyChanged);

        public DateTime EndTime
        {
            get { return (DateTime)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }

        private async void EndTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            var Curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            var model = (NewHotWorksPageViewModel)Curpage.BindingContext;

            if (model.FirewatchDuringHotWork.Count > 0 || model.FirewatchAfterHotWork.Count > 0)
            {
                var isFireWatcherReset = await Curpage.DisplayAlert(string.Empty, "Firewatch start and end times will be reset.", AppResource.OK_LABEL, AppResource.CANCEL_LABEL);
                if (!isFireWatcherReset)
                {

                }
                else
                {
                    model.FirewatchDuringHotWork.Clear();
                    model.FirewatchAfterHotWork.Clear();
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "ClearFireWatcher");
                    var EndTime1 = e.NewTime.TimeOfDay;
                    EndTimePicker.Text = Helper.TimeFormatSet(e.NewTime);
                    EndTime = EndTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
                    CheckEndDate();
                    //if (model.FirewatchDuringHotWork.Count > 0)
                    //{
                    //    foreach (var d in model.FirewatchDuringHotWork)
                    //    {
                    //        if (!(d.StartTime.TimeOfDay < EndTime1 && d.EndTime.TimeOfDay <= EndTime1))
                    //        {
                    //            Curpage.DisplayAlert(string.Empty, Validation.Time_Alert_After, "Ok");
                    //            return;
                    //        }
                    //    }
                    //}

                    //if (model.FirewatchAfterHotWork.Count > 0)
                    //{
                    //    foreach (var d in model.FirewatchAfterHotWork)
                    //    {
                    //        if (!(EndTime1 <= d.StartTime.TimeOfDay))
                    //        {
                    //            Curpage.DisplayAlert(string.Empty, Validation.Time_Alert_After, "Ok");
                    //        }
                    //    }
                    //}
                }
            }
            else
            {

                model.FirewatchDuringHotWork.Clear();
                model.FirewatchAfterHotWork.Clear();
                MessagingCenter.Send(Xamarin.Forms.Application.Current, "ClearFireWatcher");
                var EndTime1 = e.NewTime.TimeOfDay;
                EndTimePicker.Text = Helper.TimeFormatSet(e.NewTime);
                EndTime = EndTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
                CheckEndDate();
            }
        }

        static async void EndTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var control = (HotWorksDuration)bindable;


            if (App.PermitLoadingDone)
            {
                var duration = (newValue - control.StartTime).TotalHours;
                if (duration >= 12)
                {
                    await Application.Current.MainPage.DisplayAlert(AppResource.WORK_SHIFT_ERROR_LABEL, AppResource.WORKSHIFT_RESTRICTION_12HRS, AppResource.OK_LABEL);

                    control.EndDatePicker.Text = String.Empty;
                    control.EndTimePicker.Text = String.Empty;
                }
                else if (duration >= 8 && duration < 12)
                {
                    var result = await Application.Current.MainPage.DisplayAlert(AppResource.WORK_SHIFT_ERROR_LABEL, AppResource.WORKSHIFT_RESTRICTION_8HRS, AppResource.OK_LABEL, AppResource.CANCEL_LABEL);

                    if (!result)
                    {
                        control.EndDatePicker.Text = String.Empty;
                        control.EndTimePicker.Text = String.Empty;

                    }
                    else
                    {
                        control.EndTime = newValue;
                        control.EndDatePicker.Text = CustomControl.Validation.ToCultureDate(newValue);
                        control.EndTimePicker.Text = Helper.TimeFormatSet(newValue);
                    }
                }
                else
                {
                    if (newValue.Date == new DateTime(1, 1, 1))
                    {
                        control.EndDatePicker.Text = String.Empty;
                        control.EndTimePicker.Text = String.Empty;
                    }
                    else
                    {
                        control.EndTime = newValue;
                        control.EndDatePicker.Text = CustomControl.Validation.ToCultureDate(newValue);
                        control.EndTimePicker.Text = Helper.TimeFormatSet(newValue);
                    }
                }

            }
            else {
                control.EndTime = newValue;
                control.EndDatePicker.Text = CustomControl.Validation.ToCultureDate(newValue);
                control.EndTimePicker.Text = Helper.TimeFormatSet(newValue);
            }
           
        }

        private async void StartTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            var Curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            var model = (NewHotWorksPageViewModel)Curpage.BindingContext;
            if (model.FirewatchDuringHotWork.Count > 0 || model.FirewatchAfterHotWork.Count > 0)
            {
                var isFireWatcherReset = await Curpage.DisplayAlert(string.Empty, "Firewatcher will be reset.", AppResource.OK_LABEL, AppResource.CANCEL_LABEL);
                if (!isFireWatcherReset)
                {

                }
                else
                {
                    model.FirewatchDuringHotWork.Clear();
                    model.FirewatchAfterHotWork.Clear();
                    MessagingCenter.Send(Xamarin.Forms.Application.Current, "ClearFireWatcher");
                    StartTimePicker.Text = Helper.TimeFormatSet(e.NewTime);
                    StartTime = StartTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
                    CheckEndDate();

                    //if (model.FirewatchDuringHotWork.Count > 0)
                    //{
                    //    foreach (var d in model.FirewatchDuringHotWork)
                    //    {
                    //        if (d.StartTime.TimeOfDay < e.NewTime.TimeOfDay)
                    //        {
                    //            Curpage.DisplayAlert(string.Empty, Validation.Time_Alert_After, "Ok");
                    //            return;
                    //        }
                    //    }
                    //}
                    //if (model.FirewatchAfterHotWork.Count > 0)
                    //{
                    //    foreach (var d in model.FirewatchAfterHotWork)
                    //    {
                    //        if (d.StartTime.TimeOfDay < e.NewTime.TimeOfDay)
                    //        {
                    //            Curpage.DisplayAlert(string.Empty, Validation.Time_Alert_After, "Ok");
                    //            return;
                    //        }
                    //    }
                    //}
                }
            }
            else
            {
                model.FirewatchDuringHotWork.Clear();
                model.FirewatchAfterHotWork.Clear();
                MessagingCenter.Send(Xamarin.Forms.Application.Current, "ClearFireWatcher");
                StartTimePicker.Text = Helper.TimeFormatSet(e.NewTime);
                StartTime = StartTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
                CheckEndDate();
            }
        }

        private void StartDatePicker_Dateselected(object sender, CustomControl.DateChangeArgs e)
        {
            EndTimePicker.Text = "";
            EndDatePicker.Text = "";
            EndTime = DateTime.MinValue;
            StartDatePicker.Text = CustomControl.Validation.ToCultureDate(e.Newdate);
            var time = StartTime.TimeOfDay;
            StartTime = e.Newdate.Date + time;
            CheckEndDate();
        }

        private void CheckEndDate()
        {
            if (DateTime.Compare(StartTime, EndTime) > 0)
            {
                EndTimePicker.Text = "";
                EndDatePicker.Text = "";
                EndTime = DateTime.MinValue;
                //Xamarin.Forms.Application.Current.MainPage.DisplayAlert("", "End Time can not be less than Start Time", "Ok");
            }
            else if (DateTime.Compare(StartTime, EndTime) == 0)
            {
                EndTimePicker.Text = "";
                EndDatePicker.Text = "";
                EndTime = DateTime.MinValue;
                // Xamarin.Forms.Application.Current.MainPage.DisplayAlert("", "End Time can not be less than Start Time", "Ok");
            }
            else
            {
                App.HotWorksStartDate = StartTime;
                App.HotWorksEndDate = EndTime;
            }
        }
       
        private void EndDatePicker_Dateselected(object sender, CustomControl.DateChangeArgs e)
        {
            EndDatePicker.Text = CustomControl.Validation.ToCultureDate(e.Newdate);
            var time = EndTime.TimeOfDay;
            EndTime = e.Newdate.Date + time;
        }

         }
}
