﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using SiteWorkPermits.DAL.sqlIntegration;
using SiteWorkPermits.iOS.Platform;
using SQLite.Net.Cipher.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_iOS))]
namespace SiteWorkPermits.iOS.Platform
{
    class SQLite_iOS : ISQLite
    {


        public SQLite_iOS()
        {
        }

        #region ISQLite implementation

        ISecureDatabase _secureDatabase;

        public string GetConnectionPath()
        {
            var fileName = "dev_axa_siteworkpermit_v1.db3";
            var dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                    "..", "Library", "dev_axa_siteworkpermit_v1.db3");
            var path = Path.Combine(dbPath, fileName);
            App.connection = new SQLite.SQLiteConnection(dbPath);
            //    var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
            // _secureDatabase = new SQLiteDatabase(platform, path);
            return dbPath;
        }

        //public ISecureDatabase GetsecureDB()
        //{
        //    if (_secureDatabase != null)
        //    {
        //        return _secureDatabase;
        //    }
        //    else {
        //        GetConnectionPath();
        //        return _secureDatabase;
        //    }
        //}

        #endregion


    }
}