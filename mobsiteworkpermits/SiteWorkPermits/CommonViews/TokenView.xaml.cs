﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;
using SiteWorkPermits.Resx;
using SiteWorkPermits.ViewModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class TokenView : ContentView
    {
        //public string note2 = AppResource.PRECAUTION_NOTE2;
        //public string note3 = AppResource.PRECAUTION_NOTE3;
        //public string note4 = AppResource.PRECAUTION_NOTE4;
        public string note1 = AppResource.PRECAUTION_NOTE1;
       static CustomControl.CusGrid CusGrid;
        public TokenView(PrecautionsMasterLocal precautions = null)
        {
            InitializeComponent();
            lblNote.Text = note1;
            NoteVisible = false;
            CusGrid = Gdna;




            if (precautions != null)
            {

                if(precautions.PrecautionID== 6 && precautions.PrecautionActions == PrecautionActions.YES){
                    GdAdditionalView.IsVisible = true;
                }

                NoOfFireExtinguiser = precautions.NoOfFireExtinguiser;
                NoOfFireHouse = precautions.NoOfFirehouses;
                switch (precautions.PrecautionActions)
                {
                    case PrecautionActions.YES:
                        Gdyes.InnerBackground = Color.FromHex("#00AFAD");
                        Gdna.InnerBackground = Color.FromHex("#F0F0F0");                                                                  
                        break;
                    case PrecautionActions.NA:
                        Gdna.InnerBackground = Color.FromHex("#00AFAD");
                        Gdyes.InnerBackground = Color.FromHex("#F0F0F0");
                        break;
                    case PrecautionActions.SKIP:
                        break;
                }
            }
        }

        public bool NoteVisible
        {
            get
            {
                return SpNote.IsVisible;
            }
            set
            {
                SpNote.IsVisible = value;
            }
        }

        public static BindableProperty IsAdditionalProperty = BindableProperty.Create<TokenView, bool>(o => o.IsAdditional, false, BindingMode.TwoWay, null,IsAdditionalChanged);

        public bool IsAdditional
        {
            get { return (bool)GetValue(IsAdditionalProperty); }
            set { SetValue(IsAdditionalProperty, value); }
        }

        static void IsAdditionalChanged(BindableObject bindable, bool oldValue, bool newValue)
        {
            var control = (TokenView)bindable;
            if (control.BindingContext != null)
            {
                var cont = (PrecautionsMasterLocal)control.BindingContext;
                if (cont.PrecautionActions == PrecautionActions.YES)
                {
                    control.GdAdditionalView.IsVisible = newValue;
                }
            }
        }


        public static BindableProperty TitleProperty = BindableProperty.Create<TokenView, string>(o => o.Title, string.Empty, BindingMode.TwoWay, null, null);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static BindableProperty IndexProperty = BindableProperty.Create<TokenView, string>(o => o.Index, string.Empty, BindingMode.TwoWay, null, IndexPropertyChanged);

        public string Index
        {
            get { return (string)GetValue(IndexProperty); }
            set { SetValue(IndexProperty, value); }
        }

        static void IndexPropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (TokenView)bindable;
            if (newValue == "8" || newValue == "9" || newValue == "10")
            {
                CusGrid.IsVisible = true;
            }
            else {
                CusGrid.IsVisible = false;
            }
            //control.lblNO.Text = newValue;
        }

        public static BindableProperty ActionProperty = BindableProperty.Create<TokenView, PrecautionActions>(o => o.Action, PrecautionActions.SKIP, BindingMode.TwoWay, null, null);

        public PrecautionActions Action
        {
            get { return (PrecautionActions)GetValue(ActionProperty); }
            set { SetValue(ActionProperty, value); }
        }

        public static BindableProperty TotalProperty = BindableProperty.Create<TokenView, string>(o => o.Total, string.Empty, BindingMode.TwoWay, null, TotalPropertyChanged);

        public string Total
        {
            get { return (string)GetValue(TotalProperty); }
            set { SetValue(TotalProperty, value); }
        }

        static void TotalPropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (TokenView)bindable;
        }


        public static BindableProperty NoOfFireHouseProperty = BindableProperty.Create<TokenView, int>(o => o.NoOfFireHouse, 0, BindingMode.TwoWay, null, NoOfFireHousePropertyChanged);

        public int NoOfFireHouse
        {
            get { return (int)GetValue(NoOfFireHouseProperty); }
            set { SetValue(NoOfFireHouseProperty, value); }
        }

        static void NoOfFireHousePropertyChanged(BindableObject bindable, int oldValue, int newValue)
        {
            var control = (TokenView)bindable;
            control.Txtnoofhouse.Value = Convert.ToString(newValue);
            if (control.BindingContext != null)
            {
                var cont = (PrecautionsMasterLocal)control.BindingContext;
                cont.NoOfFirehouses = newValue;
            }
        }


        public static BindableProperty NoOfFireExtinguiserProperty = BindableProperty.Create<TokenView, int>(o => o.NoOfFireHouse, 0, BindingMode.TwoWay, null, NoOfFireExtinguiserPropertyPropertyChanged);

        public int NoOfFireExtinguiser
        {
            get { return (int)GetValue(NoOfFireExtinguiserProperty); }
            set { SetValue(NoOfFireExtinguiserProperty, value); }
        }

        static void NoOfFireExtinguiserPropertyPropertyChanged(BindableObject bindable, int oldValue, int newValue)
        {
            var control = (TokenView)bindable;
            control.TxtnoofFire.Value = Convert.ToString(newValue);

            if (control.BindingContext != null)
            {
                var cont = (PrecautionsMasterLocal)control.BindingContext;
                cont.NoOfFireExtinguiser = newValue;
            }
        }

        private void NA_Tapped(object sender, System.EventArgs e)
        {
            var cont = (PrecautionsMasterLocal)this.BindingContext;

            if(cont.PrecautionActions== PrecautionActions.NA){
                cont.PrecautionActions = PrecautionActions.SKIP;
                Gdna.InnerBackground = Color.FromHex("#F0F0F0");
                Gdyes.InnerBackground = Color.FromHex("#F0F0F0");   
            }else{
                cont.PrecautionActions = PrecautionActions.NA;
                Gdna.InnerBackground = Color.FromHex("#00AFAD");
                Gdyes.InnerBackground = Color.FromHex("#F0F0F0");    
            }
            GdAdditionalView.IsVisible = false;
            NoOfFireHouse = 0;
            NoOfFireExtinguiser = 0;
        }

        private void Yes_Tapped(object sender, System.EventArgs e)
        {
            var cont = (PrecautionsMasterLocal)this.BindingContext;
            if(cont.PrecautionActions == PrecautionActions.YES){
                cont.PrecautionActions = PrecautionActions.SKIP;
                Gdyes.InnerBackground = Color.FromHex("#F0F0F0");
                Gdna.InnerBackground = Color.FromHex("#F0F0F0");
                if (IsAdditional)
                {
                    GdAdditionalView.IsVisible = false;
                }    
            }else{
                cont.PrecautionActions = PrecautionActions.YES;
                Gdyes.InnerBackground = Color.FromHex("#00AFAD");
                Gdna.InnerBackground = Color.FromHex("#F0F0F0");
                if (IsAdditional)
                {
                    GdAdditionalView.IsVisible = true;
                }    
            }
        }
    }
}
