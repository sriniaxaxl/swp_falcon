﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public class ImpairmentClassService:IImpairmentClassService
    {
        private readonly DatabaseContext _databaseContext;
        public ImpairmentClassService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddImpairmentClassType(ImpairmentClassType impairmentClassType)
        {
            try
            {
                var status = _databaseContext.ImpairmentClassType.Add(impairmentClassType);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<ImpairmentClassType> GetImpairmentClassType()
        {
            try
            {
                var impairmentClasses = _databaseContext.ImpairmentClassType.ToList();
                return impairmentClasses;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ImpairmentClassType GetImpairmentClassTypeById(int id)
        {
            try
            {
                var impairmentClassTypes = _databaseContext.ImpairmentClassType.Where(p => p.Id == id);
                return impairmentClassTypes.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<ImpairmentClassType>();
            _databaseContext.ImpairmentClassType.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
