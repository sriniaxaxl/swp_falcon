﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class Tagview : ContentView
    {
        public EventHandler<TagArgs> DeleteEvent;
        public Tagview()
        {
            InitializeComponent();
        }

        void Delete_Tapped(object sender, System.EventArgs e)
        {
            DeleteEvent.Invoke(this, new TagArgs { Name = Tagname });
        }

        public string Tagname
        {
            get
            {
                return Lbltag.Text;
            }
            set
            {
                Lbltag.Text = value;
            }
        }
    }

    public class TagArgs
    {
        public string Name { get; set; }
    }
}
