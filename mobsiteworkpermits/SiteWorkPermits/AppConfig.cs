﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiteWorkPermits
{
   public class AppConfig
    {
        // * DB Pathb
        public static string passwordKey = "#HM#=3#s2ydEC_+";

        public static int ScanPaperPermitLimit = 5;
        //NPD
        public static string NPD_MAMC_URL = "https://xlcazueus2npdmamcsa.table.core.windows.net/MAMCDEV(PartitionKey='1'" +
            ",RowKey='SiteWorkPermitsnpddev')?st=2019-06-19T11%3A40%3A39Z&se=2029-06-20T11%3A40%3A00Z&sp=r&sv=2018-03-28&tn" +
            "=mamcdev&sig=sV1yZWruDonQ9OEBhF16%2FrLWuLXyIN05nYxl89uG4rE%3D";
        public static string NPD_BASE_URL = "https://SiteWorkPermits-api-npd.azu-xlc-pcsnpd.net/api/v1/";

        //PROD
        public static string PROD_MAMC_URL = "https://xlcazueus2npdmamcsa.table.core.windows.net/MAMCDEV(PartitionKey='1'" +
            ",RowKey='SiteWorkPermitsnpddev')?st=2019-06-19T11%3A40%3A39Z&se=2029-06-20T11%3A40%3A00Z&sp=r&sv=2018-03-28&tn" +
            "=mamcdev&sig=sV1yZWruDonQ9OEBhF16%2FrLWuLXyIN05nYxl89uG4rE%3D";
        public static string PROD_BASE_URL = "https://SiteWorkPermits-api.axaxl.com/api/v1/";

        //TEST
        public static string TEST_MAMC_URL = "https://xlcazueus2npdmamcsa.table.core.windows.net/MAMCDEV(PartitionKey='1'" +
            ",RowKey='SiteWorkPermitsnpddev')?st=2019-06-19T11%3A40%3A39Z&se=2029-06-20T11%3A40%3A00Z&sp=r&sv=2018-03-28&tn" +
            "=mamcdev&sig=sV1yZWruDonQ9OEBhF16%2FrLWuLXyIN05nYxl89uG4rE%3D";
        public static string TEST_BASE_URL = "https://mysite-npd-test.azurewebsites.net/test/api/v1/";

        //DEV
        public static string DEV_MAMC_URL = "https://xlcazueus2npdmamcsa.table.core.windows.net/MAMCDEV(PartitionKey='SWP'" +
            ",RowKey='en')?sp=raud&st=2021-01-04T05:38:24Z&se=2031-01-05T05:38:00Z&sv=2" +
            "019-12-12&sig=yciqzVSphpgg1D4ccjJ7OFUBWBcj9%2F0JEquIssaBZPs%3D&tn=MAMCDEV";
        public static string DEV_BASE_URL = "https://mysite-api-npd-dev.azurewebsites.net/api/v1/";

        public static string BASE_URL = DEV_BASE_URL; // do not change the line number - should be at 34
        public static string BASE_MAMC_URL = DEV_MAMC_URL; // do not change the line number - should be at 35

        public static string USER_LOGIN_URL = BASE_URL + "login";
        public static string SYNC_USER_DATA_URL = BASE_URL + "Sync";
        public static string ARCHIVE_USER_DATA_URL = BASE_URL + "ArchivedHotWorks";
        public static string ARCHIVE_IMPAIRMENTS_URL = BASE_URL + "ArchivedImpairments";
        public static string EMAIL_HW_URL = BASE_URL + "Emails/Hotwork";
        public static string DOWNLOAD_PERMIT_PDF_URL = BASE_URL + "Downloads/HotWork/";
        public static string CREATE_PERMIT_URL = BASE_URL + "HotWorks";
        public static string CREATE_IMP_URL = BASE_URL + "Impairments";
        public static string GET_PERMIT_DETAILS = BASE_URL + "HotWorks/";
        public static string EMAIL_IMP_URL = BASE_URL + "Emails/Impairment";
        public static string DOWNLOAD_IMP_PDF_URL = BASE_URL + "Downloads/Impairment/";
        public static string DOWNLOAD_SCAN_PERMIT_IMG_URL = BASE_URL + "ScanPermits/";
        public static string SYNC_SCAN_PERMIT_URL = BASE_URL + "SyncScanPermits";
        public static string LOGOUT_URL = BASE_URL + "LogOut";
        public static string TEST_ABOUTUS_URL = "https://mysite-npd-test.azurewebsites.net/AboutUs.html";
        public static string DEV_ABOUTUS_URL = "https://mysite-api-npd-dev.azurewebsites.net/AboutUs.html";

    }
}
