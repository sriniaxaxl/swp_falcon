﻿using System;
using System.ComponentModel;
using CoreGraphics;
using CustomControl;
using CustomControl.iOS.Renderer;
using FPT.Framework.iOS.UI.DropDown;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusDropboxView), typeof(CusDropboxViewRenderer))]
namespace CustomControl.iOS.Renderer
{
    public class CusDropboxViewRenderer : ViewRenderer<CusDropboxView, UIKit.UIView>
    {
        UIView Gridview;
        CusDropboxView AutoComplete;
        UIButton Btn;
        DropLabel lbl;
        protected override void OnElementChanged(ElementChangedEventArgs<CusDropboxView> e)
        {
            if (e.OldElement != null || this.Element == null)
                return;

            AutoComplete = (CusDropboxView)Element;
            Gridview = new UIView();
            DropDown d = new DropDown();
            d.DataSource = new string[] { "One", "Two", "Three", "Four", "Five" };
            lbl = new DropLabel();
            lbl.Text = "Select";                     
            lbl.UserInteractionEnabled = true;
            lbl.TextAlignment = UITextAlignment.Left;           
            d.AnchorView = new WeakReference<UIView>(lbl);
            d.BottomOffset = new CGPoint(0, lbl.Bounds.Height);
            lbl.AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                d.Show();
            }));       
            d.SelectionAction += D_SelectionAction;          
            SetNativeControl(lbl);
            SetBackground();
            SetBorder();           
        }

        void D_SelectionAction(nint arg1, string arg2)
        {
            lbl.Text = arg2;
            DropSelectedArgs args = new DropSelectedArgs();
            args.Name = arg2;
            args.Selectedindex = (int)arg1;
            AutoComplete.ItemSelectionChanged.Invoke(this, args);
        }


        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
        }


        void SetBackground()
        {
            if (AutoComplete.InnerBackground != Color.Transparent)
            {
                Layer.BackgroundColor = AutoComplete.InnerBackground.ToCGColor();
            }
            else
            {
                Layer.BackgroundColor = AutoComplete.BackgroundColor.ToCGColor();
            }
        }

        void SetBorder()
        {
            Layer.BorderColor = AutoComplete.BorderColor.ToCGColor();
            Layer.BorderWidth = AutoComplete.BorderWidth;
            Layer.CornerRadius = (nfloat)AutoComplete.BorderRadius.Top;
        }

        void SetShadow()
        {
            Layer.ShadowColor = AutoComplete.ShadowColor.ToCGColor();
            Layer.ShadowOpacity = 1.0f;
            Layer.ShadowRadius = (nfloat)AutoComplete.ShadowRadius.Left;
            Layer.ShadowOffset = new System.Drawing.SizeF(3.0f, (float)3.0);
            Layer.ShouldRasterize = true;
            Layer.MasksToBounds = false;
        }
    }

    public sealed class DropLabel : UILabel
    {
        private UIEdgeInsets EdgeInsets { get; set; } = new UIEdgeInsets(0,10,0, 10);

        public override void DrawText(CoreGraphics.CGRect rect)
        {
            var newRect = new CoreGraphics.CGRect(0, 0,rect.Width, rect.Height);
            base.DrawText(EdgeInsets.InsetRect(newRect));
        }
    }
}
