﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Android.Renderer.CommanRender;
using Android.Widget;
using CustomControl;
using Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CusDropboxView), typeof(CusDropboxViewRenderer))]
namespace Droid.Renderer
{
    public class CusDropboxViewRenderer : ViewRenderer<CusDropboxView, Spinner>
    {
        private BorderRenderer _renderer;

        CusDropboxView autoCompleteView;
        List<string> lstdata = new List<string>();
        Spinner spinner ;
        protected override void OnElementChanged(ElementChangedEventArgs<CusDropboxView> e)
		{
            base.OnElementChanged(e);
            lstdata.Add("Pranav");
            lstdata.Add("Pranav1");
            lstdata.Add("Prana2");
            lstdata.Add("Prana3");
            lstdata.Add("Prana4");
            lstdata.Add("Prana5");
            spinner = new Spinner(Forms.Context);
            autoCompleteView = Element as CusDropboxView;   
            ArrayAdapter ad = new ArrayAdapter(Forms.Context, Android.Resource.Layout.SimpleSpinnerItem, lstdata);
            spinner.Adapter = ad;
            SetNativeControl(spinner);           
            UpdateBackground(autoCompleteView);           
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
            base.OnElementPropertyChanged(sender, e);
		}

        void UpdateBackground(CusDropboxView Gd)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }

            _renderer = new BorderRenderer();

            spinner.Background = _renderer.GetBorderBackground(Gd.BorderColor, Gd.InnerBackground, Gd.BorderWidth, Gd.BorderRadius);
        }
	}
}
