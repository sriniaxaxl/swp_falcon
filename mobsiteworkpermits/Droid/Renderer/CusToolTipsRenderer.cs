﻿using System;
using System.ComponentModel;
using Android.App;
using Android.Views;
using Android.Widget;
using CustomControl;
using Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CusToolTip), typeof(CusToolTipsRenderer))]
namespace Droid.Renderer
{
    public class CusToolTipsRenderer : ViewRenderer<CusToolTip, GridLayout>
    {
        CusToolTip FormTooltip;
        public CusToolTipsRenderer()
        {
            FormTooltip = (CusToolTip)this.Element;
            var curactivity = (Activity)Forms.Context;
            var v = curactivity.Window.DecorView;
            ViewGroup vg = (ViewGroup)v;
            var rootview = vg.GetChildAt(0);
        }

		protected override void OnElementChanged(ElementChangedEventArgs<CusToolTip> e)
		{
            base.OnElementChanged(e);
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
            base.OnElementPropertyChanged(sender, e);
		}
	}
}
