﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class CreationPanelView : ContentView
    {
        public CreationPanelView()
        {
            InitializeComponent();
        }

        public bool _ActionButtonDisplay = true;
        public bool ActionButtonDisplay
        {
            get
            {
                return _ActionButtonDisplay;
            }
            set
            {
                _ActionButtonDisplay = value;
            }
        }

        public View SectionView
        {
            get
            {
                return Pageview.Content;
            }
            set
            {
                Pageview.Content = value;
            }
        }
    }
}
