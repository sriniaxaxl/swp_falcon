﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.ViewModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class AuthorizationView : ContentView
    {


        public Color Yescolor = Color.FromHex("#00AFAD");
        public Color Skipcolor = Color.FromHex("#F0F0F0");
        public AuthorizationView(NewHotWorksPageViewModel vm =null)
        {
            InitializeComponent();
            AuthTimePicker.Timeselected += StartTimePicker_Timeselected;
            AuthDatePicker.Dateselected += StartDatePicker_Dateselected;
            if(vm != null)
            {
                AuthorizeTime = new DateTime();
                AuthorizeTime = vm.AuthDate;
                GdAdditionalView.IsVisible = vm.IsHighHazardArea;
                if(vm.IsHighHazardArea)
                {
                    Gdyes.InnerBackground = Color.FromHex("#00AFAD");
                    Gdna.InnerBackground = Color.FromHex("#F0F0F0");
                }
                else
                {
                    Gdna.InnerBackground = Color.FromHex("#00AFAD");
                    Gdyes.InnerBackground = Color.FromHex("#F0F0F0");
                }
            }
        }

        public static BindableProperty AuthorizeTimeProperty = BindableProperty.Create<AuthorizationView, DateTime>(o => o.AuthorizeTime, DateTime.Now, BindingMode.TwoWay, null, StartTimePropertyChanged);

        public DateTime AuthorizeTime
        {
            get { return (DateTime)GetValue(AuthorizeTimeProperty); }
            set { SetValue(AuthorizeTimeProperty, value); }
        }

        static void StartTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var control = (AuthorizationView)bindable;
            control.AuthorizeTime = newValue;
            control.AuthDatePicker.Text = Helper.DateFormatSet(newValue);
            control.AuthTimePicker.Text = Helper.TimeFormatSet(newValue);
        }

        private void StartDatePicker_Dateselected(object sender, CustomControl.DateChangeArgs e)
        {
            AuthDatePicker.Text = DateFormatSet(e.Newdate);
            var time = AuthorizeTime.TimeOfDay;
            AuthorizeTime = e.Newdate.Date + time;
            var vm = (NewHotWorksPageViewModel)this.BindingContext;
            vm.AuthDate = AuthorizeTime;
        }

        private void StartTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            AuthTimePicker.Text = TimeFormatSet(e.NewTime);
            AuthorizeTime = AuthorizeTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
        }

        private void Yes_Tapped(object sender, System.EventArgs e)
        {
            var vm = (NewHotWorksPageViewModel)this.BindingContext;
            if(Gdyes.InnerBackground == Yescolor)
            {
                Gdyes.InnerBackground = Skipcolor;
                GdAdditionalView.IsVisible = false;
                vm.IsHighHazardArea = false;
            }
            else
            {
                Gdyes.InnerBackground = Yescolor;
                Gdna.InnerBackground = Skipcolor;
                GdAdditionalView.IsVisible = true;
                vm.IsHighHazardArea = true;
            }

            //if(GdAdditionalView.IsVisible)
            //{
            //    Gdyes.InnerBackground = Color.FromHex("#F0F0F0");
            //    Gdna.InnerBackground = Color.FromHex("#00AFAD");
            //    GdAdditionalView.IsVisible = false;
            //    vm.IsHighHazardArea = false;
            //}
            //else
            //{
                //Gdyes.InnerBackground = Color.FromHex("#00AFAD");
                //Gdna.InnerBackground = Color.FromHex("#F0F0F0");
                //GdAdditionalView.IsVisible = true;
                //vm.IsHighHazardArea = true;
            //}
        }

        private void NA_Tapped(object sender, System.EventArgs e)
        { 
            var vm = (NewHotWorksPageViewModel)this.BindingContext;
            if (Gdna.InnerBackground == Yescolor)
            {
                Gdna.InnerBackground = Skipcolor;               
            }
            else
            {
                Gdna.InnerBackground = Yescolor;
                Gdyes.InnerBackground = Skipcolor;
            }
            GdAdditionalView.IsVisible = false;
            vm.IsHighHazardArea = false;

            //vm.IsHighHazardArea = false;
            //if (GdAdditionalView.IsVisible)
            //{
            //    Gdyes.InnerBackground = Color.FromHex("#F0F0F0");
            //    Gdna.InnerBackground = Color.FromHex("#00AFAD");
            //    GdAdditionalView.IsVisible = false;
            //}
            //else
            //{
            //    Gdyes.InnerBackground = Color.FromHex("#00AFAD");
            //    Gdna.InnerBackground = Color.FromHex("#F0F0F0");
            //    GdAdditionalView.IsVisible = true;
            //}
        }

        private string DateFormatSet(DateTime dt)
        {
            return dt.ToString("dd") + "," + dt.ToString("MMM") + "," + dt.ToString("yyyy");
        }

        private string TimeFormatSet(DateTime dt)
        {
            return dt.ToString("HH:mm");
        }
    }
}
