using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using CustomControl;
using System.ComponentModel;
using Xamarin.Forms;
using Android.Renderer;
using Android.Support.V4.View;

[assembly: ExportRenderer(typeof(CusButton), typeof(CustomButtonRender))]
namespace Android.Renderer
{    
    public class CustomButtonRender : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            var btn = Element as CusButton;
            Control.SetAllCaps(false);
            UpdatePadding(btn);
            Control.StateListAnimator = new Animation.StateListAnimator();

            UpdateMaterialShadow();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            var btn = Element as CusButton;
            if (e.PropertyName == CusButton.PaddingProperty.PropertyName)
            {
                UpdatePadding(btn);
            }
            if (e.PropertyName == "Elevation")
            {
                UpdateMaterialShadow();
            }
        }

        private void UpdatePadding(CusButton Kcsbtn)
        {
            Control.SetPadding((int)Forms.Context.ToPixels(Kcsbtn.Padding.Left), (int)Forms.Context.ToPixels(Kcsbtn.Padding.Top), (int)Forms.Context.ToPixels(Kcsbtn.Padding.Right), (int)Forms.Context.ToPixels(Kcsbtn.Padding.Bottom));
        }

        private void UpdateMaterialShadow()
        {
            var materialButton = (CusButton)Element;
            ViewCompat.SetElevation(this, materialButton.Elevation);
            ViewCompat.SetElevation(Control, materialButton.Elevation);
            UpdateLayout();
        }
    }
}