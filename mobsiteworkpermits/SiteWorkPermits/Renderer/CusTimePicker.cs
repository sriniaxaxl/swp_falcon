﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusTimePicker :Entry
    {
        public EventHandler<TimeChangeArgs> Timeselected;
        public EventHandler<EventArgs> Clicked;
        public CusTimePicker() :base()
        {
            HeightRequest = 30;
        }


        #region Properties   

        public static BindableProperty PaddingProperty = BindableProperty.Create<CusTimePicker, Thickness>(o => o.Padding, new Thickness(5));
        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusTimePicker, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty UpdateTimeProperty = BindableProperty.Create<CusTimePicker, DateTime>(o => o.UpdateTime, DateTime.Now);

        public DateTime UpdateTime
        {
            get { return (DateTime)GetValue(UpdateTimeProperty); }
            set { SetValue(UpdateTimeProperty, value); }
        }

        public static BindableProperty IsOpenProperty = BindableProperty.Create<CusTimePicker, bool>(o => o.IsOpen, false);

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }


        public static BindableProperty IsvalidProperty = BindableProperty.Create<CusTimePicker, bool>(o => o.Isvalid, true);

        public bool Isvalid
        {
            get { return (bool)GetValue(IsvalidProperty); }
            set { SetValue(IsvalidProperty, value); }
        }


        public static BindableProperty MinimumDateProperty = BindableProperty.Create<CusTimePicker, DateTime?>(o => o.MinimumDate, null);

        public DateTime? MinimumDate
        {
            get { return (DateTime?)GetValue(MinimumDateProperty); }
            set { SetValue(MinimumDateProperty, value); }
        }

        public static BindableProperty MaximumDateProperty = BindableProperty.Create<CusTimePicker, DateTime?>(o => o.MaximumDate, null);

        public DateTime? MaximumDate
        {
            get { return (DateTime?)GetValue(MaximumDateProperty); }
            set { SetValue(MaximumDateProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusTimePicker, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusTimePicker, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusTimePicker, float>(o => o.BorderRadius, 0);

        public float BorderRadius
        {
            get { return (float)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        #endregion
    }

    public class TimeChangeArgs
    {
        public DateTime NewTime { get; set; }
        public DateTime OldTime { get; set; }
    }
}
