using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System.ComponentModel;
using CustomControl;
using Android.Renderer.CommanRender;
using Android.Renderer;
using static Android.Views.View;

[assembly: ExportRenderer(typeof(CusPicker), typeof(CustomPickerRenderer))]
namespace Android.Renderer
{
    public class CustomPickerRenderer : EntryRenderer
    {
        CusPicker FormPicker;
        AlertDialog _dialog;
        private BorderRenderer _renderer;
        NumberPicker picker;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            FormPicker = Element as CusPicker;
            Control.Focusable = false;
            UpdateBackground(FormPicker);
            UpdatePadding(FormPicker);
            picker = new NumberPicker(Context);
            FormPicker.TextColor = Xamarin.Forms.Color.Black;
            FormPicker.PlaceholderColor = Xamarin.Forms.Color.Gray;
            if (FormPicker.ItemSource != null && FormPicker.ItemSource.Any())
            {
                picker.MaxValue = FormPicker.ItemSource.Count - 1;
                picker.MinValue = 0;
                picker.SetDisplayedValues(FormPicker.ItemSource.ToArray());
                picker.WrapSelectorWheel = false;
                picker.DescendantFocusability = DescendantFocusability.BlockDescendants;
                picker.Value = FormPicker.Selected != string.Empty ? FormPicker.ItemSource.IndexOf(FormPicker.Selected) : 0;
                //picker.Value = 2;
            }

            var layout = new LinearLayout(Context) { Orientation = Orientation.Vertical };
            layout.AddView(picker);

            var builder = new AlertDialog.Builder(Context);
            builder.SetView(layout);

            TextView title = new TextView(Forms.Context);
            title.Text = FormPicker.Title;
            title.SetPadding(10, 20, 10, 10);
            //title.SetBackgroundColor(Android.Graphics.Color.Blue);
            title.Gravity = GravityFlags.Center;
            title.SetTextColor(Android.Graphics.Color.Red);
            title.TextSize = 20;
            builder.SetCustomTitle(title);

            builder.SetNegativeButton(global::Android.Resource.String.Cancel, (s, a) =>
            {
                Control?.ClearFocus();
            });

            builder.SetPositiveButton(global::Android.Resource.String.Ok, (s, a) =>
            {
                if (Element != null)
                {
                    if (FormPicker.ItemSource.Count > 0 && picker.Value >= 0)
                        Control.Text = FormPicker.ItemSource[picker.Value];
                    Control?.ClearFocus();

                    FormPicker.ValueChanged.Invoke(this, new ValueArgs()
                    {
                        SelectedItem = FormPicker.ItemSource[picker.Value],
                        SelectedIndex = picker.Value
                    });
                }
            });


            _dialog = builder.Create();
            _dialog.DismissEvent += (sender, args) =>
            {
                //ElementController?.SetValueFromRenderer(VisualElement.IsFocusedPropertyKey, false);
            };



            //Spinner Dropdown = new Spinner(Forms.Context, SpinnerMode.Dropdown);
            //var items = FormPicker.ItemSource.ToList();
            //var adapter = new ArrayAdapter<string>(Forms.Context, Android.Resource.Layout.SimpleSpinnerItem, items);
            //Dropdown.Adapter = adapter;

            //Dropdown.ItemSelected += (sender, er) =>
            //{
            //     ValueArgs args = new ValueArgs();
            //     args.SelectedItem = items[er.Position];
            //     args.SelectedIndex = er.Position;
            //     FormPicker.ValueChanged(FormPicker, args);
            //};

            Control.Click += (sender, er) =>
            {
                _dialog.Show();
            };
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var entryEx = Element as CusPicker;
            if (e.PropertyName == CusPicker.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusPicker.BorderColorProperty.PropertyName ||
                e.PropertyName == CusPicker.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusPicker.BackgroundColorProperty.PropertyName)
            {
                UpdateBackground(entryEx);
            }
            else if (e.PropertyName == CusPicker.PaddingProperty.PropertyName)
            {
                UpdatePadding(entryEx);
            }
            else if (e.PropertyName == CusPicker.TextProperty.PropertyName)
            {
                Control.Text = FormPicker.Text;
            }
            else if (e.PropertyName == CusPicker.ItemSourceProperty.PropertyName)
            {
                if (FormPicker.ItemSource != null && FormPicker.ItemSource.Any())
                {
                    //picker.MaxValue = FormPicker.ItemSource.Count - 1;
                    //picker.MinValue = 0;
                    picker.SetDisplayedValues(FormPicker.ItemSource.ToArray());
                    //picker.WrapSelectorWheel = false;
                    //picker.DescendantFocusability = DescendantFocusability.BlockDescendants;
                    //picker.Value = 0;
                }
            }
            else if (e.PropertyName == CusPicker.SelectedProperty.PropertyName)
            {
                picker.Value = FormPicker.ItemSource.IndexOf(entryEx.Selected);
            }
        }


        #region Utility methods

        private void UpdateBackground(CusPicker entryEx)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }
            _renderer = new BorderRenderer();
            if (entryEx.InnerBackground != null)
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.InnerBackground, entryEx.BorderWidth, entryEx.BorderRadius);
            }
            else
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.BackgroundColor, entryEx.BorderWidth, entryEx.BorderRadius);
            }

        }

        private void UpdatePadding(CusPicker entryEx)
        {
            Control.SetPadding((int)Forms.Context.ToPixels(entryEx.Padding.Left), (int)Forms.Context.ToPixels(entryEx.Padding.Top), (int)Forms.Context.ToPixels(entryEx.Padding.Right), (int)Forms.Context.ToPixels(entryEx.Padding.Bottom));
        }

        #endregion

    }
}