﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android.Content;
using Android.OS;
using Android.Support.V4.Content;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Platform;
using Xamarin.Forms;

[assembly: Dependency(typeof(Share))]
namespace SiteWorkPermits.Droid.Platform
{
    public class Share : IShare
    {
        private readonly Context _context;
        public Share()
        {
            _context = Android.App.Application.Context;
        }
        
        public Task Show(string title, string message, string filePath)
        {
            var extension = filePath.Substring(filePath.LastIndexOf(".") + 1).ToLower();
            var contentType = string.Empty;
            switch (extension)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;
                case "png":
                    contentType = "image/png";
                    break;
                default:
                    contentType = "application/octetstream";
                    break;
            }

           // string extension = Android.Webkit.MimeTypeMap.GetFileExtensionFromUrl(Android.Net.Uri.FromFile(file).ToString());

            string mimeType = Android.Webkit.MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension);

            //var contentUri = FileProvider.GetUriForFile(Forms.Context.ApplicationContext, "com.xlcatlin.clpbeta.fileProvider", new Java.IO.File(filePath));

            var contentUri = Android.Net.Uri.Parse(filePath);

            var intent = new Intent(Intent.ActionSend);
            intent.SetType("application/pdf");
            if (Build.VERSION.SdkInt > BuildVersionCodes.M)
            {
                intent.SetDataAndType(contentUri,mimeType);
            }
            else
            {
                intent.PutExtra(Intent.ExtraStream, Android.Net.Uri.Parse(contentUri.ToString()));
            }
            intent.AddFlags(ActivityFlags.GrantReadUriPermission);

            var chooserIntent = Intent.CreateChooser(intent, "");
          
            Forms.Context.StartActivity(Intent.CreateChooser(chooserIntent, "Choose App"));
            return Task.FromResult(true);
        }
    }
}