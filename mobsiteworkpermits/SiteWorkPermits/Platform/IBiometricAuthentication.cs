﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiteWorkPermits.Platform
{
   public interface IBiometricAuthentication
    {
        bool isBiometricSuccess();
    }
}
