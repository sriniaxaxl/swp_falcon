﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IImpairmentClassService
    {
        IEnumerable<ImpairmentClassType> GetImpairmentClassType();

        ImpairmentClassType GetImpairmentClassTypeById(int id);

        bool AddImpairmentClassType(ImpairmentClassType impairmentClassType);

        void RemoveAllData();
    }
}
