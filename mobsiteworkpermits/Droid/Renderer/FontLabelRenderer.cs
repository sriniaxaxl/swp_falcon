﻿using System;
using Android.Graphics;
using SiteWorkPermits.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Label), typeof(FontLabelRenderer))]
namespace SiteWorkPermits.Droid.Renderer
{
    public class FontLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            // var label = (TextView)Control; // for example
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Effra_Std_Rg.ttf");  // font name specified here
            Control.Typeface = font;
            Control.SetPadding(0, 0, 0, 10);
            //Control.SetTextColor(Android.Graphics.Color.Black);
        }
    }
}
