﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class FirePreventionMeasuresService:IFirePreventionMeasuresService
    {
        private readonly DatabaseContext _databaseContext;
        public FirePreventionMeasuresService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public  bool AddFirePreventionMeasuresAsync(FirePreventionMeasures firePreventionMeasures)
        {
            try
            {
                var firePreventionMeasure =  _databaseContext.FirePreventionMeasures.Add(firePreventionMeasures);
                 _databaseContext.SaveChanges();
                var isAdded = firePreventionMeasure.State == EntityState.Unchanged;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public  IEnumerable<FirePreventionMeasures> GetFirePreventionMeasuresAsync()
        {
            try
            {
                var firePreventionMeasures =  _databaseContext.FirePreventionMeasures.ToList();
                return firePreventionMeasures;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public FirePreventionMeasures GetFirePreventionMeasuresByIdAsync(int id)
        {
            try
            {
                var scope = _databaseContext.FirePreventionMeasures.Find(id);
                return scope;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool RemoveFirePreventionMeasuresAsync(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateFirePreventionMeasuresAsync(FirePreventionMeasures firePreventionMeasures)
        {
            try
            {
                var firewatchPersonStatus = _databaseContext.FirePreventionMeasures.Update(firePreventionMeasures);
                 _databaseContext.SaveChanges();
                var isUpdated = firewatchPersonStatus.State == EntityState.Unchanged;
                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<FirePreventionMeasures>();
            _databaseContext.FirePreventionMeasures.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
