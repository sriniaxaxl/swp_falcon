﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL
{
    public class ReporterDetailService:IReporterDetailService
    {
        private readonly DatabaseContext _databaseContext;
        public ReporterDetailService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddReporterDetail(ReporterDetail reporterDetail)
        {
            try
            {
                var status = _databaseContext.ReporterDetail.Add(reporterDetail);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<ReporterDetail> GetReportDetails()
        {
            try
            {
                var reporters = _databaseContext.ReporterDetail.ToList();
                return reporters;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ReporterDetail GetReporterDetailById(int id)
        {
            try
            {
                var reporters = _databaseContext.ReporterDetail.Where(p => p.ReporterDetailId == id);
                return reporters.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<ReporterDetail>();           
            _databaseContext.ReporterDetail.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }

        public bool RemoveReporterDetail(int id)
        {
            try
            {
                var scope = _databaseContext.ReporterDetail.Find(id);
                var scopeStatus = _databaseContext.ReporterDetail.Remove(scope);
                _databaseContext.SaveChanges();
                var isDeleted = scopeStatus.State == EntityState.Deleted;
                return isDeleted;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateReporterDetail(ReporterDetail reporterDetail)
        {
            try
            {
                var status = _databaseContext.ReporterDetail.Update(reporterDetail);
                _databaseContext.SaveChanges();
                var isUpdated = status.State == EntityState.Unchanged;
                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
