﻿using System;
using System.Collections.Generic;
namespace SiteWorkPermits.Platform
{
    public interface ISetAlertHotworks
    {
        void SetStartAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime startTime);
        void SetEndAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime);
        void SetGoingToEndAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime);

        void SetFirewatcher1AlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime);
        void SetFirewatcher2AlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime);

        void CancelAlertNotification(int alertId);
    }
}
