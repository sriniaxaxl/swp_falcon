﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Prism.Common;
using Xamarin.Forms;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public partial class ImpairmentsDetailView : ContentView
    {
        NewImpairmentsPageViewModel Model;
        public ImpairmentsDetailView(NewImpairmentsPageViewModel Vm = null)
        {
            InitializeComponent();
            if (Vm != null)
            {
                ImpairmentPicker.Selected = Vm.SelImpairmenType.Name;
                ImpairmentPicker.Text = Vm.SelImpairmenType.Name;
                ImpairmentclassPicker.Selected = Vm.SelImpairmenClass.Name;
                ImpairmentclassPicker.Text = Vm.SelImpairmenClass.Name;
                ReasonForShutDownPicker.Selected = Vm.SelReasonForShutDown.Name;
                ReasonForShutDownPicker.Text = Vm.SelReasonForShutDown.Name;
                StartDatePicker.Text = Vm.StartImpairment.Date != (new DateTime(1, 1, 1)).Date ? CustomControl.Validation.ToCultureDate(Vm.StartImpairment) : string.Empty;
                StartTimePicker.Text = Vm.StartImpairment.Date != (new DateTime(1, 1, 1)).Date ? Helper.TimeFormatSet(Vm.StartImpairment) : string.Empty;
                RestorationDatePicker.Text = Vm.RestorationImpairment.Date != (new DateTime(1, 1, 1)).Date ? CustomControl.Validation.ToCultureDate(Vm.RestorationImpairment) : string.Empty;
                RestorationTimePicker.Text = Vm.RestorationImpairment.Date != (new DateTime(1, 1, 1)).Date ? Helper.TimeFormatSet(Vm.RestorationImpairment) : string.Empty;
                StartTime = Vm.StartImpairment;
                RestorationTime = Vm.RestorationImpairment;
                if (Vm.MajorImpairmentCollection.Where(s => s.Index.Equals(1)).FirstOrDefault().IsChecked)
                {
                    chkspinkler.Checked = true;
                }
                if (Vm.MajorImpairmentCollection.Where(s => s.Index.Equals(2)).FirstOrDefault().IsChecked)
                {
                    chkduration.Checked = true;
                }
                if (Vm.MajorImpairmentCollection.Where(s => s.Index.Equals(3)).FirstOrDefault().IsChecked)
                {
                    chkwater.Checked = true;
                }
                if (Vm.MajorImpairmentCollection.Where(s => s.Index.Equals(4)).FirstOrDefault().IsChecked)
                {
                    chkhotworks.Checked = true;
                }
            }
            else
            {
                RestorationDatePicker.Text = string.Empty;
                StartDatePicker.Text = string.Empty;
            }

            ImpairmentPicker.ValueChanged += ImpairmentTypeSelection;
            ImpairmentclassPicker.ValueChanged += ImpairmentclassSelection;
            ReasonForShutDownPicker.ValueChanged += ReasonForShutDownSelection;
            StartDatePicker.Dateselected += StartDatePicker_Changed_Date;
            StartTimePicker.Timeselected += StartTimePicker_Timeselected;
            RestorationDatePicker.Dateselected += RestorationDatePicker_Dateselected;
            RestorationTimePicker.Timeselected += RestorationTimePicker_Timeselected;
            chkspinkler.Checked_Changed += ChkImapirChanged;
            chkduration.Checked_Changed += ChkImapirChanged;
            chkwater.Checked_Changed += ChkImapirChanged;
            chkhotworks.Checked_Changed += ChkImapirChanged;
        }

        private void ChkImapirChanged(object sender, CustomControl.CheckedArgs e)
        {
            CustomCheckbox customCheckbox = (CustomCheckbox)sender;
            Label lblval = (Label)((CusGrid)customCheckbox.Parent).Children[0];
            Debug.WriteLine(e.Checked);

            if (_selectedClass == 1 && lblval.Text == AppResource.IMP_MAJOR_IMP_3)
            {

                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        if (!customCheckbox.Checked)
                        {
                            customCheckbox.Checked = true;
                        }
                        break;
                }



                customCheckbox.Checked = false;
            }
            else
            {
                if (Model != null)
                {
                    Model.MajorImpairmentCollection.Where(s => s.Name.Equals(lblval.Text)).First().IsChecked = e.Checked;
                    customCheckbox.Checked = e.Checked;
                }
            }
        }


        public static BindableProperty StartTimeProperty = BindableProperty.Create<ImpairmentsDetailView, DateTime>(o => o.StartTime, DateTime.Now, BindingMode.TwoWay, null, null);

        public DateTime StartTime
        {
            get { return (DateTime)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }


        public static BindableProperty RestorationTimeProperty = BindableProperty.Create<ImpairmentsDetailView, DateTime>(o => o.RestorationTime, DateTime.Now, BindingMode.TwoWay, null, null);

        public DateTime RestorationTime
        {
            get { return (DateTime)GetValue(RestorationTimeProperty); }
            set { SetValue(RestorationTimeProperty, value); }
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            var bind = this.BindingContext;

            if (bind != null)
            {
                Model = (NewImpairmentsPageViewModel)bind;
                ImpairmentPicker.ItemSource = Model.ImpairList.ToList();
                ImpairmentclassPicker.ItemSource = Model.ImpairClassList.ToList();
                ReasonForShutDownPicker.ItemSource = Model.ResonShutdownList.ToList();
            }
        }

        #region DropDown Selection
        private void ImpairmentTypeSelection(object sender, CustomControl.ValueArgs e)
        {
            if (e.SelectedItem != null)
            {
                var selitem = Model.ImpairtypeCollection.Where(s => s.Index == e.SelectedIndex+1).First();
                Model.SelImpairmenType = selitem;
                ImpairmentPicker.Text = Model.ImpairtypeCollection.Where(s => s.Index == e.SelectedIndex+1).First().Name;
            }
        }
        private int _selectedClass = 0;
        private void ImpairmentclassSelection(object sender, CustomControl.ValueArgs e)
        {
            if (e.SelectedItem != null)
            {
                var selitem = Model.ImpairClassCollection.Where(s => s.Index == e.SelectedIndex+1).First();
                Model.SelImpairmenClass = selitem;
                if (selitem.Index == 1)
                {
                    _selectedClass = selitem.Index;
                    entireWaterLabel.TextColor = Color.FromHex("#EBEBE4");
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            if (!chkwater.Checked)
                            {
                                chkwater.Checked = true;
                            }
                            break;
                    }
                    chkwater.Checked = false;
                    Model.MajorImpairmentCollection.Where(p => p.Index == 2).FirstOrDefault().IsChecked = false; ;
                }
                else
                {
                    _selectedClass = selitem.Index;
                    entireWaterLabel.TextColor = Color.Black;
                }
                ImpairmentclassPicker.Text = Model.ImpairClassCollection.Where(s => s.Index == e.SelectedIndex+1).First().Name;
            }
        }

        private void ReasonForShutDownSelection(object sender, CustomControl.ValueArgs e)
        {
            if (e.SelectedItem != null)
            {
                var selitem = Model.ReasonShutdownCollection.Where(s => s.Index == e.SelectedIndex+1).First();
                Model.SelReasonForShutDown = selitem;
                ReasonForShutDownPicker.Text = Model.ReasonShutdownCollection.Where(s => s.Index== e.SelectedIndex+1).First().Name;
            }
        }
        #endregion

        private void StartDatePicker_Changed_Date(object sender, CustomControl.DateChangeArgs e)
        {
            var pr = e.Newdate.Date + StartTime.TimeOfDay;
            if (pr <= RestorationTime)
            {
                if (e.Newdate.Date != new DateTime(1, 1, 1) )
                {
                    StartDatePicker.Text =   CustomControl.Validation.ToCultureDate(e.Newdate);
                    var time = StartTime.TimeOfDay;
                    StartTime = e.Newdate.Date + time;
                    Model.StartImpairment = StartTime;
                }
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                curpage.DisplayAlert(string.Empty, "Start Date Should be less than Restoration Date", "Ok");
            }
        }

        private void StartTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            var pr = StartTime = StartTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second); ;
            if (pr <= RestorationTime)
            {
                StartTimePicker.Text = Helper.TimeFormatSet(e.NewTime);
                StartTime = StartTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
                Model.StartImpairment = StartTime;
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                curpage.DisplayAlert(string.Empty, "Start Date Should be less than Restoration Date", "Ok");
            }
        }

        private void RestorationDatePicker_Dateselected(object sender, CustomControl.DateChangeArgs e)
        {
            var pr = e.Newdate.Date + RestorationTime.TimeOfDay;
            if (StartTime <= pr)
            {
                if (e.Newdate.Date != new DateTime(1, 1, 1))
                {
                    RestorationDatePicker.Text = CustomControl.Validation.ToCultureDate(e.Newdate);
                    var time = RestorationTime.TimeOfDay;
                    RestorationTime = e.Newdate.Date + time;
                    Model.RestorationImpairment = RestorationTime;
                }
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                curpage.DisplayAlert(string.Empty, "Restoration Date Should be greater than Start Date", "Ok");
            }
        }

        private void RestorationTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            var pr = RestorationTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
            if (StartTime <= pr)
            {
                RestorationTimePicker.Text = Helper.TimeFormatSet(e.NewTime);
                RestorationTime = RestorationTime.Date + new TimeSpan(e.NewTime.Hour, e.NewTime.Minute, e.NewTime.Second);
                Model.RestorationImpairment = RestorationTime;
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                curpage.DisplayAlert(string.Empty, "Restoration Date Should be greater than Start Date", "Ok");
            }
        }
    }
}
