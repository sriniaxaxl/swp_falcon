﻿using System;
using System.ComponentModel;
using CoreGraphics;
using CustomControl;
using CustomControl.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusTimePicker), typeof(CustomTimePickerRenderer))]
namespace CustomControl.iOS.Renderer
{
    public class CustomTimePickerRenderer : EntryRenderer
    {
        CusTimePicker FormTimePicker;
        UIDatePicker timePicker;
        UIToolbar toolbar;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;

            FormTimePicker = Element as CusTimePicker;
            if (FormTimePicker.Timeselected == null)
            {
                FormTimePicker.Timeselected += FormTimePicker_Timeselected;
            }
            SetBackgroud();
            SetBorder();
            SetPadding();
            Control.BorderStyle = UITextBorderStyle.None;
            Control.ClipsToBounds = true;
            //Control.AddGestureRecognizer(new UITapGestureRecognizer(HandleAction));

            //Control.EditingDidBegin +=(sender, er) => 
            //{
            //    FormTimePicker.Clicked.Invoke(FormTimePicker, new EventArgs());
            //};

            timePicker = new UIDatePicker();
            timePicker.Mode = UIDatePickerMode.Time;
            if(FormTimePicker.MinimumDate != null)
            {
                SetMinimumtime(Convert.ToDateTime(FormTimePicker.MinimumDate));
            }
            if (FormTimePicker.MaximumDate != null)
            {
                SetMaximumtime(Convert.ToDateTime(FormTimePicker.MaximumDate));
            }
            //timePicker.MinimumDate = FormTimePicker.MinimumDate.ToNSDate();
            //timePicker.MaximumDate = FormTimePicker.MaximumDate.ToNSDate();
            //Setup the toolbar
            toolbar = new UIToolbar();
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.BackgroundColor = UIColor.Blue;

            toolbar.Translucent = true;
            toolbar.SizeToFit();
            UIBarButtonItem CancelButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Done, (s, er) =>
            {
                Control.ResignFirstResponder();
            });

            UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done, (s, er) =>
            {
                Control.ResignFirstResponder();
                TimeChangeArgs args = new TimeChangeArgs();
                args.NewTime =((DateTime) timePicker.Date).ToLocalTime();
                FormTimePicker.Timeselected.Invoke(FormTimePicker, args);
                //Control.Text = datePicker.Date.ToDateTime().ToString("dd/MM/yyyy");               
            });           
            UIBarButtonItem flexiblespace = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
            CancelButton.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            doneButton.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            toolbar.SetItems(new UIBarButtonItem[] { CancelButton, flexiblespace, doneButton }, true);
            //Control.InputView = null;
            Control.InputView = timePicker;
            //Display the toolbar over the pickers
            Control.InputAccessoryView = toolbar;
        }
             
        void HandleAction()
        {
            if(string.IsNullOrEmpty(FormTimePicker.Text))
            {
                FormTimePicker.Clicked.Invoke(FormTimePicker, new EventArgs());
            }
        }


        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CusTimePicker.TextProperty.PropertyName)
            {
                Control.Text = FormTimePicker.Text;
            }
            else if (e.PropertyName == CusTimePicker.BorderWidthProperty.PropertyName ||
                     e.PropertyName == CusTimePicker.BorderColorProperty.PropertyName ||
                     e.PropertyName == CusTimePicker.BorderRadiusProperty.PropertyName ||
                     e.PropertyName == CusTimePicker.BackgroundColorProperty.PropertyName ||
                     e.PropertyName == CusTimePicker.InnerBackgroundColorProperty.PropertyName)
            {
                SetBackgroud();
                SetBorder();
            }
            else if (e.PropertyName == CusTimePicker.MinimumDateProperty.PropertyName)
            {
                SetMinimumtime(Convert.ToDateTime(FormTimePicker.MinimumDate));
            }
            else if (e.PropertyName == CusTimePicker.MaximumDateProperty.PropertyName)
            {
                SetMaximumtime(Convert.ToDateTime(FormTimePicker.MaximumDate));
            }
            else if (e.PropertyName == CusTimePicker.IsEnabledProperty.PropertyName)
            {
                Control.Enabled = FormTimePicker.IsEnabled;
            }
            //else if (e.PropertyName == CusTimePicker.IsOpenProperty.PropertyName)
            //{
            //    if(FormTimePicker.IsOpen)
            //    {                                        
                    
            //    }
            //    else
            //    {
            //        Control.ResignFirstResponder();
            //    }
            //}
            //else if (e.PropertyName == CusTimePicker.MinimumDateProperty.PropertyName)
            //{
            //    TimeZone localZone = TimeZone.CurrentTimeZone;
            //    TimeSpan currentOffset =localZone.GetUtcOffset(FormTimePicker.MinimumDate);
            //    FormTimePicker.MinimumDate = FormTimePicker.MinimumDate.AddHours(currentOffset.TotalHours);
            //    timePicker.MinimumDate = FormTimePicker.MinimumDate.ToNSDate();
            //}
            //else if (e.PropertyName == CusTimePicker.MaximumDateProperty.PropertyName)
            //{
            //    timePicker.MinimumDate = FormTimePicker.MaximumDate.ToNSDate();
            //}
        }


        void SetMinimumtime(DateTime dt)
        {
            TimeZone timeZone = TimeZone.CurrentTimeZone;
            var diff = timeZone.GetUtcOffset(DateTime.Now).TotalHours;
            var pr = dt.AddHours(-1 * diff);
            timePicker.MinimumDate = pr.ToNSDate();
        }

        void SetMaximumtime(DateTime dt)
        {
            TimeZone timeZone = TimeZone.CurrentTimeZone;
            var diff = timeZone.GetUtcOffset(DateTime.Now).TotalHours;
            var pr = dt.AddHours(-1 * diff);
            timePicker.MaximumDate = pr.ToNSDate();
        }

        void FormTimePicker_Timeselected(object sender, TimeChangeArgs e)
        {
            if (e.NewTime != null)
            {
                TimeZone timeZone = TimeZone.CurrentTimeZone;
                var diff = timeZone.GetUtcOffset(e.NewTime).TotalHours;
                var pr = e.NewTime.AddHours(-1 * diff);
                FormTimePicker.Text = pr.ToString("hh:mm tt");
            }
        }


        void SetBackgroud()
        {
            if (FormTimePicker.InnerBackground != Color.Transparent)
            {
                Control.BackgroundColor = FormTimePicker.InnerBackground.ToUIColor();
            }
            else
            {
                Control.BackgroundColor = FormTimePicker.BackgroundColor.ToUIColor();
            }
        }

        void SetBorder()
        {
            Control.Layer.BorderColor = FormTimePicker.BorderColor.ToCGColor();
            Control.Layer.BorderWidth = FormTimePicker.BorderWidth;
            Control.Layer.CornerRadius = FormTimePicker.BorderRadius;
        }

        void SetPadding()
        {
            if (FormTimePicker.Padding.Left != 0)
            {
                Control.LeftView = new UIView(new CGRect(0, 0, FormTimePicker.Padding.Left, 0));
                Control.RightView = new UIView(new CGRect(0, 0, FormTimePicker.Padding.Right, 0));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightViewMode = UITextFieldViewMode.Always;
            }
        }
    }
}
