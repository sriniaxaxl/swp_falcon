﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class Parrallex_Test : ContentPage,ISwipeCallBack
    {
        public Parrallex_Test()
        {
            InitializeComponent();
            SwipeListener swipeListener = new SwipeListener(this.GdRectangle, this);
        }

        public void onBottomSwipe(View view)
        {
            
        }

        public void onLeftSwipe(View view)
        {
            
        }

        public void onNothingSwiped(View view)
        {
            
        }

        public void onRightSwipe(View view)
        {
            
        }

        public void onTopSwipe(View view)
        {
            
        }
    }
}
