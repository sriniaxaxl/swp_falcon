﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits.ViewPage
{
    public partial class DownloadPage : ContentPage
    {
        public DownloadPage()
        {
            InitializeComponent();    
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }


        void ReadioSel_Tapped(object sender, System.EventArgs e)
        {
            var md = (DownloadViewModel)this.BindingContext;
            CusGrid Selgd = (CusGrid)sender;
            Label sellbl = (Label)Selgd.Children[1];
            if (sellbl.Text == "A4")
            {
                md.SelFormat = "A4";
                A4Img.Source = "radio_on.png";
                UsLetter.Source = "radio_off.png";
            }
            else
            {
                md.SelFormat = "USLETTER";
                A4Img.Source = "radio_off.png";
                UsLetter.Source = "radio_on.png";
            }

           // Debug.WriteLine(md.SelFormat);
        }
    }
}