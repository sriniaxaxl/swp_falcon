﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits
{
    public partial class DashBoardPage : ContentPage
    {
        ObservableCollection<TabInfo> TabCollection = new ObservableCollection<TabInfo>();
        ObservableCollection<ModelPermits> PermitCollection = new ObservableCollection<ModelPermits>();
        HomePageViewModel viewModel;
        public DashBoardPage()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {

            }
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            TabCollection.Add(new TabInfo { Title = "HOT WORK PERMITS", IsSelected = true });
            TabCollection.Add(new TabInfo { Title = "RAISED IMPAIRMENTS", IsSelected = false });
            //Spworktabs.WidthRequest = App.ScreenWidth;
            foreach (var d in TabCollection)
            {
                TabView tab = new TabView();
                tab.HorizontalOptions = LayoutOptions.FillAndExpand;
                tab.Title = d.Title;
                tab.TabColor = d.IsSelected ? Color.FromHex("#00AFAD") : Color.Transparent;
                tab.GestureRecognizers.Add(new TapGestureRecognizer(HandleAction));
                //Spworktabs.Children.Add(tab);
            }

            ImpairmentView.RestoreImpairmentEvent += ImpairmentView_RestoreImpairmentEvent;
            ImpairmentView.CancelImpairmentEvent += ImpairmentView_CancelImpairmentEvent;
            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "DashRestorePopup", (sender) =>
            {
                //Model = (ImpairmentsViewModel)this.BindingContext;
                //Model.GetImpairment();
               // viewModel.GetImpairment();
                GdImpairmentview.IsVisible = true;
            });
        }

        private void ImpairmentView_RestoreImpairmentEvent(object sender, EventArgs e)
        {
            GdImpairmentview.IsVisible = false;
        }

        private void ImpairmentView_CancelImpairmentEvent(object sender, EventArgs e)
        {
            GdImpairmentview.IsVisible = false;
        }

        private void HandleAction(View arg1, object arg2)
        {
            Deselectview();
            TabView selv = (TabView)arg1;
            Selectview(selv);
        }

        private void Deselectview()
        {
            var presel = TabCollection.Where(p => p.IsSelected).First();
            var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).First());
            //TabView tab = (TabView)Spworktabs.Children[Selind];
            //tab.TabColor = Color.Transparent;
            //TabCollection.Where(p => p.IsSelected).First().IsSelected = false;
        }

        private void Selectview(TabView v)
        {
            TabView selv = v;
            var seltext = selv.Title;
            selv.TabColor = Color.FromHex("#00AFAD");
            var seltab = TabCollection.Where(s => s.Title == seltext).First();
            seltab.IsSelected = true;
            //Pageview.Content = null;
            //Pageview.Content = TabCollection.Where(s => s.IsSelected).First().view.Content;
        }

        private void ImpairPopup_Tapped(object sender, System.EventArgs e)
        {
            GdImpairmentview.IsVisible = false;
        }

        void Create_HotWork_Tapped(object sender, System.EventArgs e)
        {
            App.curhomepage.Detail = new NewHotWorksPage();
        }

        private void Handle_Tapped(object sender, System.EventArgs e)
        {
            App.curhomepage.IsPresented = true;
        }

        protected override void OnAppearing()
        {
            viewModel = (HomePageViewModel)BindingContext;
            viewModel.RefreshListEvent += ViewModel_RefreshListEvent;
            TabCollection.Where(s => s.Title == "HOT WORK PERMITS").First().view = new HotWorksListview(viewModel.ActivePermitCollection, false);
            TabCollection.Where(s => s.Title == "RAISED IMPAIRMENTS").First().view = new ImpairmentsListView(viewModel.ActiveImpairmentCollection, false);
          //  Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
            base.OnAppearing();
        }

        void ViewModel_RefreshListEvent()
        {
            TabCollection.Where(s => s.Title == "HOT WORK PERMITS").First().view = new HotWorksListview(viewModel.ActivePermitCollection, false);
            TabCollection.Where(s => s.Title == "RAISED IMPAIRMENTS").First().view = new ImpairmentsListView(viewModel.ActiveImpairmentCollection, false);
          //  Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
        }
    }
}
