﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.Model;
using SiteWorkPermits.Model.SyncHotWorks;
using Plugin.Connectivity;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.Model.SyncImpairements;
using CustomControl;
using SiteWorkPermits.Platform;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SiteWorkPermits.DAL.sqlIntegration;
using SQLiteNetExtensions.Extensions;
using static CustomControl.Validation;
using System.Windows.Input;
using SQLite;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public class ImpairmentsViewModel : BindableBase, INavigatedAware
    {
        private IHotworkPermitService _hotworkPermitService;
        IImpairmentTypeService _impairmentTypeService;
        IImpairmentClassService _impairmentClassService;
        private IImpairmentService _impairmentService;
        IScopeService _scopeService;
        SQLiteConnection connection;
        public IPageDialogService _pageDialog;
        IWorkOrdersService _workOrdersService;
        IAuthorizationService _authorizationService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        IHighHazardAreasService _highHazardAreasService;
        IShutDownReasonService _shutDownReasonService;
        IIncUniqueid _incUniqueid;
        public INavigationService _navigationService;
        private ObservableCollection<ModelWorkPermit> _workPermits;
        // public DelegateCommand OnSyncCommand { get; set; }
        private ObservableCollection<ModelPermits> _ActiveFormsCollection;
        public ObservableCollection<ModelPermits> ActiveFormsCollection
        {
            get { return _ActiveFormsCollection; }
            set { SetProperty(ref _ActiveFormsCollection, value); }
        }

        private ObservableCollection<ModelPermits> _DraftFormsCollection;
        public ObservableCollection<ModelPermits> DraftFormsCollection
        {
            get { return _DraftFormsCollection; }
            set { SetProperty(ref _DraftFormsCollection, value); }
        }

        private ObservableCollection<ModelPermits> _UpcomingFormsCollection;
        public ObservableCollection<ModelPermits> UpcomingFormsCollection
        {
            get { return _UpcomingFormsCollection; }
            set { SetProperty(ref _UpcomingFormsCollection, value); }
        }

        private string _impType;
        public string ImpType
        {
            get { return _impType; }
            set { SetProperty(ref _impType, value); }
        }

        private string _impClass;
        public string ImpClass
        {
            get { return _impClass; }
            set { SetProperty(ref _impClass, value); }
        }

        private string _ImpStartDate;
        public string ImpStartDate
        {
            get { return _ImpStartDate; }
            set { SetProperty(ref _ImpStartDate, value); }
        }

        private DateTime _closedDate;
        public DateTime ClosedDate
        {
            get { return _closedDate; }
            set { SetProperty(ref _closedDate, value); }
        }

        private bool _showCloseAuthorizerName;
        public bool ShowCloseAuthorizerName
        {
            get { return _showCloseAuthorizerName; }
            set { SetProperty(ref _showCloseAuthorizerName, value); }
        }

        private ObservableCollection<ModelPermits> _ExpiredFormsCollection;
        public ObservableCollection<ModelPermits> ExpiredFormsCollection
        {
            get { return _ExpiredFormsCollection; }
            set { SetProperty(ref _ExpiredFormsCollection, value); }
        }
        public DelegateCommand<string> OnNavigateCommand { get; set; }


        public ImpairmentsViewModel(IHotworkPermitService hotworkPermitService,
                                     INavigationService navigationService,
                                     IPageDialogService pageDialog,
                                    IWorkOrdersService workOrdersService,
                                 IScopeService scopeService, IAuthorizationService authorizationService,
                                 IFirePreventionMeasuresService firePreventionMeasuresService,
                                 IPrecautionsService precautionsService,
                                    IIncUniqueid incUniqueid,
                                    IShutDownReasonService shutDownReasonService,
                                     IImpairmentService impairmentService,
                                 IFirewatchPersonsService firewatchPersonsService,
                                 IHighHazardAreasService highHazardAreasService,
                                    IImpairmentTypeService impairmentTypeService,
                                    IImpairmentClassService impairmentClassService)
        {
            OnNavigateCommand = new DelegateCommand<string>(NavigateAsync);
            _hotworkPermitService = hotworkPermitService;
            _impairmentService = impairmentService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _scopeService = scopeService;
            _incUniqueid = incUniqueid;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _highHazardAreasService = highHazardAreasService;
            _authorizationService = authorizationService;
            _impairmentClassService = impairmentClassService;
            _impairmentTypeService = impairmentTypeService;
            _navigationService = navigationService;
            _shutDownReasonService = shutDownReasonService;
            _pageDialog = pageDialog;
            ShowSyncIcon = true;
            //   OnSyncCommand = new DelegateCommand(async () => await SyncData());
            SetHotworksData();
            SelectedDate = DateTime.Now;
            SelectedTime = SelectedDate.TimeOfDay;
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            ShowCloseAuthorizerName = false;
            ClosureText = AppResource.IMPAIRMENT_CLOSE_TEXT;
            ClosureTypeBtnText = AppResource.CLOSE_IMPAIRMENT;
            CloseType = AppResource.CLOSE_IMPAIRMENT;
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("IMPAIRMENTS DASHBOARD");
            PopupMsg = AppResource.DASHBOARD_SYNC_MSG;
            activeTabColor = Color.FromHex("#00AFAD");
        }


        public ICommand OnSyncCommand
        {
            get
            {
                return new Command(async (x) =>
                {
                    try
                    {
                        await SyncData();
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
        }


        private string _ClosureText;
        public string ClosureText
        {
            get { return _ClosureText; }
            set { SetProperty(ref _ClosureText, value); }
        }

        private string _CloseType;
        public string CloseType
        {
            get { return _CloseType; }
            set { SetProperty(ref _CloseType, value); }
        }

        private string _closureTypeBtnText;
        public string ClosureTypeBtnText
        {
            get { return _closureTypeBtnText; }
            set { SetProperty(ref _closureTypeBtnText, value); }
        }

        private string _impReason;
        public string ImpReason
        {
            get { return _impReason; }
            set { SetProperty(ref _impReason, value); }
        }

        public void GetImpairment()
        {
            try
            {
                var impairment = _impairmentService.GetImpairmentById(App.ImpairementEditableId);
                ImpType = _impairmentTypeService.GetImpairmentTypeById(impairment.ImpairmentTypeId).TypeDescription;
                ImpClass = _impairmentClassService.GetImpairmentClassTypeById(impairment.ImpairmentClassId).ClassDescription;
                ImpReason = _shutDownReasonService.GetShutDownReasonById(impairment.ShutDownReasonId).ClassDescription;
                ImpStartDate = impairment.StartDateTime;
                //  ClosedDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
                _pageDialog.DisplayAlertAsync(string.Empty, AppResource.SOMETHING_WENT_WRONG, AppResource.OK_LABEL);
            }
        }

        private string _PopupMsg;
        public string PopupMsg
        {
            get { return _PopupMsg; }
            set { SetProperty(ref _PopupMsg, value); }
        }

        private string _elementCategory_sql;
        public string ElementCategory_sql
        {
            get { return _elementCategory_sql; }
            set { SetProperty(ref _elementCategory_sql, value); }
        }

        private string _elementDescription_Sql;
        public string ElementDescription_Sql
        {
            get { return _elementDescription_Sql; }
            set { SetProperty(ref _elementDescription_Sql, value); }
        }

        private string _elementStartDate_Sql;
        public string ElementStartDate_Sql
        {
            get { return _elementStartDate_Sql; }
            set { SetProperty(ref _elementStartDate_Sql, value); }
        }

        private bool _showClosePopup;
        public bool showClosePopup
        {
            get { return _showClosePopup; }
            set { SetProperty(ref _showClosePopup, value); }
        }

        private TimeSpan _selectedTime;
        public TimeSpan SelectedTime
        {
            get { return _selectedTime; }
            set { SetProperty(ref _selectedTime, value); }
        }

        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set { SetProperty(ref _selectedDate, value); }
        }

        Impairment_Sql _impairment_Sql;
        public ICommand OnImpCloseCommand
        {
            get
            {
                return new Command(async (x) =>
                {

                    try
                    {

                        var element = (x as CustomControl.Validation.ListViewElement);


                        if (element.ElementStatus.ToLower() == "draft" || element.ElementStatus.ToLower() == "closed")
                        {
                            App.isImpairementEditorDuplicate = "Duplicate";
                            var impairment = EditImpairmentByID((x as ListViewElement).ElementId);
                            var navigationParams = new NavigationParameters();
                            navigationParams.Add("impairmentModel", impairment);
                            await _navigationService.NavigateAsync("NewImpairmentPage", navigationParams, null, false);
                        }
                        else
                        {
                            _impairment_Sql = connection.GetWithChildren<Impairment_Sql>(element.ElementId, true);
                            var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == _impairment_Sql.impairmentTypeId).FirstOrDefault().TypeDescription;
                            var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == _impairment_Sql.impairmentClassId).FirstOrDefault().ClassDescription;
                            var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == _impairment_Sql.shutDownReasonId).FirstOrDefault().ClassDescription;
                            ElementCategory_sql = impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason;
                            ElementDescription_Sql = _impairment_Sql.impairmentDescription;
                            ElementStartDate_Sql = _impairment_Sql.startDateTime;
                            showClosePopup = true;
                        }


                    }
                    catch (Exception ex)
                    {
                        showClosePopup = false;
                    }
                });
            }
        }

        
        public ICommand CloseHotworkCommand
        {
            get
            {
                return new Command(async (x) =>
                {

                    try
                    {
                        _impairment_Sql.isEdited = true;
                        _impairment_Sql.impairmentCloseDateTime = SelectedDate.Add(SelectedTime).ToString();
                        _impairment_Sql.workFlowStatus = "CLOSED";
                        connection.UpdateWithChildren(_impairment_Sql);

                        SetHotworksData();

                        showClosePopup = false;
                        await IsCloseEmailSent(_impairment_Sql);
                    }
                    catch (Exception ex)
                    {
                        showClosePopup = false;
                    }
                });
            }
        }

        public ICommand CancelHotworkCommand
        {
            get
            {
                return new Command((x) =>
                {
                    try
                    {
                        showClosePopup = false;
                    }
                    catch (Exception ex) { }
                });
            }
        }

        public ICommand OnElementSelectEdit
        {
            get
            {
                return new Command(async (x) =>
                {
                    App.isImpairementEditorDuplicate = "";
                    var element = x as ListViewElement;
                    if (element.ElementStatus.ToLower() == "closed")
                    {
                        var impairment = EditImpairmentByID(element.ElementId);
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("impairmentArchieveModel", impairment);
                        await _navigationService.NavigateAsync("NewImpairmentPage", navigationParams, null, false);

                    }
                    else
                    {

                        App.isImpairementEditorDuplicate = "Edit";
                        var impairment = EditImpairmentByID(element.ElementId);
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("impairmentModel", impairment);
                        await _navigationService.NavigateAsync("NewImpairmentPage", navigationParams, null, false);
                    }

                });
            }
        }








        private int impHandheldId;
        //public async void CloseImpairment()
        //{
        //    if (ClosedDate == new DateTime(1, 1, 1))
        //    {
        //        await Task.Yield();
        //        await _pageDialog.DisplayAlertAsync("", "Close Date cannot be blank", "ok");
        //        return;
        //    }
        //    else
        //    {
        //        var impairment = _impairmentService.GetImpairmentById(App.ImpairementEditableId);

        //        if (Convert.ToDateTime(impairment.StartDateTime).CompareTo(ClosedDate) > 0)
        //        {
        //            await _pageDialog.DisplayAlertAsync(string.Empty, "Close Date Should be greater than Start Date", "Ok");
        //        }
        //        else
        //        {

        //            impHandheldId = impairment.ImpairmentMobileId;
        //            PopupMsg = "Closing and Submitting Impairment..";
        //            impairment.CloseTime = ClosedDate.ToString();
        //            impairment.WorkFlowStatus = HWStatus.CLOSED;
        //            impairment.IsEdited = true;
        //            _impairmentService.UpdateImpairment(impairment);
        //            var closedSuccessfully = await IsCloseEmailSent(impairment);
        //            if (closedSuccessfully)
        //            {
        //                await Task.Yield();
        //                await _pageDialog.DisplayAlertAsync("", "Impairment successfully closed and" +
        //                                                    " an email has been sent to" +
        //                                                    " RSVP_Americas@axaxl.com", "ok");
        //                SetHotworksData();
        //                RefreshImpListEvent.Invoke();
        //            }
        //            else
        //            {
        //                await Task.Yield();
        //                await _pageDialog.DisplayAlertAsync(string.Empty, "Impairment not closed, please try again", "ok");
        //            }
        //        }
        //        Device.BeginInvokeOnMainThread(() =>
        //        {
        //            ShowActivityLoader = false;
        //        });
        //    }
        //}

        private async Task<SyncImpairements> GetSyncImp(Impairment impairement)
        {
            SyncImpairements serverImpairement = new SyncImpairements();
            serverImpairement.ImpairmentMobileId = impairement.ImpairmentMobileId;
            serverImpairement.IsParent = false;
            serverImpairement.ParentImpairmentId = 0;
            if (impairement.IsDeleted)
            {
                serverImpairement.IsDeleted = true;
            }
            else
            {
                serverImpairement.IsDeleted = false;
            }

            serverImpairement.Id = impairement.ImpairmentServerId;
            serverImpairement.WorkFlowStatus = impairement.WorkFlowStatus + "";
            serverImpairement.ImpairmentTypeId = impairement.ImpairmentTypeId;
            serverImpairement.ImpairmentClassId = impairement.ImpairmentClassId;
            serverImpairement.ShutDownReasonId = impairement.ShutDownReasonId;
            serverImpairement.ImpairmentDescription = impairement.ImpairmentDescription;

            serverImpairement.StartDateTime = Convert.ToDateTime(impairement.StartDateTime);
            serverImpairement.EndDateTime = Convert.ToDateTime(impairement.EndDateTime);
            serverImpairement.IsDeleted = impairement.IsDeleted;
            serverImpairement.ReporterDetail = new Model.SyncImpairements.ReporterDetail
            {
                Name = impairement.ReporterDetail.Name,
                Email = impairement.ReporterDetail.Email,
                Phone = impairement.ReporterDetail.Phone,
            };

            List<ImpairmentPrecautionList> impairmentPrecautionLists = new List<ImpairmentPrecautionList>();

            foreach (ImpairmentPrecautionTaken impairmentPreTaken in impairement.ImpairmentPrecautionTakens)
            {
                impairmentPrecautionLists.Add(new ImpairmentPrecautionList
                {
                    ImpairmentPrecautionMasterId = impairmentPreTaken.ImpairmentPrecautionId,
                    OtherDescription = impairmentPreTaken.OtherDescription
                });
            }
            serverImpairement.ImpairmentPrecautionList = impairmentPrecautionLists;

            List<int> ImpairmentMeasureMasterIdLists = new List<int>();

            foreach (ImpairmentImpairmentMeasureMaster impairmentImpairmentMeasureMaster in impairement.ImpairmentImpairmentMeasureMasters)
            {
                ImpairmentMeasureMasterIdLists.Add(impairmentImpairmentMeasureMaster.ImpairmentMeasureMasterId);
            }

            serverImpairement.ImpairmentMeasureMasterIdList = ImpairmentMeasureMasterIdLists;
            return serverImpairement;
        }

        private async Task<bool> IsCloseEmailSent(Impairment_Sql impairment)
        {

            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });
                try
                {
                    var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                    analyticsService.LogEvent("", "Imapirment Submit", "Impairment Submit");
                    string impguid = string.Empty;
                    int impserverid = 0;
                    if (impairment.server_id == 0)
                    {
                        var impairements = CustomControl.Validation.GetServerImpaierment(impairment);
                        //    impairements.Result.CloseDateTime = Convert.ToDateTime(impairment.impairmentCloseDateTime);
                        connection.UpdateWithChildren(impairment);
                        var createImpResponse = await RestApiHelper<CreateIMPResponse>.CreateImp_Post(impairements.Result);

                        if (createImpResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            return false;
                        }
                        else if (createImpResponse.Data != null)
                        {
                            impguid = createImpResponse.Data.GUID;
                            impserverid = createImpResponse.Data.Id;
                        }
                        else if (createImpResponse.Status != null)
                        {
                            if (createImpResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", createImpResponse.Status.StatusMessage, "ok");
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                // Xamarin.Forms.Application.Current.Properties.Clear();

                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                return false;
                            }
                        }

                        impairment.server_id = impserverid;
                        impairment.impairmentGUID = impguid;
                        //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                        //    var connection = hotworkDatabase.GetdbConnection();
                        connection.UpdateWithChildren(impairment);
                    }
                    else
                    {
                        connection.UpdateWithChildren(impairment);
                        await IsSyncDone();
                        impguid = impairment.impairmentGUID;
                    }

                    var isEmailSentResponse = await RestApiHelper<EmailHWResponse>.SendEmailImp_Post(impguid, string.Empty, "USLETTER");

                    //bool isEmailSent;
                    if (isEmailSentResponse == null)
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                        return false;
                    }
                    else if (isEmailSentResponse.Status != null)
                    {
                        if (isEmailSentResponse.Status.StatusCode == 401)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", isEmailSentResponse.Status.StatusMessage, AppResource.OK_LABEL);
                            Xamarin.Forms.Application.Current.Properties.Clear();
                            await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                            ShowActivityLoader = false;
                            return false;
                        }
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    if (isEmailSentResponse.Data.Value)
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.IMP_SUBMITTED_SUCCESS, AppResource.OK_LABEL);
                        // await _navigationService.NavigateAsync("HomePage");
                        return true;
                    }
                    else
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                    }

                }
                catch (Exception e)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    Console.WriteLine("IOException source: {0}", e.Source);
                    return false;
                }
            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                return false;
            }
            return false;
        }

        private ObservableCollection<ListViewElement> _listViewElements;

        public ObservableCollection<ListViewElement> ListViewElements
        {
            get { return _listViewElements; }
            set { SetProperty(ref _listViewElements, value); }
        }
        private ObservableCollection<ListViewElement> ListViewElements_Temp;


        private Color _activeTabColor;
        public Color activeTabColor
        {
            get { return _activeTabColor; }
            set { SetProperty(ref _activeTabColor, value); }
        }

        private Color _closedTabColor;
        public Color closedTabColor
        {
            get { return _closedTabColor; }
            set { SetProperty(ref _closedTabColor, value); }
        }

        private Color _upcomingTabColor;
        public Color upcomingTabColor
        {
            get { return _upcomingTabColor; }
            set { SetProperty(ref _upcomingTabColor, value); }
        }

        private Color _expiredTabColor;
        public Color expiredTabColor
        {
            get { return _expiredTabColor; }
            set { SetProperty(ref _expiredTabColor, value); }
        }

        private Color _draftTabColor;
        public Color draftTabColor
        {
            get { return _draftTabColor; }
            set { SetProperty(ref _draftTabColor, value); }
        }


        private bool _showNoIMPLabel;
        public bool showNoIMPLabel
        {
            get { return _showNoIMPLabel; }
            set { SetProperty(ref _showNoIMPLabel, value); }
        }

        private string _noIMPString;
        public string noIMPString
        {
            get { return _noIMPString; }
            set { SetProperty(ref _noIMPString, value); }
        }

        private string selectedTab = string.Empty;
        public ICommand OnTabChangedCommand
        {
            get
            {
                return new Command<string>((x) =>
                {
                    try
                    {
                        selectedTab = x;
                        ChangeTabColor(x);
                        ListViewElements = ListViewElements_Temp;
                        var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals(x.ToLower()));
                        ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                        if (ListViewElements.Count > 0)
                        {
                            showNoIMPLabel = false;
                            noIMPString = "";
                        }
                        else
                        {
                            showNoIMPLabel = true;
                            noIMPString = x.ToUpper();
                        }
                    }
                    catch (Exception ex) { }
                });
            }
        }

        private void ChangeTabColor(string x)
        {
            switch (x.ToLower())
            {
                case "active":
                    activeTabColor = Color.FromHex("#00AFAD");
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.Transparent;
                    expiredTabColor = Color.Transparent;
                    closedTabColor = Color.Transparent;
                    break;
                case "draft":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.FromHex("#00AFAD");
                    upcomingTabColor = Color.Transparent;
                    expiredTabColor = Color.Transparent;
                    closedTabColor = Color.Transparent;
                    break;
                case "upcoming":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.FromHex("00AFAD");
                    expiredTabColor = Color.Transparent;
                    closedTabColor = Color.Transparent;
                    break;
                case "closed":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.Transparent;
                    closedTabColor = Color.FromHex("00AFAD");
                    expiredTabColor = Color.Transparent;
                    break;
                case "expired":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.Transparent;
                    closedTabColor = Color.Transparent;
                    expiredTabColor = Color.FromHex("00AFAD");
                    break;
            }
        }

        private async void SetHotworksData()
        {
            try
            {
                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                connection = hotworkDatabase.GetdbConnection();
                var impairment_sql_List = connection.GetAllWithChildren<Impairment_Sql>().ToList();
                _workPermits = new ObservableCollection<ModelWorkPermit>();
                ListViewElements = new ObservableCollection<ListViewElement>();


                if (impairment_sql_List.Count > 0)
                {

                    foreach (Impairment_Sql impairment_Sql in impairment_sql_List)
                    {

                        var impairmentType = connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == impairment_Sql.impairmentTypeId).FirstOrDefault().TypeDescription;
                        var impairmentClass = connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == impairment_Sql.impairmentClassId).FirstOrDefault().ClassDescription;
                        var impairmentShutdownReason = connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == impairment_Sql.shutDownReasonId).FirstOrDefault().ClassDescription;


                        if (!impairment_Sql.isDeleted)
                        {
                            ReporterDetail_sql reporter;
                            try
                            {
                                reporter = connection.GetWithChildren<ReporterDetail_sql>(impairment_Sql.reporterDetail_sql_id, recursive: true);
                            }
                            catch (Exception ex)
                            {
                                reporter = connection.GetWithChildren<ReporterDetail_sql>(impairment_Sql.reporterDetail_Sql.id, recursive: true);
                            }

                            ListViewElements.Add(new ListViewElement
                            {
                                ElementId = impairment_Sql.id,
                                ElementCategory = impairment_Sql.server_id == 0 ? impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason : "#" + impairment_Sql.server_id + "\n" + impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason,
                                ElementDescription = impairment_Sql.impairmentDescription,
                                ElementEndDate = impairment_Sql.endDateTime,
                                ElementStartDate = impairment_Sql.startDateTime,
                                ElementStatus = impairment_Sql.workFlowStatus,
                                ElementEvent = CustomControl.Validation.GetElementEventName(impairment_Sql.workFlowStatus),
                                ElementType = "IMPAIRMENTS"
                            });
                            ListViewElements_Temp = new ObservableCollection<ListViewElement>(ListViewElements);
                        }
                    }
                }


                // * Close the Impairments From Active & Expired
                if (selectedTab.ToLower().Equals("expired"))
                {
                    var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals("expired"));
                    ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                }
                else
                {
                    var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals("active"));
                    ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                }

                //var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals("active"));
                //ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());


                if (ListViewElements.Count > 0)
                {
                    showNoIMPLabel = false;
                    noIMPString = "";
                }
                else
                {
                    showNoIMPLabel = true;
                    noIMPString = "ACTIVE";
                }


                //var hotworkList = _impairmentService.GetImpairments();

                //_workPermits = new ObservableCollection<ModelWorkPermit>(from data in hotworkList
                //                                                         where data.IsDeleted == false
                //                                                         select new ModelWorkPermit
                //                                                         {
                //                                                             PermitId = data.ImpairmentId,
                //                                                             Category = _impairmentTypeService.GetImpairmentTypeById(data.ImpairmentTypeId).TypeDescription + "\n\n" +
                //                                          _impairmentClassService.GetImpairmentClassTypeById(data.ImpairmentClassId).ClassDescription + "\n\n" + _shutDownReasonService.GetShutDownReasonById(data.ShutDownReasonId).ClassDescription,
                //                                                             Description = data.ImpairmentDescription,
                //                                                             ExpiryDate = Helper.ElapsedTime(Convert.ToDateTime(data.EndDateTime)),
                //                                                             HWStatus = data.WorkFlowStatus
                //                                                         });

                UpcomingFormsCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                                 where d.HWStatus == HWStatus.UPCOMING
                                                                                 group d by d.ExpiryDate into g
                                                                                 select new ModelPermits
                                                                                 {
                                                                                     Month = g.Key,
                                                                                     WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                                 });


                ActiveFormsCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                               where d.HWStatus == HWStatus.ACTIVE
                                                                               group d by d.ExpiryDate into g
                                                                               select new ModelPermits
                                                                               {
                                                                                   Month = g.Key,
                                                                                   WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                               });


                ExpiredFormsCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                                where d.HWStatus == HWStatus.CLOSED
                                                                                group d by d.ExpiryDate into g
                                                                                select new ModelPermits
                                                                                {
                                                                                    Month = g.Key,
                                                                                    WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                                });

                DraftFormsCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                              where d.HWStatus == HWStatus.DRAFT
                                                                              group d by d.ExpiryDate into g
                                                                              select new ModelPermits
                                                                              {
                                                                                  Month = g.Key,
                                                                                  WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                              });
                App.isDuplicate = false;

                RefreshImpListEvent.Invoke();
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }

        private async void NavigateAsync(string page)
        {
            if (page.Equals("NewImpairmentPage"))
            {
                await _navigationService.NavigateAsync("HomePage/NavigationPage/NewImpairmentPage", null, null, false);
                App.FromImpToNewImp = true;
                //await _navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/NewImpairmentPage", System.UriKind.Relative),null,null, false);
            }
            else if (page.Equals("Archive"))
            {
                var navigationParams = new NavigationParameters();
                navigationParams.Add("fromWhere", "fromImpairments");
                await _navigationService.NavigateAsync("HomePage/NavigationPage/ArchivePage", navigationParams, null, false);
                App.FromImpToArchive = true;
                //await _navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/ArchivePage", System.UriKind.Relative), null, null, false);
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            App.FromImpToNewImp = true;
            App.FromImpToArchive = true;
            App.FromHwToNewHw = false;
            App.FromHwToArchive = false;
        }

        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            //App.FromImpToArchive = "YES";
            //App.FromImpToNewImp = "YES";


        }

        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

        private Scopes _scope;
        private Scopes GetScope(string typeOfWork,
                               string locatioOfWork, string departementOfWork,
                               List<string> personPerforming, DateTime startTime,
                               DateTime endTime)
        {

            _scope = new Scopes
            {
                Location = locatioOfWork,
                WorkType = typeOfWork,
                CompanyDepartementName = departementOfWork,
                StartDate = startTime,
                EndDate = endTime,
                _personPerforming = personPerforming
            };

            return _scope;
        }

        private async Task SyncData()
        {

            PopupMsg = AppResource.DASHBOARD_SYNC_MSG;
            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });


                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();
                var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                //todo: store scanned permit in temp list
                App.scannedPermitModels = new List<ScannedPermitModel>();
                foreach (var hw in hotwork_sql_List)
                {

                    if (hw.ScannedPaperPermit != null && hw.ScannedPaperPermitGUID != null)
                    {
                        hw.IsScannedPermits = true;
                        connection.UpdateWithChildren(hw);
                        App.scannedPermitModels.Add(new ScannedPermitModel
                        {
                            ScannedPaperPermit = hw.ScannedPaperPermit,
                            ScannedPaperPermitGUID = hw.ScannedPaperPermitGUID
                        });
                    }
                }




                //ShowActivityLoader = true;
                bool isSyncDone = await CustomControl.Validation.IsSyncDone();

                if (isSyncDone)
                {
                    SetUnSyncAlarm();
                    //TODO: sync paper permit
                    // PopupMsg = "Submitting scanned paper permit. Please wait..";
                    //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                    //var connection = hotworkDatabase.GetdbConnection();
                    //var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                    ScanPermitRootObject syncPermitResponse = new ScanPermitRootObject();
                    if (App.scannedPermitModels.Count() > 0)
                    {
                        syncPermitResponse = await RestApiHelper<ScanPermitRootObject>.
                           SyncMultipartData(App.scannedPermitModels);
                    }

                    if (syncPermitResponse != null)
                    {

                        if (string.IsNullOrEmpty(syncPermitResponse.Data))
                        {

                        }
                        else
                        {

                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.SCANNED_PAPER_SUBMIT_SUCCESS, AppResource.OK_LABEL);

                        }

                    }
                }


                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = false;
                    SetHotworksData();
                    ChangeTabColor("active");
                });

            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public PrecautionActions GetPrecautionAction(string permitAction)
        //{

        //    switch (permitAction)
        //    {
        //        case "YES":
        //            return PrecautionActions.YES;

        //        case "NA":
        //            return PrecautionActions.NA;

        //        default:
        //            return PrecautionActions.SKIP;

        //    }
        //}

        //private async Task<Model.SyncHotWorks.RootObject> GetOnlineDataList()
        //{
        //    try
        //    {
        //        // getting hotworks permit and impairment list which is converted to the api objects
        //        var hotWorksPermitsList = _hotworkPermitService.GetHotworksAsync();
        //        var impairementList = await GetOnlineImpairementList();


        //        if (hotWorksPermitsList == null)
        //        {
        //            return null;
        //        }
        //        if (hotWorksPermitsList != null)
        //        {
        //            List<HotWork> HotWorks = new List<HotWork>();
        //            if (hotWorksPermitsList.Count() > 0)
        //            {
        //                // getting only newly created and updated permits
        //                hotWorksPermitsList = hotWorksPermitsList.Where(p => p.IsNewlyCreated == true ||
        //                                                                p.IsEdited == true || p.IsDeleted == true);
        //                // mapping offline permits to the server permit model object
        //                foreach (HotworksPermits hotworkspermit in hotWorksPermitsList)
        //                {
        //                    HotWork serverPermit = new HotWork();
        //                    serverPermit.Id = hotworkspermit.HotWorkServerId;
        //                    serverPermit.HotWorkMobileId = hotworkspermit.HandHeldId;
        //                    serverPermit.IsParent = false;
        //                    serverPermit.ParentHotWorkId = 0;
        //                    if (hotworkspermit.IsDeleted)
        //                    {
        //                        serverPermit.isDeleted = true;
        //                    }
        //                    else
        //                    {
        //                        serverPermit.isDeleted = false;
        //                    }
        //                    serverPermit.WorkFlowStatus = hotworkspermit.Status + "";
        //                    #region map scope

        //                    var scope = hotworkspermit.Scopes;
        //                    //scope
        //                    serverPermit.Scope = new Scope
        //                    {
        //                        WorkDetail = scope.WorkType,
        //                        WorkLocation = scope.Location,
        //                        CompanyName = scope.CompanyDepartementName,
        //                        StartDateTime = scope.StartDate.ToUniversalTime(),
        //                        EndDateTime = scope.EndDate.ToUniversalTime(),
        //                        EquipmentDescription = scope.EquipmentDetails
        //                    };

        //                    serverPermit.Scope.ScopePersons = new List<ScopePerson>();

        //                    foreach (string personName in scope.PersonPerforming.Split(',').ToList())
        //                    {
        //                        serverPermit.Scope.ScopePersons.Add(new ScopePerson { PersonName = personName });
        //                    }

        //                    serverPermit.Scope.ScopeWorkOrders = new List<Model.SyncHotWorks.ScopeWorkOrder>();

        //                    foreach (Model.ScopeWorkOrder workOrder in scope.ScopeWorkOrders)
        //                    {
        //                        serverPermit.Scope.ScopeWorkOrders.Add(
        //                            new Model.SyncHotWorks.ScopeWorkOrder
        //                            {
        //                                WorkOrderId = workOrder.WorkOrderId
        //                            });
        //                    }
        //                    #endregion

        //                    #region map fire prevention
        //                    //fire watcher

        //                    var fireMeasure = hotworkspermit.FirePreventionMeasures;

        //                    FirePrevention firePrevention = new FirePrevention();

        //                    firePrevention.FireWatchPreventions = new List<FireWatchPrevention>();
        //                    firePrevention.NearestFireAlarmLocation = fireMeasure.NearestAlarm;
        //                    firePrevention.NearestPhone = fireMeasure.NearestPhone;
        //                    //adding fire watcher after
        //                    if (fireMeasure.FirewatchPersons != null)
        //                    {
        //                        foreach (FirewatchPersons fireWatcher in fireMeasure.FirewatchPersons.
        //                                 Where(p => p.FireWatcherType == FireWatcherType.AFTER))
        //                        {
        //                            firePrevention.FireWatchPreventions.Add(new FireWatchPrevention
        //                            {
        //                                PersonName = fireWatcher.PersonName,
        //                                StartTime = fireWatcher.StartTime.TimeOfDay.ToString(),
        //                                EndTime = fireWatcher.EndTime.TimeOfDay.ToString(),
        //                                FireWatchType = "AFTER"
        //                            });
        //                        }
        //                    }
        //                    //adding fire watcher during
        //                    if (fireMeasure.FirewatchPersons != null)
        //                    {
        //                        foreach (FirewatchPersons fireWatcher in fireMeasure.FirewatchPersons.
        //                                 Where(p => p.FireWatcherType == FireWatcherType.DURING))
        //                        {
        //                            firePrevention.FireWatchPreventions.Add(new FireWatchPrevention
        //                            {
        //                                PersonName = fireWatcher.PersonName,
        //                                StartTime = fireWatcher.StartTime.TimeOfDay.ToString(),
        //                                EndTime = fireWatcher.EndTime.TimeOfDay.ToString(),
        //                                FireWatchType = "DURING"
        //                            });
        //                        }
        //                    }

        //                    serverPermit.FirePrevention = firePrevention;
        //                    #endregion

        //                    #region precaution measure
        //                    // precautions
        //                    List<PrecautionList> precautionList = new List<PrecautionList>();
        //                    foreach (PermitPrecautions permitPrecaution in hotworkspermit.PermitPrecautions)
        //                    {
        //                        precautionList.Add(new PrecautionList
        //                        {
        //                            MeasureId = permitPrecaution.PrecautionsId,
        //                            MeasureStatus = permitPrecaution.PrecautionActions + "",
        //                            FireHoseCount = string.IsNullOrEmpty(permitPrecaution.NoOfFirehouses) ? 0 : Convert.ToInt32(permitPrecaution.NoOfFirehouses),
        //                            FireExtinguisherCount = string.IsNullOrEmpty(permitPrecaution.NoOfFirehouses) ? 0 : Convert.ToInt32(permitPrecaution.NoOfFireExtinguiser)
        //                        });
        //                    }

        //                    serverPermit.PrecautionList = precautionList;

        //                    var auth = hotworkspermit.Authorizations;

        //                    #endregion

        //                    #region authorization
        //                    // authorization 
        //                    SiteWorkPermits.Model.SyncHotWorks.Authorization authorization = new SiteWorkPermits.Model.SyncHotWorks.Authorization
        //                    {
        //                        AuthorizerName = auth.Name,
        //                        Department = auth.Departement,
        //                        Location = auth.Location,
        //                        AuthorizationDateTime = (auth.AuthDate == null) ?
        //                            DateTime.UtcNow : auth.AuthDate.ToUniversalTime(),
        //                        IsHighHazardArea = auth.HighHazardArea ? "YES" : "NA",
        //                        AreaSupervisorName = auth.AreaSupervisorName,
        //                        AreaSupervisorDepartment = auth.AreaSupervisorDepartement
        //                    };

        //                    serverPermit.Authorization = authorization;
        //                    #endregion
        //                    HotWorks.Add(serverPermit);
        //                }
        //            }

        //            Model.SyncHotWorks.Data syncHotWorksRequest = new Model.SyncHotWorks.Data
        //            {
        //                HotWorks = HotWorks,
        //                Impairments = impairementList
        //            };
        //            var syncHotWorksResponse = await RestApiHelper<RootObject>.SyncUserDate_Post(syncHotWorksRequest);
        //            return syncHotWorksResponse;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("IOException source: {0}", e.Source);
        //        return null;
        //    }
        //}

        //private async Task SaveImpairments(List<SyncImpairements> impairments)
        //{
        //    if (impairments == null)
        //    {
        //        Device.BeginInvokeOnMainThread(() =>
        //        {
        //            ShowActivityLoader = false;
        //        });
        //        return;
        //    }
        //    else if (impairments.Count() == 0)
        //    {
        //        return;
        //    }
        //    else
        //    {
        //        // drop all data 
        //        _impairmentService.RemoveAllData();

        //        try
        //        {
        //            foreach (SyncImpairements syncServerImp in impairments)
        //            {
        //                var reportDetail = syncServerImp.ReporterDetail;
        //                Model.ReporterDetail reporterDetail = new Model.ReporterDetail
        //                {

        //                    Name = reportDetail.Name,
        //                    Email = reportDetail.Email,
        //                    Phone = reportDetail.Phone
        //                };

        //                Impairment impairment = new Impairment
        //                {
        //                    IsParent = true,
        //                    ParentImpairmentId = 0,
        //                    IsDeleted = (syncServerImp.IsDeleted.HasValue == true && syncServerImp.IsDeleted.Value) ? true : false,
        //                    ImpairmentServerId = syncServerImp.Id,
        //                    ImpairmentTypeId = syncServerImp.ImpairmentTypeId,
        //                    ImpairmentClassId = syncServerImp.ImpairmentClassId,
        //                    ShutDownReasonId = syncServerImp.ShutDownReasonId,
        //                    StartDateTime = syncServerImp.StartDateTime.ToLocalTime().ToString(),
        //                    EndDateTime = syncServerImp.EndDateTime.ToLocalTime().ToString(),
        //                    ReporterDetail = reporterDetail,
        //                    SiteId = App.SiteId,
        //                    ImpairmentDescription = syncServerImp.ImpairmentDescription,
        //                    // CloseTime = Convert.ToDateTime(syncServerImp.CloseDateTime).ToLocalTime().ToString(),
        //                };

        //                if (syncServerImp.CloseDateTime == null)
        //                {
        //                    impairment.CloseTime = "";
        //                }
        //                else
        //                {
        //                    impairment.CloseTime = Convert.ToDateTime(syncServerImp.CloseDateTime).ToLocalTime().ToString();
        //                }


        //                //var hotWorksHandHeldID = Validation.GetHandHeldId() + "" + _impairmentService.GetImpairments()
        //                //.ToList().Count().ToString();
        //                if (!syncServerImp.ImpairmentMobileId.HasValue)
        //                {
        //                    int uniqueId = _incUniqueid.GetUniqueIds().LastOrDefault().UniqueIncrementalId;
        //                    //HWUniqueNumberLabel = "No. " + Validation.GetHandHeldId() + "" + _hotworkPermitService.GetHotworksAsync().ToList().Count().ToString();
        //                    var hotWorksHandHeldID = CustomControl.Validation.GetHandHeldId() + "" + (uniqueId + 1).ToString();
        //                    _incUniqueid.AddIncUniqueId(new IncUniqueId { UniqueIncrementalId = uniqueId + 1 });
        //                    impairment.ImpairmentMobileId = Convert.ToInt32(hotWorksHandHeldID);
        //                }
        //                else
        //                {
        //                    impairment.ImpairmentMobileId = Convert.ToInt32(syncServerImp.ImpairmentMobileId);
        //                }

        //                switch (syncServerImp.WorkFlowStatus)
        //                {
        //                    case "DRAFT":
        //                        impairment.WorkFlowStatus = HWStatus.DRAFT;
        //                        break;
        //                    case "ACTIVE":
        //                        impairment.WorkFlowStatus = HWStatus.ACTIVE;
        //                        break;
        //                    case "UPCOMING":
        //                        impairment.WorkFlowStatus = HWStatus.UPCOMING;
        //                        break;
        //                    case "CLOSED":
        //                        impairment.WorkFlowStatus = HWStatus.CLOSED;
        //                        break;
        //                    default:
        //                        impairment.WorkFlowStatus = HWStatus.DRAFT;
        //                        impairment.IsDeleted = true;
        //                        break;
        //                }

        //                // major impairements
        //                List<ImpairmentImpairmentMeasureMaster> impairmentImpairmentMeasureMasters = new List<ImpairmentImpairmentMeasureMaster>();

        //                foreach (int majorImpId in syncServerImp.ImpairmentMeasureMasterIdList)
        //                {
        //                    impairmentImpairmentMeasureMasters.Add(new ImpairmentImpairmentMeasureMaster
        //                    {
        //                        ImpairmentMeasureMasterId = majorImpId,
        //                    });
        //                }
        //                impairment.ImpairmentImpairmentMeasureMasters = impairmentImpairmentMeasureMasters;

        //                List<ImpairmentPrecautionTaken> impairmentsPrecautions = new List<ImpairmentPrecautionTaken>();

        //                foreach (ImpairmentPrecautionList precautionTaken in syncServerImp.ImpairmentPrecautionList)
        //                {

        //                    string otherDesc = string.Empty;
        //                    if (precautionTaken.ImpairmentPrecautionMasterId == 15)
        //                    {
        //                        otherDesc = precautionTaken.OtherDescription;
        //                    }

        //                    impairmentsPrecautions.Add(new ImpairmentPrecautionTaken
        //                    {
        //                        ImpairmentPrecautionId = precautionTaken.ImpairmentPrecautionMasterId,
        //                        OtherDescription = otherDesc,
        //                    });
        //                }
        //                impairment.ImpairmentPrecautionTakens = impairmentsPrecautions;

        //                var isImpairementSaved = _impairmentService.AddImpairment(impairment);
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            ShowActivityLoader = false;
        //            Console.WriteLine("IOException source: {0}", e.Source);
        //        }
        //        return;
        //    }
        //}

        //private async Task<List<SyncImpairements>> GetOnlineImpairementList()
        //{
        //    try
        //    {
        //        var impairmentList = _impairmentService.GetImpairments();
        //        if (impairmentList == null)
        //        {
        //            return null;
        //        }
        //        if (impairmentList != null)
        //        {
        //            List<SyncImpairements> syncImpairements = new List<SyncImpairements>();
        //            if (impairmentList.Count() > 0)
        //            {
        //                impairmentList = impairmentList.Where(p => p.IsNewlyCreated == true ||
        //                                                               p.IsEdited == true || p.IsDeleted == true);

        //                foreach (Impairment impairement in impairmentList)
        //                {
        //                    SyncImpairements serverImpairement = new SyncImpairements();

        //                    serverImpairement.ImpairmentMobileId = impairement.ImpairmentMobileId;
        //                    serverImpairement.IsParent = false;
        //                    serverImpairement.Id = impairement.ImpairmentServerId;
        //                    serverImpairement.ParentImpairmentId = 0;
        //                    if (impairement.IsDeleted)
        //                    {
        //                        serverImpairement.IsDeleted = true;
        //                    }
        //                    else
        //                    {
        //                        serverImpairement.IsDeleted = false;
        //                    }
        //                    serverImpairement.WorkFlowStatus = impairement.WorkFlowStatus + "";
        //                    serverImpairement.ImpairmentTypeId = impairement.ImpairmentTypeId;
        //                    serverImpairement.ImpairmentClassId = impairement.ImpairmentClassId;
        //                    serverImpairement.ShutDownReasonId = impairement.ShutDownReasonId;
        //                    serverImpairement.ImpairmentDescription = impairement.ImpairmentDescription;
        //                    serverImpairement.StartDateTime = Convert.ToDateTime(impairement.StartDateTime).ToUniversalTime();
        //                    serverImpairement.EndDateTime = Convert.ToDateTime(impairement.EndDateTime).ToUniversalTime();
        //                    serverImpairement.IsDeleted = impairement.IsDeleted;
        //                    serverImpairement.ReporterDetail = new Model.SyncImpairements.ReporterDetail
        //                    {
        //                        Name = impairement.ReporterDetail.Name,
        //                        Email = impairement.ReporterDetail.Email,
        //                        Phone = impairement.ReporterDetail.Phone,
        //                    };

        //                    if (string.IsNullOrEmpty(impairement.CloseTime))
        //                    {
        //                        serverImpairement.CloseDateTime = null;
        //                    }
        //                    else
        //                    {
        //                        serverImpairement.CloseDateTime = Convert.ToDateTime(impairement.CloseTime).ToUniversalTime();
        //                    }

        //                    List<ImpairmentPrecautionList> impairmentPrecautionLists = new List<ImpairmentPrecautionList>();

        //                    foreach (ImpairmentPrecautionTaken impairmentPreTaken in impairement.ImpairmentPrecautionTakens)
        //                    {
        //                        impairmentPrecautionLists.Add(new ImpairmentPrecautionList
        //                        {
        //                            ImpairmentPrecautionMasterId = impairmentPreTaken.ImpairmentPrecautionId,
        //                            OtherDescription = impairmentPreTaken.OtherDescription
        //                        });
        //                    }
        //                    serverImpairement.ImpairmentPrecautionList = impairmentPrecautionLists;

        //                    List<int> ImpairmentMeasureMasterIdLists = new List<int>();

        //                    foreach (ImpairmentImpairmentMeasureMaster impairmentImpairmentMeasureMaster in impairement.ImpairmentImpairmentMeasureMasters)
        //                    {
        //                        ImpairmentMeasureMasterIdLists.Add(impairmentImpairmentMeasureMaster.ImpairmentMeasureMasterId);
        //                    }

        //                    serverImpairement.ImpairmentMeasureMasterIdList = ImpairmentMeasureMasterIdLists;
        //                    syncImpairements.Add(serverImpairement);
        //                }
        //                return syncImpairements;
        //            }
        //            else
        //            {
        //                return syncImpairements;
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("IOException source: {0}", e.Source);
        //        return null;
        //    }

        //}


        public event RefreshImpListAction RefreshImpListEvent;
        public delegate void RefreshImpListAction();
    }
}
