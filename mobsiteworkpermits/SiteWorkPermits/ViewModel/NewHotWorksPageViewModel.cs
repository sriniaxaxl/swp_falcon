﻿using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.DAL.sqlIntegration;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using SiteWorkPermits.Resx;
using CustomControl;
using Prism.Commands;
using Prism.Common;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Plugin.Connectivity;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.Model.SyncHotWorks;

namespace SiteWorkPermits.ViewModel
{
    public class NewHotWorksPageViewModel : BindableBase, INavigatedAware
    {
        public delegate void ChangeTabAction(string HWstatus);
        public event ChangeTabAction ChangeTabEvent;
        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        IWorkOrdersService _workOrdersService;
        IScopeService _scopeService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        IHighHazardAreasService _highHazardAreasService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IAuthorizationService _authorizationService;
        IHotworkPermitService _hotworkPermitService;
        IIncUniqueid _incUniqueid;
        SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
        SQLiteConnection connection;
        public bool _isUpdate = false;
        public bool _fromDraft = false;
        public bool _fromArchive = false;
        private HotworksPermits EditableHotworksPermits;
        private HotWorkPermit_sql EditableHotworksPermits_sql;
        private Scope_sql EditableScope_sql;
        private Authorization_sql EditableAuthorization_Sql;
        private FirePrevention_sql EditableFirePrevention_sql;
        public DelegateCommand<string> OnSubmitHotworksCommand { get; set; }
        public DelegateCommand<string> OnCloseHotworksPopUpCommand { get; set; }
        public DelegateCommand<string> OnPreviousCommand { get; set; }

        public string _saveORupdate = string.Empty;

        public NewHotWorksPageViewModel(INavigationService navigationService,
                                        IPageDialogService pageDialog,
                                        IIncUniqueid incUniqueid,
                                        IWorkOrdersService workOrdersService,
                                        IScopeService scopeService,
                                        IFirewatchPersonsService firewatchPersonsService,
                                        IPrecautionsService precautionsService,
                                        IHighHazardAreasService highHazardAreasService,
                                        IFirePreventionMeasuresService firePreventionMeasuresService,
                                        IAuthorizationService authorizationService,
                                        IHotworkPermitService hotworkPermitService)
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            _workOrdersService = workOrdersService;
            _scopeService = scopeService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _highHazardAreasService = highHazardAreasService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _hotworkPermitService = hotworkPermitService;
            _navigationService = navigationService;
            _incUniqueid = incUniqueid;
            _pageDialog = pageDialog;
            ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
            IsPreviousBtnlabel = false;
            IsContinueBtnlabel = true;
            ShowSyncIcon = false;
            LoaderLayout = false;
            OnSubmitHotworksCommand = new DelegateCommand<string>(SubmitHotwork);
            OnPreviousCommand = new DelegateCommand<string>(PreviousTab);
            OnCloseHotworksPopUpCommand = new DelegateCommand<string>(CloseHWPopUp);
         //   WorkOrders = new ObservableCollection<WorkOrders>(_workOrdersService.GetWorkOrdersAsync().ToList());
          //  WorkOrders.ForEach(s => s.IsSelected = false);
            ShowSubmitHWPoopup = false;
            StartHotWork = DateTime.Now;
            EndHotWork = DateTime.Now.AddHours(1);
            connection = hotworkDatabase.GetdbConnection();
            SelectedDate = DateTime.Now;
            SelectedTime = SelectedDate.TimeOfDay;
            ShowCloseAuthorizerName = true;
            PrecautionCollectionLocal = new ObservableCollection<PrecautionsMasterLocal>();
            var precautionmaster_sql = connection.GetAllWithChildren<PrecautionMaster_sql>().ToList();

            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION1, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP, PrecautionID = 1 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION2, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP , PrecautionID = 2 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION3, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP, PrecautionID = 3 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION4, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP, PrecautionID = 4});
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION5, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP , PrecautionID = 5 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION6, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP , PrecautionID = 6 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION7, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP, PrecautionID = 7});
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION8, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP , PrecautionID = 8 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION9, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP , PrecautionID = 9 });
            PrecautionCollectionLocal.Add( new PrecautionsMasterLocal {  Description = AppResource.HW_PRECAUTION10, NoOfFireExtinguiser = 0, NoOfFirehouses = 0, PrecautionActions = PrecautionActions.SKIP, PrecautionID = 10 });

            //foreach (var precaution_sql in precautionmaster_sql) {
            //    PrecautionCollectionLocal.Add(new PrecautionsMasterLocal { Description = precaution_sql.precautionName,
            //        NoOfFireExtinguiser =0,NoOfFirehouses=0,PrecautionActions=PrecautionActions.SKIP});
            //}
            ShowScanBtn = false;
            CloseAuthorizerName = string.Empty;
            ClosureText = AppResource.HOTWORK_CLOSE_TEXT;
            CloseType = AppResource.CLOSE_HOTWORK_PERMIT;
            ClosureTypeBtnText = AppResource.CLOSE_HOTWORK_PERMIT;
            isCloseDetailVisible = false;
            WorkOrders = new ObservableCollection<WorkOrderMaster_sql>(connection.GetAllWithChildren<WorkOrderMaster_sql>().ToList());
            WorkOrdersLocal = new ObservableCollection<WorkOrderMasterLocal>();
            foreach (WorkOrderMaster_sql workOrderMaster_Sql in WorkOrders) {
                WorkOrdersLocal.Add(new WorkOrderMasterLocal { workorderName = workOrderMaster_Sql.workOrderName, isSelected = false });
            }


           // _workOrdersLocal = new ObservableCollection<WorkOrderMasterLocal>();
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("CREATE HOT WORKS");
            _saveORupdate = string.Empty;
        }


        private bool _ShowScanBtn;
        public bool ShowScanBtn
        {
            get { return _ShowScanBtn; }
            set { SetProperty(ref _ShowScanBtn, value); }
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }


        private bool _showHWClosePopup;
        public bool showHWClosePopup
        {
            get { return _showHWClosePopup; }
            set { SetProperty(ref _showHWClosePopup, value); }
        }

        private bool _showCloseAuthorizerName;
        public bool ShowCloseAuthorizerName
        {
            get { return _showCloseAuthorizerName; }
            set { SetProperty(ref _showCloseAuthorizerName, value); }
        }
         private string _closeAuthorizerName;
        public string CloseAuthorizerName
        {
            get { return _closeAuthorizerName; }
            set { SetProperty(ref _closeAuthorizerName, value); }
        }


        private string _elementCategory_sql;
        public string ElementCategory_sql
        {
            get { return _elementCategory_sql; }
            set { SetProperty(ref _elementCategory_sql, value); }
        }

        private string _elementDescription_Sql;
        public string ElementDescription_Sql
        {
            get { return _elementDescription_Sql; }
            set { SetProperty(ref _elementDescription_Sql, value); }
        }

        private string _elementStartDate_Sql;
        public string ElementStartDate_Sql
        {
            get { return _elementStartDate_Sql; }
            set { SetProperty(ref _elementStartDate_Sql, value); }
        }
        
        private string _CloseAuthorizer;
        public string CloseAuthorizer
        {
            get { return _CloseAuthorizer; }
            set { SetProperty(ref _CloseAuthorizer, value); }
        }

            private bool _ShowDeleteBtn;
        public bool ShowDeleteBtn
        {
            get { return _ShowDeleteBtn; }
            set { SetProperty(ref _ShowDeleteBtn, value); }
        }
           private bool _ShowCloseLayout;
        public bool ShowCloseLayout
        {
            get { return _ShowCloseLayout; }
            set { SetProperty(ref _ShowCloseLayout, value); }
        }

           private bool _ShowAttachment;
        public bool ShowAttachment
        {
            get { return _ShowAttachment; }
            set { SetProperty(ref _ShowAttachment, value); }
        }

        public ICommand CancelHotworkCommand
        {
            get
            {
                return new Command((x) =>
                {
                    try
                    {
                        showHWClosePopup = false;
                    }
                    catch (Exception ex) { }
                });
            }
        }

          public ICommand OnAttachmentDownloadTapped
        {
            get
            {
                return new Command(async (x) =>
                {
                    try
                    {
                        LoaderLayout = true;

                        var imageByte =  await RestApiHelper<string>.GetScanPermitImage(App.ArchivePermitScanGuid);

                        bool issaved = false;
                        if (imageByte != null) {
                            issaved = await Xamarin.Forms.DependencyService.Get<IPicture>().
                        SavePictureToDisk(App.ArchivePermitScanGuid,
                        imageByte);
                        }

                        if (issaved)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync(AppResource.DOWNLOAD_SCANNED_PAPERPERMIT_LABEL, AppResource.SCANNED_PAPER_DOWNLOAD_SUCCESS, AppResource.OK_LABEL);

                        }
                        else {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync(AppResource.DOWNLOAD_SCANNED_PAPERPERMIT_LABEL, AppResource.SCANNED_PAPER_DOWNLOAD_ERROR, AppResource.OK_LABEL);

                        }

                        LoaderLayout = false;
                    }
                    catch (Exception ex) {

                        LoaderLayout = false;
                    }
                });
            }
        }


        public ICommand OnCameraCancelCommand
        {
            get
            {
                return new Command(async (x) =>
                {
                    showHWScanPopup = false;
                });
            }
        }

        public ICommand OnCameraCommand
        {
            get
            {
                return new Command(async (x) =>
                {
                    try
                    {
                        // await SyncData();
                        if (string.IsNullOrEmpty(CloseAuthorizerName))
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.AUTHORIZER_NAME_MANDATORY, "ok");
                            return;
                        }



                        isCreationInProgress = true;
                        App.FromScanPermit = true;
                        var byteArray = await Xamarin.Forms.DependencyService.Get<IFileUtility>().GetImageWithCamera();
                        if (EditableHotworksPermits_sql != null) {
                            EditableHotworksPermits_sql.ScannedPaperPermit = byteArray;
                            EditableHotworksPermits_sql.isEdited = true;

                            if (string.IsNullOrEmpty(EditableHotworksPermits_sql.ScannedPaperPermitGUID)) {
                                EditableHotworksPermits_sql.ScannedPaperPermitGUID = new Guid().ToString();
                            }

                            EditableHotworksPermits_sql.hotWorkCloseDate = SelectedDate.Add(SelectedTime).ToString();
                            EditableHotworksPermits_sql.workFlowStatus = "CLOSED";
                            EditableHotworksPermits_sql.CloseAuthorizedBy = CloseAuthorizerName;
                            connection.UpdateWithChildren(EditableHotworksPermits_sql);
                            showHWClosePopup = false;
                            showHWScanPopup = false;
                           // ShowAttachment = true;

                            await Task.Yield();
                            isCreationInProgress = false;
                            
                         //   CustomControl.Validation.SetAlarm(hwinstance);
                            await _pageDialog.DisplayAlertAsync(AppResource.SCANNED_PERMIT_LABEL, AppResource.SCANNED_PAPER_SAVED_SUCCESS, AppResource.OK_LABEL);
                            // App.PermitEditableId = connection.Table<HotWorkPermit_sql>().ToList().LastOrDefault().id;
                            // await _navigationService.NavigateAsync("HomePage", null, null, false);
                            App.FromScanPermit = false;
                            //await _navigationService.NavigateAsync("NavigationPage/HomePage", null, null, false);


                            await _navigationService.NavigateAsync(new Uri("/HomePage", UriKind.Relative), null, null, false);


                            // await IsHWCloseEmailSent(EditableHotworksPermits_sql);
                            // connection.UpdateWithChildren(EditableHotworksPermits_sql);
                        }
                    }
                    catch (Exception ex)
                    {
                        isCreationInProgress = false;
                        showHWClosePopup = false;
                        showHWScanPopup = false;
                        ShowAttachment = false;
                        App.FromScanPermit = false;
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync(AppResource.SCANNED_PERMIT_LABEL, AppResource.SCANNED_PAPER_SAVED_ERROR, AppResource.OK_LABEL);

                    }
                });
            }
        }

        public ICommand OnDeleteHotworksCommand
        {
            get
            {
                return new Command<string>(async (x) => {
                    //showClosePopup = false;
                    EditableHotworksPermits_sql.isDeleted = true;
                    EditableHotworksPermits_sql.isEdited = true;
                    connection.UpdateWithChildren(EditableHotworksPermits_sql);
                    //showHWClosePopup = true;
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("",AppResource.HOTWORK_DELETED_SUCCESSFULLY, AppResource.OK_LABEL);
                    await _navigationService.NavigateAsync(new Uri("/HomePage", UriKind.Relative), null, null, false);
                    //ElementCategory_sql = string.Join(", ", EditableScope_sql.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName));
                    //ElementDescription_Sql = EditableScope_sql.WorkLocation;
                    //ElementStartDate_Sql = EditableScope_sql.StartDateTime;
                    //showHWClosePopup = true;
                });
            }
        }
        private bool _showHWScanPopup;
        public bool showHWScanPopup
        {
            get { return _showHWScanPopup; }
            set { SetProperty(ref _showHWScanPopup, value); }
        }

        public ICommand OnScanHotworksCommand
        {
            get
            {
                return new Command<string>((x) => {

                    ShowCloseAuthorizerName = true;
                    showHWScanPopup = !showHWScanPopup;
                });
            }
        }

        public ICommand OnCloseHotworksCommand
        {
            get
            {
                return new Command<string>((x) => {
                    showHWClosePopup = true;

                    ElementCategory_sql = string.Join(", ", EditableScope_sql.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName));
                    ElementDescription_Sql = EditableScope_sql.WorkLocation;
                    ElementStartDate_Sql = EditableScope_sql.StartDateTime;
                    showHWClosePopup = true;
                });
            }
        }

        private TimeSpan _selectedTime;
        public TimeSpan SelectedTime
        {
            get { return _selectedTime; }
            set { SetProperty(ref _selectedTime, value); }
        }

        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set { SetProperty(ref _selectedDate, value); }
        }

        public ICommand CloseHotworkCommand
        {
            get
            {
                return new Command(async (x) => {

                    if (string.IsNullOrEmpty(CloseAuthorizerName)) {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.AUTHORIZER_NAME_MANDATORY, "ok");
                        return;
                    }

                    EditableHotworksPermits_sql.isEdited = true;
                    EditableHotworksPermits_sql.hotWorkCloseDate = SelectedDate.Add(SelectedTime).ToString();
                    EditableHotworksPermits_sql.workFlowStatus = "CLOSED";
                    EditableHotworksPermits_sql.CloseAuthorizedBy = CloseAuthorizerName;
                    connection.UpdateWithChildren(EditableHotworksPermits_sql);
                    showHWClosePopup = false;
                    await IsHWCloseEmailSent(EditableHotworksPermits_sql);
                    await _navigationService.NavigateAsync(new Uri("/HomePage", UriKind.Relative), null, null, false);
                });
            }
        }


        private async Task<bool> IsHWCloseEmailSent(HotWorkPermit_sql permit_Sql)
        {
            LoaderLayout = true;
            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    LoaderLayout = true;
                });
                try
                {
                    var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                    analyticsService.LogEvent("", "Permit Submit", "Permit Submit");
                    string permitguid = string.Empty;
                    int permitserverid = 0;
                    if (permit_Sql.server_id == 0)
                    {
                        var serverHotwork = CustomControl.Validation.GetServerHotwork(permit_Sql);
                        serverHotwork.Result.CloseAuthorizedBy = CloseAuthorizerName;
                        //    impairements.Result.CloseDateTime = Convert.ToDateTime(impairment.impairmentCloseDateTime);
                        connection.UpdateWithChildren(permit_Sql);
                        var createHWResponse = await RestApiHelper<CreateIMPResponse>.CreateHW_Post(serverHotwork.Result);

                        if (createHWResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                LoaderLayout = false;
                            });
                            return false;
                        }
                        else if (createHWResponse.Data != null)
                        {
                            permitguid = createHWResponse.Data.GUID;
                            permitserverid = createHWResponse.Data.Id;
                        }
                        else if (createHWResponse.Status != null)
                        {
                            if (createHWResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", createHWResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                // Xamarin.Forms.Application.Current.Properties.Clear();

                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync(new Uri("NavigationPage/LoginPage", UriKind.Relative), null, null, false);
                                LoaderLayout = false;
                                return false;
                            }
                        }

                        permit_Sql.server_id = permitserverid;
                        permit_Sql.hotworkGUID = permitguid;
                        //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                        //    var connection = hotworkDatabase.GetdbConnection();
                        connection.UpdateWithChildren(permit_Sql);
                    }
                    else
                    {
                        connection.UpdateWithChildren(permit_Sql);
                        await CustomControl.Validation.IsSyncDone();
                        permitguid = permit_Sql.hotworkGUID;
                    }

                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.PERMIT_CLOSE_SUCCESS, AppResource.OK_LABEL);


                    Device.BeginInvokeOnMainThread(() =>
                    {
                        LoaderLayout = false;
                    });
                    return true;
                }
                catch (Exception e)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        LoaderLayout = false;
                    });
                    Console.WriteLine("IOException source: {0}", e.Source);
                    return false;
                }
            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("",AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                return false;
            }
        }



        public ICommand SaveHotworkPermit
        {
            get
            {
                return new Command<string>((x) => {

                    if (EditableHotworksPermits_sql != null)
                    {
                        // Active
                        if (EditableHotworksPermits_sql.workFlowStatus == "ACTIVE" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as draft";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "ACTIVE" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            _saveORupdate = "update";
                        }


                        // Upcoming
                        if (EditableHotworksPermits_sql.workFlowStatus == "UPCOMING" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as draft";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "UPCOMING" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            _saveORupdate = "update";
                        }

                        // draft
                        if (EditableHotworksPermits_sql.workFlowStatus == "DRAFT" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as draft";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "DRAFT" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            _saveORupdate = "update";
                            _fromDraft = true;
                        }

                        // archive
                        if (EditableHotworksPermits_sql.workFlowStatus == "ARCHIVE" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as draft";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "ARCHIVE" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            // read only
                        }

                        // closed
                        if (EditableHotworksPermits_sql.workFlowStatus == "CLOSED" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as draft";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "CLOSED" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            // read only
                        }

                    }

                    if (EditableHotworksPermits_sql == null) {
                        _saveORupdate = "save as draft";
                    }

                    if (_saveORupdate == "save as draft")
                    {
                        SubmitHW("Draft");
                    }
                    else if (_saveORupdate == "update")
                    {
                        UpdateHotWorksPermit();
                    }
                    else {
                        SubmitHW("");
                    }                   
                });
            }
        }
               
        public ICommand SubmitHotworkPermit
        {
            get
            {
                return new Command<string>((x) => {

                    if (EditableHotworksPermits_sql != null) {

                        // Active
                        if (EditableHotworksPermits_sql.workFlowStatus == "ACTIVE" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as new";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "ACTIVE" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            _saveORupdate = "update";
                        }


                        // Upcoming
                        if (EditableHotworksPermits_sql.workFlowStatus == "UPCOMING" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as new";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "UPCOMING" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            _saveORupdate = "update";
                        }

                        // draft
                        if (EditableHotworksPermits_sql.workFlowStatus == "DRAFT" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as draft";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "DRAFT" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            _saveORupdate = "update";
                        }

                        // archive
                        if (EditableHotworksPermits_sql.workFlowStatus == "ARCHIVE" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as new";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "ARCHIVE" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            // read only
                        }



                        // archive
                        if (EditableHotworksPermits_sql.workFlowStatus == "CLOSED" && App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _saveORupdate = "save as new";
                        }
                        else if (EditableHotworksPermits_sql.workFlowStatus == "CLOSED" && App.isImpairementEditorDuplicate == "Edit")
                        {
                            // read only
                        }

                    }                 


                    if (_saveORupdate == "save as draft")
                    {
                        SubmitHW("Draft");
                    }
                    else if (_saveORupdate == "update")
                    {
                        UpdateHotWorksPermit();
                    }
                    else
                    {
                        SubmitHW("");
                    }
                });
            }
        }


        #region Tab 1 Scope

        private ObservableCollection<WorkOrderMaster_sql> _workOrders;
        public ObservableCollection<WorkOrderMaster_sql> WorkOrders
        {
            get { return _workOrders; }
            set { SetProperty(ref _workOrders, value); }
        }


        public ObservableCollection<WorkOrderMasterLocal> _workOrdersLocal;
        public ObservableCollection<WorkOrderMasterLocal> WorkOrdersLocal
        {
            get { return _workOrdersLocal; }
            set { SetProperty(ref _workOrdersLocal, value); }
        }


        private DateTime _startHotWork;
        public DateTime StartHotWork
        {
            get { return _startHotWork; }
            set { SetProperty(ref _startHotWork, value); }
        }

        private DateTime _endHotWork;
        public DateTime EndHotWork
        {
            get { return _endHotWork; }
            set { SetProperty(ref _endHotWork, value); }
        }

        private string _describeWork = string.Empty;
        public string DescribeWork
        {
            get { return _describeWork; }
            set { SetProperty(ref _describeWork, value); }
        }

        private string _department = string.Empty;
        public string Department
        {
            get { return _department; }
            set { SetProperty(ref _department, value); }
        }

        private string _equipmentDetails = string.Empty;
        public string EquipmentDetails
        {
            get { return _equipmentDetails; }
            set { SetProperty(ref _equipmentDetails, value); }
        }

        private string _locationWork = string.Empty;
        public string LocationWork
        {
            get { return _locationWork; }
            set { SetProperty(ref _locationWork, value); }
        }

        private ObservableCollection<FirewatchPersonsLocal> _personHotWork =
            new ObservableCollection<FirewatchPersonsLocal>();
        public ObservableCollection<FirewatchPersonsLocal> PersonHotWork
        {
            get { return _personHotWork; }
            set { SetProperty(ref _personHotWork, value); }
        }

        private Scopes _scope;

        private Scopes GetScope(string typeOfWork,
                                string locatioOfWork, string departementOfWork,
                                List<string> personPerforming, DateTime startTime,
                                DateTime endTime)
        {

            _scope = new Scopes
            {
                Location = locatioOfWork,
                WorkType = typeOfWork,
                CompanyDepartementName = departementOfWork,
                StartDate = startTime,
                EndDate = endTime,
                _personPerforming = personPerforming
            };

            return _scope;
        }

        #endregion

        #region Tab 2 Fire Prevention Mesure
        private ObservableCollection<FirewatchPersonsLocal> _firewatchDuringHotWork = new ObservableCollection<FirewatchPersonsLocal>();
        public ObservableCollection<FirewatchPersonsLocal> FirewatchDuringHotWork
        {
            get { return _firewatchDuringHotWork; }
            set { SetProperty(ref _firewatchDuringHotWork, value); }
        }

        private ObservableCollection<FirewatchPersonsLocal> _firewatchAfterHotWork =
            new ObservableCollection<FirewatchPersonsLocal>();
        public ObservableCollection<FirewatchPersonsLocal> FirewatchAfterHotWork
        {
            get { return _firewatchAfterHotWork; }
            set { SetProperty(ref _firewatchAfterHotWork, value); }
        }

        private string _nearFireAlarmLocation;
        public string NearFireAlarmLocation
        {
            get { return _nearFireAlarmLocation; }
            set { SetProperty(ref _nearFireAlarmLocation, value); }
        }


        private string _nearPhoneLocation;
        public string NearPhoneLocation
        {
            get { return _nearPhoneLocation; }
            set { SetProperty(ref _nearPhoneLocation, value); }
        }

        #endregion

        #region Tab 3 Precaution Measure

        private ObservableCollection<PrecautionsMasterLocal> _PrecautionCollectionLocal;
        public ObservableCollection<PrecautionsMasterLocal> PrecautionCollectionLocal
        {
            get { return _PrecautionCollectionLocal; }
            set { SetProperty(ref _PrecautionCollectionLocal, value); }
        }

        private FirePreventionMeasures GetFirePreventionMeasure(
            ObservableCollection<FirewatchPersons> firewatchDuringHotWork,
            ObservableCollection<FirewatchPersons> firewatchAfterHotWork,
            string nearFireAlarmLocation, string nearPhoneLocation)
        {
            var firePreventionMeasure = new FirePreventionMeasures
            {
                FirewatchPersons = firewatchAfterHotWork.Union(firewatchDuringHotWork).ToList(),
                NearestAlarm = nearFireAlarmLocation,
                NearestPhone = nearPhoneLocation
            };
            return firePreventionMeasure;
        }

        #endregion

        #region Tab 4 Authorization
        private string _authName;
        public string AuthName
        {
            get { return _authName; }
            set { SetProperty(ref _authName, value); }
        }

        private string _authDepartment;
        public string AuthDepartment
        {
            get { return _authDepartment; }
            set { SetProperty(ref _authDepartment, value); }
        }


        private string _authLocation;
        public string AuthLocation
        {
            get { return _authLocation; }
            set { SetProperty(ref _authLocation, value); }
        }

        private DateTime _authDate;
        public DateTime AuthDate
        {
            get { return _authDate; }
            set { SetProperty(ref _authDate, value); }
        }

        private string _areaSupervisorName;
        public string AreaSupervisorName
        {
            get { return _areaSupervisorName; }
            set { SetProperty(ref _areaSupervisorName, value); }
        }

        private string _HazardDepartment;
        public string HazardDepartment
        {
            get { return _HazardDepartment; }
            set { SetProperty(ref _HazardDepartment, value); }
        }

        private string _areaSupervisorDepartement;
        public string AreaSupervisorDepartement
        {
            get { return _areaSupervisorDepartement; }
            set { SetProperty(ref _areaSupervisorDepartement, value); }
        }

        private bool _isHighHazardArea;
        public bool IsHighHazardArea
        {
            get { return _isHighHazardArea; }
            set { SetProperty(ref _isHighHazardArea, value); }
        }
          private bool _loaderLayout;
        public bool LoaderLayout
        {
            get { return _loaderLayout; }
            set { SetProperty(ref _loaderLayout, value); }
        }

        //private Authorizations GetAuthorizations(string AuthorizationName, string AuthDept, string AuthLocation,
        //                                         DateTime AuthDate, HighHazardAreas highHazardAreas)
        //{
        //    var authorization = new Authorizations
        //    {
        //        Name = AuthorizationName,
        //        Departement = AuthDept,
        //        Location = AuthLocation,
        //        AuthDate = AuthDate,
        //        HighHazardArea = highHazardAreas
        //    };
        //    return authorization;
        //}

        //private HighHazardAreas GetHighHazardArea()
        //{
        //    var highHazardArea = new HighHazardAreas
        //    {
        //        IsHazardArea = IsHighHazardArea,
        //        AreaSupervisorName = AreaSupervisorName,
        //        Departement = AreaSupervisorDepartement
        //    };
        //    return highHazardArea;
        //}

        #endregion

        private FirewatchPersonsLocal _CurrentHotWorkPerson = new FirewatchPersonsLocal();
        public FirewatchPersonsLocal CurrentHotWorkPerson
        {
            get { return _CurrentHotWorkPerson; }
            set { SetProperty(ref _CurrentHotWorkPerson, value); }
        }

        private FirewatchPersonsLocal _CurrentFireWatchDuringPerson = new FirewatchPersonsLocal();
        public FirewatchPersonsLocal CurrentFireWatchDuringPerson
        {
            get { return _CurrentFireWatchDuringPerson; }
            set { SetProperty(ref _CurrentFireWatchDuringPerson, value); }
        }


        private FirewatchPersonsLocal _CurrentFireWatchAfterPerson = new FirewatchPersonsLocal();
        public FirewatchPersonsLocal CurrentFireWatchAfterPerson
        {
            get { return _CurrentFireWatchAfterPerson; }
            set { SetProperty(ref _CurrentFireWatchAfterPerson, value); }
        }

        private string _continueBtnlabel;
        public string ContinueBtnlabel
        {
            get { return _continueBtnlabel; }
            set { SetProperty(ref _continueBtnlabel, value); }
        }

        private bool _showSubmitHWPoopup;
        public bool ShowSubmitHWPoopup
        {
            get { return _showSubmitHWPoopup; }
            set { SetProperty(ref _showSubmitHWPoopup, value); }
        }

        private bool _ShowCloseBtn;
        public bool ShowCloseBtn
        {
            get { return _ShowCloseBtn; }
            set { SetProperty(ref _ShowCloseBtn, value); }
        }

        private string _hWUniqueNumberLabel;
        public string HWUniqueNumberLabel
        {
            get { return _hWUniqueNumberLabel; }
            set { SetProperty(ref _hWUniqueNumberLabel, value); }
        }

        private int _NoOfFireHouse;
        public int NoOfFireHouse
        {
            get { return _NoOfFireHouse; }
            set { SetProperty(ref _NoOfFireHouse, value); }
        }

        private int _NoOfFireExtinguiser;
        public int NoOfFireExtinguiser
        {
            get { return _NoOfFireExtinguiser; }
            set { SetProperty(ref _NoOfFireExtinguiser, value); }
        }

        private bool _isPreviousBtnlabel;
        public bool IsPreviousBtnlabel
        {
            get { return _isPreviousBtnlabel; }
            set { SetProperty(ref _isPreviousBtnlabel, value); }
        }

        private HotWorkAction _ActionHotWork;
        public HotWorkAction ActionHotWork
        {
            get { return _ActionHotWork; }
            set { SetProperty(ref _ActionHotWork, value); }
        }

        private bool _isContinueBtnlabel;
        public bool IsContinueBtnlabel
        {
            get { return _isContinueBtnlabel; }
            set { SetProperty(ref _isContinueBtnlabel, value); }
        }


        #region Save buttons

        private bool _showDraftBtn;
        public bool ShowDraftBtn
        {
            get { return _showDraftBtn; }
            set { SetProperty(ref _showDraftBtn, value); }
        }

        private bool _showWorkflowBtn;
        public bool ShowWorkflowBtn
        {
            get { return _showWorkflowBtn; }
            set { SetProperty(ref _showWorkflowBtn, value); }
        }

        private bool _showSaveBtn;
        public bool ShowSaveBtn
        {
            get { return _showSaveBtn; }
            set { SetProperty(ref _showSaveBtn, value); }
        }

           private string _ClosureText;
        public string ClosureText
        {
            get { return _ClosureText; }
            set { SetProperty(ref _ClosureText, value); }
        }
             private bool _isCloseDetailVisible;
        public bool isCloseDetailVisible
        {
            get { return _isCloseDetailVisible; }
            set { SetProperty(ref _isCloseDetailVisible, value); }
        }

           private string _CloseType;
        public string CloseType
        {
            get { return _CloseType; }
            set { SetProperty(ref _CloseType, value); }
        }
          private string _closureTypeBtnText;
        public string ClosureTypeBtnText
        {
            get { return _closureTypeBtnText; }
            set { SetProperty(ref _closureTypeBtnText, value); }
        }

        #endregion
        private bool isCreationInProgress = false;
        private bool SendToPrint = false;
        #region Save Hotworks
        private bool _submitDraftToWorkflow = false;

        private async void SubmitHotwork(string hotworksStatus)
        {
            if (hotworksStatus.Equals("Update"))
            {
                SendToPrint = false;
            }
            else
            {
                SendToPrint = true;
            }

            if (hotworksStatus.Equals("Draft"))
            {
                SubmitHW(hotworksStatus);
            }
            else if (hotworksStatus.Equals("Submit"))
            {
                if (_fromDraft)
                {
                    _submitDraftToWorkflow = true;
                    UpdateHotWorksPermit();
                    return;
                }
                if (_isUpdate)
                {
                    _submitDraftToWorkflow = false;
                    UpdateHotWorksPermit();
                }
                else
                {
                    ChangeTabEvent?.Invoke(hotworksStatus);
                }

            }
            else if (hotworksStatus.Equals("Update"))
            {
                UpdateHotWorksPermit();
            }
            else if (hotworksStatus.Equals(""))
            {
                ChangeTabEvent?.Invoke(hotworksStatus);
            }
            else if (hotworksStatus.Equals("Delete"))
            {
                var ssss = EditableHotworksPermits.HotworksPermitsId;
                var hotWorks = _hotworkPermitService.GetHotworksByIdAsync(ssss);
                hotWorks.IsDeleted = true;
                _hotworkPermitService.UpdateHotworksAsync(hotWorks);
                await _pageDialog.DisplayAlertAsync("", AppResource.HOTWORK_DELETED_SUCCESSFULLY, AppResource.OK_LABEL);
                await _navigationService.NavigateAsync(new Uri("/HomePage", UriKind.Relative), null, null, false);
            }
        }

        private void PreviousTab(String ss)
        {
            ChangeTabEvent?.Invoke("Previous");
        }

        private void CloseHWPopUp(string ss)
        {
            ShowSubmitHWPoopup = false;
        }


        public async void UpdateHotWorksPermit()
        {
            var hperson = CurrentHotWorkPerson;
            var Fdperson = CurrentFireWatchDuringPerson;
            var Faperson = CurrentFireWatchAfterPerson;

            if (!string.IsNullOrEmpty(hperson.PersonName))
            {
                PersonHotWork.Add(hperson);
            }
            if (!string.IsNullOrEmpty(Fdperson.PersonName))
            {
                FirewatchDuringHotWork.Add(Fdperson);
            }
            if (!string.IsNullOrEmpty(Faperson.PersonName))
            {
                FirewatchAfterHotWork.Add(Faperson);
            }

            try
            {


                var totalHour = (EndHotWork - StartHotWork).TotalHours;

                if (totalHour < 8)
                {

                }
                else
                {
                    if (totalHour >= 12)
                    {
                        await _pageDialog.DisplayAlertAsync(AppResource.WORK_SHIFT_ERROR_LABEL, AppResource.WORKSHIFT_RESTRICTION_12HRS, AppResource.OK_LABEL);
                        isCreationInProgress = false;
                        //var curpage = (NewHotWorksPage)PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                        //StackLayout TabPanel = curpage.FindByName<StackLayout>("Spworktabs");
                        //TabView tab = (TabView)TabPanel.Children[0];
                        //curpage.SelectionTab(tab);

                        return;
                    }
                    else if (totalHour >= 8 && totalHour < 12)
                    {
                      //  var result = await _pageDialog.DisplayAlertAsync("Work shift warning", "Work shift is more than 8 hours. Do you want to continue?", "Ok", "Cancel");
                        //if (!result) {
                        //    isCreationInProgress = false;
                        //    //var curpage = (NewHotWorksPage)PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                        //    //StackLayout TabPanel = curpage.FindByName<StackLayout>("Spworktabs");
                        //    //TabView tab = (TabView)TabPanel.Children[0];
                        //    //curpage.SelectionTab(tab);

                        //    return;
                        //}
                    }
                    else
                    {
                        await _pageDialog.DisplayAlertAsync(string.Empty, CustomControl.Validation.START_END_DATE_SAME, AppResource.OK_LABEL);
                        isCreationInProgress = false;
                        return;
                    }
                }



                var isDataValide = HotWorkUtility.IsHotworkMandatoryDataValid(StartHotWork, EndHotWork, WorkOrdersLocal, LocationWork);
                if (!isDataValide.Key)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync(string.Empty, isDataValide.Value, AppResource.OK_LABEL);
                    return;
                }

                #region update scope

                //var scopeee = EditableHotworksPermits_sql.scope_Sql;
                var personList = new List<string>(PersonHotWork.Select(s => s.PersonName));
                EditableScope_sql.WorkDetail = DescribeWork;
                EditableScope_sql.WorkLocation = LocationWork;
                EditableScope_sql.CompanyName = Department;

                EditableScope_sql.scopePerson_Sqls.Clear();
                foreach (string personName in personList)
                {
                    EditableScope_sql.scopePerson_Sqls.Add(new ScopePerson_sql { PersonName = personName });
                }
                EditableScope_sql.StartDateTime = StartHotWork.ToString();
                EditableScope_sql.EndDateTime = EndHotWork.ToString();
                EditableScope_sql.EquipmentDescription = EquipmentDetails;
                EditableScope_sql.workOrderMaster_Sqls.Clear();
                foreach (var workOrderMasterLocal in WorkOrdersLocal.ToList().Where(p => p.isSelected == true).ToList())
                {
                    EditableScope_sql.workOrderMaster_Sqls.Add(connection.Table<WorkOrderMaster_sql>().Where(p => p.workOrderName.ToLower() == workOrderMasterLocal.workorderName.ToLower()).FirstOrDefault());
                }

                // update scope person then update scope itself
                try
                {
                    connection.InsertAll(EditableScope_sql.scopePerson_Sqls);
                    connection.UpdateWithChildren(EditableScope_sql);

                }
                catch (Exception ex)
                {
                    var ss = ex;
                }

                #endregion

                #region fire prevention

                EditableFirePrevention_sql.fireWatcherPrevention_Sqls.Clear();

                for (int i = 0; i < FirewatchAfterHotWork.Count; i++)
                {
                    EditableFirePrevention_sql.fireWatcherPrevention_Sqls.Add(new FireWatcherPrevention_sql
                    {
                        PersonName = FirewatchAfterHotWork[i].PersonName,
                        StartTime = FirewatchAfterHotWork[i].StartTime.ToString("HH:mm:ss"),
                        EndTime = FirewatchAfterHotWork[i].EndTime.ToString("HH:mm:ss"),
                        FireWatchType = "AFTER",
                    });
                }

                for (int i = 0; i < FirewatchDuringHotWork.Count; i++)
                {
                    EditableFirePrevention_sql.fireWatcherPrevention_Sqls.Add(new FireWatcherPrevention_sql
                    {
                        PersonName = FirewatchAfterHotWork[i].PersonName,
                        StartTime = FirewatchAfterHotWork[i].StartTime.ToString("HH:mm:ss"),
                        EndTime = FirewatchAfterHotWork[i].EndTime.ToString("HH:mm:ss"),
                        FireWatchType = "DURING",
                    });
                }

                connection.InsertAll(EditableFirePrevention_sql.fireWatcherPrevention_Sqls);

                EditableFirePrevention_sql.NearestFireAlarmLocation = NearFireAlarmLocation;
                EditableFirePrevention_sql.NearestPhone = NearPhoneLocation;
                connection.UpdateWithChildren(EditableFirePrevention_sql);

                #endregion

                #region update auth

                var auth = EditableAuthorization_Sql;
                // auth.HighHazardArea = IsHighHazardArea;
                auth.AuthorizerName = AuthName;
                auth.Department = AuthDepartment;
                auth.AuthorizationDateTime = AuthDate==DateTime.MinValue? DateTime.Now.ToString():AuthDate.ToString();
                // EditableHotworksPermits.Authorizations = auth;

                if (!IsHighHazardArea)
                {
                    auth.IsHighHazardArea = IsHighHazardArea;
                    auth.AreaSupervisorName = "";
                    auth.AreaSupervisorDepartment = "";
                }
                else
                {
                    auth.IsHighHazardArea = IsHighHazardArea;
                    auth.AreaSupervisorName = AreaSupervisorName;
                    auth.AreaSupervisorDepartment = AreaSupervisorDepartement;
                }

                if (EditableHotworksPermits_sql.authorization_sql.IsHighHazardArea)
                {
                    if (string.IsNullOrEmpty(EditableHotworksPermits_sql.authorization_sql.AreaSupervisorName) || string.IsNullOrEmpty(EditableHotworksPermits_sql.authorization_sql.AreaSupervisorName))
                    {
                        await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HIGH_HAZARD_DETAIL_MANDATORY, AppResource.OK_LABEL);
                        return;
                    }
                }
                connection.Update(EditableHotworksPermits_sql.authorization_sql);
                #endregion

                #region update_precaution

             
                    if (PrecautionCollectionLocal.Where(p => p.PrecautionID == 6).FirstOrDefault().PrecautionActions == PrecautionActions.YES)
                    {
                        if (PrecautionCollectionLocal.Where(p => p.PrecautionID == 6).FirstOrDefault().NoOfFireExtinguiser ==0 ||
                            PrecautionCollectionLocal.Where(p => p.PrecautionID == 6).FirstOrDefault().NoOfFirehouses ==0)
                        {
                            await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.FIRE_HOUSE_EXTINGUSER_CANNOT_BLANK, AppResource.OK_LABEL);
                            return;
                        }
                        else
                        {
                            NoOfFireExtinguiser = Convert.ToInt32( PrecautionCollectionLocal.Where(p => p.PrecautionID== 6).FirstOrDefault().NoOfFireExtinguiser);
                            NoOfFireHouse = Convert.ToInt32(PrecautionCollectionLocal.Where(p => p.PrecautionID== 6).FirstOrDefault().NoOfFirehouses);
                        }
                    }

                


                var precautionMaster_sql = new List<PrecautionMaster_sql>();

                foreach (var precautions in PrecautionCollectionLocal.ToList())
                {
                    if (precautions.PrecautionActions == PrecautionActions.YES)
                        precautionMaster_sql.Add(connection.Table<PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.PrecautionID).FirstOrDefault());
                }

                EditableHotworksPermits_sql.precautionMaster_Sqls.Clear();
                EditableHotworksPermits_sql.precautionMaster_Sqls = precautionMaster_sql;
                EditableHotworksPermits_sql.fireExtinguisherCount = Convert.ToInt32(NoOfFireExtinguiser);
                EditableHotworksPermits_sql.fireHoseCount = Convert.ToInt32(NoOfFireHouse);
                #endregion

                EditableHotworksPermits_sql.isEdited = true;


                // update scope               
                if (_fromDraft && !_submitDraftToWorkflow)
                {
                    EditableHotworksPermits_sql.workFlowStatus = "DRAFT";
                }
                else
                {
                    if (StartHotWork > DateTime.Now)
                    {
                        EditableHotworksPermits_sql.workFlowStatus = "UPCOMING";
                    }
                    else if (StartHotWork.Ticks <= DateTime.Now.Ticks && EndHotWork.Ticks >= DateTime.Now.Ticks)
                    {
                        EditableHotworksPermits_sql.workFlowStatus = "ACTIVE";
                    }
                }

                EditableHotworksPermits_sql.location_number = App.SiteId;
                // update scope

                EditableHotworksPermits_sql.scope_Sql = EditableScope_sql;
                EditableHotworksPermits_sql.firePrevention_sql = EditableFirePrevention_sql;
                App.PermitEditableId = EditableHotworksPermits_sql.id;
                connection.UpdateWithChildren(EditableHotworksPermits_sql);
                await Task.Yield();
                CustomControl.Validation.UpdateAlarm(EditableHotworksPermits_sql);
                await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HOTWORKS_SUCCESS_UPDATE, "OK");
                _isUpdate = false;
                if (SendToPrint)
                {
                    //await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HOTWORKS_SUCCESS_UPDATE, AppResource.OK_LABEL);

                    await _navigationService.NavigateAsync(new Uri("/PrintPermitPage", UriKind.Relative), null, null, false);
                    // await _navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/NavigateToPrintPermit", System.UriKind.Relative));
                    //MessagingCenter.Send(Xamarin.Forms.Application.Current, "NavigateToPrintPermit");
                }
                else
                {
                    // await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HOTWORKS_SUCCESS_UPDATE, AppResource.OK_LABEL);
                    // await _navigationService.NavigateAsync(new Uri("/HomePage", UriKind.Relative), null, null, false);
                    await _navigationService.NavigateAsync(new Uri("/PrintPermitPage", UriKind.Relative), null, null, false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("IOException source: {0}", e.Source);
            }
        }

        public async void SubmitHW(string hotworksStatus)
        {
            if (!isCreationInProgress)
            {
                isCreationInProgress = true;

             
                var totalHour = (EndHotWork - StartHotWork).TotalHours;

                if (totalHour < 8)
                {

                }
                else
                {
                    if (totalHour >= 12)
                    {
                        await _pageDialog.DisplayAlertAsync(AppResource.WORK_SHIFT_ERROR_LABEL, AppResource.WORKSHIFT_RESTRICTION_12HRS, AppResource.OK_LABEL);
                        isCreationInProgress = false;
                        //var curpage = (NewHotWorksPage)PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                        //StackLayout TabPanel = curpage.FindByName<StackLayout>("Spworktabs");
                        //TabView tab = (TabView)TabPanel.Children[0];
                        //curpage.SelectTabCommon(tab);
                        
                        return;
                    }
                    else if (totalHour >= 8 && totalHour < 12)
                    {
                       // var result = await _pageDialog.DisplayAlertAsync("Work shift sarning", "Work shift is more than 8 hours. Do you want to continue?", "Ok", "Cancel");
                        //if (!result)
                        //{
                        //    isCreationInProgress = false;
                        //    //var curpage = (NewHotWorksPage)PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                        //    //StackLayout TabPanel = curpage.FindByName<StackLayout>("Spworktabs");
                        //    //TabView tab = (TabView)TabPanel.Children[0];
                        //    //curpage.SelectTabCommon(tab);
                        //    //return;
                            
                        //}
                    }
                    else
                    {
                        await _pageDialog.DisplayAlertAsync(string.Empty, CustomControl.Validation.START_END_DATE_SAME, AppResource.OK_LABEL);
                        isCreationInProgress = false;
                        return;
                    }
                }








                var isDataValide = HotWorkUtility.IsHotworkMandatoryDataValid(StartHotWork, EndHotWork, WorkOrdersLocal, LocationWork);
                if (!isDataValide.Key)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync(string.Empty, isDataValide.Value, AppResource.OK_LABEL);
                    isCreationInProgress = false;
                    var curpage = (NewHotWorksPage)PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                    StackLayout TabPanel = curpage.FindByName<StackLayout>("Spworktabs");
                    TabView tab = (TabView)TabPanel.Children[0];
                    curpage.SelectionTab(tab);
                    return;
                }
                try
                {
                    //In Field Display String Add in Listing
                    var hperson = CurrentHotWorkPerson;
                    var Fdperson = CurrentFireWatchDuringPerson;
                    var Faperson = CurrentFireWatchAfterPerson;

                    if (!string.IsNullOrEmpty(hperson.PersonName))
                    {
                        PersonHotWork.Add(hperson);
                    }
                    if (!string.IsNullOrEmpty(Fdperson.PersonName))
                    {
                        FirewatchDuringHotWork.Add(Fdperson);
                    }
                    if (!string.IsNullOrEmpty(Faperson.PersonName))
                    {
                        FirewatchAfterHotWork.Add(Faperson);
                    }

                    var personListq = new List<ScopePerson_sql>();
                    var workorderList = new List<WorkOrderMaster_sql>();

                    foreach (FirewatchPersonsLocal personHotWork in PersonHotWork)
                    {
                        personListq.Add(new ScopePerson_sql() { PersonName = personHotWork.PersonName });
                    }

                    foreach (var workOrder in WorkOrdersLocal.Where(p => p.isSelected == true).ToList())
                    {
                        workorderList.Add(connection.Table<WorkOrderMaster_sql>().Where(p => p.workOrderName.ToLower() == workOrder.workorderName.ToLower()).FirstOrDefault());
                    }

                    var firewatcherList = new List<FireWatcherPrevention_sql>();

                    for (int i = 0; i < FirewatchDuringHotWork.Count; i++)
                    {

                        firewatcherList.Add(new FireWatcherPrevention_sql
                        {
                            PersonName = FirewatchDuringHotWork[i].PersonName,
                            StartTime = FirewatchDuringHotWork[i].StartTime.ToString("HH:mm:ss"),
                            EndTime = FirewatchDuringHotWork[i].EndTime.ToString("HH:mm:ss"),
                            FireWatchType = "DURING",
                        });
                    }

                    for (int i = 0; i < FirewatchAfterHotWork.Count; i++)
                    {
                        firewatcherList.Add(new FireWatcherPrevention_sql
                        {
                            PersonName = FirewatchAfterHotWork[i].PersonName,
                            StartTime = FirewatchAfterHotWork[i].StartTime.ToString("HH:mm:ss"),
                            EndTime = FirewatchAfterHotWork[i].EndTime.ToString("HH:mm:ss"),
                            FireWatchType = "AFTER",
                        });
                    }

                    var precautionMaster_sql = new List<PrecautionMaster_sql>();

                    foreach (var precautions in PrecautionCollectionLocal.ToList())
                    {

                        if (precautions.PrecautionID == 6 && precautions.PrecautionActions == PrecautionActions.YES)
                        {
                            NoOfFireHouse = Convert.ToInt32(precautions.NoOfFirehouses);
                            NoOfFireExtinguiser = Convert.ToInt32(precautions.NoOfFireExtinguiser);
                        }


                        if (precautions.PrecautionActions == PrecautionActions.YES)
                            precautionMaster_sql.Add(connection.Table<PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.PrecautionID).FirstOrDefault());
                    }

                    string hwstatus = string.Empty;
                    if (hotworksStatus.Equals("Draft"))
                    {
                        hwstatus = "DRAFT";
                    }
                    else
                    {
                        if (StartHotWork > DateTime.Now)
                        {
                            hwstatus = "UPCOMING";
                        }
                        else if (StartHotWork.Ticks <= DateTime.Now.Ticks && EndHotWork.Ticks >= DateTime.Now.Ticks)
                        {
                            hwstatus = "ACTIVE";
                        }
                        else
                        {
                            hwstatus = "EXPIRED";
                        }
                    }

                    //int uniqueId = _incUniqueid.GetUniqueIds().LastOrDefault().UniqueIncrementalId;
                    //var hotWorksHandHeldID = CustomControl.Validation.GetHandHeldId() + "" + (uniqueId + 1).ToString();
                    //_incUniqueid.AddIncUniqueId(new IncUniqueId { UniqueIncrementalId = uniqueId + 1 });

                    var SCOPE = HotWorkUtility.CreateScopeInstanceSQL(DescribeWork, LocationWork, Department, EquipmentDetails, StartHotWork.ToString(), EndHotWork.ToString(), personListq, workorderList, connection);
                    var FIRE_PREVENTION = HotWorkUtility.CreateFirePreventionInstanceSQL(NearPhoneLocation, NearFireAlarmLocation, firewatcherList, connection);

                    var dateAuth = AuthDate == DateTime.MinValue ? DateTime.Now.ToString() : AuthDate.ToString();


                    var AUTHORIZATION = HotWorkUtility.CreateAuthorizationInstanceSQL(AuthName, AuthDepartment, AuthLocation, dateAuth, IsHighHazardArea, AreaSupervisorName, AreaSupervisorDepartement, connection);

                    if (SCOPE != null && FIRE_PREVENTION != null && AUTHORIZATION != null)
                    {
                        var hwinstance = HotWorkUtility.CreateHotWorkInstanceSQL(
                                                   false, true, true, false, hwstatus, "", App.SiteId, 0, Convert.ToInt32(NoOfFireHouse), Convert.ToInt32(0), Convert.ToInt32(NoOfFireExtinguiser),
                                                SCOPE, FIRE_PREVENTION, precautionMaster_sql, AUTHORIZATION, connection);

                        if (hwinstance != null)
                        {
                            App.isDuplicate = false;
                            ShowSubmitHWPoopup = false;
                            await Task.Yield();
                            isCreationInProgress = false;
                            CustomControl.Validation.SetAlarm(hwinstance);
                            await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HOTWORKS_SUCCESS_CREATE, AppResource.OK_LABEL);
                            App.PermitEditableId = connection.Table<HotWorkPermit_sql>().ToList().LastOrDefault().id;
                            MessagingCenter.Send(Xamarin.Forms.Application.Current, "NavigateToPrintPermit");
                            // await _navigationService.NavigateAsync("PrintPermitPage", null, null, false);
                            //await _navigationService.NavigateAsync(new Uri("/PrintPermitPage", UriKind.Relative), null, null, false);

                        }
                        else
                        {
                            isCreationInProgress = false;
                            await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HOTWORKS_NOT_CREATE, AppResource.OK_LABEL);
                        }
                    }
                    else
                    {
                        isCreationInProgress = false;
                        await _pageDialog.DisplayAlertAsync("", CustomControl.Validation.HOTWORKS_NOT_CREATE, AppResource.OK_LABEL);
                    }
                }
                catch (Exception ex)
                {
                    isCreationInProgress = false;
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }
        }

        #endregion

        private NewHotWorksPageViewModel() { }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        PrecautionActions SetStatusprecaution(string Status)
        {
            PrecautionActions action = new PrecautionActions();
            switch (Status)
            {
                case "skip":
                    action = PrecautionActions.SKIP;
                    break;
                case "yes":
                    action = PrecautionActions.YES;
                    break;
                case "na":
                    action = PrecautionActions.NA;
                    break;
            }
            return action;
        }


        public void OnNavigatedTo(NavigationParameters parameters)
        {
            try
            {
                ShowCloseBtn = false;
                ShowDeleteBtn = false;
                ShowScanBtn = false;
                ShowAttachment = false;
                if (parameters.Count > 0)
                {
                    App.PermitFlow = "edit";
                    ActionHotWork = HotWorkAction.Edit;
                    if (parameters.ContainsKey("archiveModel_sql"))
                    {
                        var hotWork = (HotWorkPermit_sql)parameters["archiveModel_sql"];
                        ShowCloseBtn = false;


                        if (hotWork.workFlowStatus.ToLower() == "closed") {
                            ShowCloseLayout = true;
                            CloseAuthorizer = hotWork.CloseAuthorizedBy;
                        }

                        if (!hotWork.IsScannedPermits)
                        {
                            ShowAttachment = false;
                        }
                        else
                        {
                            ShowAttachment = true;
                            App.ArchivePermitScanGuid = hotWork.hotworkGUID;
                        }


                        if (App.isImpairementEditorDuplicate == "Archieve")
                        {
                            _fromArchive = true;
                        }
                        else if (App.isImpairementEditorDuplicate == "Duplicate")
                        {
                            _fromArchive = false;
                            App.isDuplicate = true;
                        }

                        if (App.isDuplicate)
                        {
                            _isUpdate = false;
                        }
                        else
                        {
                            _isUpdate = true;
                        }

                        #region Scope
                        var scope = hotWork.scope_Sql;


                        for (int i = 0;i< scope.workOrderMaster_Sqls.Count();i++)
                        {


                            var workourder  = WorkOrdersLocal.Where(workorderName => workorderName.workorderName == scope.workOrderMaster_Sqls[i].workOrderName).FirstOrDefault();

                            if (workourder != null) {
                                workourder.isSelected = true;
                            }

                            //if (WorkOrdersLocal[i].workorderName == scope.workOrderMaster_Sqls[i].workOrderName) {
                            //    WorkOrdersLocal[i].isSelected = true;
                            //}

                            //WorkOrdersLocal.Where(p => p.workorderName.ToLower() == workorder_sql.workOrderName).FirstOrDefault().isSelected = true; ;
                        }


                        foreach (var personName in scope.scopePerson_Sqls)
                        {
                            PersonHotWork.Add(new FirewatchPersonsLocal
                            {
                                PersonName = personName.PersonName,
                                StartTime = DateTime.MinValue,
                                EndTime = DateTime.MinValue
                            });
                        }

                        DescribeWork = scope.WorkDetail;
                        LocationWork = scope.WorkLocation;
                        Department = scope.CompanyName;
                        EquipmentDetails = scope.EquipmentDescription;

                        // if duplicate date time should be current 
                        if (App.isDuplicate)
                        {
                            ResetPermitButtons(HWStatus.NEW);
                            App.HotEditStartTime = null;
                            App.HotEditEndTime = null;
                            StartHotWork = DateTime.Now;
                            EndHotWork = DateTime.Now.AddHours(1);
                        }
                        else
                        {
                            StartHotWork = Convert.ToDateTime(scope.StartDateTime);
                            EndHotWork = Convert.ToDateTime(scope.EndDateTime);
                        }

                        #endregion

                        #region FirePrevention



                        var firewatcher = hotWork.firePrevention_sql;
                        EditableFirePrevention_sql = firewatcher;

                        try
                        {
                            //var pr = _firePreventionMeasuresService.GetFirePreventionMeasuresByIdAsync(EditableHotworksPermits.FirePreventionMeasures.FirePreventionMeasuresId);

                            FirewatchDuringHotWork = new ObservableCollection<FirewatchPersonsLocal>(from data in firewatcher.fireWatcherPrevention_Sqls.Where(p => p.FireWatchType == "DURING")
                                                                                                select new FirewatchPersonsLocal
                                                                                                {
                                                                                                    PersonName = data.PersonName,
                                                                                                    StartTime = Convert.ToDateTime(data.StartTime),
                                                                                                    EndTime = Convert.ToDateTime(data.EndTime)
                                                                                                });
                            FirewatchAfterHotWork = new ObservableCollection<FirewatchPersonsLocal>(from data in firewatcher.fireWatcherPrevention_Sqls.Where(p => p.FireWatchType == "AFTER")
                                                                                               select new FirewatchPersonsLocal
                                                                                               {
                                                                                                   PersonName = data.PersonName,
                                                                                                   StartTime = Convert.ToDateTime(data.StartTime),
                                                                                                   EndTime = Convert.ToDateTime(data.EndTime)
                                                                                               });
                            //var firePrevention = _firePreventionMeasuresService.GetFirePreventionMeasuresByIdAsync(EditableHotworksPermits.FirePreventionMeasures.FirePreventionMeasuresId);
                            NearPhoneLocation = firewatcher.NearestPhone;
                            NearFireAlarmLocation = firewatcher.NearestFireAlarmLocation;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("IOException source: {0}", e.Source);
                        }

                        #endregion

                        #region Precaution

                        foreach (var permitPrecautions in hotWork.precautionMaster_Sqls)
                        {
                            var item = PrecautionCollectionLocal.Where(s => s.PrecautionID == permitPrecautions.id).FirstOrDefault();
                            if (item != null)
                            {
                                item.PrecautionActions = PrecautionActions.YES;
                                item.NoOfFirehouses = 0;
                                item.NoOfFireExtinguiser = 0;
                            }
                            if (item.PrecautionID == 6&& item.PrecautionActions==PrecautionActions.YES)
                            {
                                NoOfFireHouse = Convert.ToInt32(hotWork.fireHoseCount );
                                NoOfFireExtinguiser = Convert.ToInt32(hotWork.fireExtinguisherCount );
                            }
                        }

                        #endregion

                        #region Authorization

                        var auth = hotWork.authorization_sql;
                        AuthName = auth.AuthorizerName;
                        AuthDepartment = auth.Department;
                        AuthLocation = auth.Location;
                        AuthDate = Convert.ToDateTime(auth.AuthorizationDateTime);
                        IsHighHazardArea = auth.IsHighHazardArea;
                        AreaSupervisorDepartement = auth.AreaSupervisorDepartment;
                        AreaSupervisorName = auth.AreaSupervisorName;

                        #endregion


                        HWUniqueNumberLabel = "No. " + hotWork.server_id.ToString();
                        //HWUniqueNumberLabel = "No. " + EditableHotworksPermits.HandHeldId.ToString();
                    }
                    else if (parameters.ContainsKey("hotworkPermit_sql"))
                    {

                        if (App.isImpairementEditorDuplicate.ToLower() == "edit")
                        {
                            ShowScanBtn = true;
                        }
                        else
                        {
                            ShowScanBtn = false;
                        }
                        var modalClass = (HotWorkPermit_sql)parameters["hotworkPermit_sql"];
                        EditableHotworksPermits_sql = modalClass;
                        App.ArchivePermitScanGuid = modalClass.hotworkGUID;
                        //if (EditableHotworksPermits_sql.ScannedPaperPermit == null)
                        //{
                        //    ShowAttachment = false;
                        //}
                        //else {
                        //    ShowAttachment = true;
                        //}

                        if (modalClass.workFlowStatus.ToLower() == "closed")
                        {
                            ShowCloseLayout = true;
                            CloseAuthorizer = modalClass.CloseAuthorizedBy;
                        }

                        HWUniqueNumberLabel = modalClass.server_id == 0 ? string.Empty : "No. " + modalClass.server_id;

                        if (modalClass.workFlowStatus.ToLower().Equals("expired") || modalClass.workFlowStatus.ToLower().Equals("closed"))
                        {
                            if (modalClass.IsScannedPermits)
                            {
                                ShowAttachment = true;
                            }
                            else {
                                ShowAttachment = false;
                            }
                            _fromArchive = true;
                            ShowScanBtn = false;
                            //ShowCloseBtn = false;
                        }
                        else
                        {
                            //ShowScanBtn = true;
                            if (modalClass.workFlowStatus.ToLower().Equals("draft")) {
                                ShowDeleteBtn = true;
                                ShowScanBtn = false;
                            } else {
                                ShowDeleteBtn = false;

                                if (modalClass.workFlowStatus.ToLower().Equals("active"))
                                {
                                    ShowScanBtn = true;
                                }
                                else {
                                    ShowScanBtn = false;
                                }
                                


                            }

                            _fromArchive = false;
                        }
                       ResetPermitButtons(HotWorkUtility.GetWorkFlowStatus(modalClass.workFlowStatus));

                        //setting scope value
                        var scopee = connection.GetWithChildren<Scope_sql>(modalClass.scope_Sql.id, recursive: true);

                        EditableScope_sql = scopee;
                        foreach (WorkOrderMaster_sql workorder_sql in scopee.workOrderMaster_Sqls)
                        {
                            WorkOrdersLocal.Where(p => p.workorderName.ToLower() == workorder_sql.workOrderName.ToLower()).First().isSelected = true;
                        }

                        foreach (var personName in scopee.scopePerson_Sqls)
                        {
                            PersonHotWork.Add(new FirewatchPersonsLocal
                            {
                                PersonName = personName.PersonName,
                                StartTime = DateTime.MinValue,
                                EndTime = DateTime.MinValue
                            });
                        }

                        DescribeWork = scopee.WorkDetail;
                        LocationWork = scopee.WorkLocation;
                        Department = scopee.CompanyName;
                        EquipmentDetails = scopee.EquipmentDescription;
                        // if duplicate date time should be current 
                        if (App.isDuplicate)
                        {
                           ResetPermitButtons(HWStatus.NEW);
                            App.HotEditStartTime = null;
                            App.HotEditEndTime = null;
                            StartHotWork = DateTime.Now;
                            EndHotWork = DateTime.Now.AddHours(1);
                        }
                        else
                        {
                            ShowCloseBtn = true;
                            StartHotWork = Convert.ToDateTime(scopee.StartDateTime);
                            EndHotWork = Convert.ToDateTime(scopee.EndDateTime);
                        }

                        App.HotEditStartTime = StartHotWork;
                        App.HotEditEndTime = EndHotWork;


                        if (modalClass.workFlowStatus.ToLower().Equals("expired") || modalClass.workFlowStatus.ToLower().Equals("closed"))
                        {
                            ShowCloseBtn = false;
                        }
                        else {
                            ShowCloseBtn = true;
                        }


                            var firewatcher = connection.GetWithChildren<FirePrevention_sql>(modalClass.firePrevention_sql.id, recursive: true);
                        EditableFirePrevention_sql = firewatcher;

                        try
                        {
                            //var pr = _firePreventionMeasuresService.GetFirePreventionMeasuresByIdAsync(EditableHotworksPermits.FirePreventionMeasures.FirePreventionMeasuresId);

                            FirewatchDuringHotWork = new ObservableCollection<FirewatchPersonsLocal>(from data in firewatcher.fireWatcherPrevention_Sqls.Where(p => p.FireWatchType == "DURING")
                                                                                                select new FirewatchPersonsLocal
                                                                                                {
                                                                                                    PersonName = data.PersonName,
                                                                                                    StartTime = Convert.ToDateTime(data.StartTime),
                                                                                                    EndTime = Convert.ToDateTime(data.EndTime)
                                                                                                });
                            FirewatchAfterHotWork = new ObservableCollection<FirewatchPersonsLocal>(from data in firewatcher.fireWatcherPrevention_Sqls.Where(p => p.FireWatchType == "AFTER")
                                                                                               select new FirewatchPersonsLocal
                                                                                               {
                                                                                                   PersonName = data.PersonName,
                                                                                                   StartTime = Convert.ToDateTime(data.StartTime),
                                                                                                   EndTime = Convert.ToDateTime(data.EndTime)
                                                                                               });
                            //var firePrevention = _firePreventionMeasuresService.GetFirePreventionMeasuresByIdAsync(EditableHotworksPermits.FirePreventionMeasures.FirePreventionMeasuresId);
                            NearPhoneLocation = firewatcher.NearestPhone;
                            NearFireAlarmLocation = firewatcher.NearestFireAlarmLocation;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("IOException source: {0}", e.Source);
                        }


                        foreach (var permitPrecautions in modalClass.precautionMaster_Sqls)
                        {
                            var item = PrecautionCollectionLocal.Where(s => s.PrecautionID == permitPrecautions.id).FirstOrDefault();
                            if (item != null)
                            {
                                item.PrecautionActions = PrecautionActions.YES;
                                item.NoOfFirehouses = 0;
                                item.NoOfFireExtinguiser = 0;
                            }
                            if (item.PrecautionID == 6&& item.PrecautionActions==PrecautionActions.YES)
                            {
                                item.NoOfFirehouses = modalClass.fireHoseCount ;
                                item.NoOfFireExtinguiser = modalClass.fireExtinguisherCount;
                            }
                        }

                        EditableAuthorization_Sql = modalClass.authorization_sql;
                        AuthName = EditableAuthorization_Sql.AuthorizerName;
                        AuthDepartment = EditableAuthorization_Sql.Department;
                        AuthLocation = EditableAuthorization_Sql.Location;
                        AuthDate = Convert.ToDateTime(EditableAuthorization_Sql.AuthorizationDateTime);
                        IsHighHazardArea = EditableAuthorization_Sql.IsHighHazardArea;
                        AreaSupervisorDepartement = EditableAuthorization_Sql.AreaSupervisorDepartment;
                        AreaSupervisorName = EditableAuthorization_Sql.AreaSupervisorName;
                    }
                }
                else
                {
                    App.PermitFlow = "new";
                    ActionHotWork = HotWorkAction.Add;
                   ResetPermitButtons(HWStatus.NEW);
                    App.HotEditStartTime = null;
                    App.HotEditEndTime = null;
                 //   int uniqueId = _incUniqueid.GetUniqueIds().LastOrDefault().UniqueIncrementalId;
                    HWUniqueNumberLabel = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
        }

        private void ResetPermitButtons(HWStatus hWStatus)
        {
            switch (hWStatus)
            {
                case HWStatus.ACTIVE:
                    ShowDraftBtn = false;
                    ShowSaveBtn = true;
                    break;
                case HWStatus.ARCHIVE:
                    ShowDraftBtn = false;
                    ShowSaveBtn = false;
                    break;
                case HWStatus.DRAFT:
                    ShowDraftBtn = false;
                    ShowSaveBtn = true;
                    break;
                case HWStatus.EXPIRED:
                    ShowDraftBtn = false;
                    ShowSaveBtn = false;
                    break;
                case HWStatus.UPCOMING:
                    ShowDraftBtn = false;
                    ShowSaveBtn = true;
                    break;
                case HWStatus.NEW:
                    ShowDraftBtn = true;
                    ShowSaveBtn = false;
                    break;
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }

    public  class WorkOrderMasterLocal
    {
       public string workorderName { get; set; }
       public  bool isSelected { get; set; }
    }

    public class PrecautionsMasterLocal
    {
        public string Description { get; set; }
        public int NoOfFirehouses { get; set; }
        public int PrecautionID { get; set; }
        public int NoOfFireExtinguiser { get; set; }
        public PrecautionActions PrecautionActions { get; set; }
    }

    public class FirewatchPersonsLocal {
        public string PersonName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public FireWatcherType FireWatcherType { get; set; }
    }

    public enum HotWorkAction
    {
        Add,
        Edit
    }
}