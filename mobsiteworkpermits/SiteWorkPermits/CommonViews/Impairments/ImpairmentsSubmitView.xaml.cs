﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SiteWorkPermits.ViewModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class ImpairmentsSubmitView : ContentView
    {

        public event EventHandler RestoreImpairmentEvent;
        public event EventHandler CancelImpairmentEvent;
        public ImpairmentsSubmitView()
        {
            InitializeComponent();
            StartDatePicker.Dateselected += StartDatePicker_Dateselected;
            StartTimePicker.Timeselected += StartTimePicker_Timeselected;
        }

        private void Handle_BindingContextChanged(object sender, EventArgs e)
        {
            if(this.BindingContext != null)
            {
                if(this.BindingContext is ImpairmentsViewModel)
                {
                    var val = (ImpairmentsViewModel)this.BindingContext;
                    if (StartDatePicker.Text == string.Empty && StartTimePicker.Text == string.Empty)
                    {
                        StartTimePicker.Text = val.ClosedDate.ToString("HH:mm");
                        StartDatePicker.Text = val.ClosedDate.ToString("dd/MM/yyyy");
                    }   
                }
                else
                {
                    var val = (HomePageViewModel)this.BindingContext;
                    if (StartDatePicker.Text == string.Empty && StartTimePicker.Text == string.Empty)
                    {
                        StartTimePicker.Text = val.ClosedDate.ToString("HH:mm");
                        StartDatePicker.Text = val.ClosedDate.ToString("dd/MM/yyyy");
                    }
                }

            }
        }
                                          
        private void StartTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {  
           
            if (this.BindingContext is ImpairmentsViewModel)
            {
                var vm = (ImpairmentsViewModel)this.BindingContext;
                if (string.IsNullOrEmpty(StartDatePicker.Text))
                {
                    vm._pageDialog.DisplayAlertAsync(string.Empty, "Please Select Date First.", "Ok");
                    return;
                }
                if (vm.ClosedDate.Date == Convert.ToDateTime(vm.ImpStartDate).Date)
                {
                    if (e.NewTime.TimeOfDay > Convert.ToDateTime(vm.ImpStartDate).TimeOfDay)
                    {
                        vm.ClosedDate = vm.ClosedDate.Date + e.NewTime.TimeOfDay;
                        StartTimePicker.Text = e.NewTime.ToString("HH:mm");
                    }
                    else
                    {
                        vm._pageDialog.DisplayAlertAsync(string.Empty, "Close Date Should be greater than Start Date", "Ok");
                    }
                }
                else
                {
                    vm.ClosedDate = vm.ClosedDate.Date + e.NewTime.TimeOfDay;
                    StartTimePicker.Text = e.NewTime.ToString("HH:mm");
                }
            }
            else
            {
                var vm = (HomePageViewModel)this.BindingContext;
                if (string.IsNullOrEmpty(StartDatePicker.Text))
                {
                    vm._pageDialog.DisplayAlertAsync(string.Empty, "Please Select Date First.", "Ok");
                    return;
                }

                if (vm.ClosedDate.Date == Convert.ToDateTime(vm.ImpStartDate).Date)
                {
                    if (e.NewTime.TimeOfDay > Convert.ToDateTime(vm.ImpStartDate).TimeOfDay)
                    {
                        vm.ClosedDate = vm.ClosedDate.Date + e.NewTime.TimeOfDay;
                        StartTimePicker.Text = e.NewTime.ToString("HH:mm");
                    }
                    else
                    {
                        vm._pageDialog.DisplayAlertAsync(string.Empty, "Close Date Should be greater than Start Date", "Ok");
                    }
                }
                else
                {
                    vm.ClosedDate = vm.ClosedDate.Date + e.NewTime.TimeOfDay;
                    StartTimePicker.Text = e.NewTime.ToString("HH:mm");
                }                                            
            }                      
        }

        private void StartDatePicker_Dateselected(object sender, CustomControl.DateChangeArgs e)
        {           
            if (this.BindingContext is ImpairmentsViewModel)
            {
                var vm = (ImpairmentsViewModel)this.BindingContext; 

                DateTime _newDate = new DateTime(e.Newdate.Date.Year, e.Newdate.Date.Month, e.Newdate.Date.Day);
                DateTime _ImpStartDate = new DateTime(Convert.ToDateTime(vm.ImpStartDate).Year, Convert.ToDateTime(vm.ImpStartDate).Month, Convert.ToDateTime(vm.ImpStartDate).Day);

                if (_newDate >= _ImpStartDate)
                {
                    vm.ClosedDate = e.Newdate.Date + vm.ClosedDate.TimeOfDay;
                    StartDatePicker.Text = e.Newdate.ToString("dd/MM/yyyy");
                }
                else
                {
                    vm._pageDialog.DisplayAlertAsync(string.Empty, "Close Date Should be greater than Start Date", "Ok");
                }
            }
            else
            {
                var vm = (HomePageViewModel)this.BindingContext;

                DateTime _newDate = new DateTime(e.Newdate.Date.Year, e.Newdate.Date.Month, e.Newdate.Date.Day);
                DateTime _ImpStartDate =    new DateTime(Convert.ToDateTime(vm.ImpStartDate).Year,Convert.ToDateTime(vm.ImpStartDate).Month, Convert.ToDateTime(vm.ImpStartDate).Day);

                if (_newDate >= _ImpStartDate)
                {
                    vm.ClosedDate = e.Newdate.Date + vm.ClosedDate.TimeOfDay;
                    StartDatePicker.Text = e.Newdate.ToString("dd/MM/yyyy");
                }
                else
                {
                    vm._pageDialog.DisplayAlertAsync(string.Empty, "Close Date Should be greater than Start Date", "Ok");
                }



                //if (e.Newdate.Date >= Convert.ToDateTime(vm.ImpStartDate))
                //{
                //    vm.ClosedDate = e.Newdate.Date + vm.ClosedDate.TimeOfDay;
                //    StartDatePicker.Text = e.Newdate.ToString("dd/MM/yyyy");
                //}
                //else
                //{
                //    vm._pageDialog.DisplayAlertAsync(string.Empty, "Close Date Should be greater than Start Date", "Ok");
                //}
            }
            //vm.ClosedDate =vm.ClosedDate.Date + e.Newdate.TimeOfDay;
        }

        private void RestoreImpairments_Tapped(object sender, System.EventArgs e)
        {
            RestoreImpairmentEvent.Invoke(this, new EventArgs());

            if (this.BindingContext is ImpairmentsViewModel)
            {
                var vm = (ImpairmentsViewModel)this.BindingContext;
              //  vm.CloseImpairment();
            }
            else
            {
                var vm = (HomePageViewModel)this.BindingContext;
              //  vm.CloseImpairment();
            }
        }

        private void Cancel_Tapped(object sender, System.EventArgs e)
        {            
            CancelImpairmentEvent.Invoke(this, new EventArgs());           
        }

        private void PopupInside_Tapped(object sender, System.EventArgs e)
        {
            
        }
    }
}
