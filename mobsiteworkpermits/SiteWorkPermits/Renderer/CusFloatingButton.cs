﻿using System;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusFloatingButton :Button
    {
        public static BindableProperty ButtonColorProperty = BindableProperty.Create<CusFloatingButton, Color>(o => o.ButtonColor, Color.Pink);

        public Color ButtonColor
        {
            get
            {
                return (Color)GetValue(ButtonColorProperty);
            }
            set
            {
                SetValue(ButtonColorProperty, value);
            }
        }              
    }

    public class CusFloatButton : Grid
    {

        public EventHandler<EventArgs> Clicked;
        public static BindableProperty ButtonColorProperty = BindableProperty.Create<CusFloatButton, Color>(o => o.ButtonColor, Color.Pink);
              
        public Color ButtonColor
        {
            get
            {
                return (Color)GetValue(ButtonColorProperty);
            }
            set
            {
                SetValue(ButtonColorProperty, value);
            }
        }
    }
}
