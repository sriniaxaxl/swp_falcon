﻿using SiteWorkPermits.Resx;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase
{
    public class SQLiteDatabase
    {
        private SQLiteConnection _connection;

        public SQLiteDatabase()
        {

            var _connectionpath = DependencyService.Get<ISQLite>().GetConnectionPath();
            if (_connection == null)
            {
                _connection = new SQLiteConnection(_connectionpath);
            }

            _connection.CreateTable<HotWorkPermit_sql>();
            _connection.CreateTable<Scope_sql>();
            _connection.CreateTable<ScopePerson_sql>();
            _connection.CreateTable<WorkOrderMaster_sql>();
            _connection.CreateTable<FirePrevention_sql>();
            _connection.CreateTable<FireWatcherPrevention_sql>();
            _connection.CreateTable<Authorization_sql>();
            _connection.CreateTable<ScopeWorkOrder_sql>();
            _connection.CreateTable<PrecautionMaster_sql>();
            _connection.CreateTable<HotworkPrecautionMeasure_sql>();

            _connection.CreateTable<Impairment_Sql>();
            _connection.CreateTable<MajorImpairmentMaster_sql>();
            _connection.CreateTable<ImpairmentMajorImpairmentMaster_sql>();
            _connection.CreateTable<ReporterDetail_sql>();
            _connection.CreateTable<ImpairmentType_Sql>();
            _connection.CreateTable<ImpairmentClassType_Sql>();
            _connection.CreateTable<ShutDownReason_Sql>();
            _connection.CreateTable<Impairment_PrecautionMaster_sql>();
            _connection.CreateTable<ImpairmentPrecautionMeasure_sql>();


            var workOrderMaster = (from t in _connection.Table<WorkOrderMaster_sql>()
                                   select t).ToList();
            if (workOrderMaster.Count == 0)
            {
                _connection.InsertAll(new List<WorkOrderMaster_sql>() {
                    new WorkOrderMaster_sql { workOrderName = "Thawing" },
                    new WorkOrderMaster_sql { workOrderName = "Grinding" },
                    new WorkOrderMaster_sql { workOrderName = "Brazing" },
                    new WorkOrderMaster_sql { workOrderName = "Cutting" },
                    new WorkOrderMaster_sql { workOrderName = "Welding" },
                    new WorkOrderMaster_sql { workOrderName = "Other" }
                });
            }

            var hotworkPrecautionMaster = (from t in _connection.Table<PrecautionMaster_sql>()
                                           select t).ToList();
            if (hotworkPrecautionMaster.Count == 0)
            {
                _connection.InsertAll(new List<PrecautionMaster_sql>() {
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION1 },
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION2},
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION3 },
                new PrecautionMaster_sql {  precautionName =  AppResource.HW_PRECAUTION4},
                new PrecautionMaster_sql {  precautionName =  AppResource.HW_PRECAUTION5},
                new PrecautionMaster_sql {  precautionName =  AppResource.HW_PRECAUTION6},
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION7},
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION8},
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION9},
                new PrecautionMaster_sql {  precautionName = AppResource.HW_PRECAUTION10}
                });
            }


            var impairmentType_sql = (from t in _connection.Table<ImpairmentType_Sql>()
                                      select t).ToList();
            if (impairmentType_sql.Count == 0)
            {
                _connection.InsertAll(new List<ImpairmentType_Sql>() {
                      new ImpairmentType_Sql {  TypeDescription = AppResource.IMP_TYPE_1_PLACEHOLDER},
                      new ImpairmentType_Sql {  TypeDescription = AppResource.IMP_TYPE_2_PLACEHOLDER},
                      new ImpairmentType_Sql {  TypeDescription = AppResource.IMP_TYPE_3_PLACEHOLDER },
                      new ImpairmentType_Sql {  TypeDescription = AppResource.IMP_TYPE_4_PLACEHOLDER },
                      new ImpairmentType_Sql {  TypeDescription = AppResource.IMP_TYPE_5_PLACEHOLDER},
                });
            }


            var impairmentClassType_sql = (from t in _connection.Table<ImpairmentClassType_Sql>()
                                           select t).ToList();
            if (impairmentClassType_sql.Count == 0)
            {
                _connection.InsertAll(new List<ImpairmentClassType_Sql>() {
                      new ImpairmentClassType_Sql {  ClassDescription = AppResource.IMP_CLASS_1_PLACEHOLDER},
                new ImpairmentClassType_Sql {  ClassDescription = AppResource.IMP_CLASS_2_PLACEHOLDER},
                new ImpairmentClassType_Sql {  ClassDescription = AppResource.IMP_CLASS_3_PLACEHOLDER },
                new ImpairmentClassType_Sql {  ClassDescription = AppResource.IMP_CLASS_4_PLACEHOLDER },
                new ImpairmentClassType_Sql {  ClassDescription = AppResource.IMP_CLASS_5_PLACEHOLDER},
                new ImpairmentClassType_Sql { ClassDescription = AppResource.IMP_CLASS_6_PLACEHOLDER},
                new ImpairmentClassType_Sql {  ClassDescription =AppResource.IMP_CLASS_7_PLACEHOLDER },
                new ImpairmentClassType_Sql { ClassDescription = AppResource.IMP_CLASS_8_PLACEHOLDER},
                });
            }


            var shutDownReason_Sqls = (from t in _connection.Table<ShutDownReason_Sql>()
                                       select t).ToList();
            if (shutDownReason_Sqls.Count == 0)
            {
                _connection.InsertAll(new List<ShutDownReason_Sql>() {
                new ShutDownReason_Sql { ClassDescription = AppResource.IMP_SHUTDOWN_REASON_1_PLACEHOLDER},
                new ShutDownReason_Sql { ClassDescription = AppResource.IMP_SHUTDOWN_REASON_2_PLACEHOLDER},
                new ShutDownReason_Sql {  ClassDescription = AppResource.IMP_SHUTDOWN_REASON_3_PLACEHOLDER },
                new ShutDownReason_Sql {  ClassDescription = AppResource.IMP_SHUTDOWN_REASON_4_PLACEHOLDER },
                new ShutDownReason_Sql { ClassDescription = AppResource.IMP_SHUTDOWN_REASON_5_PLACEHOLDER},
                new ShutDownReason_Sql { ClassDescription = AppResource.IMP_SHUTDOWN_REASON_6_PLACEHOLDER},
            });
            }

            var majorImpairmentMaster_Sqls = (from t in _connection.Table<MajorImpairmentMaster_sql>()
                                              select t).ToList();
            if (majorImpairmentMaster_Sqls.Count == 0)
            {
                _connection.InsertAll(new List<MajorImpairmentMaster_sql>() {
                new MajorImpairmentMaster_sql { majorImpairmentMeasureDescription = AppResource.IMP_MAJOR_IMP_1},
                new MajorImpairmentMaster_sql { majorImpairmentMeasureDescription = AppResource.IMP_MAJOR_IMP_2},
                new MajorImpairmentMaster_sql {  majorImpairmentMeasureDescription = AppResource.IMP_MAJOR_IMP_3 },
                new MajorImpairmentMaster_sql {  majorImpairmentMeasureDescription = AppResource.IMP_MAJOR_IMP_4 },
            });
            }

            var impairment_PrecautionMaster_Sqls = (from t in _connection.Table<Impairment_PrecautionMaster_sql>()
                                                    select t).ToList();
            if (impairment_PrecautionMaster_Sqls.Count == 0)
            {
                _connection.InsertAll(new List<Impairment_PrecautionMaster_sql>() {

                    new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_1},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_2},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_3 },
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_4 },
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_5},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_6},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_7},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_8},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_9},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_10},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_11},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_12},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_13},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_14},
                new Impairment_PrecautionMaster_sql {  precautionName = AppResource.IMP_PRECAUTION_15},
                });
            }
        }

        public SQLiteConnection GetdbConnection()
        {

            var _connectionpath = DependencyService.Get<ISQLite>().GetConnectionPath();
            if (_connection == null)
            {
                _connection = new SQLiteConnection(_connectionpath);
            }
            return _connection;
        }
    }
}
