﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiteWorkPermits.DAL.RestHelper
{
   public class RequestJSONUtil
    {

        public static string GetLoginJSON(string accountNumber, string siteNumber) {
           return "{\"IsHandHeldNumberRequired\":\"true\",\"AccountNumber\":" + accountNumber + ",\"SiteNumber\":" + siteNumber + "}";
        }

    }
}
