﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SiteWorkPermits.CommonViews.HotWorksTemplate
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HotWorksListViewTemplate : ContentView
	{


        public static readonly BindableProperty WorkOrderTextProperty = BindableProperty.Create(
                                                        propertyName: "WorkOrderText",
                                                        returnType: typeof(string),
                                                        declaringType: typeof(HotWorksListViewTemplate),
                                                        defaultValue: "",
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: WorkOrderTextPropertyChanged);

        public string WorkOrderText
        {
            get { return base.GetValue(WorkOrderTextProperty).ToString(); }
            set { base.SetValue(WorkOrderTextProperty, value); }
        }

        private static void WorkOrderTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (HotWorksListViewTemplate)bindable;
            control.hw_workorders.Text = newValue.ToString();
        }




        public static readonly BindableProperty HotWorkTimeTextProperty = BindableProperty.Create(
                                                        propertyName: "HotWorkTimeText",
                                                        returnType: typeof(string),
                                                        declaringType: typeof(HotWorksListViewTemplate),
                                                        defaultValue: "",
                                                        defaultBindingMode: BindingMode.TwoWay,
                                                        propertyChanged: HotWorkTimeTextPropertyChanged);

        public string HotWorkTimeText
        {
            get { return base.GetValue(HotWorkTimeTextProperty).ToString(); }
            set { base.SetValue(HotWorkTimeTextProperty, value); }
        }

        private static void HotWorkTimeTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (HotWorksListViewTemplate)bindable;
            control.hw_date_time.Text = newValue.ToString();
        }



        public static readonly BindableProperty HotWorkLocationTextProperty = BindableProperty.Create(
                                                       propertyName: "HotWorkLocationText",
                                                       returnType: typeof(string),
                                                       declaringType: typeof(HotWorksListViewTemplate),
                                                       defaultValue: "",
                                                       defaultBindingMode: BindingMode.TwoWay,
                                                       propertyChanged: HotWorkLocationTextPropertyChanged);

        public string HotWorkLocationText
        {
            get { return base.GetValue(HotWorkLocationTextProperty).ToString(); }
            set { base.SetValue(HotWorkLocationTextProperty, value); }
        }

        private static void HotWorkLocationTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (HotWorksListViewTemplate)bindable;
            control.hw_location.Text = newValue.ToString();
        }



        public static readonly BindableProperty HotWorkButtonTextProperty = BindableProperty.Create(
                                                       propertyName: "HotWorkButtonText",
                                                       returnType: typeof(string),
                                                       declaringType: typeof(HotWorksListViewTemplate),
                                                       defaultValue: "",
                                                       defaultBindingMode: BindingMode.TwoWay,
                                                       propertyChanged: HotWorkButtonTextPropertyChanged);

        public string HotWorkButtonText
        {
            get { return base.GetValue(HotWorkButtonTextProperty).ToString(); }
            set { base.SetValue(HotWorkButtonTextProperty, value); }
        }

        private static void HotWorkButtonTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (HotWorksListViewTemplate)bindable;
            control.hw_btn.Text = newValue.ToString();
        }

        public HotWorksListViewTemplate ()
		{
			InitializeComponent ();
		}
	}
}