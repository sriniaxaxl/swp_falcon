﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Foundation;
using SiteWorkPermits.iOS.Platform;
using SiteWorkPermits.Platform;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileUtility))]
namespace SiteWorkPermits.iOS.Platform
{
    public class FileUtility : IFileUtility
    {
        UIImagePickerController imagePicker;
        TaskCompletionSource<byte[]> cameraImagePath;
        public async Task<byte[]> GetImageWithCamera()
        {
             cameraImagePath = new TaskCompletionSource<byte[]>();
            CancellationToken? token = null;
            // requestStop = false;
            if (!token.HasValue)
                token = CancellationToken.None;
            // We create a TaskCompletionSource of decimal
            var taskCompletionSource = new TaskCompletionSource<byte[]>();
            // Registering a lambda into the cancellationToken
            token.Value.Register(() =>
            {
                //    requestStop = true;
                taskCompletionSource.TrySetCanceled();
            });
            // _isLoading = true;
            var task = GetImageFromCamera();
            // Wait for the first task to finish among the two
            var completedTask = await Task.WhenAny(task, taskCompletionSource.Task);
            //_isLoading = false;
            return await cameraImagePath.Task;
        }

        private async Task GetImageFromCamera()
        {
            if (true)
            {
                //Create an image picker object
                imagePicker = new UIImagePickerController { SourceType = UIImagePickerControllerSourceType.Camera };
                //Make sure we can find the top most view controller to launch the camera
                var window = UIApplication.SharedApplication.KeyWindow;
                var vc = window.RootViewController;
                while (vc.PresentedViewController != null)
                {
                    vc = vc.PresentedViewController;
                }
                vc.PresentViewController(imagePicker, true, null);
                imagePicker.FinishedPickingMedia += Handle_FinishedPickingMedia;
                imagePicker.Canceled += Handle_Canceled;
            }
            else
            {
                //permission denied
            }
        }

        protected void Handle_FinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs e)
        {
            // determine what was selected, video or image
            bool isImage = false;
            switch (e.Info[UIImagePickerController.MediaType].ToString())
            {
                case "public.image":
                    Console.WriteLine("Image selected");
                    isImage = true;
                    break;
                case "public.video":
                    Console.WriteLine("Video selected");
                    break;
            }
            // get common info (shared between images and video)
            NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;
            if (referenceURL != null)
                Console.WriteLine("Url:" + referenceURL.ToString());
            // if it was an image, get the other image info
            if (isImage)
            {
                // get the original image
                UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
                if (originalImage != null)
                {
                    // do something with the image
                    Console.WriteLine("got the original image");
                    var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    string pngFilename = System.IO.Path.Combine(documentsDirectory, "Photo.png");
                    NSData imgData = originalImage.AsJPEG();
                    NSError err = null;
                    if (imgData.Save(pngFilename, false, out err))
                    {
                        Console.WriteLine("saved as " + pngFilename);

                        File.ReadAllBytes(pngFilename);
                        cameraImagePath.TrySetResult(File.ReadAllBytes(pngFilename));
                    }
                    else
                    {
                        Console.WriteLine("NOT saved as" + pngFilename + " because" + err.LocalizedDescription);
                        cameraImagePath.TrySetResult(null);
                    }
                }
                else
                { // if it’s a video
                  // get video url
                    NSUrl mediaURL = e.Info[UIImagePickerController.MediaURL] as NSUrl;
                    if (mediaURL != null)
                    {
                        Console.WriteLine(mediaURL.ToString());
                    }
                }

                imagePicker.DismissViewController(true, null);
            }
        }

        void Handle_Canceled(object sender, EventArgs e)
        {
            cameraImagePath.TrySetResult(null);
            imagePicker.DismissViewController(true, null);
        }
    }
}