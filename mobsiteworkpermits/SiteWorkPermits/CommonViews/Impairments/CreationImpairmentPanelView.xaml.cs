﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class CreationImpairmentPanelView : ContentView
    {
        public EventHandler RaiseImpairmentEvent;
        public CreationImpairmentPanelView()
        {
            InitializeComponent();
        }

        void Submit_Tapped(object sender, System.EventArgs e)
        {
            if(RaiseImpairmentEvent != null)
            {
                RaiseImpairmentEvent.Invoke(this, new EventArgs());
            }
        }

        public View SectionView
        {
            get
            {
                return Pageview.Content;
            }
            set
            {
                Pageview.Content = value;
            }
        }
    }
}
