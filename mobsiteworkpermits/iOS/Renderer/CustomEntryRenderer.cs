﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using SiteWorkPermits;
using SiteWorkPermits.iOS;
using CoreGraphics;
using CustomControl;
using CustomControl.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusEntry), typeof(CustomEntryRenderer))]
namespace CustomControl.iOS.Renderer
{
    public class CustomEntryRenderer : EntryRenderer
    {
        CusEntry FormEntry;
        EasyTipView.EasyTipView _myTooltip;
        UIImageView vimg;
        UIButton btnright;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            FormEntry = Element as CusEntry;
            Control.TextColor = UIColor.FromRGB(0, 0, 0);
            SetBackgroud();
            SetBorder();
            SetPadding();
            Control.BorderStyle = UITextBorderStyle.None;
            Control.ClipsToBounds = true;
            if (FormEntry.LeftImage != null)
            {
                UIImage imgerror = UIImage.FromBundle("user.png");
                vimg = new UIImageView(new CGRect(0, 0, 30, 30));
                vimg.Image = imgerror;
                btnright = new UIButton(new CGRect(0, 0, 30, 30));
                btnright.SetBackgroundImage(imgerror, UIControlState.Normal);
                btnright.TouchUpInside += Btnright_TouchUpInside;
                UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(Tap);
                tapGesture.NumberOfTapsRequired = 1;
                vimg.AddGestureRecognizer(tapGesture);
                Control.RightView = btnright;
                Control.UserInteractionEnabled = true;
            }
            _myTooltip = new EasyTipView.EasyTipView();
            _myTooltip.ForegroundColor = UIColor.White;
            _myTooltip.ArrowPosition = EasyTipView.ArrowPosition.Top;
            Control.EditingDidEnd += Control_EditingDidEnd;
        }


        void Control_EditingDidEnd(object sender, EventArgs e)
        {
            if (FormEntry.Validation == CusEntry.ValidationType.Email)
            {
                var checkvalid = Validation.Emailvalid(FormEntry.Text);
                if (!checkvalid)
                {
                    _myTooltip.Hidden = false;
                    _myTooltip.Text = new Foundation.NSString("Invalid Email.");
                    //_myTooltip.Show(Control, AppDelegate.window.RootViewController.View, true);
                }
                else
                {
                    _myTooltip.Hidden = true;
                }
            }
            else if (FormEntry.Validation == CusEntry.ValidationType.Numeric)
            {
                var checkvalid = Validation.Numericvalid(FormEntry.Text);
                if (!checkvalid)
                {
                    _myTooltip.Hidden = false;
                    _myTooltip.Text = new Foundation.NSString("Invalid Number.");
                    //_myTooltip.Show(Control, AppDelegate.window.RootViewController.View, true);
                }
                else
                {
                    _myTooltip.Hidden = true;
                }
            }
            else if (FormEntry.Validation == CusEntry.ValidationType.MobileNumber)
            {
                var checkvalid = Validation.Mobilevalid(FormEntry.Text);
                if (!checkvalid)
                {
                    _myTooltip.Hidden = false;
                    _myTooltip.Text = new Foundation.NSString("Invalid Mobile Number.");
                    //_myTooltip.Show(Control, AppDelegate.window.RootViewController.View, true);
                }
                else
                {
                    _myTooltip.Hidden = true;
                }
            }
            else if (FormEntry.Validation == CusEntry.ValidationType.Length)
            {
                var checkvalid = Validation.Lengthvalid(FormEntry.Text, FormEntry.TextLength);
                if (!checkvalid)
                {
                    _myTooltip.Hidden = false;
                    _myTooltip.Text = new Foundation.NSString("Length Exceed.");
                    //_myTooltip.Show(Control, AppDelegate.window.RootViewController.View, true);
                }
                else
                {
                    _myTooltip.Hidden = true;
                }
            }
            else if (FormEntry.Validation == CusEntry.ValidationType.Password)
            {
                var checkvalid = Validation.PasswordValidate(FormEntry.Text);
                if (!checkvalid)
                {
                    _myTooltip.Hidden = false;
                    _myTooltip.Text = new Foundation.NSString("Password not valid.");
                    //_myTooltip.Show(Control, AppDelegate.window.RootViewController.View, true);
                }
                else
                {
                    _myTooltip.Hidden = true;
                }
            }

            //if (Control.Text.Contains("1"))
            //{
            //    if (_myTooltip.Hidden == true)
            //    {
            //        _myTooltip.Hidden = false;
            //        _myTooltip.Show(Control, AppDelegate.window.RootViewController.View, true);
            //    }
            //}
            //else
            //{
            //    _myTooltip.Hidden = true;
            //}
        }



        void Btnright_TouchUpInside(object sender, EventArgs e)
        {
            //UIWindow  window = new UIWindow(UIScreen.MainScreen.Bounds);
            //_myTooltip.Show(this.btnright, AppDelegate.window.RootViewController.View, true);
        }

        void Tap(UITapGestureRecognizer tap1)
        {
            _myTooltip.Show(this.vimg, App.Current.MainPage.CreateViewController().View, true);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;

            FormEntry = Element as CusEntry;
            if (Control != null)
            {
                Control.TextColor = UIColor.FromRGB(0, 0, 0);
            }
            if (e.PropertyName == CusEntry.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusEntry.BorderColorProperty.PropertyName ||
                e.PropertyName == CusEntry.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusEntry.BackgroundColorProperty.PropertyName ||
                e.PropertyName == CusEntry.InnerBackgroundColorProperty.PropertyName)
            {
                SetBackgroud();
                SetBorder();
            }
        }

        void SetBackgroud()
        {
            if (FormEntry.InnerBackground != Color.Transparent)
            {
                Control.BackgroundColor = FormEntry.InnerBackground.ToUIColor();
            }
            else
            {
                Control.BackgroundColor = FormEntry.BackgroundColor.ToUIColor();
            }
        }

        void SetBorder()
        {
            Control.Layer.BorderColor = FormEntry.BorderColor.ToCGColor();
            Control.Layer.BorderWidth = FormEntry.BorderWidth;
            Control.Layer.CornerRadius = FormEntry.BorderRadius;
        }

        void SetPadding()
        {
            if (FormEntry.Padding.Left != 0)
            {
                Control.LeftView = new UIView(new CGRect(0, 0, FormEntry.Padding.Left, 0));
                Control.RightView = new UIView(new CGRect(0, 0, FormEntry.Padding.Right, 0));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightViewMode = UITextFieldViewMode.Always;
            }
        }
    }
}
