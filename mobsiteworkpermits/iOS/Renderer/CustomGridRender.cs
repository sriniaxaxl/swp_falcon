﻿using System;
using System.ComponentModel;
using CustomControl;
using iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusGrid), typeof(CustomGridRender))]
namespace iOS.Renderer
{
    public class CustomGridRender : ViewRenderer<CusGrid, UIKit.UIView>
    {       
        CusGrid FormGd;
        protected override void OnElementChanged(ElementChangedEventArgs<CusGrid> e)
        {
            if (e.OldElement != null || this.Element == null)
                return;
            FormGd = Element as CusGrid;
            SetBackground();
            SetBorder();
            SetShadow();           
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.Element == null)
                return;
            FormGd = Element as CusGrid;
            if (e.PropertyName == CusGrid.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusGrid.BorderColorProperty.PropertyName ||
                e.PropertyName == CusGrid.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusGrid.InnerBackgroundColorProperty.PropertyName)
            {
                SetBackground();
                SetBorder();
            }
        }


        void SetBackground()
        {
            if (FormGd.InnerBackground != Color.Transparent)
            {
                Layer.BackgroundColor = FormGd.InnerBackground.ToCGColor();
            }
            else
            {
                Layer.BackgroundColor = FormGd.BackgroundColor.ToCGColor();
            }
        }

        void SetBorder()
        {
            Layer.BorderColor = FormGd.BorderColor.ToCGColor();
            Layer.BorderWidth = FormGd.BorderWidth;
            Layer.CornerRadius = (nfloat)FormGd.BorderRadius.Top;
        }

        void SetShadow()
        {
            //Layer.ShadowColor = UIColor.DarkGray.CGColor;
            //Layer.ShadowOpacity = 1.0f;
            //Layer.ShadowRadius = 6.0f;
            //Layer.ShadowOffset = new System.Drawing.SizeF(0f, 3f);
            //Layer.ShouldRasterize = true;
            //Layer.MasksToBounds = false;



            //if(FormGd.ShadowColor != null)                
            //{
            //    Layer.ShadowColor = FormGd.ShadowColor.ToCGColor();
            //    Layer.ShadowOpacity = 1.0f;
            //    Layer.ShadowRadius = (nfloat)FormGd.ShadowRadius.Left;
            //    Layer.ShadowOffset = new System.Drawing.SizeF(3.0f, (float)3.0);
            //    Layer.ShouldRasterize = true;
            //    Layer.MasksToBounds = false;
            //}
        }
    }
}
