﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class HotWorksSubmitView : ContentView
    {
        public event EventHandler PrintEvent;
        public HotWorksSubmitView()
        {
            InitializeComponent();
        }

        void Print_Tapped(object sender, System.EventArgs e)
        {
            PrintEvent.Invoke(this, new EventArgs());
        }
    }
}
