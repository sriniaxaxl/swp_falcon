﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using System.ComponentModel.DataAnnotations;
using CustomControl;
using Newtonsoft.Json;
//using ModernHttpClient;
using SiteWorkPermits.Model.SyncHotWorks;
using System.Collections.Generic;
using System.Net;
using SiteWorkPermits.Platform;
using SiteWorkPermits.Model.SyncImpairements;
using System.Net.Http.Headers;
using SiteWorkPermits.DAL.sqlIntegration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace SiteWorkPermits.DAL.RestHelper
{
    public class RestApiHelper<T> where T:class
    {
        public static async Task<UserLogout> LogoutUser(string siteNumber, string AccountNumber)
        {
            var loginRequest = "{\"AccountNumber\":"
                   + AccountNumber + ",\"SiteNumber\":" + siteNumber + "}";

            using (var cl = new HttpClient())
            {
                try
                {
                    var content = new StringContent(loginRequest, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.LOGOUT_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var logoutResponse = JsonConvert.DeserializeObject<UserLogout>(response);
                    return logoutResponse;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }



        public static async Task<UserLogin> LoginUser(string accountNumber, string siteNumber)
        {
            var loginRequest = "{\"IsHandHeldNumberRequired\":\"true\",\"AccountNumber\":"
                   + accountNumber + ",\"SiteNumber\":" + siteNumber + "}";

            using (var cl = new HttpClient())
            {
                try
                {
                    var content = new StringContent(loginRequest, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.USER_LOGIN_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<UserLogin>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static async Task<ScanPermitRootObject> SyncMultipartData(List<ScannedPermitModel> scannedPermitModels) {
            try
            {
                int permitCount = 0;
                var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                using (var formData = new MultipartFormDataContent())
                {
                    formData.Headers.ContentType.MediaType = "multipart/form-data";
                    foreach (var model in scannedPermitModels)
                    {
                        if (permitCount < SiteWorkPermits.AppConfig.ScanPaperPermitLimit)
                        {
                            if (model.ScannedPaperPermit != null && model.ScannedPaperPermitGUID != null)
                            {
                                var upfilebytes = model.ScannedPaperPermit;
                                var fileByteArray = new ByteArrayContent(upfilebytes, 0, upfilebytes.Count());

                                fileByteArray.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                                {
                                    FileName = model.ScannedPaperPermitGUID.ToString() + ".png",
                                    Name = model.ScannedPaperPermitGUID.ToString()
                                };


                                formData.Add(fileByteArray);
                                //formData.Add(new ByteArrayContent(upfilebytes, 0, upfilebytes.Count()));
                                //formData.Add(new StringContent(model.ScannedPaperPermitGUID.ToString()));
                                //formData.Add(new StringContent(model.ScannedPaperPermitGUID.ToString()+".png"));
                                permitCount++;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    using (HttpClient client = new HttpClient())
                    {
                        if (!string.IsNullOrEmpty(userToken.ToString()))
                        {
                            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                        }

                        var ss = SiteWorkPermits.AppConfig.SYNC_SCAN_PERMIT_URL;
                        var httpresponse = await client.PostAsync(SiteWorkPermits.AppConfig.SYNC_SCAN_PERMIT_URL, formData);
                        string contents = await httpresponse.Content.ReadAsStringAsync();
                        var SyncPermitResponse = JsonConvert.DeserializeObject<ScanPermitRootObject>(contents);
                        return SyncPermitResponse;
                    }
                }
            }
            catch (Exception ex) {
                return null;
            }
        }

    public static async Task<RootObject> SyncUserDate_Post(SiteWorkPermits.Model.SyncHotWorks.Data syncHotWorks)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    var ss = JsonConvert.SerializeObject(syncHotWorks);
                    var content = new StringContent(ss, Encoding.UTF8, "application/json");
                                       
                    var httpResponse = await cl.PostAsync( SiteWorkPermits.AppConfig.SYNC_USER_DATA_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<RootObject>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<RootObject> GetArchiveHotWorks_Post(string archiveRequest)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    var content = new StringContent(archiveRequest, Encoding.UTF8, "application/json");

               //     var loginResponse = await HTTPRestWrapper<T>.PostRequest(SiteWorkPermits.AppConfig.ARCHIVE_USER_DATA_URL, content);

                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.ARCHIVE_USER_DATA_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<RootObject>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<MamcData> Get_Mamc()
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    cl.DefaultRequestHeaders.Add("Accept", "application/json;odata=nometadata");
                    //var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    //if (!string.IsNullOrEmpty(userToken.ToString()))
                    //{
                    //    
                    //}
                    //cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var httpResponse = await cl.GetAsync(SiteWorkPermits.AppConfig.BASE_MAMC_URL).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var MamcData = JsonConvert.DeserializeObject<MamcData>(response);
                    return MamcData;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }
               
        public static async Task<RootObject> GetArchiveImpaitments_Post(int pagenumber, string archiveRequest)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    var content = new StringContent(archiveRequest, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.ARCHIVE_IMPAIRMENTS_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<RootObject>(response);
                    //var loginResponse = await HTTPRestWrapper<T>.PostRequest(SiteWorkPermits.AppConfig.ARCHIVE_IMPAIRMENTS_URL, content);

                    return loginResponse;
                    //return loginResponse.Data.HotWorks;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<TempHtWorkDetailResponse> GetArchiveDetail_Get(int permitId)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var httpResponse = await cl.GetAsync(SiteWorkPermits.AppConfig.GET_PERMIT_DETAILS + permitId).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();

                    var loginResponse = JsonConvert.DeserializeObject<TempHtWorkDetailResponse>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        #region hotworks print and email

        // hot works permit 
        public static async Task<EmailHWResponse> SendEmail_Post(string permitguid, string email, string pageFormat)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    await Task.Yield();
                    // string pageFormat = "A4";
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                        // var webclient = new WebClient();
                    }
                    var now = DateTime.Now;
                    var utcNow = now.ToUniversalTime();
                    var ts = now - utcNow;
                    var MinDiff = ts.TotalMinutes;
                    HotWorkApiRequest HotReq = new HotWorkApiRequest();
                    HotReq.HotWorkId = permitguid;
                    HotReq.ToEmailAddress = email;
                    HotReq.PaperSize = pageFormat;
                    HotReq.UtcOffsetInMinute = MinDiff;
                  
                    var EmailRequest = JsonConvert.SerializeObject(HotReq);
                    //var EmailRequest = "{\"HotWorkId\": " + permitId + ",\"ToEmailAddress\": \"" + email + "\"}";
                    var content = new StringContent(EmailRequest, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.EMAIL_HW_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<EmailHWResponse>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<CreateHWResponse> CreateHW_Post(HotWork hotWork)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    var ss = JsonConvert.SerializeObject(hotWork);
                    var content = new StringContent(ss, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.CREATE_PERMIT_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<CreateHWResponse>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<string> DownloadHwPermitPDF_Post(string permitguid, string email, string pageFormat)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var webClient = new WebClient();
                    var now = DateTime.Now;
                    var utcNow = now.ToUniversalTime();
                    var ts = now - utcNow;
                    var MinDiff = ts.TotalMinutes;

                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        webClient.Headers.Set("Authorization", "Bearer " + userToken);
                        webClient.Headers.Set("Content-Type", "application/pdf");
                    }
                    string documentsPath = Xamarin.Forms.DependencyService.Get<IDBPath>().GetPDFPath();
                    string localFilename = $"HotWorksPermit_" + DateTime.Now.Millisecond + ".pdf";
                    var path = System.IO.Path.Combine(documentsPath, localFilename);
                    var url = new Uri(SiteWorkPermits.AppConfig.DOWNLOAD_PERMIT_PDF_URL + permitguid + "?paperSize=" + pageFormat + "&utcOffsetInMinute=" + MinDiff);
                    webClient.DownloadFile(url, path);
                    return path;
                }
                catch (Exception ex)
                {
                    if (ex.ToString().Contains("401"))
                    {
                        return "Session Out";
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }

        #endregion

        #region impairments print and email

        // hot works permit 
        public static async Task<EmailHWResponse> SendEmailImp_Post(string  impguid, string email, string pageFormat)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                        // var webclient = new WebClient();
                    }

                    TimeZone timeZone = TimeZone.CurrentTimeZone;
                    var tmname = timeZone.StandardName;
                    //var tmname = "UK";
                    var now = DateTime.Now;
                    var utcNow = now.ToUniversalTime();
                    var ts = now - utcNow;
                    var MinDiff = ts.TotalMinutes;
                    ImpairmentApiRequest impairmentRequest = new ImpairmentApiRequest();
                    impairmentRequest.ImpairmentId = impguid;
                    impairmentRequest.ToEmailAddress = email;
                    impairmentRequest.PaperSize = pageFormat;
                    impairmentRequest.UtcOffsetInMinute = MinDiff;
                    impairmentRequest.TimeZone = tmname;
                    var EmailRequest = JsonConvert.SerializeObject(impairmentRequest);
                    //var EmailRequest = "{\"ImpairmentId\": " + permitId + ",\"ToEmailAddress\": \"" + email + "\", \"PaperSize\":\"" + pageFormat + "\"}";
                    var content = new StringContent(EmailRequest, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.EMAIL_IMP_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<EmailHWResponse>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<CreateIMPResponse> CreateImp_Post(SyncImpairements syncImpairements)
        {
            using (var cl = new HttpClient())
            {
                try
                {
                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        cl.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    // var archiveRequest =   "{\"PageSize\": 20,\"PageNumber\": "+pagenumber+",\"WorkOrderIdFilterList\": [],\"DateFilter\": \"ThisYear\"}";                   

                    var ss = JsonConvert.SerializeObject(syncImpairements);
                    var content = new StringContent(ss, Encoding.UTF8, "application/json");
                    var httpResponse = await cl.PostAsync(SiteWorkPermits.AppConfig.CREATE_IMP_URL, content).ConfigureAwait(false);
                    var response = await httpResponse.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<CreateIMPResponse>(response);
                    return loginResponse;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                    return null;
                }
            }
        }

        public static async Task<string> DownloadHwImpPDF_Post(string impguid, string email, string pageFormat)
        {
            var webClient = new WebClient();
            using (var cl = new HttpClient())
            {
                try
                {
                    //var webClient = new WebClient();
                    var now = DateTime.Now;
                    var utcNow = now.ToUniversalTime();
                    var ts = now - utcNow;
                    var MinDiff = ts.TotalMinutes;
                    TimeZone timeZone = TimeZone.CurrentTimeZone;
                    var tmname = timeZone.StandardName;
                    //var tmname = "UK";

                    var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        webClient.Headers.Set("Authorization", "Bearer " + userToken);
                        webClient.Headers.Set("Content-Type", "application/pdf");
                    }
                    string documentsPath = Xamarin.Forms.DependencyService.Get<IDBPath>().GetPDFPath();
                    string localFilename = $"Impairments_" + DateTime.Now.Millisecond + ".pdf";
                    var path = System.IO.Path.Combine(documentsPath, localFilename);
                    var url = new Uri(SiteWorkPermits.AppConfig.DOWNLOAD_IMP_PDF_URL + impguid + "?paperSize=" + pageFormat+"&utcOffsetInMinute="+MinDiff+"&timeZone="+tmname);
                    webClient.DownloadFile(url, path);
                    return path;
                }
                catch (Exception ex)
                {
                    if(ex.ToString().Contains("401")){
                        return "Session Out";
                    }else{
                        return "";    
                    }
                }
            }
        }


        public static async Task<byte[]> GetScanPermitImage(string GUID) {

            var userToken = Convert.ToString(Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"]);

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    if (!string.IsNullOrEmpty(userToken.ToString()))
                    {
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + userToken);
                    }
                    //MNimg_URL = MNimg_URL + parammodel.modelname;
                    HttpResponseMessage response = await client.GetAsync(SiteWorkPermits.AppConfig.DOWNLOAD_SCAN_PERMIT_IMG_URL + GUID);
                    var content = await response.Content.ReadAsStringAsync();
                    var loginResponse = JsonConvert.DeserializeObject<ScanPermitImgResponse>(content);

                    byte[] imagebytes = null;
                    if (!string.IsNullOrEmpty(loginResponse.Data))
                    {

                        var webClient = new WebClient();
                        imagebytes = webClient.DownloadData(loginResponse.Data);

                        //HttpResponseMessage response1 = await client.GetAsync(loginResponse.Data);
                        //imagebytes = await response1.Content.ReadAsByteArrayAsync();
                    }

                    //return "data:image/png;base64," + Convert.ToBase64String(content);
                    // return File(content, "image/png", parammodel.modelname);
                    return imagebytes;
                }
                catch (Exception ex) {
                    return null;
                }
            }

        }

        #endregion
    }          
}
