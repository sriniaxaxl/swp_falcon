﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class Grayview : ContentView
    {

        public static BindableProperty TitleProperty = BindableProperty.Create<Grayview, string>(o => o.Title, string.Empty, BindingMode.TwoWay, null, HandleBindingPropertyChangedDelegate);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        static void HandleBindingPropertyChangedDelegate(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (Grayview)bindable;
            control.Lbltxt.Text = newValue;
        }

        public static BindableProperty LeftImageProperty = BindableProperty.Create<Grayview, string>(o => o.LeftImage, string.Empty, BindingMode.TwoWay, null, RightImagePropertyChanged);

        public string LeftImage
        {
            get { return (string)GetValue(LeftImageProperty); }
            set { SetValue(LeftImageProperty, value); }
        }

        static void RightImagePropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (Grayview)bindable;
            control.leftimg.Source = newValue;
        }
        public Grayview()
        {
            InitializeComponent();
        }
    }
}
