﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SiteWorkPermits.DAL.sqlIntegration
{
    public class Impairment_Sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }


        public string  impairmentCloseDateTime { get; set; }
        public string impairmentGUID { get; set; }

        public int server_id { get; set; }
        public string siteId { get; set; }
        public string accountId { get; set; }
        public bool isEdited { get; set; }
        public bool isNewlyCreated { get; set; }
        public int impairmentMobileId { get; set; }
      //  public int impairmentServerId { get; set; }
        public bool isParent { get; set; }
        public int parentImpairmentId { get; set; }
        public string workFlowStatus { get; set; }
        public bool isDeleted { get; set; }

        public string precautionOtherDescription { get; set; }

        [OneToOne]
        public ReporterDetail_sql reporterDetail_Sql { get; set; }
        [ForeignKey(typeof(ReporterDetail_sql))]
        public int reporterDetail_sql_id { get; set; }
               
        public int impairmentTypeId { get; set; }
        public int impairmentClassId { get; set; }
        public int shutDownReasonId { get; set; }
        public string impairmentDescription { get; set; }
        public string startDateTime { get; set; }
        public string endDateTime { get; set; }

        [ManyToMany(typeof(ImpairmentMajorImpairmentMaster_sql))]
        public List<MajorImpairmentMaster_sql> majorImpairmentMaster_sqls { get; set; }
                     
        [ManyToMany(typeof(ImpairmentPrecautionMeasure_sql))]
        public List<Impairment_PrecautionMaster_sql> impairment_PrecautionMaster_Sqls { get; set; }               
        
    }


    public class MajorImpairmentMaster_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string majorImpairmentMeasureDescription { get; set; }

        [ManyToMany(typeof(ImpairmentMajorImpairmentMaster_sql))]
        public List<Impairment_Sql> impairment_sql { get; set; }
    }

    public class ImpairmentMajorImpairmentMaster_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        [ForeignKey(typeof(Impairment_Sql))]
        public int impairment_sql_id { get; set; }

        [ForeignKey(typeof(MajorImpairmentMaster_sql))]
        public int majorImpairmentMaster_sql_id { get; set; }

    }

    #region Reporter Details
    public class ReporterDetail_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string ReporterName { get; set; }
        public string ReporterEmail { get; set; }
        public string ReporterPhone { get; set; }
        
        [OneToOne]
        public Impairment_Sql impairment_sql { get; set; }
        [ForeignKey(typeof(Impairment_Sql))]
        public int impairment_Sql_id { get; set; }
    }
    #endregion

    public class ImpairmentType_Sql
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string TypeDescription { get; set; }
    }

    public class ImpairmentClassType_Sql
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string ClassDescription { get; set; }
    }

    public class ShutDownReason_Sql
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string ClassDescription { get; set; }
    }

    #region Precaution Measure

    public class ImpairmentPrecautionMeasure_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        [ForeignKey(typeof(Impairment_Sql))]
        public int impairmentt_sql_id { get; set; }

        [ForeignKey(typeof(Impairment_PrecautionMaster_sql))]
        public int impairment_PrecautionMaster_sql { get; set; }
    }

    public class Impairment_PrecautionMaster_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string precautionName { get; set; }
      
        [ManyToMany(typeof(ImpairmentPrecautionMeasure_sql))]
        public List<Impairment_Sql> impairment_Sqls { get; set; }
    }

    #endregion

}
