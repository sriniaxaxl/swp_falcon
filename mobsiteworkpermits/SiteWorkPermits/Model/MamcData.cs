﻿using System;

namespace SiteWorkPermits
{
    public class MamcData
    {       
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTime Timestamp { get; set; }
        public string AndroidMDMURL { get; set; }
        public string AndroidMinimumAppVersion { get; set; }
        public bool DowntimeAlert { get; set; }
        public DateTime FromDateTime { get; set; }
        public string GooglePlayURL { get; set; }
        public string ReasonForDowntime { get; set; }
        public DateTime ToDateTime { get; set; }
        public string TypeOfUpgradeAlert { get; set; }
        public string iOSMDMURL { get; set; }
        public string iOSMinimumAppVersion { get; set; }
        public string iTunesURL { get; set; }
    }
}
