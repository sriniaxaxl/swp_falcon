﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CarouselView.FormsPlugin.iOS;
using SiteWorkPermits.ViewModel;
using Foundation;
using ObjCRuntime;
using Prism;
using Prism.Common;
using Prism.Ioc;
using Prism.Navigation;
using UIKit;
using Xamarin.Forms;
using Google.TagManager;
using SiteWorkPermits.iOS.Platform;

namespace SiteWorkPermits.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //public override UIWindow Window
        //{
        //    get;
        //    set;
        //}


        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            SQLitePCL.Batteries_V2.Init();
            global::Xamarin.Forms.Forms.Init();
            CarouselViewRenderer.Init();
            LoadApplication(new App());

            //Firebase.Core.App.Configure();
            //TagManager.Configure();

            //Window = new UIWindow(UIScreen.MainScreen.Bounds);
            //window.RootViewController = new UIViewController();
            //window.RootViewController = App.Current.MainPage.CreateViewController();
            //Window.MakeKeyAndVisible();

            App.ScreenHeight = (int)UIScreen.MainScreen.Bounds.Height;
            App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(notificationSettings);
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }
            UINavigationBar.Appearance.BarTintColor = UIColor.Black;                       
            return base.FinishedLaunching(app, options);
        }



       


        public string navigationpath = "HomePage/NavigationPage/HotWorksPage";
        public async override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            if (application.ApplicationState == UIApplicationState.Active)
            {
                //UIAlertController okayAlertController = UIAlertController.Create(notification.AlertAction, notification.AlertBody, UIAlertControllerStyle.Alert);
                //okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
                //App.Current.MainPage.CreateViewController().PresentViewController(okayAlertController, true, null);
            }
            else
            {
                var obj = notification.UserInfo["notification"];
                var val = ((NSString)notification.UserInfo["notification"]).ToString();
                //App.EditHotworkID = val;
                var Curpage = PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                var key = Helper.Getval("JWT_TOKEN");
                if (string.IsNullOrEmpty(key))
                {
                    navigationpath = "LoginPage";
                }              

                var navigationParams = new NavigationParameters();
                navigationParams.Add("FromNotification", "true");
                if (Curpage.BindingContext is AboutUsPageViewModel)
                {
                    var model = (AboutUsPageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is ArchieveViewModel)
                {
                    var model = (ArchieveViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is ContactUsViewModel)
                {
                    var model = (ContactUsViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is DownloadViewModel)
                {
                    var model = (DownloadViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is LoginPageViewModel)
                {
                    var model = (LoginPageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is GuideViewModel)
                {
                    var model = (GuideViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is HomePageViewModel)
                {
                    var model = (HomePageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is HotWorksPageViewModel)
                {
                    var model = (HotWorksPageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is ImpairmentsViewModel)
                {
                    var model = (ImpairmentsViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is NewHotWorksPageViewModel)
                {
                    var model = (NewHotWorksPageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is NewImpairmentsPageViewModel)
                {
                    var model = (NewImpairmentsPageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is PrintPermitViewModel)
                {
                    var model = (PrintPermitViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is PrintImpairmentViewModel)
                {
                    var model = (PrintImpairmentViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }
                else if (Curpage.BindingContext is TermsAndConditionPageViewModel)
                {
                    var model = (TermsAndConditionPageViewModel)Curpage.BindingContext;
                    await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                }                                             
            }

        }

        //public class iOSInitializer : IPlatformInitializer
        //{
        //    public void RegisterTypes(IContainerRegistry containerRegistry)
        //    {

        //    }
        //}
    }
}
