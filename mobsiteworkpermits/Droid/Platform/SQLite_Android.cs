﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SiteWorkPermits.DAL.sqlIntegration;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SiteWorkPermits.Droid.Platform;
using SQLite.Net.Cipher.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Android))]
namespace SiteWorkPermits.Droid.Platform
{
    class SQLite_Android : ISQLite
    {
        public SQLite_Android()
        {
        }

        #region ISQLite implementation

        ISecureDatabase _secureDatabase;

        public string GetConnectionPath()
        {
            var fileName = "dev_axa_siteworkpermit_v1.db3";
            var fileDBpath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(fileDBpath, fileName);
        //    var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
           // _secureDatabase = new SQLiteDatabase(platform, path);
            return path;
        }

        //public ISecureDatabase GetsecureDB()
        //{
        //    if (_secureDatabase != null)
        //    {
        //        return _secureDatabase;
        //    }
        //    else {
        //        GetConnectionPath();
        //        return _secureDatabase;
        //    }
        //}

        #endregion
    }
}