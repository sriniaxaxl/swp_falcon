﻿using System;
using System.Collections.Generic;
using Plugin.DeviceOrientation;
using Plugin.DeviceOrientation.Abstractions;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class HeaderView : ContentView
    {       
        public static BindableProperty TitleProperty = BindableProperty.Create<HeaderView, string>(o => o.Title, string.Empty,BindingMode.TwoWay,null,HandleBindingPropertyChangedDelegate);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        static void HandleBindingPropertyChangedDelegate(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (HeaderView)bindable;
            control.Lbltitle.Text = newValue;
        }
                       
        public HeaderView()
        {
            try
            {
                InitializeComponent();

                if (CrossDeviceOrientation.Current.CurrentOrientation == DeviceOrientations.Portrait || CrossDeviceOrientation.Current.CurrentOrientation == DeviceOrientations.PortraitFlipped)
                {
                    GdBurgerMenu.IsVisible = true;
                }
                else
                {
                    GdBurgerMenu.IsVisible = false;
                }

                CrossDeviceOrientation.Current.OrientationChanged += (sender, args) =>
                {
                    if (args.Orientation == DeviceOrientations.Portrait || args.Orientation == DeviceOrientations.PortraitFlipped)
                    {
                        GdBurgerMenu.IsVisible = true;
                    }
                    else
                    {
                        GdBurgerMenu.IsVisible = false;
                    }
                };
            }
            catch (Exception ex) {
                var ss = "";
            }
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            MessagingCenter.Send(Xamarin.Forms.Application.Current, "ShowHambergerMenu");
        }

        void Handle_Sync_Tapped(object sender, System.EventArgs e)
        {
            //Xamarin.Forms.Application.Current.MainPage.DisplayAlert("","Coming Soon", "Ok");
        }
    }
}
