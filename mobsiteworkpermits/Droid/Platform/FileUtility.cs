﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Platform;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileUtility))]
namespace SiteWorkPermits.Droid.Platform
{
    public class FileUtility : IFileUtility
    {
        public Task<byte[]> GetImageWithCamera()
        {

            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);

         //   Activity activity = Forms.Context as Activity;
            Intent cameraIntent = new Intent(MediaStore.ActionImageCapture);
            MainActivity.Instance.StartActivityForResult(cameraIntent, 102);

            // Save the TaskCompletionSource object as a MainActivity property
            MainActivity.Instance.PickImageTaskCompletionSource = new TaskCompletionSource<byte[]>();

            // Return Task object

            return  MainActivity.Instance.PickImageTaskCompletionSource.Task;
           
            
        }

        //protected void onActivityResult(int requestCode, int resultCode, Intent data)
        //{
        //    if (requestCode == 102)
        //    {
        //        Bitmap photo = (Bitmap)data.getExtras().get("data");
        //        //   imageView.setImageBitmap(photo);
        //        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        //        photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
        //        byte[] byteArray = stream.toByteArray();
        //    }
        //}

    }
}