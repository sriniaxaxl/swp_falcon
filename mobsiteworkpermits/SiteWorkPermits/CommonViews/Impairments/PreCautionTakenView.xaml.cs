﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class PreCautionTakenView : ContentView
    {
        NewImpairmentsPageViewModel Model;
        public PreCautionTakenView(NewImpairmentsPageViewModel Vm = null)
        {
            InitializeComponent();
            if (Vm != null)
            {
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(1)).FirstOrDefault().IsChecked)
                {
                    ChkGaps.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(2)).FirstOrDefault().IsChecked)
                {
                    ChkDiscntinueWork.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(3)).FirstOrDefault().IsChecked)
                {
                    ChkNotifyDepartment.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(4)).FirstOrDefault().IsChecked)
                {
                    ChkDiscntinueSmoking.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(5)).FirstOrDefault().IsChecked)
                {
                    ChkCeaseHazardous.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(6)).FirstOrDefault().IsChecked)
                {
                    ChkNotifyFire.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(7)).FirstOrDefault().IsChecked)
                {
                    ChkChargedHoseLines.Checked = true;
                }                               
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(8)).FirstOrDefault().IsChecked)
                {
                    ChkWatchmanSurveillance.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(9)).FirstOrDefault().IsChecked)
                {
                    ChkNotifyAlarm.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(10)).FirstOrDefault().IsChecked)
                {
                    ChkNotifySiteEmergency.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(11)).FirstOrDefault().IsChecked)
                {
                    ChkWorkContinuous.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(12)).FirstOrDefault().IsChecked)
                {
                    ChkPipePlugs.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(13)).FirstOrDefault().IsChecked)
                {
                    ChkEmergency.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(14)).FirstOrDefault().IsChecked)
                {
                    ChkActiveSmoke.Checked = true;
                }
                if (Vm.PrecationTakenCollection.Where(s => s.Index.Equals(15)).FirstOrDefault().IsChecked)
                {
                    ChkOther.Checked = true;
                    OtherGd.IsVisible = true;
                }
                else
                {
                    OtherGd.IsVisible = false;
                }
            }


            ChkGaps.Checked_Changed +=ChkPrecationChanged;
            ChkDiscntinueWork.Checked_Changed += ChkPrecationChanged;
            ChkNotifyDepartment.Checked_Changed += ChkPrecationChanged;
            ChkDiscntinueSmoking.Checked_Changed += ChkPrecationChanged;
            ChkCeaseHazardous.Checked_Changed += ChkPrecationChanged;
            ChkNotifyFire.Checked_Changed += ChkPrecationChanged;
            ChkChargedHoseLines.Checked_Changed += ChkPrecationChanged;
            ChkWatchmanSurveillance.Checked_Changed += ChkPrecationChanged;
            ChkNotifyAlarm.Checked_Changed += ChkPrecationChanged;
            ChkNotifySiteEmergency.Checked_Changed += ChkPrecationChanged;                        
            ChkWorkContinuous.Checked_Changed += ChkPrecationChanged;
            ChkPipePlugs.Checked_Changed += ChkPrecationChanged;
            ChkEmergency.Checked_Changed += ChkPrecationChanged;
            ChkActiveSmoke.Checked_Changed += ChkPrecationChanged;
            ChkOther.Checked_Changed+=ChkPrecationChanged;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            var bind = this.BindingContext;
            if (bind != null)
            {
                Model = (NewImpairmentsPageViewModel)bind;               
            }
        }

        private void ChkPrecationChanged(object sender, CustomControl.CheckedArgs e)
        {
            CustomCheckbox customCheckbox = (CustomCheckbox)sender;
            Label lblval = (Label)((CusGrid)customCheckbox.Parent).Children[0];
            Debug.WriteLine(e.Checked);
            if (Model != null)
            {
                Model.PrecationTakenCollection.Where(s => s.Name.Equals(lblval.Text)).FirstOrDefault().IsChecked = e.Checked;
                customCheckbox.Checked = e.Checked;
                if(Model.PrecationTakenCollection.Where(s => s.Name.Equals(lblval.Text)).FirstOrDefault().Index == 15)
                {
                    if(e.Checked)
                    {
                        OtherGd.IsVisible = true;
                    }
                    else
                    {
                        OtherGd.IsVisible = false;
                    }
                }
            }
        }
    }
}
