﻿using SiteWorkPermits.Resx;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits.ViewPage
{
    public partial class ImpairmentsPage : ContentPage
    {
        ObservableCollection<TabInfo> TabCollection = new ObservableCollection<TabInfo>();
        ObservableCollection<ModelPermits> PermitCollection = new ObservableCollection<ModelPermits>();

        public ImpairmentsPage()
        {
            InitializeComponent();
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_ACTIVE, IsSelected = true, view = new ContentView() });
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_DRAFT, IsSelected = false, view = new ContentView() });
            TabCollection.Add(new TabInfo { Title = AppResource.HW_STATUS_UPCOMING, IsSelected = false, view = new ContentView() });
            TabCollection.Add(new TabInfo { Title = AppResource.CLOSED_LABEL, IsSelected = false, view = new ContentView() });
            foreach (var d in TabCollection)
            {
                TabView tab = new TabView();
                tab.Title = d.Title;
                tab.TabColor = d.IsSelected ? Color.FromHex("#00AFAD") : Color.Transparent;
                tab.GestureRecognizers.Add(new TapGestureRecognizer(HandleAction));
                tab.BindingContext = d.Collection;
                //Spworktabs.Children.Add(tab);
            }
            //ImpairmentView.RestoreImpairmentEvent +=ImpairmentView_RestoreImpairmentEvent;
            //ImpairmentView.CancelImpairmentEvent += ImpairmentView_CancelImpairmentEvent;

            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "RestorePopup", (sender) =>
            {
                // Model = (ImpairmentsViewModel)this.BindingContext;
                Model.GetImpairment();
              //  GdImpairmentview.IsVisible = true;
            });
        }
        private ImpairmentsViewModel Model ;
        protected override void OnAppearing()
        {
            base.OnAppearing();
             Model = (ImpairmentsViewModel)this.BindingContext;   
            Model.RefreshImpListEvent += ViewModel_RefreshImpListEvent;
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_ACTIVE).First().view.Content = new ImpairmentsListView(Model.ActiveFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_ACTIVE).First().Collection = Model.ActiveFormsCollection;
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_DRAFT).First().view.Content = new ImpairmentsListView(Model.DraftFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_DRAFT).First().Collection = Model.DraftFormsCollection;
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_UPCOMING).First().view.Content = new ImpairmentsListView(Model.UpcomingFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_UPCOMING).First().Collection = Model.UpcomingFormsCollection;
            //TabCollection.Where(s => s.Title == AppResource.CLOSED_LABEL).First().view.Content = new ImpairmentsListView(Model.ExpiredFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.CLOSED_LABEL).First().Collection = Model.ExpiredFormsCollection;

            //Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
            //Pageview.BindingContext = TabCollection.Where(s => s.IsSelected == true).First().Collection;

        }


        void ViewModel_RefreshImpListEvent()
        {
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_ACTIVE).First().view.Content = new ImpairmentsListView(Model.ActiveFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_ACTIVE).First().Collection = Model.ActiveFormsCollection;
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_DRAFT).First().view.Content = new ImpairmentsListView(Model.DraftFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_DRAFT).First().Collection = Model.DraftFormsCollection;
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_UPCOMING).First().view.Content = new ImpairmentsListView(Model.UpcomingFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.HW_STATUS_UPCOMING).First().Collection = Model.UpcomingFormsCollection;
            //TabCollection.Where(s => s.Title == AppResource.CLOSED_LABEL).First().view.Content = new ImpairmentsListView(Model.ExpiredFormsCollection, false);
            //TabCollection.Where(s => s.Title == AppResource.CLOSED_LABEL).First().Collection = Model.ExpiredFormsCollection;

            //Pageview.Content = TabCollection.Where(s => s.IsSelected == true).First().view.Content;
            //Pageview.BindingContext = TabCollection.Where(s => s.IsSelected == true).First().Collection;
        }

        private void ImpairmentView_RestoreImpairmentEvent(object sender, EventArgs e)
        {
            //GdImpairmentview.IsVisible = false;
        }

        private void ImpairmentView_CancelImpairmentEvent(object sender, EventArgs e)
        {
            //GdImpairmentview.IsVisible = false;
        }
              
        private void ImpairPopup_Tapped(object sender, System.EventArgs e)
        {            
            //GdImpairmentview.IsVisible = false;
        }

        private void HandleAction(View arg1, object arg2)
        {
            Deselectview();
            TabView selv = (TabView)arg1;
            Selectview(selv);
        }

        private void Deselectview()
        {
            var presel = TabCollection.Where(p => p.IsSelected).First();
            var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).First());
           // TabView tab = (TabView)Spworktabs.Children[Selind];
           // tab.TabColor = Color.Transparent;
            TabCollection.Where(p => p.IsSelected).First().IsSelected = false;
        }

        private void Selectview(TabView v)
        {
            TabView selv = v;
            var seltext = selv.Title;
            selv.TabColor = Color.FromHex("#00AFAD");
            var seltab = TabCollection.Where(s => s.Title == seltext).First();
            seltab.IsSelected = true;
         ////   Pageview.Content = null;
         //   Pageview.Content = TabCollection.Where(s => s.IsSelected).First().view.Content;
         //   Pageview.BindingContext = TabCollection.Where(s => s.IsSelected == true).First().Collection;
        }
    }
}
