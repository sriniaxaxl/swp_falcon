using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using CustomControl;
using System.ComponentModel;
using Android.Renderer;
using Android.Renderer.CommanRender;
using Android.Text;
using Java.Text;
using Java.Util;
using System.Globalization;

[assembly: ExportRenderer(typeof(CusDatePicker), typeof(CustomDatePickerRenderer))]
namespace Android.Renderer
{
    public class CustomDatePickerRenderer : EntryRenderer
    {
        CusDatePicker FormEntry;
        private BorderRenderer _renderer;
        private const GravityFlags DefaultGravity = GravityFlags.CenterVertical;
        DatePickerDialog dialog;
        DateFormat formatter;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            FormEntry = Element as CusDatePicker;

            Control.Gravity = DefaultGravity;
                      
            
            UpdateBackground(FormEntry);
            UpdatePadding(FormEntry);           
            Control.Focusable = false;
            DateTime today = DateTime.Today;
            dialog = new DatePickerDialog(Forms.Context, OnDateSet, today.Year, today.Month - 1, today.Day);
            formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.Parse(FormEntry.MinimumDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
            long dateInLong = date.Time;
            dialog.DatePicker.MinDate = dateInLong;
            Date date1 = formatter.Parse(FormEntry.MaximumDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
            long date1InLong = date1.Time;
            dialog.DatePicker.MaxDate = date1InLong;

            Control.Click += (sender, er) =>
            {                            
                //dialog.DatePicker.MinDate = FormEntry.MinimumDate;
                //.DatePicker.MaxDate = FormEntry.MaximumDate;
                dialog.Show();               
            };

        }

        void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            DateChangeArgs args = new DateChangeArgs();
            args.Newdate = e.Date;
            if (FormEntry.Dateselected != null)
            {
                FormEntry.Dateselected.Invoke(FormEntry, args);
            }
            //FormEntry.ValueChanged.Invoke(FormEntry,args);
            //Control.Text = e.Date.ToString("dd/MM/yyyy");
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var entryEx = Element as CusDatePicker;
            if (e.PropertyName == CusDatePicker.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusDatePicker.BorderColorProperty.PropertyName ||
                e.PropertyName == CusDatePicker.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusDatePicker.BackgroundColorProperty.PropertyName)
            {
                UpdateBackground(entryEx);
            }
            else if (e.PropertyName == CusDatePicker.PaddingProperty.PropertyName)
            {
                UpdatePadding(entryEx);
            } 
            else if (e.PropertyName == CusTimePicker.TextProperty.PropertyName)
            {
                Control.Text = FormEntry.Text;
            }
            else if (e.PropertyName == CusDatePicker.MinimumDateProperty.PropertyName)
            {
                //dialog.DatePicker.MinDate = FormEntry.MinimumDate;
                Date date = formatter.Parse(FormEntry.MinimumDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                long dateInLong = date.Time;
                dialog.DatePicker.MinDate = dateInLong;
            }
            else if (e.PropertyName == CusDatePicker.MaximumDateProperty.PropertyName)
            {
                Date date1 = formatter.Parse(FormEntry.MaximumDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                long date1InLong = date1.Time;
                dialog.DatePicker.MaxDate = date1InLong;
               // dialog.DatePicker.MaxDate = FormEntry.MaximumDate;
            }
        }


        #region Utility methods

        private void UpdateBackground(CusDatePicker entryEx)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }
            _renderer = new BorderRenderer();
            if (entryEx.InnerBackground != null)
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.InnerBackground, entryEx.BorderWidth, entryEx.BorderRadius);
            }
            else
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.BackgroundColor, entryEx.BorderWidth, entryEx.BorderRadius);
            }

        }

        private void UpdatePadding(CusDatePicker entryEx)
        {
            Control.SetPadding((int)Forms.Context.ToPixels(entryEx.Padding.Left), (int)Forms.Context.ToPixels(entryEx.Padding.Top), (int)Forms.Context.ToPixels(entryEx.Padding.Right), (int)Forms.Context.ToPixels(entryEx.Padding.Bottom));
        }
              
        #endregion
    }
}