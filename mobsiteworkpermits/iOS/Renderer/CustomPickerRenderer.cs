﻿using System;
using System.ComponentModel;
using CustomControl;
using CustomControl.iOS.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Collections;
using System.Collections.Generic;
using Foundation;
using System.Linq;
using iOS.Renderer;
using CoreGraphics;

[assembly: ExportRenderer(typeof(CusPicker), typeof(CustomPickerRenderer))]
namespace CustomControl.iOS.Renderer
{
    public class CustomPickerRenderer : EntryRenderer
    {
        CusPicker FormPicker;
        List<string> Listobj = new List<string>();
        UIPickerView pickerView;
        DropdownModel bindmodel;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            FormPicker = Element as CusPicker;
            Listobj = FormPicker.ItemSource.ToList();
            UIToolbar toolbar = new UIToolbar();
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.BackgroundColor = UIColor.Blue;

            toolbar.Translucent = true;
            toolbar.SizeToFit();

            pickerView = new UIPickerView();
            bindmodel = new DropdownModel(Listobj);
            pickerView.Model = bindmodel;
            //bindmodel.ValueChanged += (sender, er) =>
            //{                

            //};
            SetBackgroud();
            SetBorder();
            SetPadding();
            UIBarButtonItem CancelButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Done, (s, er) =>
            {
                Control.ResignFirstResponder();
            });

            UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done, (s, er) =>
            {
                Control.ResignFirstResponder();
                ValueArgs args = new ValueArgs();
                args.SelectedItem = Listobj.ElementAt(bindmodel.SelectedIndex).ToString();
                args.SelectedIndex = bindmodel.SelectedIndex;
                FormPicker.ValueChanged(FormPicker, args);

                //args.Newdate = datePicker.Date.ToDateTime();
                //FormDatePicker.Dateselected.Invoke(FormDatePicker, args);
                //Control.Text = datePicker.Date.ToDateTime().ToString("dd/MM/yyyy");               
            });
            doneButton.Clicked += Done_Clicked;
            UIBarButtonItem flexiblespace = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
            CancelButton.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            doneButton.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            toolbar.SetItems(new UIBarButtonItem[] { CancelButton, flexiblespace, doneButton }, true);
            Control.InputView = pickerView;
            Control.InputAccessoryView = toolbar;
        }

        private void Done_Clicked(object sender, EventArgs e)
        {
            Control.TextColor = UIColor.FromRGB(0, 0, 0);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;

            FormPicker = Element as CusPicker;

            if (e.PropertyName == CusPicker.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusPicker.BorderColorProperty.PropertyName ||
                e.PropertyName == CusPicker.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusPicker.BackgroundColorProperty.PropertyName ||
                e.PropertyName == CusPicker.InnerBackgroundColorProperty.PropertyName)
            {
                SetBackgroud();
                SetBorder();
            }

            if (e.PropertyName == CusDatePicker.TextProperty.PropertyName)
            {
                Control.Text = FormPicker.Text;
            }
        }


        void SetBackgroud()
        {
            if (FormPicker.InnerBackground != Color.Transparent)
            {
                Control.BackgroundColor = FormPicker.InnerBackground.ToUIColor();
            }
            else
            {
                Control.BackgroundColor = FormPicker.BackgroundColor.ToUIColor();
            }
        }

        void SetBorder()
        {
            Control.Layer.BorderColor = FormPicker.BorderColor.ToCGColor();
            Control.Layer.BorderWidth = FormPicker.BorderWidth;
            Control.Layer.CornerRadius = FormPicker.BorderRadius;
        }

        void SetPadding()
        {
            if (FormPicker.Padding.Left != 0)
            {
                Control.LeftView = new UIView(new CGRect(0, 0, FormPicker.Padding.Left, 0));
                Control.RightView = new UIView(new CGRect(0, 0, FormPicker.Padding.Right, 0));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightViewMode = UITextFieldViewMode.Always;
            }
        }
    }
}
