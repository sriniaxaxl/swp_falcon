﻿using System;
using System.Threading.Tasks;

namespace SiteWorkPermits.Services
{
    public interface IBiometricAuthenticateService
    {
        string GetAuthenticationType();
        Task<bool> AuthenticateUserIDWithTouchID();
        bool fingerprintEnabled();
    }
}
