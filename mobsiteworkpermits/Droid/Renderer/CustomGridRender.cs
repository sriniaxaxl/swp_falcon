using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using CustomControl;
using System.ComponentModel;
using Xamarin.Forms;
using Android.Renderer;
using Android.Renderer.CommanRender;

[assembly: ExportRenderer(typeof(CusGrid), typeof(CustomGridRender))]
namespace Android.Renderer
{
    public class CustomGridRender : ViewRenderer<CusGrid, GridLayout>
    {
        private BorderRenderer _renderer;
        GridLayout Cusgd;
        protected override void OnElementChanged(ElementChangedEventArgs<CusGrid> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            Cusgd = new GridLayout(Context);
            if (Control == null)
            {
                var curgd = Element as CusGrid;                
                SetNativeControl(Cusgd);
                UpdateBackground(curgd);
                //UpdateMeasurelayout(curgd);
            }
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (this.Element == null)
                return;
            var entryEx = Element as CusGrid;
            if (e.PropertyName == CusGrid.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusGrid.BorderColorProperty.PropertyName ||
                e.PropertyName == CusGrid.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusGrid.InnerBackgroundColorProperty.PropertyName)
            {
                UpdateBackground(entryEx);
            }
        }

        //void UpdateMeasurelayout(CusGrid Gd)
        //{
        //    int Gdwidth = Convert.ToInt32(Forms.Context.ToPixels(Gd.WidthValue ));
        //    int Gdheight = Convert.ToInt32(Forms.Context.ToPixels(Gd.HeightValue));
        //    LayoutParams params1 = new LayoutParams(Gdwidth, Gdheight);
        //    Cusgd.LayoutParameters = params1;
        //    SetNativeControl(Cusgd);
        //}
        void UpdateBackground(CusGrid Gd)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }
            _renderer = new BorderRenderer();
            Cusgd.Background = _renderer.GetBorderBackground(Gd.BorderColor, Gd.InnerBackground, Gd.BorderWidth, Gd.BorderRadius);
        }
    }
}