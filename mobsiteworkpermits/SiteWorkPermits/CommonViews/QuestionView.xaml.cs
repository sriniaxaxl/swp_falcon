﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class QuestionView : ContentView
    {

        public string TopMessage
        {
            get
            {
                return LblTopMsg.Text;
            }
            set
            {
                if(!string.IsNullOrEmpty(value))
                {
                    LblTopMsg.Text = Convert.ToString(value);
                    LblTopMsg.IsVisible = true;
                }
                else
                {
                    LblTopMsg.IsVisible = false;
                }
            }
        }


        public string BottomMessage
        {
            get
            {
                return LblBottomMsg.Text;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    LblBottomMsg.Text = Convert.ToString(value);
                    LblBottomMsg.IsVisible = true;
                }
                else
                {
                    LblBottomMsg.IsVisible = false;
                }
            }
        }

        public static BindableProperty SectionProperty = BindableProperty.Create<QuestionView, string>(o => o.Section, string.Empty, BindingMode.TwoWay, null, SectionPropertyChanged);

        public string Section
        {
            get { return (string)GetValue(SectionProperty); }
            set { SetValue(SectionProperty, value); }
        }

        static void SectionPropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (QuestionView)bindable;
            control.LblSection.Text = newValue;
        }

        public static BindableProperty NumberProperty = BindableProperty.Create<QuestionView, string>(o => o.Number, string.Empty, BindingMode.TwoWay, null, NumberPropertyChanged);

        public string Number
        {
            get { return (string)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        static void NumberPropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (QuestionView)bindable;
            control.LblNumber.Text = newValue;
        }

        public QuestionView(ContentView view)
        {
            InitializeComponent();
            TopMessage = string.Empty;
            BottomMessage = string.Empty;
            SpQuestion.Children.Add(view);
        }
    }
}
