﻿using System;
using System.Collections.Generic;

namespace SiteWorkPermits.Model.SyncHotWorks
{
    public class ScopePerson
    {
        public int Id { get; set; }
        public string PersonName { get; set; }
    }

    public class ScopeWorkOrder
    {
        public int Id { get; set; }
        public int WorkOrderId { get; set; }
    }

    public class Scope
    {
        public int Id { get; set; }
        public string WorkDetail { get; set; }
        public string WorkLocation { get; set; }
        public string CompanyName { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public List<ScopePerson> ScopePersons { get; set; }
        public List<ScopeWorkOrder> ScopeWorkOrders { get; set; }
        public string EquipmentDescription { get; set; }
    }

    public class FireWatchPrevention
    {
        public int Id { get; set; }
        public string PersonName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string FireWatchType { get; set; }
    }

    public class FirePrevention
    {
        public int Id { get; set; }
        public string NearestFireAlarmLocation { get; set; }
        public string NearestPhone { get; set; }
        public List<FireWatchPrevention> FireWatchPreventions { get; set; }
    }

    public class PrecautionList
    {
        public int Id { get; set; }
        public int MeasureId { get; set; }
        public string MeasureStatus { get; set; }
        public int? FireHoseCount { get; set; }
        public int? FireExtinguisherCount { get; set; }
    }

    public class Authorization
    {
        public int Id { get; set; }
        public string AuthorizerName { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public DateTime? AuthorizationDateTime {
            get;
            set ;
        }
        public string IsHighHazardArea { get; set; }
        public string AreaSupervisorName { get; set; }
        public string AreaSupervisorDepartment { get; set; }
    }

    public class HotWork
    {
        // hand held ID
        public int Id { get; set; }
        public int? HotWorkMobileId { get; set; }
        public bool? IsParent { get; set; }
        public int? ParentHotWorkId { get; set; }
        public string WorkFlowStatus { get; set; }
        public Scope Scope { get; set; }
        public string GUID { get; set; }
        public bool? isDeleted { get; set; }
        public bool IsScannedPermits { get; set; }
        public string CloseAuthorizedBy { get; set; }
        public FirePrevention FirePrevention { get; set; }
        public List<PrecautionList> PrecautionList { get; set; }
        public Authorization Authorization { get; set; }
        public string ScannedPaperPermitGUID { get; set; }
        public byte[] ScannedPermits { get; set; }
        public DateTime? CloseDateTime { get; set; }
    }

    public class Data
    {
        public List<HotWork> HotWorks { get; set; }
        public List<Model.SyncImpairements.SyncImpairements> Impairments { get; set; }
        public List<HotWorkSyncResult> HotWorkSyncResults { get; set; }
    }

    public class Status
    {
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
    }

    public class RootObject
    {
        public Data Data { get; set; }
        public Status Status { get; set; }
    }


    public class ScanPermitRootObject
    {
        public string Data { get; set; }
        public Status Status { get; set; }
    }

    public class HotWorkSyncResult
    {
        public int Id { get; set; }
        public int HotWorkMobileId { get; set; }
        public string SyncStatus { get; set; }
        public string Message { get; set; }
    }

    public class TempHtWorkDetailResponse
    {
        public HotWork Data { get; set; }
        public Status Status { get; set; } 
    }

    public class CreateHWResponse
    {
        public HWCreateData Data { get; set; }
        public Status Status { get; set; }
    }

    public class HWCreateData {
        public int Id { get; set; }
        public string GUID { get; set; }
    }

    public class CreateIMPResponse
    {
        public ImpairmentCreateData Data { get; set; }
        public Status Status { get; set; }
    }

    public class ImpairmentCreateData
    {
        public int Id { get; set; }
        public string GUID { get; set; }
    }

    public class EmailHWResponse
    {
        public bool? Data { get; set; }
        public Status Status { get; set; }
    }

    public class ScanPermitImgResponse
    {
        public string Data { get; set; }
        public Status Status { get; set; }
    }

}
