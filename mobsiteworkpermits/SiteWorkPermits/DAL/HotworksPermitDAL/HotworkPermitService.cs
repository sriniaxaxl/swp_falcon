﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using CustomControl;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class HotworkPermitService : IHotworkPermitService
    {
        private readonly DatabaseContext _databaseContext;

        public HotworkPermitService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddHotworksAsync(HotworksPermits hotworksPermits)
        {
            try
            {
                var hotworksPermit = _databaseContext.HotworksPermits.Add(hotworksPermits);
                _databaseContext.SaveChanges();
                var isAdded = hotworksPermit.State == EntityState.Unchanged;
                if (isAdded)
                {
                    if (hotworksPermits.Status == HWStatus.UPCOMING)
                    {
                        var scope = _databaseContext.Scopes.Where(p => p.ScopeId == hotworksPermits.Scopes.ScopeId).First();


                        if (hotworksPermits.Scopes.StartDate >= DateTime.Now)
                        {
                            DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotworksPermits.HandHeldId.Value));
                            DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification("MySite Notification", "Hot Works Permit " + hotworksPermits.HandHeldId + " has started.", CustomControl.Validation.AlarmStartUniqueId(hotworksPermits.HandHeldId.Value), scope.StartDate);
                        }
                        if (hotworksPermits.Scopes.EndDate >= DateTime.Now)
                        {
                            DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotworksPermits.HandHeldId.Value));
                            DependencyService.Get<ISetAlertHotworks>().SetEndAlertNotification("MySite Notification", "Hot Works Permit " + hotworksPermits.HandHeldId + " has ended.", CustomControl.Validation.AlarmEndUniqueId(hotworksPermits.HandHeldId.Value), scope.EndDate);
                        }

                        if (hotworksPermits.Scopes.EndDate.AddHours(1) >= DateTime.Now)
                        {
                            DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotworksPermits.HandHeldId.Value));
                            DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("MySite Notification", "First Fire Watch for  Hot Works Permit " + hotworksPermits.HandHeldId + " is scheduled now.", CustomControl.Validation.FireWatcher1UniqueId(hotworksPermits.HandHeldId.Value), scope.EndDate.AddHours(1));
                        }

                        if (hotworksPermits.Scopes.EndDate.AddHours(2) >= DateTime.Now)
                        {
                            DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotworksPermits.HandHeldId.Value));
                            DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("MySite Notification", "Second Fire Watch for  Hot Works Permit " + hotworksPermits.HandHeldId + " is scheduled now.", CustomControl.Validation.FireWatcher2UniqueId(hotworksPermits.HandHeldId.Value), scope.EndDate.AddHours(2));
                        }


                        //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("MySite Notification", "First Fire Watch for  Hot Works Permit " + hotworksPermits.HandHeldId + " is scheduled now.", Validation.FireWatcher1UniqueId(_databaseContext.HotworksPermits.LastOrDefault().HotworksPermitsId), scope.EndDate.AddHours(1));
                        //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("MySite Notification", "Second Fire Watch for  Hot Works Permit " + hotworksPermits.HandHeldId + " is scheduled now.", Validation.FireWatcher2UniqueId(_databaseContext.HotworksPermits.LastOrDefault().HotworksPermitsId), scope.EndDate.AddHours(2));
                    }
                    if (hotworksPermits.IsDeleted)
                    {
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotworksPermits.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotworksPermits.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotworksPermits.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotworksPermits.HandHeldId.Value));
                    }
                }

                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<HotworksPermits> GetHotworksAsync()
        {
            try
            {
                var hotworksPermits = _databaseContext.HotworksPermits.
                                                      Include(bc => bc.Scopes).ThenInclude(p => p.ScopeWorkOrders).
                                                      Include(bc => bc.Authorizations).
                                                      Include(bc => bc.FirePreventionMeasures).ThenInclude(bc => bc.FirewatchPersons).
                                                      Include(bc => bc.PermitPrecautions).Where(p => p.SiteId == App.SiteId).ToList();
                return hotworksPermits;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public HotworksPermits GetHotworksByIdAsync(int id)
        {
            var hotworksPermits = _databaseContext.HotworksPermits.Where(p => p.HotworksPermitsId == id).
                                                  Include(bc => bc.Scopes).ThenInclude(p => p.ScopeWorkOrders).
                                                  Include(bc => bc.Authorizations).
                                                  Include(bc => bc.FirePreventionMeasures).ThenInclude(bc => bc.FirewatchPersons).
                                                  Include(bc => bc.PermitPrecautions);
            //  _databaseContext.SaveChanges();
            //var isUpdated = hotworksPermits.State == EntityState.Modified;
            //if (isUpdated)
            //{
            //    //if (hotworksPermit.status == HWStatus.UPCOMING ||
            //    //    hotworksPermit.status == HWStatus.ACTIVE ||
            //    //    hotworksPermit.status == HWStatus.DRAFT)
            //    //{
            //    //    var startDate = DateTime.ParseExact(hotworksPermit.scope.start_date + " " + hotworksPermit.scope.start_time, "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture).ToLocalTime();
            //    //    var endDate = DateTime.ParseExact(hotworksPermit.scope.end_date + " " + hotworksPermit.scope.end_time, "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture).ToLocalTime();
            //    //    DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification("Hotwork", "Started", Validation.AlarmStartUniqueId(hotworksPermit.id), startDate);
            //    //    DependencyService.Get<ISetAlertHotworks>().SetGoingToEndAlertNotification("Hotwork", "To be Expired", Validation.AlarmToBeEndUniqueId(hotworksPermit.id), endDate.AddMinutes(-5));
            //    //    DependencyService.Get<ISetAlertHotworks>().SetEndAlertNotification("Hotwork", "Expired", Validation.AlarmEndUniqueId(hotworksPermit.id), endDate);
            //    //}
            //}
            //return isUpdated;
            return hotworksPermits.FirstOrDefault();
        }

        public bool RemoveHotworksAsync(int id)
        {
            return true;
        }

        public bool UpdateHotworksAsync(HotworksPermits hotworksPermit)
        {
            try
            {
                var hotworksPermits = _databaseContext.HotworksPermits.Update(hotworksPermit);
                _databaseContext.SaveChanges();
                var isUpdated = hotworksPermits.State == EntityState.Unchanged;

                if (isUpdated)
                {
                    if (hotworksPermit.Status == HWStatus.UPCOMING)
                    {

                        var scope = _databaseContext.Scopes.Where(p => p.ScopeId == hotworksPermit.Scopes.ScopeId).First();
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification("MySite Notification", "Hot Works Permit " + hotworksPermit.HandHeldId + " has started.", CustomControl.Validation.AlarmStartUniqueId(hotworksPermit.HandHeldId.Value), scope.StartDate);
                        DependencyService.Get<ISetAlertHotworks>().SetEndAlertNotification("MySite Notification", "Hot Works Permit " + hotworksPermit.HandHeldId + " has ended.", CustomControl.Validation.AlarmEndUniqueId(hotworksPermit.HandHeldId.Value), scope.EndDate);

                        DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("MySite Notification", "First Fire Watch for  Hot Works Permit " + hotworksPermit.HandHeldId + " is scheduled now.", CustomControl.Validation.FireWatcher1UniqueId(hotworksPermit.HandHeldId.Value), scope.EndDate.AddHours(1));
                        DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("MySite Notification", "Second Fire Watch for  Hot Works Permit " + hotworksPermit.HandHeldId + " is scheduled now.", CustomControl.Validation.FireWatcher2UniqueId(hotworksPermit.HandHeldId.Value), scope.EndDate.AddHours(2));
                        //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("MySite Notification", "First Fire Watch for  Hot Works Permit " + hotworksPermit.HandHeldId + " is scheduled now.", Validation.FireWatcher1UniqueId(_databaseContext.HotworksPermits.LastOrDefault().HotworksPermitsId), scope.EndDate.AddHours(1));
                        //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("MySite Notification", "Second Fire Watch for  Hot Works Permit " + hotworksPermit.HandHeldId + " is scheduled now.", Validation.FireWatcher2UniqueId(_databaseContext.HotworksPermits.LastOrDefault().HotworksPermitsId), scope.EndDate.AddHours(2));
                    }
                    if (hotworksPermit.IsDeleted)
                    {
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotworksPermit.HandHeldId.Value));
                        DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotworksPermit.HandHeldId.Value));
                    }
                }

                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void RemoveAllData()
        {
            var hwList = _databaseContext.HotworksPermits.ToList();
            foreach (HotworksPermits hotWorksPermit in hwList)
            {
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotWorksPermit.HandHeldId.Value));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotWorksPermit.HandHeldId.Value));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotWorksPermit.HandHeldId.Value));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotWorksPermit.HandHeldId.Value));
            }
            var itemsToDelete = _databaseContext.Set<HotworksPermits>();
            _databaseContext.HotworksPermits.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
