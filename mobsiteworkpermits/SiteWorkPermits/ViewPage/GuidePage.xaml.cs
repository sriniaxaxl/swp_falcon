﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.ViewModel;
using Prism.Navigation;
using Xamarin.Forms;

namespace SiteWorkPermits.ViewPage
{
    public partial class GuidePage : ContentPage
    {
        List<HelpModel> HelpCollection = new List<HelpModel>();
        GuideViewModel Model; 
        public GuidePage()
        {
            InitializeComponent();
           // On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            NavigationPage.SetHasNavigationBar(this, false);

            if(Device.Idiom == TargetIdiom.Phone){
                HelpCollection.Add(new HelpModel() { ImgName = "dashboard.png", Label = "Dashboard" });
                HelpCollection.Add(new HelpModel() { ImgName = "hw_dashboard.png", Label = "Hot Works Dashboard" });
                HelpCollection.Add(new HelpModel() { ImgName = "hw_print.png", Label = "Print and Email" });
                HelpCollection.Add(new HelpModel() { ImgName = "imp_dashboard.png", Label = "Impairment Dashboard" });
                HelpCollection.Add(new HelpModel() { ImgName = "imp_print.png", Label = "Print and Submit" });    
            }else{
                HelpCollection.Add(new HelpModel() { ImgName = "dashboard_ipad.png", Label = "Dashboard" });
                HelpCollection.Add(new HelpModel() { ImgName = "hw_dashboard_ipad.png", Label = "Hot Works Dashboard" });
                HelpCollection.Add(new HelpModel() { ImgName = "hw_print_ipad.png", Label = "Print and Email" });
                HelpCollection.Add(new HelpModel() { ImgName = "imp_dashboard_ipad.png", Label = "Impairment Dashboard" });
                HelpCollection.Add(new HelpModel() { ImgName = "imp_print_ipad.png", Label = "Print and Submit" });
            }

            var deslst = HelpCollection.Select(s => s.Label);
            HelpCollection.ToList().ForEach(s => s.Index = Array.IndexOf(deslst.ToArray(), s.Label));
            CView.ItemsSource = HelpCollection;

            if (Helper.Getval(Helper.WelcomeShow).ToString() == "true")
            {
                Header.IsVisible = true;
                Skipview.IsVisible = false;
            }
            else
            {
                Header.IsVisible = false;
                Skipview.IsVisible = true;
            }
        }

        async void SkipClicked(object sender, System.EventArgs e)
        {            
            await Model._navigationService.NavigateAsync("LoginPage", null, null, false);
        }

        protected async override void OnAppearing()
		{
            base.OnAppearing();
            Model =(GuideViewModel)this.BindingContext;           
            await Helper.Setval(Helper.WelcomeShow,"true");
		}

		private void ViewChanged(object sender, System.EventArgs e)
        {
            StackLayout sp = (StackLayout)sender;
            if(sp.BindingContext != null)
            {
                sp.Children.Clear();
                HelpModel selmodel = (HelpModel)sp.BindingContext;
                var ind = selmodel.Index;
                foreach(var d in HelpCollection)
                {
                    Image imgdot = new Image();
                    if(d.Index == ind)
                    {
                        imgdot.Source = "green_dot.png";
                    }
                    else
                    {
                        imgdot.Source = "dark_gray.png";   
                    }
                    imgdot.HeightRequest = 10;
                    imgdot.WidthRequest = 10;
                    sp.Children.Add(imgdot);
                }                                  
            }
        }
    }

    public class HelpModel
    {
        public string ImgName { get; set; }
        public string Label { get; set; }
        public int Index { get; set; }
    }
}
