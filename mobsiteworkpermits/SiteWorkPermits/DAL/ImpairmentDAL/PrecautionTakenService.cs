﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public class PrecautionTakenService:IPrecautionTakenService
    {
        private readonly DatabaseContext _databaseContext;
        public PrecautionTakenService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddPrecautionTaken(PrecautionTaken precautionTaken)
        {
            try
            {
                var status = _databaseContext.PrecautionTakens.Add(precautionTaken);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<PrecautionTaken> GetPrecautionTaken()
        {
            try
            {
                var precautionTakens = _databaseContext.PrecautionTakens.ToList();
                return precautionTakens;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public PrecautionTaken GetPrecautionTakenById(int id)
        {
            try
            {
                var precautionTakens  = _databaseContext.PrecautionTakens.Where(p => p.ImpairmentPrecautionId == id);
                return precautionTakens.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<PrecautionTaken>();
            _databaseContext.PrecautionTakens.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
