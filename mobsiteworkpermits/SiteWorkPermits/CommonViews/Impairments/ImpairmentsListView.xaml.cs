﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class ImpairmentsListView : ContentView
    {
        public ObservableCollection<ModelPermits> modelPermits;
        public ImpairmentsListView(ObservableCollection<ModelPermits> lst, bool draft)
        {
            InitializeComponent();

            if (lst != null)
            {
                if (lst.Count == 0)
                {
                    nohotworksLabel.IsVisible = true;
                    return;
                }
                modelPermits = lst;

                foreach (var d in lst)
                {
                    nohotworksLabel.IsVisible = false;
                    ImpairmentsView hotview = new ImpairmentsView(draft);
                    hotview.Collection = d;
                    hotview.BindingContext = d;
                    Splst.Children.Add(hotview);
                }
            }
            else
            {
                nohotworksLabel.IsVisible = true;
            }
        }
    }
}
