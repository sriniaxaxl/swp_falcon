﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SiteWorkPermits.CommonViews
{
    public class ItemList: ObservableCollection<ModelPermits>
    {
        public string DateHeading { get; set; }
        public ObservableCollection<ModelPermits> ModelPermits => this;
    }
}
