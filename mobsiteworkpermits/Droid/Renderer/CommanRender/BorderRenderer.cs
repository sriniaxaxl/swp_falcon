using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace Android.Renderer.CommanRender
{
    public class BorderRenderer : IDisposable
    {
        #region Parent override

        private GradientDrawable _background;

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_background != null)
                {
                    _background.Dispose();
                    _background = null;
                }
            }
        }

        #endregion

        #region Public API

        public Drawable GetBorderBackground(Color borderColor, Color backgroundColor, float borderWidth, Thickness borderRadius)
        {
            System.Diagnostics.Debug.WriteLine(borderRadius.Left.ToString(), borderRadius.Top.ToString(), borderRadius.Right.ToString(), borderRadius.Bottom.ToString());
            if (_background != null)
            {
                _background.Dispose();
                _background = null;
            }
            borderWidth = borderWidth > 0 ? borderWidth : 0;
            //borderRadius = borderRadius > 0 ? borderRadius : 0;
            //borderRadius = borderRadius;
            borderColor = borderColor != Color.Default ? borderColor : Color.Transparent;
            backgroundColor = backgroundColor != Color.Default ? backgroundColor : Color.Transparent;
            var strokeWidth = Xamarin.Forms.Forms.Context.ToPixels(borderWidth);
            var TopLeftradius = Xamarin.Forms.Forms.Context.ToPixels(borderRadius.Left);
            var TopRightradius = Xamarin.Forms.Forms.Context.ToPixels(borderRadius.Top);
            var Bottomrightradius = Xamarin.Forms.Forms.Context.ToPixels(borderRadius.Right);
            var BottomLeftradius = Xamarin.Forms.Forms.Context.ToPixels(borderRadius.Bottom);
            _background = new GradientDrawable();
            _background.SetColor(backgroundColor.ToAndroid());
            
            //if (radius > 0)
            //_background.SetCornerRadius(radius);
            _background.SetCornerRadii(new float[] { TopLeftradius, TopLeftradius, TopRightradius, TopRightradius, Bottomrightradius, Bottomrightradius, BottomLeftradius, BottomLeftradius });
            if (borderColor != Color.Transparent && strokeWidth > 0)
            {
                _background.SetStroke((int)strokeWidth, borderColor.ToAndroid());
            }
            return _background;
        }

        #endregion`
    }
}