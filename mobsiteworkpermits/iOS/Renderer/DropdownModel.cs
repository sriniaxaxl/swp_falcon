﻿using System;
using System.Collections.Generic;
using CustomControl;
using UIKit;

namespace iOS.Renderer
{
    public class DropdownModel : UIPickerViewModel
    {
        List<string> LstItem;  
        public EventHandler<ValueArgs> ValueChanged;  
        public string SelectedValue;
        public int SelectedIndex;
        public DropdownModel(List < string > lstitem) {  
            this.LstItem = lstitem;  
        }  
        public override nint GetRowsInComponent(UIPickerView pickerView, nint component) {  
            return LstItem.Count;  
        }  
        public override nint GetComponentCount(UIPickerView pickerView) {  
            return 1;  
        }  
        public override string GetTitle(UIPickerView pickerView, nint row, nint component) {  
            return LstItem[(int) row];  
        }  
        public override void Selected(UIPickerView pickerView, nint row, nint component) {
            var Selitem = LstItem[(int) row];  
            SelectedValue = Selitem;
            SelectedIndex = (int)row;
            ValueArgs args = new ValueArgs();
            args.SelectedItem = SelectedValue;
            args.SelectedIndex = SelectedIndex;

            ValueChanged ? .Invoke(null, args);  
        }  
    }
}
