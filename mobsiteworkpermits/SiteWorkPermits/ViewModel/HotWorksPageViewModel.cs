﻿using System;
using System.Collections.ObjectModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.Model;
using System.Linq;
using SiteWorkPermits;
using Xamarin.Forms;
using System.Collections.Generic;
using SiteWorkPermits.Model.SyncHotWorks;
using CustomControl;
using System.Threading.Tasks;
using SiteWorkPermits.DAL.RestHelper;
using Plugin.Connectivity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SiteWorkPermits.Model.SyncImpairements;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.Platform;
using System.Diagnostics;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.DAL.sqlIntegration;
using static CustomControl.Validation;
using System.Windows.Input;
using SQLite;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public class HotWorksPageViewModel : BindableBase, INavigatedAware
    {
        private IHotworkPermitService _hotworkPermitService;
        public INavigationService _navigationService;
        IScopeService _scopeService;
        IPageDialogService _pageDialog;
        IImpairmentService _impairmentService;
        public delegate void RefreshListHotWorksAction();
        public event RefreshListHotWorksAction RefreshListHotWorksEvent;
        IWorkOrdersService _workOrdersService;

        IIncUniqueid _incUniqueid;

        IAuthorizationService _authorizationService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        IHighHazardAreasService _highHazardAreasService;

        private ObservableCollection<ModelWorkPermit> _workPermits;
        private ObservableCollection<ModelPermits> _ActivePermitCollection;
        public ObservableCollection<ModelPermits> ActivePermitCollection
        {
            get { return _ActivePermitCollection; }
            set { SetProperty(ref _ActivePermitCollection, value); }
        }

        private ObservableCollection<ModelPermits> _DraftPermitCollection;
        public ObservableCollection<ModelPermits> DraftPermitCollection
        {
            get { return _DraftPermitCollection; }
            set { SetProperty(ref _DraftPermitCollection, value); }
        }

        private ObservableCollection<ModelPermits> _UpcomingPermitCollection;
        public ObservableCollection<ModelPermits> UpcomingPermitCollection
        {
            get { return _UpcomingPermitCollection; }
            set { SetProperty(ref _UpcomingPermitCollection, value); }
        }

        private ObservableCollection<ModelPermits> _ExpiredPermitCollection;
        public ObservableCollection<ModelPermits> ExpiredPermitCollection
        {
            get { return _ExpiredPermitCollection; }
            set { SetProperty(ref _ExpiredPermitCollection, value); }
        }

        public DelegateCommand<string> OnNavigateCommand { get; set; }


        //public DelegateCommand<string> OnSyncCommand { get; set; }

        public HotWorksPageViewModel(IHotworkPermitService hotworkPermitService,
                                     INavigationService navigationService,
                                     IPageDialogService pageDialog,
                                 IWorkOrdersService workOrdersService,
                                     IIncUniqueid incUniqueid,
                                 IScopeService scopeService, IAuthorizationService authorizationService,
                                 IFirePreventionMeasuresService firePreventionMeasuresService,
                                 IPrecautionsService precautionsService,
                                     IImpairmentService impairmentService,
                                 IFirewatchPersonsService firewatchPersonsService,
                                 IHighHazardAreasService highHazardAreasService)
        {
            OnNavigateCommand = new DelegateCommand<string>(NavigateAsync);
          //  OnSyncCommand = new DelegateCommand(async () => await SyncData());
            _navigationService = navigationService;
            _workOrdersService = workOrdersService;
            _scopeService = scopeService;
            _pageDialog = pageDialog;
            _incUniqueid = incUniqueid;
            _impairmentService = impairmentService;
            _hotworkPermitService = hotworkPermitService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _scopeService = scopeService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _highHazardAreasService = highHazardAreasService;
            _authorizationService = authorizationService;
            SetHotworksData();
            ShowSyncIcon = true;
            ShowActivityLoader = false;
            App.isImpairementEditorDuplicate = "";

            SelectedDate = DateTime.Now;
            SelectedTime = SelectedDate.TimeOfDay;
           
            HWSelectedDate = DateTime.Now;
            HWSelectedTime = HWSelectedDate.TimeOfDay;
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("HOT WORKS PERMITS DASHBOARD");
            activeTabColor = Color.FromHex("#00AFAD");
        }

        public ICommand OnSyncCommand
        {
            get
            {
                return new Command(async (x) =>
                {
                    try
                    {
                        await SyncData();
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
        }

        private async Task SyncData()
        {
        //    PopupMsg = AppResource.DASHBOARD_SYNC_MSG;
            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });

                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();
                var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                //todo: store scanned permit in temp list
                App.scannedPermitModels = new List<ScannedPermitModel>();
                foreach (var hw in hotwork_sql_List)
                {

                    if (hw.ScannedPaperPermit != null && hw.ScannedPaperPermitGUID != null)
                    {
                        hw.IsScannedPermits = true;
                        connection.UpdateWithChildren(hw);
                        App.scannedPermitModels.Add(new ScannedPermitModel
                        {
                            ScannedPaperPermit = hw.ScannedPaperPermit,
                            ScannedPaperPermitGUID = hw.ScannedPaperPermitGUID
                        });
                    }
                }



                //ShowActivityLoader = true;
                bool isSyncDone = await CustomControl.Validation.IsSyncDone();


                if (isSyncDone)
                {
                    SetUnSyncAlarm();
                    //TODO: sync paper permit
                    // PopupMsg = "Submitting scanned paper permit. Please wait..";
                    //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                    //var connection = hotworkDatabase.GetdbConnection();
                    //var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                    ScanPermitRootObject syncPermitResponse = new ScanPermitRootObject();
                    if (App.scannedPermitModels.Count() > 0)
                    {
                        syncPermitResponse = await RestApiHelper<ScanPermitRootObject>.
                           SyncMultipartData(App.scannedPermitModels);
                    }

                    if (syncPermitResponse != null)
                    {

                        if (string.IsNullOrEmpty(syncPermitResponse.Data))
                        {

                        }
                        else
                        {

                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.SCANNED_PAPER_SUBMIT_SUCCESS, AppResource.OK_LABEL);

                        }

                    }
                }



                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = false;
                    SetHotworksData();
                    ChangeTabColor("active");
                });

            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_HOTWORK_LABEL, AppResource.OK_LABEL);
            }
        }

        private TimeSpan _selectedTime;
        public TimeSpan SelectedTime
        {
            get { return _selectedTime; }
            set { SetProperty(ref _selectedTime, value); }
        }

        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set { SetProperty(ref _selectedDate, value); }
        }


        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }

        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

        private string _uniqueId;
       

        private ObservableCollection<ListViewElement> _listViewElements;

        public ObservableCollection<ListViewElement> ListViewElements
        {
            get { return _listViewElements; }
            set { SetProperty(ref _listViewElements, value); }
        }

        private ObservableCollection<ListViewElement> ListViewElements_Temp;

     

        private Color _activeTabColor;
        public Color activeTabColor
        {
            get { return _activeTabColor; }
            set { SetProperty(ref _activeTabColor, value); }
        }

        private Color _upcomingTabColor;
        public Color upcomingTabColor
        {
            get { return _upcomingTabColor; }
            set { SetProperty(ref _upcomingTabColor, value); }
        }
          private Color _closeTabColor;
        public Color closeTabColor
        {
            get { return _closeTabColor; }
            set { SetProperty(ref _closeTabColor, value); }
        }

        private Color _expiredTabColor;
        public Color expiredTabColor
        {
            get { return _expiredTabColor; }
            set { SetProperty(ref _expiredTabColor, value); }
        }

        private Color _draftTabColor;
        public Color draftTabColor
        {
            get { return _draftTabColor; }
            set { SetProperty(ref _draftTabColor, value); }
        }


       

        public ICommand OnTabChangedCommand
        {
            get
            {
                return new Command<string>((x) => {
                    try
                    {
                        ChangeTabColor(x);

                        ListViewElements = ListViewElements_Temp;

                        if (x.ToLower() == "expired")
                        {
                            var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals(x.ToLower()) || w.ElementStatus.ToLower()=="closed");
                            ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                        }
                        else {
                            var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals(x.ToLower()));
                            ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
                        }


                        if (ListViewElements.Count > 0)
                        {
                            showNoHWLabel = false;
                            noHWString = "";
                        }
                        else
                        {
                            showNoHWLabel = true;
                            noHWString = x.ToUpper();
                        }

                    }
                    catch (Exception ex) { }
                });
            }
        }


        private bool _showHWClosePopup;
        public bool showHWClosePopup
        {
            get { return _showHWClosePopup; }
            set { SetProperty(ref _showHWClosePopup, value); }
        }
           private bool _LoaderLayout;
        public bool LoaderLayout
        {
            get { return _LoaderLayout; }
            set { SetProperty(ref _LoaderLayout, value); }
        }


        private bool _showNoHWLabel;
        public bool showNoHWLabel
        {
            get { return _showNoHWLabel; }
            set { SetProperty(ref _showNoHWLabel, value); }
        }
            private string _noHWString;
        public string noHWString
        {
            get { return _noHWString; }
            set { SetProperty(ref _noHWString, value); }
        }


        HotWorkPermit_sql _workPermit_Sql;
        Scope_sql _workPermit_scope_Sql;

        private string _workorder_sql;
        public string Workorder_sql
        {
            get { return _workorder_sql; }
            set { SetProperty(ref _workorder_sql, value); }
        }

        private string _workLocation_Sql;
        public string WorkLocation_Sql
        {
            get { return _workLocation_Sql; }
            set { SetProperty(ref _workLocation_Sql, value); }
        }

        private string _workPermit_date_Sql;
        public string WorkPermit_date_Sql
        {
            get { return _workPermit_date_Sql; }
            set { SetProperty(ref _workPermit_date_Sql, value); }
        }


        private TimeSpan _hWSelectedTime;
        public TimeSpan HWSelectedTime
        {
            get { return _hWSelectedTime; }
            set { SetProperty(ref _hWSelectedTime, value); }
        }

        private DateTime _hWSelectedDate;
        public DateTime HWSelectedDate
        {
            get { return _hWSelectedDate; }
            set { SetProperty(ref _hWSelectedDate, value); }
        }


        private string _elementCategory_sql;
        public string ElementCategory_sql
        {
            get { return _elementCategory_sql; }
            set { SetProperty(ref _elementCategory_sql, value); }
        }

        private string _elementDescription_Sql;
        public string ElementDescription_Sql
        {
            get { return _elementDescription_Sql; }
            set { SetProperty(ref _elementDescription_Sql, value); }
        }

        private string _elementStartDate_Sql;
        public string ElementStartDate_Sql
        {
            get { return _elementStartDate_Sql; }
            set { SetProperty(ref _elementStartDate_Sql, value); }
        }

        SQLiteConnection _connection;
        public ICommand OnHotWorkCloseCommand
        {
            get
            {
                return new Command( async(x) => {

                    var element = (x as CustomControl.Validation.ListViewElement);
                    //if (element.ElementStatus.ToLower() == "draft" || element.ElementStatus.ToLower() == "expired" || element.ElementStatus.ToLower() == "closed")
                    //{
                        App.isDuplicate = true;
                        App.isImpairementEditorDuplicate = "Duplicate";
                        var hotwork_sql = EditPermitByID((x as ListViewElement).ElementId);
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("hotworkPermit_sql", hotwork_sql);

                    //HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);

                        await _navigationService.NavigateAsync(new Uri("/NewHotWorksPage", UriKind.Relative), navigationParams, null, false);
                    //}
                    //else
                    //{
                    //    HWSelectedDate = DateTime.Now;
                    //  //  var element = (x as CustomControl.Validation.ListViewElement);
                    //    SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                    //    _connection = hotworkDatabase.GetdbConnection();
                    //    _workPermit_Sql = _connection.GetWithChildren<HotWorkPermit_sql>(element.ElementId, true);
                    //    _workPermit_scope_Sql = _connection.GetWithChildren<Scope_sql>(_workPermit_Sql.scope_Sql.id, recursive: true);
                    //    //   Workorder_sql = string.Join(", ", _workPermit_scope_Sql.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName));
                    //    //

                    //  ///  WorkLocation_Sql = _workPermit_scope_Sql.WorkLocation;
                    //  //  WorkPermit_date_Sql = _workPermit_scope_Sql.StartDateTime;



                    //    ElementCategory_sql = string.Join(", ", _workPermit_scope_Sql.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName));
                    //    ElementDescription_Sql = _workPermit_scope_Sql.WorkLocation;
                    //    ElementStartDate_Sql = _workPermit_scope_Sql.StartDateTime;



                    //    showHWClosePopup = true;
                    //}
                });

            }
        }


        public ICommand CloseHotworkCommand
        {
            get
            {
                return new Command(async (x) => {
                    _workPermit_Sql.isEdited = true;
                    _workPermit_Sql.hotWorkCloseDate = SelectedDate.Add(SelectedTime).ToString();
                    _workPermit_Sql.workFlowStatus = "CLOSED";
                    _connection.UpdateWithChildren(_workPermit_Sql);
                    showHWClosePopup = false;
                    await IsHWCloseEmailSent(_workPermit_Sql);
                });
            }
        }

        public ICommand CancelHotworkCommand
        {
            get
            {
                return new Command((x) =>
                {
                    try
                    {
                        showHWClosePopup = false;
                    }
                    catch (Exception ex) { }
                });
            }
        }


        public ICommand OnElementSelectEdit
        {
            get
            {
                return new Command(async (x) => {

                    if ((x as ListViewElement).ElementStatus.ToLower() == "expired" || (x as ListViewElement).ElementStatus.ToLower() == "closed")
                    {
                        App.isImpairementEditorDuplicate = "Archieve";
                    }
                    else {
                        App.isImpairementEditorDuplicate = "Edit";                       
                    }

                    var permit = CustomControl.Validation.EditPermitByID((x as ListViewElement).ElementId);
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("hotworkPermit_sql", permit);
                    await _navigationService.NavigateAsync(new Uri("/NewHotWorksPage", UriKind.Relative), navigationParams, null, false);

                });
            }
        }



        public void ChangeTabColor(string x)
        {
            switch (x.ToLower())
            {
                case "active":
                    activeTabColor = Color.FromHex("#00AFAD");
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.Transparent;
                    closeTabColor = Color.Transparent;
                    expiredTabColor = Color.Transparent;
                    break;
                case "draft":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.FromHex("#00AFAD");
                    upcomingTabColor = Color.Transparent;
                    closeTabColor = Color.Transparent;
                    expiredTabColor = Color.Transparent;
                    break;
                case "upcoming":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.FromHex("00AFAD");
                    closeTabColor = Color.Transparent;
                    expiredTabColor = Color.Transparent;
                    break;
                case "expired":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.Transparent;
                    closeTabColor = Color.Transparent;
                    expiredTabColor = Color.FromHex("00AFAD");
                    break;
                case "closed":
                    activeTabColor = Color.Transparent;
                    draftTabColor = Color.Transparent;
                    upcomingTabColor = Color.Transparent;
                    closeTabColor = Color.FromHex("#00AFAD");
                    expiredTabColor = Color.Transparent;
                    break;
            }
        }

        public void SetHotworksData()
        {
            try
            {
                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();

                var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                _workPermits = new ObservableCollection<ModelWorkPermit>();
                ListViewElements = new ObservableCollection<ListViewElement>();
                if (hotwork_sql_List.Count > 0)
                {
                    foreach (HotWorkPermit_sql hotWorkPermit_Sql in hotwork_sql_List)
                    {

                        if (!hotWorkPermit_Sql.isDeleted)
                        {
                            var scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql.id, recursive: true);
                            ListViewElements.Add(new ListViewElement
                            {
                                ElementId = hotWorkPermit_Sql.id,
                                ElementCategory = hotWorkPermit_Sql.server_id == 0 ? string.Join(", ", scope.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName)) : "#" + hotWorkPermit_Sql.server_id + "\n" + string.Join(", ", string.Join(", ", scope.workOrderMaster_Sqls.ToList().Select(p => p.workOrderName))),
                                ElementDescription = hotWorkPermit_Sql.scope_Sql.WorkLocation,
                                ElementEndDate = hotWorkPermit_Sql.scope_Sql.EndDateTime,
                                ElementStartDate = hotWorkPermit_Sql.scope_Sql.StartDateTime,
                                ElementStatus = hotWorkPermit_Sql.workFlowStatus,
                                ElementEvent = AppResource.DUPLICATE_PERMIT,
                                showCloseStatus = hotWorkPermit_Sql.workFlowStatus.ToLower() == "closed"?true:false                                
                            });
                            ListViewElements_Temp = new ObservableCollection<ListViewElement>(ListViewElements);
                        }                      

                    }

                    var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals("active"));
                    ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());


                    if (ListViewElements.Count > 0)
                    {
                        showNoHWLabel = false;
                        noHWString = "";
                    }
                    else {
                        showNoHWLabel = true;
                        noHWString = "ACTIVE";
                    }

                    UpcomingPermitCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                                      where d.HWStatus == HWStatus.UPCOMING
                                                                                      group d by d.HotworkDate into g
                                                                                      select new ModelPermits
                                                                                      {
                                                                                          Month = g.Key,
                                                                                          WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                                      });

                    foreach (ModelWorkPermit aa in UpcomingPermitCollection.SelectMany(k => k.WorkCollection))
                    {
                        aa.ExpiryDate = Convert.ToDateTime(aa.StartDate).ToString("dd MMM");
                    }

                    ActivePermitCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                                    where d.HWStatus == HWStatus.ACTIVE
                                                                                    group d by d.HotworkDate into g
                                                                                    select new ModelPermits
                                                                                    {
                                                                                        Month = g.Key,
                                                                                        WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                                    });

                    foreach (ModelWorkPermit aa in ActivePermitCollection.SelectMany(k => k.WorkCollection))
                    {
                        aa.ExpiryDate = Convert.ToDateTime(aa.StartDate).ToString("HH:mm") + " - " + Convert.ToDateTime(aa.ExpiryDate).ToString("HH:mm");
                    }


                    ExpiredPermitCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                                     where d.HWStatus == HWStatus.EXPIRED
                                                                                     group d by d.HotworkDate into g
                                                                                     select new ModelPermits
                                                                                     {
                                                                                         Month = g.Key,
                                                                                         WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                                     });

                    foreach (ModelWorkPermit aa in ExpiredPermitCollection.SelectMany(k => k.WorkCollection))
                    {
                        aa.ExpiryDate = "EXPIRED " + Convert.ToDateTime(aa.StartDate).ToString("dd MMM");
                    }

                    DraftPermitCollection = new ObservableCollection<ModelPermits>(from d in _workPermits
                                                                                   where d.HWStatus == HWStatus.DRAFT
                                                                                   group d by d.HotworkDate into g
                                                                                   select new ModelPermits
                                                                                   {
                                                                                       Month = g.Key,
                                                                                       WorkCollection = new ObservableCollection<ModelWorkPermit>(g.ToList())
                                                                                   });
                    foreach (ModelWorkPermit aa in DraftPermitCollection.SelectMany(k => k.WorkCollection))
                    {
                        if (!string.IsNullOrEmpty(aa.CreatedAt))
                            aa.ExpiryDate = "EDITED AT " + Convert.ToDateTime(aa.CreatedAt).ToString("dd MMM");
                    }
                    RefreshListHotWorksEvent?.Invoke();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void NavigateAsync(string page)
        {
            if (page.Equals("NewHotWorksPage"))
            {
                //MessagingCenter.Send(Xamarin.Forms.Application.Current, "NavigateToPermit");
                await _navigationService.NavigateAsync("HomePage/NavigationPage/NewHotWorksPage", null, null, false);
                App.FromHwToNewHw = true;
            }
            else if (page.Equals("Archive"))
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    await _navigationService.NavigateAsync("HomePage/NavigationPage/ArchivePage", null, null, false);
                    App.FromHwToArchive = true;                  
                }
                else
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, "ok");
                }
            }
            else
            {
                await _navigationService.NavigateAsync("HotWorksPage", null, null, false);
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            App.FromHwToArchive = true;
            App.FromHwToNewHw = true;
            App.FromImpToArchive = false;
            App.FromImpToNewImp = false;
        }

        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.Count > 0)
            {
                if (parameters.ContainsKey("FromNotification"))
                {
                    var fromNotification = (string)parameters["FromNotification"];
                    if(fromNotification.Equals("true")){
                        SyncData("");
                    }
                }
            }
        }

        private async void SyncData(string syncParameter)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });

                var hotWorksListFromServer = await GetOnlineDataList();

                if (hotWorksListFromServer == null)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.SOMETHING_WENT_WRONG, AppResource.OK_LABEL);
                    ShowActivityLoader = false;
                    return;
                }


                if (hotWorksListFromServer.Data == null)
                {
                    if (hotWorksListFromServer.Status != null)
                    {
                        if (hotWorksListFromServer.Status.StatusCode == 401)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", hotWorksListFromServer.Status.StatusMessage, "ok");
                            var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                            // Xamarin.Forms.Application.Current.Properties.Clear();

                            foreach (string key in keyList.ToList())
                            {
                                if (!key.Equals("WelcomeShow"))
                                {
                                    Application.Current.Properties.Remove(key);
                                }
                            }
                            await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                            ShowActivityLoader = false;
                            return;
                        }
                        else
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.SOMETHING_WENT_WRONG, AppResource.OK_LABEL);
                            ShowActivityLoader = false;
                            return;
                        }
                    }
                    else
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.SOMETHING_WENT_WRONG, AppResource.OK_LABEL);
                        ShowActivityLoader = false;
                        return;
                    }
                }
                //saving hotworks
                if (hotWorksListFromServer.Data.HotWorks == null)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    return;
                }
                else if (hotWorksListFromServer.Data.HotWorks.Count() == 0)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    return;
                }
                else
                {
                    // drop all data 
                    _scopeService.RemoveAllData();
                    _firewatchPersonsService.RemoveAllData();
                    _firePreventionMeasuresService.RemoveAllData();
                    _authorizationService.RemoveAllData();
                    _hotworkPermitService.RemoveAllData();

                    try
                    {
                        foreach (HotWork hotWork in hotWorksListFromServer.Data.HotWorks)
                        {
                            #region create scope 
                            // scope
                            var personNames = new List<string>();
                            if (hotWork.Scope.ScopePersons != null)
                            {
                                foreach (ScopePerson personName in hotWork.Scope.ScopePersons)
                                {
                                    personNames.Add(personName.PersonName);
                                }
                            }

                            var scopeWorkOrderList = new List<Model.ScopeWorkOrder>();

                            var scope = new Scopes
                            {
                                Location = hotWork.Scope.WorkLocation,
                                WorkType = (hotWork.Scope.WorkDetail == null) ? "" : hotWork.Scope.WorkDetail,
                                CompanyDepartementName = (hotWork.Scope.CompanyName == null) ? "" : hotWork.Scope.CompanyName,
                                _personPerforming = personNames,
                                StartDate = hotWork.Scope.StartDateTime.ToLocalTime(),
                                EndDate = hotWork.Scope.EndDateTime.ToLocalTime(),
                                EquipmentDetails = (hotWork.Scope.EquipmentDescription == null) ? "" : hotWork.Scope.EquipmentDescription
                            };

                            List<Model.ScopeWorkOrder> scopeWorkOrders = new List<Model.ScopeWorkOrder>();
                            if (hotWork.Scope.ScopeWorkOrders == null)
                            {

                            }
                            else
                            {
                                foreach (Model.SyncHotWorks.ScopeWorkOrder scopeWorkOrder in hotWork.Scope.ScopeWorkOrders)
                                {
                                    scopeWorkOrders.Add(new Model.ScopeWorkOrder
                                    {
                                        WorkOrderId = scopeWorkOrder.WorkOrderId,
                                    });
                                }
                            }
                            scope.ScopeWorkOrders = scopeWorkOrders;

                            #endregion

                            #region create fire prevention
                            // fire prevention

                            FirePreventionMeasures firePreventionMeasures = new FirePreventionMeasures();

                            firePreventionMeasures.NearestAlarm = hotWork.FirePrevention.NearestFireAlarmLocation;
                            firePreventionMeasures.NearestPhone = hotWork.FirePrevention.NearestPhone;

                            var fireWatcherPerson = new List<FirewatchPersons>();
                            //  var fireWatcherAfter = new List<FirewatchPersons>();

                            for (int i = 0; i < hotWork.FirePrevention.FireWatchPreventions.ToList().Count; i++)
                            {
                                if (hotWork.FirePrevention.FireWatchPreventions[i].FireWatchType == "AFTER")
                                {
                                    fireWatcherPerson.Add(new FirewatchPersons
                                    {
                                        FireWatcherType = FireWatcherType.AFTER,
                                        PersonName = hotWork.FirePrevention.FireWatchPreventions[i].PersonName,
                                        StartTime = Convert.ToDateTime(hotWork.FirePrevention.FireWatchPreventions[i].StartTime),
                                        EndTime = Convert.ToDateTime(hotWork.FirePrevention.FireWatchPreventions[i].EndTime),
                                    });
                                }
                                else if (hotWork.FirePrevention.FireWatchPreventions[i].FireWatchType == "DURING")
                                {
                                    fireWatcherPerson.Add(new FirewatchPersons
                                    {
                                        FireWatcherType = FireWatcherType.DURING,
                                        PersonName = hotWork.FirePrevention.FireWatchPreventions[i].PersonName,
                                        StartTime = Convert.ToDateTime(hotWork.FirePrevention.FireWatchPreventions[i].StartTime),
                                        EndTime = Convert.ToDateTime(hotWork.FirePrevention.FireWatchPreventions[i].EndTime),
                                    });
                                }
                            }

                            firePreventionMeasures.FirewatchPersons = fireWatcherPerson;

                            #endregion

                            #region auth
                            var serverAuth = hotWork.Authorization;
                            var authorization = new Authorizations
                            {
                                Name = (serverAuth.AuthorizerName == null) ? "" : serverAuth.AuthorizerName,
                                Departement = (serverAuth.Department == null) ? "" : serverAuth.Department,
                                Location = (serverAuth.Location == null) ? "" : serverAuth.Location,

                                AuthDate = serverAuth.AuthorizationDateTime.HasValue ? Convert.ToDateTime(serverAuth.AuthorizationDateTime).ToLocalTime() : DateTime.Now,
                                HighHazardArea = (serverAuth.IsHighHazardArea == "YES") ? true : false,
                                AreaSupervisorName = (serverAuth.AreaSupervisorName == null) ? "" : serverAuth.AreaSupervisorName,
                                AreaSupervisorDepartement = (serverAuth.AreaSupervisorDepartment == null) ? "" : serverAuth.AreaSupervisorDepartment

                            };

                            #endregion

                            var hotworksPermit = new HotworksPermits
                            {
                                Scopes = scope,
                                FirePreventionMeasures = firePreventionMeasures,
                                Authorizations = authorization,
                                IsDeleted = (hotWork.isDeleted.HasValue == true && hotWork.isDeleted.Value) ? true : false,
                                IsEdited = false,
                                HotWorkServerId = hotWork.Id,
                                IsNewlyCreated = false,
                                HandHeldId = hotWork.HotWorkMobileId,
                                SiteId = App.SiteId,
                            };

                            switch (hotWork.WorkFlowStatus)
                            {
                                case "DRAFT":
                                    hotworksPermit.Status = HWStatus.DRAFT;
                                    break;
                                case "ACTIVE":
                                    hotworksPermit.Status = HWStatus.ACTIVE;
                                    break;
                                case "UPCOMING":
                                    hotworksPermit.Status = HWStatus.UPCOMING;
                                    break;
                                case "EXPIRED":
                                    hotworksPermit.Status = HWStatus.EXPIRED;
                                    break;
                                case "ARCHIVE":
                                    hotworksPermit.Status = HWStatus.ARCHIVE;
                                    break;
                                default:
                                    hotworksPermit.Status = HWStatus.DRAFT;
                                    hotworksPermit.IsDeleted = true;
                                    break;
                            }


                            var permitPrecautions = new List<PermitPrecautions>();

                            foreach (Precautions precautions in _precautionsService.GetPrecautionsAsync().ToList().Take(11))
                            {
                                permitPrecautions.Add(new PermitPrecautions
                                {
                                    PrecautionsId = precautions.Id,
                                    PrecautionActions = precautions.PrecautionActions,
                                    NoOfFirehouses = "",
                                    NoOfFireExtinguiser = ""
                                });
                            }

                            if (hotWork.PrecautionList != null)
                            {
                                foreach (PrecautionList precaution in hotWork.PrecautionList)
                                {
                                    permitPrecautions.Where(pc => pc.PrecautionsId == precaution.MeasureId).FirstOrDefault().PrecautionActions = GetPrecautionAction(precaution.MeasureStatus);
                                    permitPrecautions.Where(pc => pc.PrecautionsId == precaution.MeasureId).FirstOrDefault().NoOfFirehouses = precaution.FireHoseCount.ToString();
                                    permitPrecautions.Where(pc => pc.PrecautionsId == precaution.MeasureId).FirstOrDefault().NoOfFireExtinguiser = precaution.FireExtinguisherCount.ToString();
                                }
                            }

                            if (!hotWork.HotWorkMobileId.HasValue)
                            {
                                int uniqueId = _incUniqueid.GetUniqueIds().LastOrDefault().UniqueIncrementalId;
                                //HWUniqueNumberLabel = "No. " + Validation.GetHandHeldId() + "" + _hotworkPermitService.GetHotworksAsync().ToList().Count().ToString();
                                var hotWorksHandHeldID = CustomControl.Validation.GetHandHeldId() + "" + (uniqueId + 1).ToString();
                                _incUniqueid.AddIncUniqueId(new IncUniqueId { UniqueIncrementalId = uniqueId + 1 });
                                hotworksPermit.HandHeldId = Convert.ToInt32(hotWorksHandHeldID);
                            }
                            else
                            {
                                hotworksPermit.HandHeldId = hotWork.HotWorkMobileId;
                            }

                            hotworksPermit.PermitPrecautions = permitPrecautions;
                            hotworksPermit.PermitPrecautions = hotworksPermit.PermitPrecautions.
                                GroupBy(x => x.PrecautionsId).Select(g => g.First()).ToList();
                            hotworksPermit.Scopes.ScopeWorkOrders = hotworksPermit.Scopes.ScopeWorkOrders.
                                GroupBy(x => x.WorkOrderId).Select(g => g.First()).ToList();
                            _hotworkPermitService.AddHotworksAsync(hotworksPermit);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("IOException source: {0}", e.Source);
                        ShowActivityLoader = false;
                    }
                }


                //saving impairments
                if (hotWorksListFromServer.Data.Impairments == null)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    return;
                }
                else if (hotWorksListFromServer.Data.Impairments.Count() == 0)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    SetHotworksData();
                    return;
                }
                else
                {
                    await SaveImpairments(hotWorksListFromServer.Data.Impairments);
                }

                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = false;
                    SetHotworksData();
                });

            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", "No internet connectivity is available. Please click on Sync button when internet connectivity is back.", "ok");
            }
        }

        private async Task<bool> IsHWCloseEmailSent(HotWorkPermit_sql permit_Sql)
        {

            if (CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    LoaderLayout = true;
                    ShowActivityLoader = true;
                });
                try
                {
                    var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                    analyticsService.LogEvent("", "Permit Submit", "Permit Submit");
                    string permitguid = string.Empty;
                    int permitserverid = 0;
                    if (permit_Sql.server_id == 0)
                    {
                        var serverHotwork = CustomControl.Validation.GetServerHotwork(permit_Sql);
                        //    impairements.Result.CloseDateTime = Convert.ToDateTime(impairment.impairmentCloseDateTime);
                        _connection.UpdateWithChildren(permit_Sql);
                        var createHWResponse = await RestApiHelper<CreateIMPResponse>.CreateHW_Post(serverHotwork.Result);

                        if (createHWResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", "Email not sent, Please try again.", "ok");
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                                LoaderLayout = false;
                            });
                            return false;
                        }
                        else if (createHWResponse.Data != null)
                        {
                            permitguid = createHWResponse.Data.GUID;
                            permitserverid = createHWResponse.Data.Id;
                        }
                        else if (createHWResponse.Status != null)
                        {
                            if (createHWResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", createHWResponse.Status.StatusMessage, "ok");
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                // Xamarin.Forms.Application.Current.Properties.Clear();

                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                LoaderLayout = false;
                                return false;
                            }
                        }

                        permit_Sql.server_id = permitserverid;
                        permit_Sql.hotworkGUID = permitguid;
                        //SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                        //    var connection = hotworkDatabase.GetdbConnection();
                        _connection.UpdateWithChildren(permit_Sql);
                    }
                    else
                    {
                        _connection.UpdateWithChildren(permit_Sql);
                        await IsSyncDone();
                        permitguid = permit_Sql.hotworkGUID;
                    }

                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.CLOSE_HOTWORK_PERMIT, AppResource.OK_LABEL);


                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                        LoaderLayout = false;
                    });
                    return true;
                }
                catch (Exception e)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                        LoaderLayout = false;
                    });
                    Console.WriteLine("IOException source: {0}", e.Source);
                    return false;
                }
            }
            else
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                LoaderLayout = false;
                return false;
            }
        }


        public PrecautionActions GetPrecautionAction(string permitAction)
        {

            switch (permitAction)
            {
                case "YES":
                    return PrecautionActions.YES;
                   
                case "NA":
                    return PrecautionActions.NA;
                   
                default:
                    return PrecautionActions.SKIP;
                  
            }
        }

        private async Task<Model.SyncHotWorks.RootObject> GetOnlineDataList()
        {
            try
            {
                // getting hotworks permit and impairment list which is converted to the api objects
                var hotWorksPermitsList = _hotworkPermitService.GetHotworksAsync();
                var impairementList = await GetOnlineImpairementList();


                if (hotWorksPermitsList == null)
                {
                    return null;
                }
                if (hotWorksPermitsList != null)
                {
                    List<HotWork> HotWorks = new List<HotWork>();
                    if (hotWorksPermitsList.Count() > 0)
                    {
                        // getting only newly created and updated permits
                        hotWorksPermitsList = hotWorksPermitsList.Where(p => p.IsNewlyCreated == true ||
                                                                        p.IsEdited == true || p.IsDeleted == true);
                        // mapping offline permits to the server permit model object
                        foreach (HotworksPermits hotworkspermit in hotWorksPermitsList)
                        {
                            HotWork serverPermit = new HotWork();
                            serverPermit.Id = hotworkspermit.HotWorkServerId;
                            serverPermit.HotWorkMobileId = hotworkspermit.HandHeldId;
                            serverPermit.IsParent = false;
                            serverPermit.ParentHotWorkId = 0;
                            if (hotworkspermit.IsDeleted)
                            {
                                serverPermit.isDeleted = true;
                            }
                            else
                            {
                                serverPermit.isDeleted = false;
                            }
                            serverPermit.WorkFlowStatus = hotworkspermit.Status + "";
                            #region map scope

                            var scope = hotworkspermit.Scopes;
                            //scope
                            serverPermit.Scope = new Scope
                            {
                                WorkDetail = scope.WorkType,
                                WorkLocation = scope.Location,
                                CompanyName = scope.CompanyDepartementName,
                                StartDateTime = scope.StartDate.ToUniversalTime(),
                                EndDateTime = scope.EndDate.ToUniversalTime(),
                                EquipmentDescription = scope.EquipmentDetails
                            };

                            serverPermit.Scope.ScopePersons = new List<ScopePerson>();

                            foreach (string personName in scope.PersonPerforming.Split(',').ToList())
                            {
                                serverPermit.Scope.ScopePersons.Add(new ScopePerson { PersonName = personName });
                            }

                            serverPermit.Scope.ScopeWorkOrders = new List<Model.SyncHotWorks.ScopeWorkOrder>();

                            foreach (Model.ScopeWorkOrder workOrder in scope.ScopeWorkOrders)
                            {
                                serverPermit.Scope.ScopeWorkOrders.Add(
                                    new Model.SyncHotWorks.ScopeWorkOrder
                                    {
                                        WorkOrderId = workOrder.WorkOrderId
                                    });
                            }
                            #endregion

                            #region map fire prevention
                            //fire watcher

                            var fireMeasure = hotworkspermit.FirePreventionMeasures;

                            FirePrevention firePrevention = new FirePrevention();

                            firePrevention.FireWatchPreventions = new List<FireWatchPrevention>();
                            firePrevention.NearestFireAlarmLocation = fireMeasure.NearestAlarm;
                            firePrevention.NearestPhone = fireMeasure.NearestPhone;
                            //adding fire watcher after
                            if (fireMeasure.FirewatchPersons != null)
                            {
                                foreach (FirewatchPersons fireWatcher in fireMeasure.FirewatchPersons.
                                         Where(p => p.FireWatcherType == FireWatcherType.AFTER))
                                {
                                    firePrevention.FireWatchPreventions.Add(new FireWatchPrevention
                                    {
                                        PersonName = fireWatcher.PersonName,
                                        StartTime = fireWatcher.StartTime.TimeOfDay.ToString(),
                                        EndTime = fireWatcher.EndTime.TimeOfDay.ToString(),
                                        FireWatchType = "AFTER"
                                    });
                                }
                            }
                            //adding fire watcher during
                            if (fireMeasure.FirewatchPersons != null)
                            {
                                foreach (FirewatchPersons fireWatcher in fireMeasure.FirewatchPersons.
                                         Where(p => p.FireWatcherType == FireWatcherType.DURING))
                                {
                                    firePrevention.FireWatchPreventions.Add(new FireWatchPrevention
                                    {
                                        PersonName = fireWatcher.PersonName,
                                        StartTime = fireWatcher.StartTime.TimeOfDay.ToString(),
                                        EndTime = fireWatcher.EndTime.TimeOfDay.ToString(),
                                        FireWatchType = "DURING"
                                    });
                                }
                            }

                            serverPermit.FirePrevention = firePrevention;
                            #endregion

                            #region precaution measure
                            // precautions
                            List<PrecautionList> precautionList = new List<PrecautionList>();
                            foreach (PermitPrecautions permitPrecaution in hotworkspermit.PermitPrecautions)
                            {
                                precautionList.Add(new PrecautionList
                                {
                                    MeasureId = permitPrecaution.PrecautionsId,
                                    MeasureStatus = permitPrecaution.PrecautionActions + "",
                                    FireHoseCount = string.IsNullOrEmpty(permitPrecaution.NoOfFirehouses) ? 0 : Convert.ToInt32(permitPrecaution.NoOfFirehouses),
                                    FireExtinguisherCount = string.IsNullOrEmpty(permitPrecaution.NoOfFirehouses) ? 0 : Convert.ToInt32(permitPrecaution.NoOfFireExtinguiser)
                                });
                            }

                            serverPermit.PrecautionList = precautionList;

                            var auth = hotworkspermit.Authorizations;

                            #endregion

                            #region authorization
                            // authorization 
                            SiteWorkPermits.Model.SyncHotWorks.Authorization authorization = new SiteWorkPermits.Model.SyncHotWorks.Authorization
                            {
                                AuthorizerName = auth.Name,
                                Department = auth.Departement,
                                Location = auth.Location,
                                AuthorizationDateTime = (auth.AuthDate == null) ?
                                    DateTime.UtcNow : auth.AuthDate.ToUniversalTime(),
                                IsHighHazardArea = auth.HighHazardArea ? "YES" : "NA",
                                AreaSupervisorName = auth.AreaSupervisorName,
                                AreaSupervisorDepartment = auth.AreaSupervisorDepartement
                            };

                            serverPermit.Authorization = authorization;
                            #endregion
                            HotWorks.Add(serverPermit);
                        }
                    }

                    Model.SyncHotWorks.Data syncHotWorksRequest = new Model.SyncHotWorks.Data
                    {
                        HotWorks = HotWorks,
                        Impairments = impairementList
                    };
                    var syncHotWorksResponse = await RestApiHelper<RootObject>.SyncUserDate_Post(syncHotWorksRequest);
                    return syncHotWorksResponse;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("IOException source: {0}", e.Source);
                return null;
            }
        }

        private async Task SaveImpairments(List<SyncImpairements> impairments)
        {
            if (impairments == null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = false;
                });
                return;
            }
            else if (impairments.Count() == 0)
            {
                return;
            }
            else
            {
                // drop all data 
                _impairmentService.RemoveAllData();

                try
                {
                    foreach (SyncImpairements syncServerImp in impairments)
                    {
                        var reportDetail = syncServerImp.ReporterDetail;
                        Model.ReporterDetail reporterDetail = new Model.ReporterDetail
                        {

                            Name = reportDetail.Name,
                            Email = reportDetail.Email,
                            Phone = reportDetail.Phone
                        };

                        Impairment impairment = new Impairment
                        {
                            IsParent = true,
                            ParentImpairmentId = 0,
                            IsDeleted = (syncServerImp.IsDeleted.HasValue == true && syncServerImp.IsDeleted.Value) ? true : false,
                            ImpairmentServerId = syncServerImp.Id,
                            ImpairmentTypeId = syncServerImp.ImpairmentTypeId,
                            ImpairmentClassId = syncServerImp.ImpairmentClassId,
                            ShutDownReasonId = syncServerImp.ShutDownReasonId,
                            StartDateTime = syncServerImp.StartDateTime.ToLocalTime().ToString(),
                            EndDateTime = syncServerImp.EndDateTime.ToLocalTime().ToString(),
                            ReporterDetail = reporterDetail,
                            SiteId = App.SiteId,
                            ImpairmentDescription = syncServerImp.ImpairmentDescription,
                            // CloseTime = Convert.ToDateTime(syncServerImp.CloseDateTime).ToLocalTime().ToString(),
                        };

                        if (syncServerImp.CloseDateTime == null)
                        {
                            impairment.CloseTime = "";
                        }
                        else
                        {
                            impairment.CloseTime = Convert.ToDateTime(syncServerImp.CloseDateTime).ToLocalTime().ToString();
                        }


                        //var hotWorksHandHeldID = Validation.GetHandHeldId() + "" + _impairmentService.GetImpairments()
                        //.ToList().Count().ToString();
                        if (!syncServerImp.ImpairmentMobileId.HasValue)
                        {
                            int uniqueId = _incUniqueid.GetUniqueIds().LastOrDefault().UniqueIncrementalId;
                            //HWUniqueNumberLabel = "No. " + Validation.GetHandHeldId() + "" + _hotworkPermitService.GetHotworksAsync().ToList().Count().ToString();
                            var hotWorksHandHeldID = CustomControl.Validation.GetHandHeldId() + "" + (uniqueId + 1).ToString();
                            _incUniqueid.AddIncUniqueId(new IncUniqueId { UniqueIncrementalId = uniqueId + 1 });
                            impairment.ImpairmentMobileId = Convert.ToInt32(hotWorksHandHeldID);
                        }
                        else
                        {
                            impairment.ImpairmentMobileId = Convert.ToInt32(syncServerImp.ImpairmentMobileId);
                        }

                        switch (syncServerImp.WorkFlowStatus)
                        {
                            case "DRAFT":
                                impairment.WorkFlowStatus = HWStatus.DRAFT;
                                break;
                            case "ACTIVE":
                                impairment.WorkFlowStatus = HWStatus.ACTIVE;
                                break;
                            case "UPCOMING":
                                impairment.WorkFlowStatus = HWStatus.UPCOMING;
                                break;
                            case "CLOSED":
                                impairment.WorkFlowStatus = HWStatus.CLOSED;
                                break;
                            default:
                                impairment.WorkFlowStatus = HWStatus.DRAFT;
                                impairment.IsDeleted = true;
                                break;
                        }

                        // major impairements
                        List<ImpairmentImpairmentMeasureMaster> impairmentImpairmentMeasureMasters = new List<ImpairmentImpairmentMeasureMaster>();

                        foreach (int majorImpId in syncServerImp.ImpairmentMeasureMasterIdList)
                        {
                            impairmentImpairmentMeasureMasters.Add(new ImpairmentImpairmentMeasureMaster
                            {
                                ImpairmentMeasureMasterId = majorImpId,
                            });
                        }
                        impairment.ImpairmentImpairmentMeasureMasters = impairmentImpairmentMeasureMasters;

                        List<ImpairmentPrecautionTaken> impairmentsPrecautions = new List<ImpairmentPrecautionTaken>();

                        foreach (ImpairmentPrecautionList precautionTaken in syncServerImp.ImpairmentPrecautionList)
                        {

                            string otherDesc = string.Empty;
                            if (precautionTaken.ImpairmentPrecautionMasterId == 15)
                            {
                                otherDesc = precautionTaken.OtherDescription;
                            }

                            impairmentsPrecautions.Add(new ImpairmentPrecautionTaken
                            {
                                ImpairmentPrecautionId = precautionTaken.ImpairmentPrecautionMasterId,
                                OtherDescription = otherDesc,
                            });
                        }
                        impairment.ImpairmentPrecautionTakens = impairmentsPrecautions;

                        var isImpairementSaved = _impairmentService.AddImpairment(impairment);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("IOException source: {0}", e.Source);
                    ShowActivityLoader = false;
                }
                return;
            }
        }

        private async Task<List<SyncImpairements>> GetOnlineImpairementList()
        {
            try
            {
                var impairmentList = _impairmentService.GetImpairments();
                if (impairmentList == null)
                {
                    return null;
                }
                if (impairmentList != null)
                {
                    List<SyncImpairements> syncImpairements = new List<SyncImpairements>();
                    if (impairmentList.Count() > 0)
                    {
                        impairmentList = impairmentList.Where(p => p.IsNewlyCreated == true ||
                                                                       p.IsEdited == true || p.IsDeleted == true);

                        foreach (Impairment impairement in impairmentList)
                        {
                            SyncImpairements serverImpairement = new SyncImpairements();

                            serverImpairement.ImpairmentMobileId = impairement.ImpairmentMobileId;
                            serverImpairement.IsParent = false;
                            serverImpairement.Id = impairement.ImpairmentServerId;
                            serverImpairement.ParentImpairmentId = 0;
                            if (impairement.IsDeleted)
                            {
                                serverImpairement.IsDeleted = true;
                            }
                            else
                            {
                                serverImpairement.IsDeleted = false;
                            }
                            serverImpairement.WorkFlowStatus = impairement.WorkFlowStatus + "";
                            serverImpairement.ImpairmentTypeId = impairement.ImpairmentTypeId;
                            serverImpairement.ImpairmentClassId = impairement.ImpairmentClassId;
                            serverImpairement.ShutDownReasonId = impairement.ShutDownReasonId;
                            serverImpairement.ImpairmentDescription = impairement.ImpairmentDescription;
                            serverImpairement.StartDateTime = Convert.ToDateTime(impairement.StartDateTime).ToUniversalTime();
                            serverImpairement.EndDateTime = Convert.ToDateTime(impairement.EndDateTime).ToUniversalTime();
                            serverImpairement.IsDeleted = impairement.IsDeleted;
                            serverImpairement.ReporterDetail = new Model.SyncImpairements.ReporterDetail
                            {
                                Name = impairement.ReporterDetail.Name,
                                Email = impairement.ReporterDetail.Email,
                                Phone = impairement.ReporterDetail.Phone,
                            };

                            if (string.IsNullOrEmpty(impairement.CloseTime))
                            {
                                serverImpairement.CloseDateTime = null;
                            }
                            else
                            {
                                serverImpairement.CloseDateTime = Convert.ToDateTime(impairement.CloseTime).ToUniversalTime();
                            }

                            List<ImpairmentPrecautionList> impairmentPrecautionLists = new List<ImpairmentPrecautionList>();

                            foreach (ImpairmentPrecautionTaken impairmentPreTaken in impairement.ImpairmentPrecautionTakens)
                            {
                                impairmentPrecautionLists.Add(new ImpairmentPrecautionList
                                {
                                    ImpairmentPrecautionMasterId = impairmentPreTaken.ImpairmentPrecautionId,
                                    OtherDescription = impairmentPreTaken.OtherDescription
                                });
                            }
                            serverImpairement.ImpairmentPrecautionList = impairmentPrecautionLists;

                            List<int> ImpairmentMeasureMasterIdLists = new List<int>();

                            foreach (ImpairmentImpairmentMeasureMaster impairmentImpairmentMeasureMaster in impairement.ImpairmentImpairmentMeasureMasters)
                            {
                                ImpairmentMeasureMasterIdLists.Add(impairmentImpairmentMeasureMaster.ImpairmentMeasureMasterId);
                            }

                            serverImpairement.ImpairmentMeasureMasterIdList = ImpairmentMeasureMasterIdLists;
                            syncImpairements.Add(serverImpairement);
                        }
                        return syncImpairements;
                    }
                    else
                    {
                        return syncImpairements;
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("IOException source: {0}", e.Source);
                return null;
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
