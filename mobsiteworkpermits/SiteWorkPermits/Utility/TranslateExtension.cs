﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SiteWorkPermits.Utility
{
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        const string ResourceId = "SiteWorkPermits.Resx.AppResource";
        public string Text { get; set; }
        public string Parameter { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return null;
            ResourceManager resourceManager = new ResourceManager(ResourceId, typeof(TranslateExtension).GetTypeInfo().Assembly);

            var result = resourceManager.GetString(Text, CultureInfo.CurrentCulture);
            if (!string.IsNullOrWhiteSpace(Parameter)) {
                result =  result + " " + Parameter;
            }
            return result;
        }
    }
}
