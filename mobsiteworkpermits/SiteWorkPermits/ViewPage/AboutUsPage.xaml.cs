﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Platform;
using SiteWorkPermits.Resx;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits.ViewPage
{
    public partial class AboutUsPage : ContentPage
    {
        private string _aboutXLOperations = string.Empty;
        private string _aboutXLCompany1 = string.Empty;
        private string _aboutXLCompany2 = string.Empty;
        private string _axaxlText = "http://axaxl.com";

        public AboutUsPage()
        {
            _aboutXLOperations = AppResource.ABOUT_AXAXL_INSURANCE_DESC_LABEL;

            _aboutXLCompany1 = AppResource.ABOUT_AXAXL_DESC_LABEL;

            _aboutXLCompany2 = AppResource.ABOUT_AXAXL_DESC_2_LABEL;

            InitializeComponent();

            if (AppConfig.BASE_URL.Contains("dev"))
            {
                webViewLayout.Source = AppConfig.DEV_ABOUTUS_URL;
            }
            else {
                webViewLayout.Source = AppConfig.TEST_ABOUTUS_URL;
            }

            //axaxlText.Text = _axaxlText + " ";
            //axaxlText1.Text = _axaxlText + " ";
            //aboutXLCompany1.Text = _aboutXLCompany1;
            //aboutXLCompany2.Text = _aboutXLCompany2;
            //aboutXLOperations.Text = _aboutXLOperations;

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            //appVersion.Text = AppResource.LOGIN_APP_VERSION_LABEL + " " + DependencyService.Get<IAppInfo>().GetVersion();
        }

        private void webViewLayout_Navigating(object sender, WebNavigatingEventArgs e)
        {
            progressbar.IsVisible = true;
        }

        private void webViewLayout_Navigated(object sender, WebNavigatedEventArgs e)
        {
            progressbar.IsVisible = false;
        }

        protected async override void OnAppearing() {

            base.OnAppearing();
            await progressbar.ProgressTo(0.9, 900, Easing.SpringIn);

        }

        //void Handle_Tapped(object sender, System.EventArgs e)
        //{
        //    Device.OpenUri(new Uri("http://axaxl.com"));
        //}
    }
}
