﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusButton :Button
    {
        public CusButton() :base()
        {

        }

        public static BindableProperty ElevationProperty = BindableProperty.Create<CusButton, float>(o => o.Elevation, 0.0f);

        public float Elevation
        {
            get
            {
                return (float)GetValue(ElevationProperty);
            }
            set
            {
                SetValue(ElevationProperty, value);
            }
        }

        #region Properties   

        public static BindableProperty PaddingProperty = BindableProperty.Create<CusButton, Thickness>(o => o.Padding, new Thickness(15));
        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }


        public static BindableProperty DisableBackgroundColorProperty = BindableProperty.Create<CusButton, Color>(o => o.DisableBackgroundColor, Color.Default);
        public Color DisableBackgroundColor
        {
            get { return (Color)GetValue(DisableBackgroundColorProperty); }
            set { SetValue(DisableBackgroundColorProperty, value); }
        }


        public static BindableProperty PressedBackgroundColorProperty = BindableProperty.Create<CusButton, Color>(o => o.PressedBackgroundColor, Color.Default);
        public Color PressedBackgroundColor
        {
            get { return (Color)GetValue(PressedBackgroundColorProperty); }
            set { SetValue(PressedBackgroundColorProperty, value); }
        }


        public static BindableProperty NormalImageProperty = BindableProperty.Create<CusButton, ImageSource>(o => o.NormalImage, null);
        public ImageSource NormalImage
        {
            get { return (ImageSource)GetValue(NormalImageProperty); }
            set { SetValue(NormalImageProperty, value); }
        }

        public static BindableProperty DisableImageProperty = BindableProperty.Create<CusButton, ImageSource>(o => o.DisableImage, null);
        public ImageSource DisableImage
        {
            get { return (ImageSource)GetValue(DisableImageProperty); }
            set { SetValue(DisableImageProperty, value); }
        }

        public static BindableProperty PressedImageProperty = BindableProperty.Create<CusButton, ImageSource>(o => o.PressedImage, null);
        public ImageSource PressedImage
        {
            get { return (ImageSource)GetValue(PressedImageProperty); }
            set { SetValue(PressedImageProperty, value); }
        }

             
        public static BindableProperty TagProperty = BindableProperty.Create<CusButton, object>(o => o.Tag, string.Empty);

        public object Tag
        {
            get { return (object)GetValue(TagProperty); }
            set { SetValue(TagProperty, value); }
        }

        #endregion
    }

   
}
