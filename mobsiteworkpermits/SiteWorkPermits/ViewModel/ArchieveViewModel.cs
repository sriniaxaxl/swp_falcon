﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.Model;
using SiteWorkPermits.Model.SyncHotWorks;
using SiteWorkPermits.Model.SyncImpairements;
using SiteWorkPermits.Platform;
using CustomControl;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLite;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.DAL.sqlIntegration;
using static CustomControl.Validation;
using System.Windows.Input;
using Prism.Common;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public class ArchieveViewModel : BindableBase, INavigatedAware
    {
        public delegate void ChangeTabAction();
        public bool _isScrollingEnable = true;
        public bool _isLoggedout;
        public event ChangeTabAction ChangeTabEvent;
        private ObservableCollection<ModelWorkPermit> _workPermits;
        public ObservableCollection<ModelWorkPermit> WorkPermits
        {
            get { return _workPermits; }
            set { SetProperty(ref _workPermits, value); }
        }


        private ObservableCollection<ModelWorkPermit> _Impairments;
        public ObservableCollection<ModelWorkPermit> Impairments
        {
            get { return _Impairments; }
            set { SetProperty(ref _Impairments, value); }
        }

        #region Properties

        private ObservableCollection<ModelPermits> _hotWorksPermitCollection;

        public ObservableCollection<ModelPermits> HotWorksPermitCollection
        {
            get { return _hotWorksPermitCollection; }
            set { SetProperty(ref _hotWorksPermitCollection, value); }
        }

        private ObservableCollection<ModelPermits> _ImpairmentCollection;

        public ObservableCollection<ModelPermits> ImpairmentCollection
        {
            get { return _ImpairmentCollection; }
            set { SetProperty(ref _ImpairmentCollection, value); }
        }

        private ObservableCollection<string> _hotworksFilterColletion = new ObservableCollection<string>();
        public ObservableCollection<string> HotworksFilterColletion
        {
            get { return _hotworksFilterColletion; }
            set { SetProperty(ref _hotworksFilterColletion, value); }
        }

        private ObservableCollection<int> _ImpairmentTypeFilterColletion = new ObservableCollection<int>();
        public ObservableCollection<int> ImpairmentTypeFilterColletion
        {
            get { return _ImpairmentTypeFilterColletion; }
            set { SetProperty(ref _ImpairmentTypeFilterColletion, value); }
        }


        private ObservableCollection<int> _ImpairmentClassFilterColletion = new ObservableCollection<int>();
        public ObservableCollection<int> ImpairmentClassFilterColletion
        {
            get { return _ImpairmentClassFilterColletion; }
            set { SetProperty(ref _ImpairmentClassFilterColletion, value); }
        }

        private ObservableCollection<int> _ImpairmentReasonFilterColletion = new ObservableCollection<int>();
        public ObservableCollection<int> ImpairmentReasonFilterColletion
        {
            get { return _ImpairmentReasonFilterColletion; }
            set { SetProperty(ref _ImpairmentReasonFilterColletion, value); }
        }


        private int _selectedImpairmentDuration;
        public int selectedImpairmentDuration
        {
            get { return _selectedImpairmentDuration; }
            set { SetProperty(ref _selectedImpairmentDuration, value); }
        }


        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

        private int _PageSize;
        public int PageSize
        {
            get { return _PageSize; }
            set { SetProperty(ref _PageSize, value); }
        }

        private int _HotWorksPageNumber;
        public int HotWorksPageNumber
        {
            get { return _HotWorksPageNumber; }
            set { SetProperty(ref _HotWorksPageNumber, value); }
        }

        private int _ImpairmentPageNumber;
        public int ImpairmentPageNumber
        {
            get { return _ImpairmentPageNumber; }
            set { SetProperty(ref _ImpairmentPageNumber, value); }
        }

        private int _SelDateFilter;
        public int SelDateFilter
        {
            get { return _SelDateFilter; }
            set { SetProperty(ref _SelDateFilter, value); }
        }


        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }
        #endregion


        public INavigationService _navigationService;
        public IPageDialogService _pageDialog;
        IWorkOrdersService _workOrdersService;
        IHotworkPermitService _hotworkPermitService;
        IScopeService _scopeService;
        IAuthorizationService _authorizationService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
        SQLiteConnection connection;
        IHighHazardAreasService _highHazardAreasService;
        public event RefreshArchiveListAction RefreshArchiveListEvent;
        public ICommand OnTabChangedCommand => new Command(onTabChange);

        private async void onTabChange(object obj)
        {
            var x = (string)obj;
            ChangeTabColor(x);
            if (x.ToLower() == "hotwork")
            {
                App.impfilter.Children.Clear();
                ListViewElements = ListViewElements_Temp;
                showHotworkListView = true;
                showImpListView = false;
                await GetArchivePermits(App.ArchivePageCount);
                // await GetArchieveImpairments(ImpairmentPageNumber);
                //var result = ListViewElements.Where(w => w.ElementStatus.ToLower().Equals(x.ToLower()));
                //ListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
            }
            else
            {
                App.hwfilter.Children.Clear();
                ImpairmentListViewElements = ImpairmentListViewElements_Temp;
                showHotworkListView = false;
                showImpListView = true;
                // await GetArchivePermits(App.ArchivePageCount);
                await GetArchieveImpairments(ImpairmentPageNumber);
                //var result = ImpairmentListViewElements.Where(w => w.ElementId!=null);
                //ImpairmentListViewElements = new ObservableCollection<ListViewElement>(result.ToList());
            }
            ChangeTabEvent.Invoke();
        }

        public delegate void RefreshArchiveListAction();
        public string selectedPermitDuration = String.Empty;
        public IImpairmentClassService _impairmentClassService;
        public IImpairmentTypeService _impairmentTypeService;
        public IShutDownReasonService _shutDownReasonService;
        public ArchieveViewModel(INavigationService navigationService,
                                 IPageDialogService pageDialog,
                                 IHotworkPermitService hotworkPermitService,
                                 IWorkOrdersService workOrdersService,
                                 IScopeService scopeService, IAuthorizationService authorizationService,
                                 IFirePreventionMeasuresService firePreventionMeasuresService,
                                 IPrecautionsService precautionsService,
                                 IFirewatchPersonsService firewatchPersonsService,
                                 IHighHazardAreasService highHazardAreasService,
                                 IImpairmentTypeService impairmentTypeService,
                                 IImpairmentClassService impairmentClassService,
                                 IShutDownReasonService shutDownReasonService)
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            _workOrdersService = workOrdersService;
            _scopeService = scopeService;
            _hotworkPermitService = hotworkPermitService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _scopeService = scopeService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _highHazardAreasService = highHazardAreasService;
            _authorizationService = authorizationService;
            _impairmentClassService = impairmentClassService;
            _impairmentTypeService = impairmentTypeService;
            _shutDownReasonService = shutDownReasonService;
            //SetHotworksData();
            ShowActivityLoader = false;

            PageSize = 20;
            ImpairmentPageNumber = 1;
            HotWorksPageNumber = 1;

            App.ImpairMentArchieves = new List<SyncImpairements>();
            App.HotworksArchieves = new List<HotWork>();
            App.WorkPermits = new List<ModelWorkPermit>();
            App.Impairments = new List<ModelWorkPermit>();
            connection = hotworkDatabase.GetdbConnection();
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("ARCHIVE");
            ListViewElements_Temp = new ObservableCollection<ListViewElement>();
            ImpairmentListViewElements_Temp = new ObservableCollection<ListViewElement>();
            ListViewElements = new ObservableCollection<ListViewElement>();
            ImpairmentListViewElements = new ObservableCollection<ListViewElement>();
            showHotworkListView = true;
            showImpListView = false;
            HotworkTabColor = Color.FromHex("#00AFAD");
        }

        private ObservableCollection<ListViewElement> ListViewElements_Temp;
        private ObservableCollection<ListViewElement> ImpairmentListViewElements_Temp;
        
        private void ChangeTabColor(string x)
        {
            switch (x.ToLower())
            {
                case "hotwork":
                    HotworkTabColor = Color.FromHex("#00AFAD");
                    ImpairmentTabColor = Color.Transparent;
                    break;
                case "impairment":
                    HotworkTabColor = Color.Transparent;
                    ImpairmentTabColor = Color.FromHex("#00AFAD");
                    break;

            }
        }

        private Color _hotworkTabColor;
        public Color HotworkTabColor
        {
            get { return _hotworkTabColor; }
            set { SetProperty(ref _hotworkTabColor, value); }
        }

        private Color _impairmentTabColor;
        public Color ImpairmentTabColor
        {
            get { return _impairmentTabColor; }
            set { SetProperty(ref _impairmentTabColor, value); }
        }

        private bool _showImpListView;
        public bool showImpListView
        {
            get { return _showImpListView; }
            set { SetProperty(ref _showImpListView, value); }
        }


        private bool _showHotworkListView;
        public bool showHotworkListView
        {
            get { return _showHotworkListView; }
            set { SetProperty(ref _showHotworkListView, value); }
        }

        List<HotWork> hotWorks;


        private ObservableCollection<ListViewElement> _listViewElements;

        public ObservableCollection<ListViewElement> ListViewElements
        {
            get { return _listViewElements; }
            set { SetProperty(ref _listViewElements, value); }
        }

        private ObservableCollection<ListViewElement> _ImpairmentlistViewElements;

        public ObservableCollection<ListViewElement> ImpairmentListViewElements
        {
            get { return _ImpairmentlistViewElements; }
            set { SetProperty(ref _ImpairmentlistViewElements, value); }
        }

       

        private string _uniqueId;

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        private  HotWorkPermit_sql GetHotwork(HotWork hotWork) {


            try
            {
              //  SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
             //   var connection = hotworkDatabase.GetdbConnection();

                var personListq = new List<ScopePerson_sql>();

                if (hotWork.Scope.ScopePersons != null)
                {
                    foreach (var personHotWork in hotWork.Scope.ScopePersons)
                    {
                        personListq.Add(new ScopePerson_sql() { PersonName = personHotWork.PersonName });
                    }
                }

                var workorderList = new List<WorkOrderMaster_sql>();
                foreach (var workOrder in hotWork.Scope.ScopeWorkOrders)
                {
                    workorderList.Add(connection.Table<WorkOrderMaster_sql>().Where(p => p.id == workOrder.WorkOrderId).FirstOrDefault());
                }

                var firewatcherList = new List<FireWatcherPrevention_sql>();

                if (hotWork.FirePrevention.FireWatchPreventions.Count() > 0)
                {
                    foreach (var firewatcher in hotWork.FirePrevention.FireWatchPreventions)
                    {
                        string watcherType = string.Empty;

                        watcherType = firewatcher.FireWatchType;

                        firewatcherList.Add(new FireWatcherPrevention_sql
                        {
                            PersonName = firewatcher.PersonName,
                            StartTime = firewatcher.StartTime.ToString(),
                            EndTime = firewatcher.EndTime.ToString(),
                            FireWatchType = watcherType,
                        });
                    }

                }

                var precautionMaster_sql = new List<PrecautionMaster_sql>();
                int firehouses = 0;
                int fireextinguisher = 0;
                if (hotWork.PrecautionList != null)
                {
                    foreach (var precautions in hotWork.PrecautionList)
                    {

                        if (precautions.MeasureId == 6 && precautions.MeasureStatus == "YES")
                        {
                            firehouses = Convert.ToInt32(hotWork.PrecautionList.Where(p => p.MeasureId == 5).FirstOrDefault().FireHoseCount);
                            fireextinguisher = Convert.ToInt32(hotWork.PrecautionList.Where(p => p.MeasureId == 5).FirstOrDefault().FireExtinguisherCount);
                        }

                        if (precautions.MeasureStatus == "YES")
                        {
                            precautionMaster_sql.Add(connection.Table<PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.MeasureId).FirstOrDefault());
                        }

                    }
                }

                var ishighHazard = (hotWork.Authorization.IsHighHazardArea == null || hotWork.Authorization.IsHighHazardArea == "NA") ? false : true;

                Scope_sql scope_Sql = new Scope_sql()
                {
                    WorkDetail = hotWork.Scope.WorkDetail == null ? "" : hotWork.Scope.WorkDetail,
                    WorkLocation = hotWork.Scope.WorkLocation == null ? "" : hotWork.Scope.WorkLocation,
                    CompanyName = hotWork.Scope.CompanyName == null ? "" : hotWork.Scope.CompanyName,
                    EquipmentDescription = hotWork.Scope.EquipmentDescription == null ? "" : hotWork.Scope.EquipmentDescription,
                    StartDateTime = hotWork.Scope.StartDateTime.ToString(), 
                    EndDateTime = hotWork.Scope.EndDateTime.ToString(),
                };

                scope_Sql.scopePerson_Sqls = personListq;
                scope_Sql.workOrderMaster_Sqls = workorderList;




                    FirePrevention_sql firePrevention_Sql = new FirePrevention_sql()
                    {
                        NearestPhone = hotWork.FirePrevention.NearestPhone==null?"": hotWork.FirePrevention.NearestPhone,
                        NearestFireAlarmLocation = hotWork.FirePrevention.NearestFireAlarmLocation==null?"": hotWork.FirePrevention.NearestFireAlarmLocation,
                    };

                    firePrevention_Sql.fireWatcherPrevention_Sqls = firewatcherList;


                Authorization_sql authorization_Sql = new Authorization_sql()
                {
                    AuthorizerName = hotWork.Authorization.AuthorizerName == null ? "" : hotWork.Authorization.AuthorizerName,
                    Department = hotWork.Authorization.Department == null ? "" : hotWork.Authorization.Department,
                    Location = hotWork.Authorization.Location == null ? "" : hotWork.Authorization.Location,
                    AuthorizationDateTime = hotWork.Authorization.AuthorizationDateTime.ToString(),
                    IsHighHazardArea = (hotWork.Authorization.IsHighHazardArea==null || hotWork.Authorization.IsHighHazardArea == "NA")?false:true,
                    AreaSupervisorName = hotWork.Authorization.AreaSupervisorName == null ? "" : hotWork.Authorization.AreaSupervisorName,
                    AreaSupervisorDepartment = hotWork.Authorization.Department == null ? "" : hotWork.Authorization.Department
                };


                HotWorkPermit_sql hotWorkPermit_Sql = new HotWorkPermit_sql()
                {
                    isDeleted = false,
                    isParent = true,
                    isNewlyCreated = false,
                    isEdited = false,
                    workFlowStatus = hotWork.WorkFlowStatus,
                    account_number = "",
                    location_number = App.SiteId,
                    server_id = hotWork.Id,
                    fireHoseCount = firehouses,
                    handHeldId = 0,
                    hotworkGUID = hotWork.GUID,
                    fireExtinguisherCount = fireextinguisher
                };

                //                _connection.Insert(hotWorkPermit_Sql);
                hotWorkPermit_Sql.IsScannedPermits = hotWork.IsScannedPermits;
                hotWorkPermit_Sql.scope_Sql = scope_Sql;
                hotWorkPermit_Sql.firePrevention_sql = firePrevention_Sql;
                hotWorkPermit_Sql.authorization_sql = authorization_Sql;
                hotWorkPermit_Sql.precautionMaster_Sqls = precautionMaster_sql;
              //  _connection.UpdateWithChildren(hotWorkPermit_Sql);

                return hotWorkPermit_Sql;



            }
            catch (Exception ex)
            {
                return null;
            }

            return null;

        }
        
        private Impairment_Sql GetImpairment(SyncImpairements syncImpairements) {


            try
            {

                var reporterDetails = ImpairmentUtility.GetReporterDetails(syncImpairements.ReporterDetail.Name, syncImpairements.ReporterDetail.Email, syncImpairements.ReporterDetail.Phone);
                var localImp = ImpairmentUtility.GetImpairment(reporterDetails, syncImpairements.ImpairmentTypeId, syncImpairements.ImpairmentClassId, syncImpairements.ShutDownReasonId, syncImpairements.StartDateTime, syncImpairements.EndDateTime, syncImpairements.ImpairmentDescription, syncImpairements.WorkFlowStatus, syncImpairements.Id);

                var majorImpairmentMaster_Sqls = new List<MajorImpairmentMaster_sql>();
                foreach (var majorImpairment in syncImpairements.ImpairmentMeasureMasterIdList)
                {
                    majorImpairmentMaster_Sqls.Add( connection.Table<MajorImpairmentMaster_sql>().Where(p => p.id == majorImpairment).FirstOrDefault());
                }

                var impairment_PrecautionMaster_Sqls = new List<Impairment_PrecautionMaster_sql>();
                foreach (var precautions in syncImpairements.ImpairmentPrecautionList.ToList())
                {
                    string otherDesc = string.Empty;
                    if (precautions.ImpairmentPrecautionMasterId == 15)
                    {
                        localImp.precautionOtherDescription = precautions.OtherDescription;
                    }
                    else
                    {
                        localImp.precautionOtherDescription = otherDesc;
                    }

                    impairment_PrecautionMaster_Sqls.Add( connection.Table<Impairment_PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.ImpairmentPrecautionMasterId).FirstOrDefault());
                }

                localImp.majorImpairmentMaster_sqls = majorImpairmentMaster_Sqls;
                localImp.impairment_PrecautionMaster_Sqls = impairment_PrecautionMaster_Sqls;
                return localImp;
            }
            catch (Exception ex) {
                return null;
            }
            //return null;
        }

         public ICommand OnElementCloseCommand
        {
            get
            {
                return new Command(async (x) => {

                    var element = (x as CustomControl.Validation.ListViewElement);

                    if (element.ElementType.ToLower() == "hotworks")
                    {
                        App.isImpairementEditorDuplicate = "Duplicate";
                        //  var hotwork_sql = EditPermitByID((x as ListViewElement).ElementId);
                        App.isDuplicate = true;
                        var permit = GetHotwork(App.HotworksArchieves.Where(sr => sr.Id == (x as ListViewElement).ElementId).First());
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("archiveModel_sql", permit);
                        await _navigationService.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
                    }
                    else
                    {
                        App.isImpairementEditorDuplicate = "Duplicate";
                        App.isDuplicate = true;
                        var impairment =   GetImpairment(App.ImpairMentArchieves.Where(sr => sr.Id == (x as ListViewElement).ElementId).First());
                        await Task.Yield();
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("impairmentModel", impairment);
                        await _navigationService.NavigateAsync("NewImpairmentPage", navigationParams, null, false);
                    }
                });
            }
        }

        public ICommand OnElementSelectEdit
        {
            get
            {
                return new Command(async (x) =>
                {
                    try {
                        App.isDuplicate = false;
                        App.isImpairementEditorDuplicate = "Archieve";
                        var permit = GetHotwork(App.HotworksArchieves.Where(sr => sr.Id == (x as ListViewElement).ElementId).First());
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("archiveModel_sql", permit);

                        await _navigationService.NavigateAsync(new Uri("/NewHotWorksPage",
                            UriKind.Relative), navigationParams, null, false);

                        //await _navigationService.NavigateAsync("", navigationParams, null, false);
                    } catch (Exception ex) {


                    }
                    
                });
            }
        }
               
        public ICommand OnImpElementSelectEdit
        {
            get
            {
                return new Command(async (x) =>
                {

                    try {
                        App.isImpairementEditorDuplicate = "";
                        var permit = GetImpairment(App.ImpairMentArchieves.Where(sr => sr.Id == (x as ListViewElement).ElementId).First());
                        var impairment = permit;
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("impairmentArchieveModel", impairment);
                        await _navigationService.NavigateAsync(new Uri("/NewImpairmentPage",
                            UriKind.Relative), navigationParams, null, false);

                        //await _navigationService.NavigateAsync("", navigationParams, null, false);
                    }
                    catch (Exception ex) {

                    }
                    
                    //App.isImpairementEditorDuplicate = "";
                    //var element = x as ListViewElement;
                    //var impairment = EditImpairmentByID(element.ElementId);
                    //var navigationParams = new NavigationParameters();
                    //navigationParams.Add("impairmentArchieveModel", impairment);
                    //await _navigationService.NavigateAsync("NewImpairmentPage", navigationParams, null, false);
                });
            }
        }


        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            try
            {
                App.isArchivePage = true;
                //  GetArchiveData();
              

                if (CrossConnectivity.Current.IsConnected)
                {

                    if (parameters.ContainsKey("fromWhere"))
                    {
                        string fromImp = (string)parameters["fromWhere"];

                        if (fromImp == "fromImpairments")
                        {
                            ChangeTabColor("impairment");
                            await GetArchivePermits(App.ArchivePageCount);
                            await GetArchieveImpairments(ImpairmentPageNumber);
                            onTabChange("impairment");
                        }

                    }
                    else {
                        await GetArchivePermits(App.ArchivePageCount);
                        await GetArchieveImpairments(ImpairmentPageNumber);
                    }                    
                }
                else {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);

                }
            }
            catch (Exception ex) { }
            
        }

        public async Task GetArchivePermits(int pageNumber)
        {
            _isScrollingEnable = false;
            ShowActivityLoader = true;
            ListViewElements.Clear();
            ListViewElements_Temp.Clear();
            string workorderId = string.Empty;
            foreach (string workOrder in HotworksFilterColletion)
            {
                workorderId += connection.GetAllWithChildren<WorkOrderMaster_sql>().ToList().Where(p => p.workOrderName.ToLower() == workOrder.ToLower()).FirstOrDefault().id.ToString();
            }

            workorderId = String.Join<char>(",", workorderId);
            var archiveRequest = "{\"PageSize\": 20,\"PageNumber\": " + pageNumber + ",\"WorkOrderIdFilterList\": [" + workorderId + "],\"DateFilter\": \"" + selectedPermitDuration + "\"}";
            var hw = await RestApiHelper<RootObject>.GetArchiveHotWorks_Post(archiveRequest);

            if (hw == null)
            {
                _isLoggedout = true;
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.SOMETHING_WENT_WRONG, AppResource.OK_LABEL);
                ShowActivityLoader = false;
                return;
            }
            if (hw.Data == null)
            {
                if (hw.Status != null)
                {
                    if (hw.Status.StatusCode == 401)
                    {
                        _isLoggedout = true;
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", hw.Status.StatusMessage, "ok");
                        var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                        // Xamarin.Forms.Application.Current.Properties.Clear();

                        foreach (string key in keyList.ToList())
                        {
                            if (!key.Equals("WelcomeShow"))
                            {
                                Application.Current.Properties.Remove(key);
                            }
                        }
                        await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                        ShowActivityLoader = false;
                        return;
                    }
                }
            }
            else
            {
                _isLoggedout = false;
                hotWorks = hw.Data.HotWorks;

                App.HotworksArchieves.AddRange(hotWorks.ToList());
                //  await SyncHotWorks(hotWorks);

                if (hotWorks.Count > 0)
                {
                    foreach (HotWork hotWork in hotWorks)
                    {
                        bool check = !hotWork.isDeleted ?? false;
                        if (check)
                        {
                            // var scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql.id, recursive: true);
                            ListViewElements.Add(new ListViewElement
                            {
                                ElementId = hotWork.Id,
                                ElementCategory = "#" + hotWork.Id + "\n" + string.Join(", ", hotWork.Scope.ScopeWorkOrders.Select(p =>
                                                                                        connection.GetWithChildren<WorkOrderMaster_sql>(p.WorkOrderId, true)).ToList().Select(p =>
                                                                                        p.workOrderName)),
                                ElementDescription = hotWork.Scope.WorkLocation,
                                ElementEndDate = Helper.ElapsedTime(hotWork.Scope.EndDateTime),
                                ElementStartDate = Helper.ElapsedTime(hotWork.Scope.EndDateTime),
                                ElementStatus = hotWork.WorkFlowStatus,
                                ElementEvent = CustomControl.Validation.GetElementEventName(hotWork.WorkFlowStatus),
                                ElementType = "HOTWORKS"
                            });

                        }
                    }
                    ListViewElements_Temp = ListViewElements;
                    ListViewElements = ListViewElements_Temp;
                }
                else
                {
                    ListViewElements.Clear();
                    ListViewElements_Temp.Clear();
                }

                ShowActivityLoader = false;

                //_isScrollingEnable = true;
            }
        }
        public async Task GetArchieveImpairments(int Pagenumber)
        {
            _isScrollingEnable = false;
            ShowActivityLoader = true;
            ImpairmentListViewElements.Clear();
            ImpairmentListViewElements_Temp.Clear();
            ImpairmentRequest impairmentRequest = new ImpairmentRequest();
            impairmentRequest.PageSize = PageSize;
            impairmentRequest.PageNumber = ImpairmentPageNumber;
            impairmentRequest.ImpairmentTypeIdFilterList = ImpairmentTypeFilterColletion.ToList();
            impairmentRequest.ImpairmentClassIdFilterList = ImpairmentClassFilterColletion.ToList();
            impairmentRequest.ShutDownReasonIdFilterList = ImpairmentReasonFilterColletion.ToList();
            impairmentRequest.DateFilter = selectedImpairmentDuration;
            var impairreq = JsonConvert.SerializeObject(impairmentRequest);
        var lstSyncImpairments = await RestApiHelper<RootObject>.GetArchiveImpaitments_Post(Pagenumber, impairreq);


            if (lstSyncImpairments == null)
            {
                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("",AppResource.SOMETHING_WENT_WRONG, AppResource.OK_LABEL);
                ShowActivityLoader = false;
                return;
            }


            if (lstSyncImpairments.Data == null)
            {
                if (lstSyncImpairments.Status != null)
                {
                    if (lstSyncImpairments.Status.StatusCode == 401)
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", lstSyncImpairments.Status.StatusMessage, AppResource.OK_LABEL);
                        var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                        // Xamarin.Forms.Application.Current.Properties.Clear();

                        foreach (string key in keyList.ToList())
                        {
                            if (!key.Equals("WelcomeShow"))
                            {
                                Application.Current.Properties.Remove(key);
                            }
                        }

                        await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                        ShowActivityLoader = false;
                        return;
                    }
                }
            }
            else
            {
                App.ImpairMentArchieves.AddRange(lstSyncImpairments.Data.Impairments);
                //SetImpairments(lstSyncImpairments.Data.Impairments);



                if (lstSyncImpairments.Data.Impairments.Count > 0)
                {
                    foreach (SyncImpairements syncImpairements in lstSyncImpairments.Data.Impairments)
                    {
                        bool check = !syncImpairements.IsDeleted ?? false;
                        if (check)
                        {



                            string[] impairmentType1 = new string[] { AppResource.IMP_TYPE_1_PLACEHOLDER,
                    AppResource.IMP_TYPE_2_PLACEHOLDER,
                AppResource.IMP_TYPE_3_PLACEHOLDER,
                AppResource.IMP_TYPE_4_PLACEHOLDER,
                AppResource.IMP_TYPE_5_PLACEHOLDER};
                            string[] impairmentClass1 = new string[] { AppResource.IMP_CLASS_1_PLACEHOLDER,
        AppResource.IMP_CLASS_2_PLACEHOLDER,
        AppResource.IMP_CLASS_3_PLACEHOLDER,
        AppResource.IMP_CLASS_4_PLACEHOLDER,
        AppResource.IMP_CLASS_5_PLACEHOLDER,
        AppResource.IMP_CLASS_6_PLACEHOLDER,
        AppResource.IMP_CLASS_7_PLACEHOLDER,
        AppResource.IMP_CLASS_8_PLACEHOLDER};
                            string[] impairmentShutdownReason1 = new string[] { AppResource.IMP_SHUTDOWN_REASON_1_PLACEHOLDER ,
        AppResource.IMP_SHUTDOWN_REASON_2_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_3_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_4_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_5_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_6_PLACEHOLDER};



                            var impairmentType = impairmentType1[syncImpairements.ImpairmentTypeId - 1]; //connection.Table<ImpairmentType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentTypeId).FirstOrDefault().TypeDescription;
                            var impairmentClass = impairmentClass1[syncImpairements.ImpairmentClassId - 1]; //connection.Table<ImpairmentClassType_Sql>().ToList().Where(p => p.Id == syncImpairements.ImpairmentClassId).FirstOrDefault().ClassDescription;
                            var impairmentShutdownReason = impairmentShutdownReason1[syncImpairements.ShutDownReasonId - 1]; //connection.Table<ShutDownReason_Sql>().ToList().Where(p => p.Id == syncImpairements.ShutDownReasonId).FirstOrDefault().ClassDescription;

                            ImpairmentListViewElements.Add(new ListViewElement
                            {
                                ElementId = syncImpairements.Id,
                                ElementCategory = "#" + syncImpairements.Id + "\n" + impairmentType + "\n" + impairmentClass + "\n" + impairmentShutdownReason,
                                ElementDescription = syncImpairements.ImpairmentDescription,
                                ElementEndDate = Helper.ElapsedTime(syncImpairements.EndDateTime),
                                ElementStartDate = Helper.ElapsedTime(syncImpairements.EndDateTime),
                                ElementStatus = syncImpairements.WorkFlowStatus,
                                ElementEvent = CustomControl.Validation.GetElementEventName(syncImpairements.WorkFlowStatus),
                                ElementType = "IMPAIREMENTS"
                            });

                        }
                    }
                    ImpairmentListViewElements_Temp = ImpairmentListViewElements;
                    ImpairmentListViewElements = ImpairmentListViewElements_Temp;
                }
                _isScrollingEnable = true;
                ShowActivityLoader = false;
            }
        }

    }



    public class ImpairmentRequest
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public List<int> ImpairmentTypeIdFilterList { get; set; }
        public List<int> ImpairmentClassIdFilterList { get; set; }
        public List<int> ShutDownReasonIdFilterList { get; set; }
        public int DateFilter { get; set; }
    }
}
