﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Security;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SiteWorkPermits.DAL.sqlIntegration;

namespace SiteWorkPermits.Model
{

    #region Tab 1 Scope
    public class WorkOrders:Entity
    {
        [Key]
        public int WorkOrderId { get; set; }
        public string Description { get; set; }
        [NotMapped]
        public bool IsSelected { get; set; }
        public ICollection<ScopeWorkOrder> ScopeWorkOrder { get; set; }
    }

    public class Scopes:Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ScopeId { get; set; }
        public string Location { get; set; }
        public string WorkType { get; set; }
        public string CompanyDepartementName { get; set; }
        [Required]
        public string PersonPerforming
        {
            get { return String.Join(", ", _personPerforming); }
            set { _personPerforming = value.Split(',').ToList(); }
        }
        [NotMapped]
        public List<String> _personPerforming { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public HotworksPermits HotworksPermits { get; set; }
        [ForeignKey("HotworksPermits")]
        public int HotWorksPermitID { get; set; }
        public IList<ScopeWorkOrder> ScopeWorkOrders { get; set; }


        // newly added field
        [NotMapped]
        public string EquipmentDetails { get; set; }
    }

    public class ScopeWorkOrder:Entity
    {
        public WorkOrders WorkOrders { get; set; }

        public Scopes Scopes { get; set; }

        [ForeignKey("WorkOrders")]
        public int WorkOrderId { get; set; }

        [ForeignKey("Scopes")]
        public int ScopeId { get; set; }
    }

    #endregion

    #region Tab 2 Fire Prevention Measure
    public class FirewatchPersons:Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string PersonName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public FireWatcherType FireWatcherType { get; set; }
        [ForeignKey("FirePreventionMeasures")]
        public int? FirePreventionMeasuresId { get; set; }
        public FirePreventionMeasures FirePreventionMeasures { get; set; }
    }

    public class FirePreventionMeasures:Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FirePreventionMeasuresId { get; set; }
        public virtual ICollection<FirewatchPersons> FirewatchPersons { get; set; }
        public string NearestAlarm { get; set; }
        public string NearestPhone { get; set; }
        public HotworksPermits HotworksPermits { get; set; }
        [ForeignKey("HotworksPermits")]
        public int HotWorksPermitID { get; set; }
    }

    #endregion

    #region Tab 3 Precautions
    public class Precautions:Entity
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string NoOfFirehouses { get; set; }
        public string NoOfFireExtinguiser { get; set; }
        ////[NotMapped]
        public PrecautionActions PrecautionActions { get; set; }
        public IList<PermitPrecautions> PermitPrecautions { get; set; }
    }

    public class PermitPrecautions:Entity
    {
        [ForeignKey("HotworksPermits")]
        public int PermitId { get; set; }
        [ForeignKey("Precautions")]
        public int PrecautionsId { get; set; }
        public HotworksPermits HotworksPermits { get; set; }
        public Precautions Precautions { get; set; }
        public PrecautionActions PrecautionActions { get; set; }
        public string NoOfFirehouses { get; set; }
        public string NoOfFireExtinguiser { get; set; }
    }

    public enum PrecautionActions
    {
        SKIP,
        YES,
        NA
    }
    #endregion

    #region Tab 4 Authentication
    //public class HighHazardAreas
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int HighHazardAreasId { get; set; }
    //    public bool IsHazardArea { get; set; }
    //    public string AreaSupervisorName { get; set; }
    //    public string Departement { get; set; }
    //}

    public class Authorizations:Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorizationsId { get; set; }
        public string Name { get; set; }
        public string Departement { get; set; }
        public string Location { get; set; }
        public DateTime AuthDate { get; set; }
        public HotworksPermits HotworksPermits { get; set; }
        [ForeignKey("HotworksPermits")]
        public int HotWorksPermitID { get; set; }
        public bool HighHazardArea { get; set; }
        public string AreaSupervisorName { get; set; }
        public string AreaSupervisorDepartement { get; set; }
    }
    #endregion

    #region Hotwokrs Permit
    public class HotworksPermits: Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HotworksPermitsId { get; set; }
        public int HotWorkServerId { get; set; }

        //[ForeignKey("ScopeId")]
        //public int Scopeid { get; set; }


        //tab 1
        public Scopes Scopes { get; set; }

        //tab 4
        public Authorizations Authorizations { get; set; }

        //tab 3
        public FirePreventionMeasures FirePreventionMeasures { get; set; }

        //[ForeignKey("FirePreventionMeasuresId")]
        //public int FirePreventionMeasuresid { get; set; }

        //[ForeignKey("AuthorizationsId")]
        //public int Authorizationid { get; set; }

        public bool IsDeleted { get; set; }
        public HWStatus Status { get; set; }
        public int? HandHeldId { get; set; }
        public bool IsEdited { get; set; }
        public bool IsNewlyCreated { get; set; }
        public string SiteId { get; set; }

        //tab 3
        public IList<PermitPrecautions> PermitPrecautions { get; set; }
    }

    public enum HWStatus
    {
        NEW,
        ACTIVE,
        UPCOMING,
        DRAFT,
        ARCHIVE,
        EXPIRED,
        CLOSED
    }

    public enum FireWatcherType
    {
        AFTER,
        DURING
    }

    #endregion

    #region Base Entity
    public abstract class BaseEntity
    {
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    #endregion
}










