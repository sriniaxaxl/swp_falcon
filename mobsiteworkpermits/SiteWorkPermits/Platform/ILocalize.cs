﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SiteWorkPermits.Platform
{
    public interface ILocalize
    {
        // https://www.flokri.com/c/xamarin-forms-localization/
        CultureInfo GetCurrentCultureInfo();
        void SetLocale(CultureInfo ci);
    }
}
