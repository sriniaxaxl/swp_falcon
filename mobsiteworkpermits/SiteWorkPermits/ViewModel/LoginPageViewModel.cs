﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using SiteWorkPermits.DAL.RestHelper;
using System.Threading.Tasks;
using System.Linq;
using CustomControl;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Xamarin.Auth;
using System;
using SiteWorkPermits.Resx;
using Xamarin.Forms;
using SiteWorkPermits.Services;

namespace SiteWorkPermits.ViewModel
{
    public class LoginPageViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        string AuthType;

        public DelegateCommand<string> OnNavigateCommand { get; set; }
        public LoginPageViewModel(INavigationService navigationService,
                                 IPageDialogService pageDialog
                                  )
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            ShowActivityLoader = false;

            OnNavigateCommand = new DelegateCommand<string>(NavigateAsync);


            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("LOGIN");

            //if(_IncUniqueid.GetUniqueIds().Count()==0){
            //    var uniqueId = new IncUniqueId { UniqueIncrementalId = 1 };
            //    _IncUniqueid.AddIncUniqueId(uniqueId);
            //}

            // AccountNumber = "999222";
            //SiteNumber = "801801";

            //CustomControl. Validation.IntializeDBData(_workOrdersService, _precautionsService, _majorImpairmentsService, _precautionTakenService,
            //     _impairmentTypeService, _impairmentClassService, _shutDownReasonService);

        }

        private string _accountNumber;
        public string AccountNumber
        {
            get { return _accountNumber; }
            set { SetProperty(ref _accountNumber, value); }
        }

        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

        private string _siteNumber;
        public string SiteNumber
        {
            get { return _siteNumber; }
            set { SetProperty(ref _siteNumber, value); }
        }

        private async void NavigateAsync(string page)
        {
            try
            {
                if (page == "TC")
                {
                    await _navigationService.NavigateAsync("TermsConditionLoginPage", null, null, false);
                    return;
                }


                if (!string.IsNullOrEmpty(AccountNumber) && !string.IsNullOrEmpty(SiteNumber))
                {
                    ShowActivityLoader = true;
                    var userLogin = await RestApiHelper<UserLogin>.LoginUser(AccountNumber, SiteNumber);
                    if (userLogin != null)
                    {
                        bool isSyncEnable = true;
                        var navigationParams = new NavigationParameters();
                        navigationParams.Add("syncEnable", isSyncEnable);

                        Xamarin.Forms.Application.Current.Properties["CompanyName"] = userLogin.Data.CompanyName;
                        Xamarin.Forms.Application.Current.Properties["HandHeldId"] = userLogin.Data.HandheldId;
                        Xamarin.Forms.Application.Current.Properties["CompanyAddress"] = userLogin.Data.CompanyAddress;
                        Xamarin.Forms.Application.Current.Properties["JWT_TOKEN"] = userLogin.Data.JWTToken;
                        Xamarin.Forms.Application.Current.Properties["siteId"] = SiteNumber;
                        Xamarin.Forms.Application.Current.Properties["accountId"] = AccountNumber;
                        await Xamarin.Forms.Application.Current.SavePropertiesAsync();

                        Account account = new Account
                        {
                            Username = AccountNumber
                        };
                        account.Properties.Add("LocationNumber", SiteNumber);
                        AccountStore.Create().Save(account, "xlc");
                        await _navigationService.NavigateAsync(page, navigationParams, null, false);

                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            AuthType = Xamarin.Forms.DependencyService.Get<IBiometricAuthenticateService>().GetAuthenticationType();
                            if (!AuthType.Equals("None"))
                            {

                                if (AuthType.Equals("TouchId") || AuthType.Equals("FaceId"))
                                {
                                    GetAuthResults(page, navigationParams);//7.0.0.396
                                }
                            }
                            else
                            {
                                await _navigationService.NavigateAsync(page, navigationParams, null, false);
                            }
                        }
                        else
                        {
                            MessagingCenter.Subscribe<string>("BiometricPrompt", "BiometricResultYes", async (sender) =>
                            {
                                //MessagingCenter.Unsubscribe<string>("BiometricPrompt", "BiometricResultYes");
                                await _navigationService.NavigateAsync(page, navigationParams, null, false);
                            });

                            MessagingCenter.Subscribe<string>("BiometricPrompt", "BiometricResultNo", (sender) =>
                            {

                                Xamarin.Forms.DependencyService.Get<IBiometricAuthentication>().isBiometricSuccess();
                                //MessagingCenter.Unsubscribe<string>("BiometricPrompt", "BiometricResultNo");
                            });

                            if (!App._isBiometricPass)
                            {
                                Xamarin.Forms.DependencyService.Get<IBiometricAuthentication>().isBiometricSuccess();
                            }

                        }
                    }
                    else
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync(string.Empty, AppResource.UNABLE_LOGIN, AppResource.OK_LABEL);
                    }

                }
                else
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync(string.Empty, CustomControl.Validation.EMPTY_CREDENTIALS, AppResource.OK_LABEL);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.StackTrace);
            }
            finally
            {
                ShowActivityLoader = false;
            }
        }

        private async Task GetAuthResults(string page, NavigationParameters navigationParams)
        {
            //todo according to Auth type change the authenticationmethod in interface if face id or touch id
            //string AuthType = DependencyService.Get<IFingerprintAuthService>().GetAuthenticationType();
            var result = await Xamarin.Forms.DependencyService.Get<IBiometricAuthenticateService>().AuthenticateUserIDWithTouchID();
            if (result)
            {
                if (AuthType.Equals("TouchId"))
                {
                    Console.WriteLine("TouchID authentication success");
                }
                else if (AuthType.Equals("FaceId"))
                {
                    Console.WriteLine("FaceID authentication success");
                }
                await _navigationService.NavigateAsync(page, navigationParams, null, false);
            }
            else
            {
                AuthType = Xamarin.Forms.DependencyService.Get<IBiometricAuthenticateService>().GetAuthenticationType();
                if (!AuthType.Equals("None"))
                {
                    Console.WriteLine("Please use " + AuthType + " to unlock");

                    if (AuthType.Equals("TouchId") || AuthType.Equals("FaceId"))
                    {
                        GetAuthResults(page, navigationParams);
                    }
                }
                //Console.WriteLine("Authentication failed");
            }
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
