﻿using System;
using SiteWorkPermits.iOS.Platform;
using SiteWorkPermits.Platform;
using Foundation;

[assembly: Xamarin.Forms.Dependency(typeof(AppInfo_iOS))]
namespace SiteWorkPermits.iOS.Platform
{
    public class AppInfo_iOS : IAppInfo
    {
        public string GetVersion()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString").ToString();
        }
        public int GetBuild()
        {
            return int.Parse(NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion").ToString());
        }

    }
}