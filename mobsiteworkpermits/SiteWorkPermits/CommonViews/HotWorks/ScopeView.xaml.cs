﻿using System;
using System.Linq;
using SiteWorkPermits.ViewModel;
using Xamarin.Forms;
using Plugin.InputKit.Shared.Controls;

namespace SiteWorkPermits
{
    public partial class ScopeView : ContentView
    {

        ViewCell lastCell;
        public ScopeView(NewHotWorksPageViewModel vm = null)
        {
            InitializeComponent();
            if (vm != null) {
                WorkOrderList.ItemsSource = vm.WorkOrdersLocal;
                Personview.EditValcollection = vm.PersonHotWork;
            }
        }
               
    

        private void CheckBox_CheckChanged(object sender, EventArgs e)
        {
            var cont = this.BindingContext as NewHotWorksPageViewModel;
           
            if (sender is Plugin.InputKit.Shared.Controls.CheckBox cb)
            {
                cont.WorkOrdersLocal.Where(p => p.workorderName == cb.Text).FirstOrDefault().isSelected = cb.IsChecked;
             }
        }

        private void ViewCell_Tapped(object sender, EventArgs e)
        {
            if (lastCell != null)
                lastCell.View.BackgroundColor = Color.Transparent;
            var viewCell = (ViewCell)sender;
            if (viewCell.View != null)
            {
                viewCell.View.BackgroundColor = Color.Transparent;
                lastCell = viewCell;
            }
        }
    }
}
