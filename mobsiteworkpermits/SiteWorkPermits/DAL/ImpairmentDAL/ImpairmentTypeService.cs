﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public class ImpairmentTypeService:IImpairmentTypeService
    {
        private readonly DatabaseContext _databaseContext;
        public ImpairmentTypeService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddImpairmentType(ImpairmentType impairmentType)
        {
            try
            {
                var status = _databaseContext.ImpairmentType.Add(impairmentType);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<ImpairmentType> GetImpairmentType()
        {
            try
            {
                var impairmentTypes = _databaseContext.ImpairmentType.ToList();
                return impairmentTypes;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ImpairmentType GetImpairmentTypeById(int id)
        {
            try
            {
                var impairmentTypes = _databaseContext.ImpairmentType.Where(p => p.Id == id);
                return impairmentTypes.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<ImpairmentType>();
            _databaseContext.ImpairmentType.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
