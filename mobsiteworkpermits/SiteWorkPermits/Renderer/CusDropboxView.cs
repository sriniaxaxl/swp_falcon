﻿using System;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusDropboxView :Grid
    {
        public EventHandler<DropSelectedArgs> ItemSelectionChanged;
        public CusDropboxView() : base()
        {
            this.RowSpacing = 0;
            this.ColumnSpacing = 0;
        }
        public static BindableProperty ShadowColorProperty = BindableProperty.Create<CusDropboxView, Color>(o => o.ShadowColor, Color.Transparent);

        public Color ShadowColor
        {
            get { return (Color)GetValue(ShadowColorProperty); }
            set { SetValue(ShadowColorProperty, value); }
        }

        public static BindableProperty ShadowWidthProperty = BindableProperty.Create<CusDropboxView, float>(o => o.BorderWidth, 0);

        public float ShadowWidth
        {
            get { return (float)GetValue(ShadowWidthProperty); }
            set { SetValue(ShadowWidthProperty, value); }
        }

        public static BindableProperty ShadowRadiusProperty = BindableProperty.Create<CusDropboxView, Thickness>(o => o.ShadowRadius, new Thickness(0));

        public Thickness ShadowRadius
        {
            get { return (Thickness)GetValue(ShadowRadiusProperty); }
            set { SetValue(ShadowRadiusProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusDropboxView, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusDropboxView, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusDropboxView, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusDropboxView, Thickness>(o => o.BorderRadius, new Thickness(0));

        public Thickness BorderRadius
        {
            get { return (Thickness)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        public static BindableProperty WidthValueProperty = BindableProperty.Create<CusDropboxView, int>(o => o.WidthValue, 100);

        public int WidthValue
        {
            get { return (int)GetValue(WidthValueProperty); }
            set { SetValue(WidthValueProperty, value); }
        }

        public static BindableProperty HeightValueProperty = BindableProperty.Create<CusDropboxView, int>(o => o.HeightValue, 50);

        public int HeightValue
        {
            get { return (int)GetValue(HeightValueProperty); }
            set { SetValue(HeightValueProperty, value); }
        }


        public static BindableProperty TagProperty = BindableProperty.Create<CusDropboxView, object>(o => o.Tag, string.Empty);

        public object Tag
        {
            get { return (object)GetValue(TagProperty); }
            set { SetValue(TagProperty, value); }
        }

        public static BindableProperty KMarginProperty = BindableProperty.Create<CusDropboxView, object>(o => o.KMargin, new Thickness());

        public Thickness KMargin
        {
            get { return (Thickness)GetValue(KMarginProperty); }
            set { SetValue(KMarginProperty, value); }
        }

    }


    public class DropSelectedArgs
    {
        public string Name { get; set; }
        public int Selectedindex { get; set; }
    }
}
