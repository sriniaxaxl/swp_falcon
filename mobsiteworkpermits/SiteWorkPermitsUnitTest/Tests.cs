﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using SiteWorkPermits;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.Model;
using SiteWorkPermits.ViewModel;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace SiteWorkPermitsUnitTest
{
    //[TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        //IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        /*[SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            //app.Screenshot("First screen.");
        }*/

        [Test]
        public void HotWorkImpairmentDateTimeUnitTest()
        {
            string time = DateTime.Now.ToString("HH:mm");
            string timeSet = Helper.TimeFormatSet(DateTime.Now);
            bool result = string.Equals(time, timeSet);
            Assert.True(result);
        }
    }
}