﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EFDBMigration.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EquipmentDetails",
                table: "Scopes",
                defaultValue: "",
                nullable: false
            );

            //migrationBuilder.Sql("ALTER TABLE Scopes ADD Email varchar(255);", suppressTransaction: true);
            //    migrationBuilder.CreateTable(
            //    name: "HotworksPermits",
            //    columns: table => new
            //    {
            //        HotworksPermitsId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        HandHeldId = table.Column<int>(type: "INTEGER", nullable: true),
            //        HotWorkServerId = table.Column<int>(type: "INTEGER", nullable: false),
            //        IsDeleted = table.Column<bool>(type: "INTEGER", nullable: false),
            //        IsEdited = table.Column<bool>(type: "INTEGER", nullable: false),
            //        IsNewlyCreated = table.Column<bool>(type: "INTEGER", nullable: false),
            //        SiteId = table.Column<string>(type: "TEXT", nullable: true),
            //        Status = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_HotworksPermits", x => x.HotworksPermitsId);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Impairment",
            //    columns: table => new
            //    {
            //        ImpairmentId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        CloseDateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
            //        CloseTime = table.Column<string>(type: "TEXT", nullable: true),
            //        EndDateTime = table.Column<string>(type: "TEXT", nullable: true),
            //        ImpairmentClassId = table.Column<int>(type: "INTEGER", nullable: false),
            //        ImpairmentDescription = table.Column<string>(type: "TEXT", nullable: true),
            //        ImpairmentMobileId = table.Column<int>(type: "INTEGER", nullable: false),
            //        ImpairmentServerId = table.Column<int>(type: "INTEGER", nullable: false),
            //        ImpairmentTypeId = table.Column<int>(type: "INTEGER", nullable: false),
            //        IsDeleted = table.Column<bool>(type: "INTEGER", nullable: false),
            //        IsEdited = table.Column<bool>(type: "INTEGER", nullable: false),
            //        IsNewlyCreated = table.Column<bool>(type: "INTEGER", nullable: false),
            //        IsParent = table.Column<bool>(type: "INTEGER", nullable: false),
            //        ParentImpairmentId = table.Column<int>(type: "INTEGER", nullable: false),
            //        ShutDownReasonId = table.Column<int>(type: "INTEGER", nullable: false),
            //        SiteId = table.Column<string>(type: "TEXT", nullable: true),
            //        StartDateTime = table.Column<string>(type: "TEXT", nullable: true),
            //        WorkFlowStatus = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Impairment", x => x.ImpairmentId);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ImpairmentClassType",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        ClassDescription = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ImpairmentClassType", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ImpairmentType",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        TypeDescription = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ImpairmentType", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "IncUniqueIds",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        UniqueIncrementalId = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_IncUniqueIds", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Precautions",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        Description = table.Column<string>(type: "TEXT", nullable: true),
            //        NoOfFireExtinguiser = table.Column<string>(type: "TEXT", nullable: true),
            //        NoOfFirehouses = table.Column<string>(type: "TEXT", nullable: true),
            //        PrecautionActions = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Precautions", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "PrecautionTakens",
            //    columns: table => new
            //    {
            //        ImpairmentPrecautionId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        Description = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PrecautionTakens", x => x.ImpairmentPrecautionId);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ShutDownReason",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        ClassDescription = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ShutDownReason", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "WorkOrders",
            //    columns: table => new
            //    {
            //        WorkOrderId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        Description = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_WorkOrders", x => x.WorkOrderId);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Authorizations",
            //    columns: table => new
            //    {
            //        AuthorizationsId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        AreaSupervisorDepartement = table.Column<string>(type: "TEXT", nullable: true),
            //        AreaSupervisorName = table.Column<string>(type: "TEXT", nullable: true),
            //        AuthDate = table.Column<DateTime>(type: "TEXT", nullable: false),
            //        Departement = table.Column<string>(type: "TEXT", nullable: true),
            //        HighHazardArea = table.Column<bool>(type: "INTEGER", nullable: false),
            //        HotWorksPermitID = table.Column<int>(type: "INTEGER", nullable: false),
            //        Location = table.Column<string>(type: "TEXT", nullable: true),
            //        Name = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Authorizations", x => x.AuthorizationsId);
            //        table.ForeignKey(
            //            name: "FK_Authorizations_HotworksPermits_HotWorksPermitID",
            //            column: x => x.HotWorksPermitID,
            //            principalTable: "HotworksPermits",
            //            principalColumn: "HotworksPermitsId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "FirePreventionMeasures",
            //    columns: table => new
            //    {
            //        FirePreventionMeasuresId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        HotWorksPermitID = table.Column<int>(type: "INTEGER", nullable: false),
            //        NearestAlarm = table.Column<string>(type: "TEXT", nullable: true),
            //        NearestPhone = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_FirePreventionMeasures", x => x.FirePreventionMeasuresId);
            //        table.ForeignKey(
            //            name: "FK_FirePreventionMeasures_HotworksPermits_HotWorksPermitID",
            //            column: x => x.HotWorksPermitID,
            //            principalTable: "HotworksPermits",
            //            principalColumn: "HotworksPermitsId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Scopes",
            //    columns: table => new
            //    {
            //        ScopeId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        CompanyDepartementName = table.Column<string>(type: "TEXT", nullable: true),
            //        EndDate = table.Column<DateTime>(type: "TEXT", nullable: false),
            //        HotWorksPermitID = table.Column<int>(type: "INTEGER", nullable: false),
            //        Location = table.Column<string>(type: "TEXT", nullable: true),
            //        PersonPerforming = table.Column<string>(type: "TEXT", nullable: false),
            //        StartDate = table.Column<DateTime>(type: "TEXT", nullable: false),
            //        WorkType = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Scopes", x => x.ScopeId);
            //        table.ForeignKey(
            //            name: "FK_Scopes_HotworksPermits_HotWorksPermitID",
            //            column: x => x.HotWorksPermitID,
            //            principalTable: "HotworksPermits",
            //            principalColumn: "HotworksPermitsId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ImpairmentMeasureMaster",
            //    columns: table => new
            //    {
            //        ImpairmentMeasureMasterId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        ImpairmentId = table.Column<int>(type: "INTEGER", nullable: true),
            //        ImpairmentMeasureDescription = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ImpairmentMeasureMaster", x => x.ImpairmentMeasureMasterId);
            //        table.ForeignKey(
            //            name: "FK_ImpairmentMeasureMaster_Impairment_ImpairmentId",
            //            column: x => x.ImpairmentId,
            //            principalTable: "Impairment",
            //            principalColumn: "ImpairmentId",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ReporterDetail",
            //    columns: table => new
            //    {
            //        ReporterDetailId = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        Email = table.Column<string>(type: "TEXT", nullable: true),
            //        ImpairmentId = table.Column<int>(type: "INTEGER", nullable: false),
            //        Name = table.Column<string>(type: "TEXT", nullable: true),
            //        Phone = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ReporterDetail", x => x.ReporterDetailId);
            //        table.ForeignKey(
            //            name: "FK_ReporterDetail_Impairment_ImpairmentId",
            //            column: x => x.ImpairmentId,
            //            principalTable: "Impairment",
            //            principalColumn: "ImpairmentId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "PermitPrecautions",
            //    columns: table => new
            //    {
            //        PrecautionsId = table.Column<int>(type: "INTEGER", nullable: false),
            //        PermitId = table.Column<int>(type: "INTEGER", nullable: false),
            //        NoOfFireExtinguiser = table.Column<string>(type: "TEXT", nullable: true),
            //        NoOfFirehouses = table.Column<string>(type: "TEXT", nullable: true),
            //        PrecautionActions = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PermitPrecautions", x => new { x.PrecautionsId, x.PermitId });
            //        table.ForeignKey(
            //            name: "FK_PermitPrecautions_HotworksPermits_PermitId",
            //            column: x => x.PermitId,
            //            principalTable: "HotworksPermits",
            //            principalColumn: "HotworksPermitsId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_PermitPrecautions_Precautions_PrecautionsId",
            //            column: x => x.PrecautionsId,
            //            principalTable: "Precautions",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ImpairmentPrecautionTaken",
            //    columns: table => new
            //    {
            //        ImpairmentPrecautionId = table.Column<int>(type: "INTEGER", nullable: false),
            //        ImpairmentId = table.Column<int>(type: "INTEGER", nullable: false),
            //        OtherDescription = table.Column<string>(type: "TEXT", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ImpairmentPrecautionTaken", x => new { x.ImpairmentPrecautionId, x.ImpairmentId });
            //        table.ForeignKey(
            //            name: "FK_ImpairmentPrecautionTaken_Impairment_ImpairmentId",
            //            column: x => x.ImpairmentId,
            //            principalTable: "Impairment",
            //            principalColumn: "ImpairmentId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_ImpairmentPrecautionTaken_PrecautionTakens_ImpairmentPrecautionId",
            //            column: x => x.ImpairmentPrecautionId,
            //            principalTable: "PrecautionTakens",
            //            principalColumn: "ImpairmentPrecautionId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "FirewatchPersons",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "INTEGER", nullable: false)
            //            .Annotation("Sqlite:Autoincrement", true),
            //        EndTime = table.Column<DateTime>(type: "TEXT", nullable: false),
            //        FirePreventionMeasuresId = table.Column<int>(type: "INTEGER", nullable: true),
            //        FirePreventionMeasuresId1 = table.Column<int>(type: "INTEGER", nullable: true),
            //        FireWatcherType = table.Column<int>(type: "INTEGER", nullable: false),
            //        PersonName = table.Column<string>(type: "TEXT", nullable: true),
            //        StartTime = table.Column<DateTime>(type: "TEXT", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_FirewatchPersons", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_FirewatchPersons_FirePreventionMeasures_FirePreventionMeasuresId",
            //            column: x => x.FirePreventionMeasuresId,
            //            principalTable: "FirePreventionMeasures",
            //            principalColumn: "FirePreventionMeasuresId",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_FirewatchPersons_FirePreventionMeasures_FirePreventionMeasuresId1",
            //            column: x => x.FirePreventionMeasuresId1,
            //            principalTable: "FirePreventionMeasures",
            //            principalColumn: "FirePreventionMeasuresId",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ScopeWorkOrders",
            //    columns: table => new
            //    {
            //        ScopeId = table.Column<int>(type: "INTEGER", nullable: false),
            //        WorkOrderId = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ScopeWorkOrders", x => new { x.ScopeId, x.WorkOrderId });
            //        table.ForeignKey(
            //            name: "FK_ScopeWorkOrders_Scopes_ScopeId",
            //            column: x => x.ScopeId,
            //            principalTable: "Scopes",
            //            principalColumn: "ScopeId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_ScopeWorkOrders_WorkOrders_WorkOrderId",
            //            column: x => x.WorkOrderId,
            //            principalTable: "WorkOrders",
            //            principalColumn: "WorkOrderId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "ImpairmentImpairmentMeasureMaster",
            //    columns: table => new
            //    {
            //        ImpairmentMeasureMasterId = table.Column<int>(type: "INTEGER", nullable: false),
            //        ImpairmentId = table.Column<int>(type: "INTEGER", nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ImpairmentImpairmentMeasureMaster", x => new { x.ImpairmentMeasureMasterId, x.ImpairmentId });
            //        table.ForeignKey(
            //            name: "FK_ImpairmentImpairmentMeasureMaster_Impairment_ImpairmentId",
            //            column: x => x.ImpairmentId,
            //            principalTable: "Impairment",
            //            principalColumn: "ImpairmentId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_ImpairmentImpairmentMeasureMaster_ImpairmentMeasureMaster_ImpairmentMeasureMasterId",
            //            column: x => x.ImpairmentMeasureMasterId,
            //            principalTable: "ImpairmentMeasureMaster",
            //            principalColumn: "ImpairmentMeasureMasterId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Authorizations_HotWorksPermitID",
            //    table: "Authorizations",
            //    column: "HotWorksPermitID",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_FirePreventionMeasures_HotWorksPermitID",
            //    table: "FirePreventionMeasures",
            //    column: "HotWorksPermitID",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_FirewatchPersons_FirePreventionMeasuresId",
            //    table: "FirewatchPersons",
            //    column: "FirePreventionMeasuresId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_FirewatchPersons_FirePreventionMeasuresId1",
            //    table: "FirewatchPersons",
            //    column: "FirePreventionMeasuresId1");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ImpairmentImpairmentMeasureMaster_ImpairmentId",
            //    table: "ImpairmentImpairmentMeasureMaster",
            //    column: "ImpairmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ImpairmentMeasureMaster_ImpairmentId",
            //    table: "ImpairmentMeasureMaster",
            //    column: "ImpairmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ImpairmentPrecautionTaken_ImpairmentId",
            //    table: "ImpairmentPrecautionTaken",
            //    column: "ImpairmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_PermitPrecautions_PermitId",
            //    table: "PermitPrecautions",
            //    column: "PermitId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_ReporterDetail_ImpairmentId",
            //    table: "ReporterDetail",
            //    column: "ImpairmentId",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Scopes_HotWorksPermitID",
            //    table: "Scopes",
            //    column: "HotWorksPermitID",
            //    unique: true);

            //migrationBuilder.CreateIndex(
                //name: "IX_ScopeWorkOrders_WorkOrderId",
                //table: "ScopeWorkOrders",
                //column: "WorkOrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Authorizations");

            migrationBuilder.DropTable(
                name: "FirewatchPersons");

            migrationBuilder.DropTable(
                name: "ImpairmentClassType");

            migrationBuilder.DropTable(
                name: "ImpairmentImpairmentMeasureMaster");

            migrationBuilder.DropTable(
                name: "ImpairmentPrecautionTaken");

            migrationBuilder.DropTable(
                name: "ImpairmentType");

            migrationBuilder.DropTable(
                name: "IncUniqueIds");

            migrationBuilder.DropTable(
                name: "PermitPrecautions");

            migrationBuilder.DropTable(
                name: "ReporterDetail");

            migrationBuilder.DropTable(
                name: "ScopeWorkOrders");

            migrationBuilder.DropTable(
                name: "ShutDownReason");

            migrationBuilder.DropTable(
                name: "FirePreventionMeasures");

            migrationBuilder.DropTable(
                name: "ImpairmentMeasureMaster");

            migrationBuilder.DropTable(
                name: "PrecautionTakens");

            migrationBuilder.DropTable(
                name: "Precautions");

            migrationBuilder.DropTable(
                name: "Scopes");

            migrationBuilder.DropTable(
                name: "WorkOrders");

            migrationBuilder.DropTable(
                name: "Impairment");

            migrationBuilder.DropTable(
                name: "HotworksPermits");
        }
    }
}
