﻿using System;
using System.Threading.Tasks;
using Foundation;
using LocalAuthentication;
using SiteWorkPermits.iOS.Renderer;
using SiteWorkPermits.Services;
using UIKit;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(BiometricAuthenticationService))]
namespace SiteWorkPermits.iOS.Renderer
{
    public class BiometricAuthenticationService : IBiometricAuthenticateService
    {
        public BiometricAuthenticationService() { }

        public Task<bool> AuthenticateUserIDWithTouchID()
        {
            bool outcome = false;
            var tcs = new TaskCompletionSource<bool>();

            var context = new LAContext();
            if (context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthentication, out NSError AuthError)) // DeviceOwnerAuthenticationWithBiometrics
            {
                var replyHandler = new LAContextReplyHandler((success, error) => {

                    Device.BeginInvokeOnMainThread(() => {
                        if (success)
                        {
                            outcome = true;
                        }
                        else
                        {
                            outcome = false;
                        }
                        tcs.SetResult(outcome);
                    });

                });
                //This will call both TouchID and FaceId 
                context.EvaluatePolicy(LAPolicy.DeviceOwnerAuthentication, "Login with touch ID", replyHandler); // DeviceOwnerAuthenticationWithBiometrics
            };
            return tcs.Task;
        }

        public bool fingerprintEnabled()
        {
            throw new NotImplementedException();
        }

        private static int GetOsMajorVersion()
        {
            return int.Parse(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);
        }

        public string GetAuthenticationType()
        {
            var localAuthContext = new LAContext();
            NSError AuthError;

            if (localAuthContext.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthentication, out AuthError))
            {
                if (localAuthContext.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthentication, out AuthError)) // DeviceOwnerAuthenticationWithBiometrics
                {
                    if (GetOsMajorVersion() >= 11 && localAuthContext.BiometryType == LABiometryType.FaceId)
                    {
                        return "FaceId";
                    }

                    return "TouchId";
                }

                return "PassCode";
            }

            return "None";
        }
    }
}
