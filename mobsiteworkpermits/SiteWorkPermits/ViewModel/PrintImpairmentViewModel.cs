﻿using System;
using System.Threading.Tasks;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.Platform;
using Plugin.Connectivity;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using SiteWorkPermits.Model.SyncImpairements;
using SiteWorkPermits.Model;
using SiteWorkPermits.DAL.ImpairmentDAL;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model.SyncHotWorks;
using SiteWorkPermits.DAL.sqlIntegration;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLite;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits.ViewModel
{
    public class PrintImpairmentViewModel : BindableBase, INavigatedAware
    {
        public DelegateCommand<string> OnEmailImpCommand { get; set; }
        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        IWorkOrdersService _workOrdersService;
        IHotworkPermitService _hotworkPermitService;
        IScopeService _scopeService;
        IImpairmentService _impairmentService;
        IAuthorizationService _authorizationService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        IHighHazardAreasService _highHazardAreasService;
        private int emailPermitId;
        public PrintImpairmentViewModel(INavigationService navigationService,
                                 IPageDialogService pageDialog,
                                 IHotworkPermitService hotworkPermitService,
                                 IWorkOrdersService workOrdersService,
                                 IImpairmentService impairmentService,
                                 IScopeService scopeService, IAuthorizationService authorizationService,
                                 IFirePreventionMeasuresService firePreventionMeasuresService,
                                 IPrecautionsService precautionsService,
                                 IFirewatchPersonsService firewatchPersonsService,
                                 IHighHazardAreasService highHazardAreasService)
        {
            _impairmentService = impairmentService;

            _navigationService = navigationService;
            _pageDialog = pageDialog;
            _workOrdersService = workOrdersService;
            _scopeService = scopeService;
            _hotworkPermitService = hotworkPermitService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _scopeService = scopeService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _highHazardAreasService = highHazardAreasService;
            _authorizationService = authorizationService;
            ShowActivityLoader = false;
            ShowSyncIcon = false;
            SelFormat = "USLETTER";
            OnEmailImpCommand = new DelegateCommand<string>(SendEmail);
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("PRINT OR SUBMIT IMPAIRMENT");
        }


        private async void SendEmail(string obj)
        {
            if (obj.Equals("Print"))
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    await _pageDialog.DisplayAlertAsync("", AppResource.SIGN_MANDATORY_TEXT_PRINT, AppResource.OK_LABEL);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = true;
                    });
                    try
                    {
                        var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                        analyticsService.LogEvent("", "IMPAIRMENT PRINT", "IMPAIRMENT PRINT");
                        string impguiId = string.Empty;
                        int impserverId = 0;
                        if (printable_impairment.server_id== 0)
                        {
                            var impairements =  CustomControl.Validation.GetServerImpaierment(printable_impairment);
                            var createImpResponse = await RestApiHelper<CreateIMPResponse>.CreateImp_Post(impairements.Result);
                            await Task.Yield();

                            if (createImpResponse == null)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", AppResource.PRINT_NOT_SUCCESS, AppResource.OK_LABEL);
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    ShowActivityLoader = false;
                                });
                                return;
                            }
                            else if (createImpResponse.Data != null)
                            {
                                impguiId = createImpResponse.Data.GUID;
                                impserverId = createImpResponse.Data.Id;
                            }
                            else if(createImpResponse.Status!=null)
                            {
                                if (createImpResponse.Status.StatusCode == 401)
                                {
                                    await Task.Yield();
                                    await _pageDialog.DisplayAlertAsync("", createImpResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                    var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                    // Xamarin.Forms.Application.Current.Properties.Clear();

                                    foreach (string key in keyList.ToList())
                                    {
                                        if (!key.Equals("WelcomeShow"))
                                        {
                                            Application.Current.Properties.Remove(key);
                                        }
                                    }
                                    await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                    ShowActivityLoader = false;
                                    return;
                                }
                            }

                            printable_impairment.server_id = impserverId;
                            printable_impairment.impairmentGUID= impguiId;
                            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                            var connection = hotworkDatabase.GetdbConnection();
                            connection.UpdateWithChildren(printable_impairment);
                        }
                        else
                        {
                            impguiId = printable_impairment.impairmentGUID;
                        }
                        var pdfPath = await RestApiHelper<string>.DownloadHwImpPDF_Post(impguiId, EmailIdEntry, SelFormat);

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                        if (string.IsNullOrEmpty(pdfPath))
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.PRINT_NOT_SUCCESS, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                        }
                        else
                        {
                            //await Task.Yield();
                            //await _pageDialog.DisplayAlertAsync("", "Email Sent Succesfully", "ok");


                            if (pdfPath == "Session Out")
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", "Unable to access, Please login again.", "ok");
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                // Xamarin.Forms.Application.Current.Properties.Clear();

                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                return;
                            }
                            else
                            {
                                Xamarin.Forms.DependencyService.Get<IShare>().Show("title", "Impairment Permit", pdfPath);
                            }
                        }
                        //await _navigationService.NavigateAsync("HomePage");
                    }
                    catch (Exception e)
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.PRINT_NOT_SUCCESS, AppResource.OK_LABEL);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                        Console.WriteLine("IOException source: {0}", e.Source);
                    }
                }
                else
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync(string.Empty, AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                }
            }
            else
            { // email
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (string.IsNullOrEmpty(EmailIdEntry))
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_BLANK, AppResource.OK_LABEL);
                        return;
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = true;
                    });
                    try
                    {
                        var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                        analyticsService.LogEvent("", "IMPAIRMENT SUBMIT", "IMPAIRMENT SUBMIT");
                        string impguid = string.Empty;
                        int impserverid = 0;
                        if (printable_impairment.server_id == 0)
                        {
                            var impairements = CustomControl.Validation.GetServerImpaierment(printable_impairment);
                            var createImpResponse = await RestApiHelper<CreateIMPResponse>.CreateImp_Post(impairements.Result);

                            if (createImpResponse == null)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    ShowActivityLoader = false;
                                });
                                return;
                            }
                            else if (createImpResponse.Data != null)
                            {
                                impguid = createImpResponse.Data.GUID;
                                impserverid = createImpResponse.Data.Id;
                            }
                            else if(createImpResponse.Status!=null)
                            {
                                if (createImpResponse.Status.StatusCode == 401)
                                {
                                    await Task.Yield();
                                    await _pageDialog.DisplayAlertAsync("", createImpResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                    var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                    // Xamarin.Forms.Application.Current.Properties.Clear();

                                    foreach (string key in keyList.ToList())
                                    {
                                        if (!key.Equals("WelcomeShow"))
                                        {
                                            Application.Current.Properties.Remove(key);
                                        }
                                    }
                                    await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                    ShowActivityLoader = false;
                                    return;
                                }
                            }

                            printable_impairment.server_id = impserverid;
                            printable_impairment.impairmentGUID = impguid;
                            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                            var connection = hotworkDatabase.GetdbConnection();
                            connection.UpdateWithChildren(printable_impairment);
                        }
                        else
                        {
                            impguid = printable_impairment.impairmentGUID;
                        }

                        var isEmailSentResponse = await RestApiHelper<EmailHWResponse>.SendEmailImp_Post(impguid, EmailIdEntry, SelFormat);
                        await Task.Yield();
                        //bool isEmailSent;
                        if (isEmailSentResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            return;
                        }
                        else if (isEmailSentResponse.Status != null)
                        {
                            if (isEmailSentResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", isEmailSentResponse.Status.StatusMessage, "ok");
                                Xamarin.Forms.Application.Current.Properties.Clear();
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                return;
                            }
                        }

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                        if (isEmailSentResponse.Data.Value)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.IMP_SUBMITTED_SUCCESS, AppResource.OK_LABEL);
                            // await _navigationService.NavigateAsync("HomePage");
                        }
                        else
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                            Console.WriteLine("IOException source: {0}", e.Source);
                        });
                    }


                }
                else
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                }
            }
        }

        //private async Task<SyncImpairements> GetSyncImp(Impairment_Sql impairement_sql)
        //{

          
        //}


        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

        private string _selFormat;
        public string SelFormat
        {
            get { return _selFormat; }
            set { SetProperty(ref _selFormat, value); }
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }
        private string _emailIdEntry;
        public string EmailIdEntry
        {
            get { return _emailIdEntry; }
            set { SetProperty(ref _emailIdEntry, value); }
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
        SQLiteConnection _connection;
        private Impairment_Sql printable_impairment;
        private Impairment PrintableImpairment;
        public void OnNavigatedTo(NavigationParameters parameters)
        {
            SQLiteDatabase impairmentDatabase = new SQLiteDatabase();
            _connection = impairmentDatabase.GetdbConnection();
            printable_impairment = _connection.GetWithChildren<Impairment_Sql>(App.PrintEditableImpId, true);
           // var scope = _connection.GetWithChildren<Scope_sql>(printablePermit.scope_Sql.id, recursive: true);
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
