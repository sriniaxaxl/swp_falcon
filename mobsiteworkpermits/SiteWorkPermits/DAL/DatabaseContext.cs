﻿using System;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Sqlite;
using SQLitePCL;

namespace SiteWorkPermits
{
    public class DatabaseContext : DbContext
    {
        private readonly string _databasePath;

        // Hotworks related tables
        public DbSet<WorkOrders> WorkOrders { get; set; }
        public DbSet<IncUniqueId> IncUniqueIds { get; set; }
        public DbSet<Scopes> Scopes { get; set; }
        public DbSet<ScopeWorkOrder> ScopeWorkOrders { get; set; }
        public DbSet<FirewatchPersons> FirewatchPersons { get; set; }
        public DbSet<FirePreventionMeasures> FirePreventionMeasures { get; set; }
        public DbSet<Precautions> Precautions { get; set; }
        public DbSet<PermitPrecautions> PermitPrecautions { get; set; }
        //public DbSet<HighHazardAreas> HighHazardAreas { get; set; }
        public DbSet<Authorizations> Authorizations { get; set; }
        public DbSet<HotworksPermits> HotworksPermits { get; set; }

        //impiarements related tabels
        public DbSet<ReporterDetail> ReporterDetail { get; set; }
        public DbSet<ImpairmentType> ImpairmentType { get; set; }
        public DbSet<ImpairmentClassType> ImpairmentClassType { get; set; }
        public DbSet<ShutDownReason> ShutDownReason { get; set; }
        public DbSet<PrecautionTaken> PrecautionTakens { get; set; }
        public DbSet<Impairment> Impairment { get; set; }
        public DbSet<ImpairmentMeasureMaster> ImpairmentMeasureMaster { get; set; }


               
        public DatabaseContext(string databasePath)
        {
            _databasePath = databasePath;

            if(Database.EnsureCreated()){
                Database.Migrate();    
            }else{
                Database.EnsureCreated();
            }
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddTimestamps();
            return (await base.SaveChangesAsync(true, cancellationToken));
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && 
                                                         (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                var now = DateTime.UtcNow; // current datetime

                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedAt = now;
                }
            ((BaseEntity)entity.Entity).UpdatedAt = now;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FirewatchPersons>()
                        .HasOne(pc => pc.FirePreventionMeasures);

            modelBuilder.Entity<FirePreventionMeasures>()
                        .HasMany(pc => pc.FirewatchPersons);
            
            modelBuilder.Entity<ImpairmentPrecautionTaken>()
                        .HasKey(bc => new { bc.ImpairmentPrecautionId, bc.ImpairmentId });

            //modelBuilder.Entity<ImpairmentPrecautionTaken>()
            //            .HasOne(bc => bc.Impairment)
            //            .WithMany(b => b.ImpairmentPrecautionTakens)
            //            .HasForeignKey(bc => bc.ImpairmentPrecautionId);

            //modelBuilder.Entity<ImpairmentPrecautionTaken>()
                        //.HasOne(bc => bc.PrecautionTaken)
                        //.WithMany(c => c.ImpairmentPrecautionTakens)
                        //.HasForeignKey(bc => bc.ImpairmentId);
            
            modelBuilder.Entity<ImpairmentImpairmentMeasureMaster>()
                        .HasKey(bc => new { bc.ImpairmentMeasureMasterId, bc.ImpairmentId });


            modelBuilder.Entity<PermitPrecautions>()
                        .HasKey(bc => new { bc.PrecautionsId, bc.PermitId });
            
            modelBuilder.Entity<ScopeWorkOrder>()
                        .HasKey(bc => new { bc.ScopeId, bc.WorkOrderId});

            //modelBuilder.Entity<ImpairmentImpairmentMeasureMaster>()
            //            .HasOne(bc => bc.Impairment)
            //            .WithMany(b => b.ImpairmentImpairmentMeasureMasters)
            //            .HasForeignKey(bc => bc.ImpairmentId);

            //modelBuilder.Entity<ImpairmentImpairmentMeasureMaster>()
                        //.HasOne(bc => bc.ImpairmentMeasureMaster)
                        //.WithMany(c => c.ImpairmentImpairmentMeasureMasters)
                        //.HasForeignKey(bc => bc.ImpairmentMeasureMasterId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_databasePath}");    
        }
    }
}
