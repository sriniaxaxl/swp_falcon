﻿using SiteWorkPermits.Model;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SiteWorkPermits.DAL.sqlIntegration
{

    public class HotWorkPermit_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public bool isDeleted { get; set; }
        public bool isParent { get; set; }
        public bool isNewlyCreated { get; set; }
        public bool isEdited { get; set; }
        public bool IsScannedPermits { get; set; }
        public string hotworkGUID { get; set; }

        public string hotWorkCloseDate { get; set; }
        public string workFlowStatus { get; set; }
        public string account_number { get; set; }
        public string location_number { get; set; }
        public string CloseAuthorizedBy { get; set; }

        public int server_id { get; set; }
        public int handHeldId { get; set; }

        public int fireHoseCount { get; set; }
        public int fireExtinguisherCount { get; set; }
               
         public byte[] ScannedPaperPermit { get; set; }

        public string ScannedPaperPermitGUID { get; set; }

        [OneToOne]
        public Scope_sql scope_Sql { get; set; }
        [ForeignKey(typeof(Scope_sql))]
        public int scope_Sql_id { get; set; }


        [OneToOne]
        public FirePrevention_sql firePrevention_sql { get; set; }
        [ForeignKey(typeof(FirePrevention_sql))]
        public int firePrevention_sql_id { get; set; }

        [ManyToMany(typeof(HotworkPrecautionMeasure_sql))]
        public List<PrecautionMaster_sql> precautionMaster_Sqls { get; set; }


        [OneToOne]
        public Authorization_sql authorization_sql { get; set; }
        [ForeignKey(typeof(Authorization_sql))]
        public int authorization_sql_id { get; set; }
    }

    #region Scope

    public class Scope_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string WorkDetail { get; set; }
        public string WorkLocation { get; set; }
        public string CompanyName { get; set; }
        public string EquipmentDescription { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }

        [OneToOne]
        public HotWorkPermit_sql hotWorkPermit_Sql { get; set; }
        [ForeignKey(typeof(HotWorkPermit_sql))]
        public int hotWorkPermit_Sql_id { get; set; }
               
        [OneToMany]
        public List<ScopePerson_sql> scopePerson_Sqls { get; set; }

        [ManyToMany(typeof(ScopeWorkOrder_sql))]
        public List<WorkOrderMaster_sql> workOrderMaster_Sqls { get; set; }
    }

    public class WorkOrderMaster_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string workOrderName { get; set; }

        [ManyToMany(typeof(ScopeWorkOrder_sql))]
        public List<Scope_sql> scope_Sql { get; set; }
    }

    public class ScopeWorkOrder_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        [ForeignKey(typeof(Scope_sql))]
        public int Scope_sql_id { get; set; }

        [ForeignKey(typeof(WorkOrderMaster_sql))]
        public int WorkOrderMaster_sql_id { get; set; }

    }

    public class ScopePerson_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string PersonName { get; set; }


        //[ManyToOne]
        //public Scope_sql scope_Sql { get; set; }
        [ForeignKey(typeof(Scope_sql))]
        public int scope_Sql_id { get; set; }
    }

    #endregion

    #region Fire Prevention

    public class FirePrevention_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string NearestPhone { get; set; }
        public string NearestFireAlarmLocation { get; set; }

        [OneToOne]
        public HotWorkPermit_sql hotWorkPermit_Sql { get; set; }
        [ForeignKey(typeof(HotWorkPermit_sql))]
        public int hotWorkPermit_Sql_id { get; set; }

        [OneToMany]
        public List<FireWatcherPrevention_sql> fireWatcherPrevention_Sqls { get; set; }

    }

    public class FireWatcherPrevention_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string PersonName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string FireWatchType { get; set; }

        [ForeignKey(typeof(FirePrevention_sql))]
        public int firePrevention_sql_id { get; set; }
    }

    #endregion
    
    #region Precaution Measure
  
    public class HotworkPrecautionMeasure_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        [ForeignKey(typeof(HotWorkPermit_sql))]
        public int HotWorkPermit_sql_id { get; set; }

        [ForeignKey(typeof(PrecautionMaster_sql))]
        public int PrecautionMaster_sql_id { get; set; }
    }

    public class PrecautionMaster_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string precautionName { get; set; }

        [ManyToMany(typeof(HotworkPrecautionMeasure_sql))]
        public List<HotWorkPermit_sql> hotWorkPermit_Sqls { get; set; }
    }

    #endregion

    #region Authorization
    public class Authorization_sql
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string AuthorizerName { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string AuthorizationDateTime { get; set; }
        public bool IsHighHazardArea { get; set; }
        public string AreaSupervisorName { get; set; }
        public string AreaSupervisorDepartment { get; set; }

        [OneToOne]
        public HotWorkPermit_sql hotWorkPermit_Sql { get; set; }
        [ForeignKey(typeof(HotWorkPermit_sql))]
        public int hotWorkPermit_Sql_id { get; set; }
    }
    #endregion

    #region ViewModel Hotwork Modal Class

    public class VMWorkPermit
    {
        public int ElementID { get; set; }
        public string Category { get; set; } 
        public string CreatedAt { get; set; }
        public string Description { get; set; }
        public string HotworkDate { get; set; }
        public string StartDate { get; set; } 
        public string ExpiryDate { get; set; } 
        public string WorkFlowStatus { get; set; }
        private bool _FromArchieve = false;
        public bool FromArchieve
        {
            get
            {
                return _FromArchieve;
            }
            set
            {
                _FromArchieve = value;
            }
        }
    }

    #endregion

    #region ViewModel Impairment Modal Class

    public class VMImpairment
    {
        public int ElementID { get; set; }
        public string Category { get; set; } 
        public string CreatedAt { get; set; }
        public string Description { get; set; } 
        public string ExpiryDate { get; set; } 
        public string WorkFlowStatus { get; set; }
        private bool _FromArchieve = false;
        public bool FromArchieve
        {
            get
            {
                return _FromArchieve;
            }
            set
            {
                _FromArchieve = value;
            }
        }
    }

    #endregion
}
