﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using Foundation;
using SiteWorkPermits.iOS.Platform;
using SiteWorkPermits.Platform;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(Localize_iOS))]
namespace SiteWorkPermits.iOS.Platform
{
    class Localize_iOS : ILocalize
    {
        public CultureInfo GetCurrentCultureInfo()
        {
            var dotnetLanguage = "en";
            var languagePrefix = "en";

            if (NSLocale.PreferredLanguages.Length > 0)
            {
                var language = NSLocale.PreferredLanguages[0];
                languagePrefix = language.Substring(0, 2);

                dotnetLanguage = language.Replace("_", "-");
            }

            CultureInfo cultInfo;
            try
            {
                cultInfo = new CultureInfo(dotnetLanguage);
            }
            catch { cultInfo = new CultureInfo(languagePrefix); }

            return cultInfo;
        }

        public void SetLocale(CultureInfo ci)
        {
            var iosLocalAuto = NSLocale.AutoUpdatingCurrentLocale.LocaleIdentifier;
            var netLocale = iosLocalAuto.Replace("_", "-");
            CultureInfo cultInfo;
            try
            {
                cultInfo = new CultureInfo(netLocale);
            }
            catch { cultInfo = GetCurrentCultureInfo(); }

            Thread.CurrentThread.CurrentCulture = cultInfo;
            Thread.CurrentThread.CurrentUICulture = cultInfo;
        }

         
    }
}