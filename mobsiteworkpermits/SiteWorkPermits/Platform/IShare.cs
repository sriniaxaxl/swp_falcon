﻿using System;
using System.Threading.Tasks;

namespace SiteWorkPermits.Platform
{
    public interface IShare
    {
        Task Show(string title, string message, string filePath);
    }
}
