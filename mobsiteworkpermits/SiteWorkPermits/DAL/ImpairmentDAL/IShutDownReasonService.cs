﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IShutDownReasonService
    {
        IEnumerable<ShutDownReason> GetShutDownReason();

        ShutDownReason GetShutDownReasonById(int id);

        bool AddShutDownReason(ShutDownReason shutDownReason);

        void RemoveAllData();
    }
}
