﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using Prism;
using Prism.Ioc;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using Xamarin.Forms;
using Android.App.Job;
using Java.Lang;
using System.Threading.Tasks;
using Java.Util;
using Android;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Content.Res;
using SiteWorkPermits.Platform;
using Java.IO;
using CarouselView.FormsPlugin.Android;
using Prism.Navigation;
using Prism.Common;
using SiteWorkPermits.ViewPage;
using SiteWorkPermits.ViewModel;
using SiteWorkPermits.DAL.RestHelper;
using Prism.Autofac;
using Android.Security.Keystore;
using Java.Security;
using Javax.Crypto;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Model.SyncHotWorks;
using Android.Graphics;
using Java.Nio;

namespace SiteWorkPermits.Droid
{
    [Activity(Label = "SiteWorkPermits", Icon = "@mipmap/ic_launcher",
              RoundIcon = "@mipmap/ic_launcher_round", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        private static int REQUEST_EXTERNAL_STORAGE = 1;
        private static string[] PERMISSIONS_STORAGE = {
            Manifest.Permission.ReadExternalStorage,
            Manifest.Permission.WriteExternalStorage
};

      
        internal static MainActivity Instance { get; private set; }
        public TaskCompletionSource<byte[]> PickImageTaskCompletionSource { set; get; }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            Instance = this;

            Window.RequestFeature(WindowFeatures.ActionBar);
            //ActionBar.Hide();

            base.OnCreate(bundle);
           
            //this.Window.AddFlags(WindowManagerFlags.Fullscreen); // hide the status bar

            //int uiOptions = (int)Window.DecorView.SystemUiVisibility;

            //uiOptions |= (int)SystemUiFlags.LowProfile;
            //uiOptions |= (int)SystemUiFlags.Fullscreen;
            //uiOptions |= (int)SystemUiFlags.HideNavigation;
            //uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

            //Window.DecorView.SystemUiVisibility =
            // (StatusBarVisibility)uiOptions;

            try
            {
                global::Xamarin.Forms.Forms.Init(this, bundle);
                //Window.SetFlags(WindowManagerFlags.Secure, WindowManagerFlags.Secure);
                CarouselViewRenderer.Init();

                Plugin.InputKit.Platforms.Droid.Config.Init(this, bundle);
                LoadApplication(new App(new AndroidInitializer()));
            }
            catch (System.Exception e)
            {
            }
            verifyStoragePermissions(this);
            App.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density);
            App.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density);
            DatabasePath.Init(this);
            BiometricAuthentication.Init(this);

           
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {

            if (requestCode == 1)
            {

                if (resultCode == Result.Ok)
                {
                    App._isBiometricPass = true;
                    Xamarin.Forms.MessagingCenter.Send("BiometricPrompt", "BiometricResultYes");
                }
                else if (resultCode == Result.Canceled) {
                    Finish();
                }
                else
                {

                    App._isBiometricPass = false;
                    Xamarin.Forms.MessagingCenter.Send("BiometricPrompt", "BiometricResultNo");
                }
            }

            if (requestCode == 102)
            {
                if ((resultCode == Result.Ok) && (data != null))
                {
                   var image =  (Bitmap)data.Extras.Get("data");

                    // ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // image.Compress(Bitmap.CompressFormat.Png, 100, stream);
                    //byte[] byteArray = stream.ToByteArray();


                    //int size = image.RowBytes * image.Height;
                    //ByteBuffer b = ByteBuffer.Allocate(size);
                    //image.CopyPixelsToBuffer(b);
                    //byte[] bytes = new byte[size];
                    //b.Get(bytes, 0, bytes.Length);
                   



                   var stream = new MemoryStream(); 
                  
                    image.Compress(Bitmap.CompressFormat.Png, 100, stream);
                    PickImageTaskCompletionSource.SetResult(stream.ToArray());
                }
                else
                {
                    PickImageTaskCompletionSource.SetResult(null);
                }
            }

        }

        public override async void OnBackPressed()
        {
            var Curpage = PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);

            if (Curpage is DashBoardPage || Curpage is LoginPage)
            {
                this.Finish();
                return;
            }
            else
            {
                if (Curpage is TermsConditionLoginPage)
                {
                    var vm = Curpage.BindingContext as TermsAndConditionLoginViewModel;
                    await vm._navigationService.GoBackToRootAsync();
                    return;
                }

                if (Curpage is NewHotWorksPage && App.FromHwToNewHw == true)
                {
                    var vm = Curpage.BindingContext as NewHotWorksPageViewModel;
                    var hotWorks = await RestApiHelper<TempHtWorkDetailResponse>.GetArchiveDetail_Get(App.PermitEditableId);
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("archiveModel", hotWorks.Data);
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/HotWorksPage", UriKind.Relative), null, null, false);
                    return;
                }
                else if (Curpage is ArchivePage && App.FromHwToArchive == true)
                {
                    var vm = Curpage.BindingContext as ArchieveViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/HotWorksPage", UriKind.Relative), null, null, false);
                    return;
                }
                else if (Curpage is ArchivePage && App.FromHwToArchive == true)
                {
                    var vm = Curpage.BindingContext as ArchieveViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/HotWorksPage", UriKind.Relative), null, null, false);
                    return;

                }
                else if (Curpage is ArchivePage && App.FromImpToNewImp == true)
                {
                    var vm = Curpage.BindingContext as ArchieveViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/ImpairmentsPage", UriKind.Relative), null, null, false);
                    return;
                }
                else if (Curpage is ImpairmentsPage && App.FromHwToArchive == true)
                {
                    var vm = Curpage.BindingContext as ImpairmentsViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/HotWorksPage", UriKind.Relative), null, null, false);
                    return;
                }
                else if (Curpage is HotWorksPage && App.FromImpToNewImp == true)
                {
                    var vm = Curpage.BindingContext as HotWorksPageViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/ImpairmentsPage", UriKind.Relative), null, null, false);
                    return;
                }
                else if (Curpage is NewImpairmentPage && App.FromImpToArchive == true)
                {
                    var vm = Curpage.BindingContext as NewImpairmentsPageViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/ImpairmentsPage", UriKind.Relative), null, null, false);
                    return;
                }
                else if (Curpage is ArchivePage && App.FromImpToNewImp == true)
                {
                    var vm = Curpage.BindingContext as ArchieveViewModel;
                    await vm._navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/ImpairmentsPage", UriKind.Relative), null, null, false);
                    return;
                }
                else
                {
                    await App.navigation.NavigateAsync("/HomePage", null, null, false);
                    return;
                }
            }
        }

        public static void verifyStoragePermissions(Activity activity)
        {
            if (ContextCompat.CheckSelfPermission((Activity)Forms.Context, Manifest.Permission.WriteExternalStorage)
                != (int)Android.Content.PM.Permission.Granted)
            {
                ActivityCompat.RequestPermissions(
                       activity,
                       PERMISSIONS_STORAGE,
                       REQUEST_EXTERNAL_STORAGE
               );
            }
        }


        public string navigationpath = "HomePage/NavigationPage/HotWorksPage";
        protected async override void OnNewIntent(Intent intent)
        {
            // Send message to the PCL (XF) if a certain page should be opened.
            if (intent.HasExtra("OpenPage"))
            {
                string pageName = intent.GetStringExtra("OpenPage") ?? "None";
                var Curpage = PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                if (pageName != "None")
                {
                    var key = Helper.Getval("JWT_TOKEN");
                    if (string.IsNullOrEmpty(key))
                    {
                        navigationpath = "LoginPage";
                    }
                    string HotworkID = intent.GetStringExtra("HotWorkID");

                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("FromNotification", "true");
                    if (Curpage.BindingContext is AboutUsPageViewModel)
                    {
                        var model = (AboutUsPageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is ArchieveViewModel)
                    {
                        var model = (ArchieveViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is ContactUsViewModel)
                    {
                        var model = (ContactUsViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is DownloadViewModel)
                    {
                        var model = (DownloadViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is LoginPageViewModel)
                    {
                        var model = (LoginPageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is GuideViewModel)
                    {
                        var model = (GuideViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is HomePageViewModel)
                    {
                        var model = (HomePageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is HotWorksPageViewModel)
                    {
                        var model = (HotWorksPageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is ImpairmentsViewModel)
                    {
                        var model = (ImpairmentsViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is NewHotWorksPageViewModel)
                    {
                        var model = (NewHotWorksPageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is NewImpairmentsPageViewModel)
                    {
                        var model = (NewImpairmentsPageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is PrintPermitViewModel)
                    {
                        var model = (PrintPermitViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is PrintImpairmentViewModel)
                    {
                        var model = (PrintImpairmentViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                    else if (Curpage.BindingContext is TermsAndConditionPageViewModel)
                    {
                        var model = (TermsAndConditionPageViewModel)Curpage.BindingContext;
                        await model._navigationService.NavigateAsync(new Uri(navigationpath, UriKind.Relative), navigationParams, null, false);
                    }
                }
            }
            base.OnNewIntent(intent);
        }
    }
}

public class AndroidInitializer : IPlatformInitializer
{

    public void RegisterTypes(IContainerRegistry containerRegistry)
    {

    }
}