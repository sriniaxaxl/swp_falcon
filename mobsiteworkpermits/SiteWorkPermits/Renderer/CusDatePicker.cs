﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusDatePicker : Entry
    {
        public delegate void DateChanged(object sender, DateChangeArgs e);
        //public DateChanged ValueChanged;
        public EventHandler<DateChangeArgs> Dateselected;
       
        public CusDatePicker() : base()
        {
            HeightRequest = 30;
            //ValueChanged = new DateChanged(Changed_Date);
            //dateselected += CusDatePicker_Dateselected;
        }

        public void Changed_Date(object sender, DateChangeArgs e)
        {
            CusDatePicker entry = (CusDatePicker)sender;
            entry.Text = e.Newdate.ToString("dd/MM/yyyy");
        }


        #region Properties   

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusDatePicker, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

       

        public static BindableProperty PaddingProperty = BindableProperty.Create<CusDatePicker, Thickness>(o => o.Padding, new Thickness(5));
        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public static BindableProperty MinimumDateProperty = BindableProperty.Create<CusDatePicker, DateTime>(o => o.MinimumDate, new DateTime(01, 01, 01));

        public DateTime MinimumDate
        {
            get { return (DateTime)GetValue(MinimumDateProperty); }
            set { SetValue(MinimumDateProperty, value); }
        }

        public static BindableProperty MaximumDateProperty = BindableProperty.Create<CusDatePicker, DateTime>(o => o.MaximumDate, new DateTime(3000, 12, 31));

        public DateTime MaximumDate
        {
            get { return (DateTime)GetValue(MaximumDateProperty); }
            set { SetValue(MaximumDateProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusDatePicker, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusDatePicker, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusDatePicker, float>(o => o.BorderRadius, 0);

        public float BorderRadius
        {
            get { return (float)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        #endregion
    }

    public class DateChangeArgs
    {
        public DateTime _Newdate;
        public DateTime Newdate
        {
            get
            {
                return this._Newdate;
            }
            set 
            { 
                this._Newdate = value;

            }
        }
        public DateTime Olddate { get; set; }
    }

    public class ValueArgs
    {
        public string SelectedItem { get; set; }
        public int SelectedIndex { get; set; }
    }
}
