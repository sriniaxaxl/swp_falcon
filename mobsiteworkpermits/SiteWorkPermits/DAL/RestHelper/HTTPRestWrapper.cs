﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SiteWorkPermits.DAL.RestHelper
{
    public class HTTPRestWrapper<T> where T: class
    {

        public static async Task<T> Get(string url)
        {
            T result = null;
            using (var httpClient = new HttpClient())
            {
                var response = httpClient.GetAsync(new Uri(url)).Result;

                response.EnsureSuccessStatusCode();
                await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;

                    result = JsonConvert.DeserializeObject<T>(x.Result);
                });
            }

            return result;
        }

        public static async Task<T> PostRequest(string apiUrl, StringContent postObject)
        {
            try
            {
                T result = null;
                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(apiUrl, postObject).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                    {
                        if (x.IsFaulted)
                            throw x.Exception;
                        result = JsonConvert.DeserializeObject<T>(x.Result);
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



    }
}
