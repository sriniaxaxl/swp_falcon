﻿using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiteWorkPermits.Droid.Platform
{
    public class SampleContentProvider : ContentProvider
    {

        public override ParcelFileDescriptor OpenFile(Android.Net.Uri uri, string mode)
        {
            File privateFile = new File(uri.Path);
            return ParcelFileDescriptor.Open(privateFile,ParcelFileMode.ReadOnly);
        }

        public override int Delete(Android.Net.Uri uri, string selection, string[] selectionArgs)
        {
            return 0;
        }

        public override string GetType(Android.Net.Uri uri)
        {
            return null;
        }

        public override Android.Net.Uri Insert(Android.Net.Uri uri, ContentValues values)
        {
            return null;
        }

        public override bool OnCreate()
        {
            return false;
        }

        public override ICursor Query(Android.Net.Uri uri, string[] projection, string selection, string[] selectionArgs, string sortOrder)
        {
            return null;
        }

        public override int Update(Android.Net.Uri uri, ContentValues values, string selection, string[] selectionArgs)
        {
            return 0;
        }
    }
}