﻿using System;
using System.ComponentModel;
using CoreGraphics;
using CustomControl;
using CustomControl.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusDatePicker), typeof(CustomDatepickerRenderer))]
namespace CustomControl.iOS.Renderer
{
    public class CustomDatepickerRenderer : EntryRenderer
    {
        CusDatePicker FormDatePicker;
        UIDatePicker datePicker;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            FormDatePicker = Element as CusDatePicker;
            SetBackgroud();
            SetBorder();
            SetPadding();
            if (FormDatePicker.Dateselected == null)
            {
                FormDatePicker.Dateselected += FormDatePicker_Dateselected;
            }
            Control.BorderStyle = UITextBorderStyle.None;
            Control.ClipsToBounds = true;
            datePicker = new UIDatePicker();
            datePicker.Mode = UIDatePickerMode.Date;
            datePicker.MinimumDate = FormDatePicker.MinimumDate.ToNSDate();
            datePicker.MaximumDate = FormDatePicker.MaximumDate.ToNSDate();
            //Setup the toolbar
            UIToolbar toolbar = new UIToolbar();
            toolbar.BarStyle = UIBarStyle.Black;
            toolbar.BackgroundColor = UIColor.Blue;

            toolbar.Translucent = true;
            toolbar.SizeToFit();

            UIBarButtonItem CancelButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Done, (s, er) =>
            {
                Control.ResignFirstResponder();
            });

            UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done, (s, er) =>
            {
                Control.ResignFirstResponder();
                DateChangeArgs args = new DateChangeArgs();
                args.Newdate = datePicker.Date.ToDateTime();
                FormDatePicker.Dateselected.Invoke(FormDatePicker, args);
                //Control.Text = datePicker.Date.ToDateTime().ToString("dd/MM/yyyy");               
            });

            UIBarButtonItem flexiblespace = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
            CancelButton.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            doneButton.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            toolbar.SetItems(new UIBarButtonItem[] { CancelButton, flexiblespace, doneButton }, true);
            Control.InputView = datePicker;
            //Display the toolbar over the pickers
            Control.InputAccessoryView = toolbar;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CusDatePicker.TextProperty.PropertyName)
            {
                Control.Text = FormDatePicker.Text;
            }
            else if (e.PropertyName == CusDatePicker.BorderWidthProperty.PropertyName ||
                     e.PropertyName == CusDatePicker.BorderColorProperty.PropertyName ||
                     e.PropertyName == CusDatePicker.BorderRadiusProperty.PropertyName ||
                     e.PropertyName == CusDatePicker.BackgroundColorProperty.PropertyName ||
                     e.PropertyName == CusDatePicker.InnerBackgroundColorProperty.PropertyName)
            {
                SetBackgroud();
                SetBorder();
            }
            else if(e.PropertyName == CusDatePicker.MinimumDateProperty.PropertyName)
            {
                datePicker.MinimumDate = FormDatePicker.MinimumDate.ToNSDate();              
            }
            else if (e.PropertyName == CusDatePicker.MaximumDateProperty.PropertyName)
            {
                datePicker.MinimumDate = FormDatePicker.MaximumDate.ToNSDate();    
            }
        }

        void FormDatePicker_Dateselected(object sender, DateChangeArgs e)
        {
            if (e.Newdate != null)
            {
                FormDatePicker.Text = e.Newdate.ToString("dd/MM/yyyy");
            }
        }

        void SetBackgroud()
        {
            if (FormDatePicker.InnerBackground != Color.Transparent)
            {
                Control.BackgroundColor = FormDatePicker.InnerBackground.ToUIColor();
            }
            else
            {
                Control.BackgroundColor = FormDatePicker.BackgroundColor.ToUIColor();
            }
        }

        void SetBorder()
        {
            Control.Layer.BorderColor = FormDatePicker.BorderColor.ToCGColor();
            Control.Layer.BorderWidth = FormDatePicker.BorderWidth;
            Control.Layer.CornerRadius = FormDatePicker.BorderRadius;
        }

        void SetPadding()
        {
            if (FormDatePicker.Padding.Left != 0)
            {
                Control.LeftView = new UIView(new CGRect(0, 0, FormDatePicker.Padding.Left, 0));
                Control.RightView = new UIView(new CGRect(0, 0, FormDatePicker.Padding.Right, 0));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightViewMode = UITextFieldViewMode.Always;
            }
        }
    }
}
