﻿using System;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IHotworkPermitService
    {
        IEnumerable<HotworksPermits> GetHotworksAsync();

        HotworksPermits GetHotworksByIdAsync(int id);

        bool AddHotworksAsync(HotworksPermits hotworksPermit);

        bool UpdateHotworksAsync(HotworksPermits hotworksPermit);

        bool RemoveHotworksAsync(int id);

        void RemoveAllData();
    }
}
