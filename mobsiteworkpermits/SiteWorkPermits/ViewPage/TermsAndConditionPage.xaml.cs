﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits.ViewPage
{
    public partial class TermsAndConditionPage : ContentPage
    {

        string TandCText = string.Empty;
        string TandCTextLink = string.Empty;
        string TandCText2 = string.Empty;
        public TermsAndConditionPage()
        {
            InitializeComponent();
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            TandCText = "By downloading or using this Site Work Permits app (the \"App\") you agree to be bound by the following terms and conditions " +
                "(the \"Terms\") and our Privacy and Cookies policy";

            termsDesc.Text = TandCText;

            TandCTextLink = "http://axaxl.com/privacy-and-cookies";
            termsDesc1.Text = TandCTextLink;

            TandCText2 = "If after you Agree to these terms and conditions, you change your mind and no longer agree with these Terms, " +
                "then you should stop using and delete the App immediately. The App is made available to you by XL Catlin Services SE, an AXA Group company, " +
                "for your own, personal use. The App must not be used in any unlawful manner or for any unlawful purpose including (without limitation) " +
                "copyright infringement. Additionally, you must not:\n\n i. Attempt to gain unauthorised access to the App or any servers, " +
                "computer systems or other networks connected to the App; or\n\nii. Modify, enhance, create derivative works, copy, " +
                "reverse engineer any part of the App or reformat or frame any portion of the pages comprising the App or " +
                "extract or attempt to extract any part of the source code\n\n It is your responsibility to ensure all devices and access to the App are secure and in respect" +
                " of any impairment notice submitted to check that you have received confirmation that the impairment notice has been received.\n\n All intellectual property rights - " +
                "including copyright, database rights, first publication rights, patents, trademarks and know-how-contained in, on or available through this App unless otherwise indicated are owned either by XL Catlin Services " +
                "SE or its related, affiliated and subsidiary companies or made available to us under licence by third parties. This App and its content, unless " +
                "indicated otherwise is protected by copyright. All trademarks and devices displayed on this App, unless otherwise indicated, are owned by AXA Group " +
                "companies and/or our licensors and may be registered in jurisdictions the world. Except as provided below, any use or reproduction of these trademarks" +
                " and devices is prohibited.\n\n" +
                "Subject to any prohibitions stated on third-party websites accessible via hyperlinks on the website, you may view content published on this App and are welcome to print hardcopies of material on it for your personal or internal business purposes. All other copying is strictly prohibited.\n\n" +
                "DISCLAIMER/LIABILITY: USE OF THE APP IS AT YOUR OWN RISK. THE APP AND ITS CONTENTS ARE PROVIDED ON AN “AS IS” BASIS AND WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESSED OR IMPLIED.\n\n" +
                "AXA SA and its related, affiliated and subsidiary companies, disclaim all warranties, express or implied, including but not limited to, implied of merchantability and fitness for a particular purpose.\n\n" +
                "It is your responsibility to evaluate the accuracy, completeness and usefulness of any opinions, advice, services or other information provided. All information contained on this App is distributed with the understanding that the authors, distributors are not rendering legal, accounting or other professional advice or opinions on specific facts or matters and accordingly assume no liability whatsoever in connection with its use. Consult your legal or tax advisor regarding your specific situation.\n\n" +
                "In no event shall XL Catlin Services SE and its related, affiliated and subsidiary companies be liable for any direct, indirect, special, incidental or consequential damages arising from the use of the information on this App.\n\n" +
                "Any records stored in the archive of the App will be kept for a period of seven years from the date creation of that record and then destroyed. Should you wish to retain any records for a longer period, it is your responsibility to print and save these records separately into your own record keeping system and in accordance with applicable law.\n\n" +
                "Nothing in these Terms shall be construed as excluding or limiting the liability of AXA SA or its group companies for death or personal injury caused by its negligence or for any other liability which cannot be excluded by English law.\n\n" +
                "These terms (as may be amended) constitute the entire agreement between you and AXA SA concerning your use of the App.\n\n" +
                "AXA SA reserves the right to update the App and/or these Terms from time to time. If it does so, the updated Terms would be effective immediately, and the current Terms are available through a link in the App to this page. You are responsible for regularly reviewing these Terms so that you are aware of any changes to them and you will be bound by the new policy upon your continued use of the App.\n\n" +
                "These terms shall be governed by and construed in accordance with an English law disputes, claims or proceedings between the parties relating to the validity, construction all forms of these term shall be subject to the exclusive jurisdiction of the courts of England and Wales.\n\n" +
                "If any provision of these terms (in whole or in part) is how to be illegal, invalid, or otherwise unenforceable, the other provisions shall remain in full force and effect.\n\n" +
                "XL Catlin Service SE’s failure to exercise or enforce any rights or provision of these Terms shall not constitute a waiver to subsequently exercise or enforce any such right or provision.\n\n" +
                "Nothing in these Terms shall create any rights or other benefits under the Contracts (rights of Third Parties) Act 1999 (as may be amended) in favour of any person other than you, AXA or other AXA XL companies.\n\n";
            termsDesc2.Text = TandCText2;
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("http://axaxl.com/privacy-and-cookies"));
        }
    }
}
