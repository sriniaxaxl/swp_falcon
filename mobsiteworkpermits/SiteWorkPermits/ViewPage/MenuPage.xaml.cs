﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("CompanyName"))
            {
                comapnyName.Text = Xamarin.Forms.Application.Current.Properties["CompanyName"] as string;
            }
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("CompanyAddress"))
            {
                comapnyAddress.Text = Xamarin.Forms.Application.Current.Properties["CompanyAddress"] as string;
            }
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("siteId"))
            {
                App.SiteId = Xamarin.Forms.Application.Current.Properties["siteId"] as string;
            }

        }

        void Archive_Tapped(object sender, System.EventArgs e)
        {
            App.curhomepage.Detail = new ArchivePage();
            App.curhomepage.IsPresented = false;
        }
    }
}
