﻿using System;
using Xamarin.Forms;
using SiteWorkPermits.Platform;
namespace SiteWorkPermits.DAL
{
    public class DataContextEntity
    {
        private static DatabaseContext _dbInstance;

        protected DataContextEntity()
        {
        }

        public static DatabaseContext Instance()
        {
            if (_dbInstance == null)
            {
                var connection = DependencyService.Get<IDBPath>().GetDbPath();
                _dbInstance = new DatabaseContext(App.connection.DatabasePath);
            }
            return _dbInstance;
        }
    }
}

