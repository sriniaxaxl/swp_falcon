﻿using System;
using Android.App;
using Android.Content;
using Android.Security.Keystore;
using Android.Widget;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Platform;
using Java.Security;
using Javax.Crypto;
using Xamarin.Forms;

[assembly: Dependency(typeof(BiometricAuthentication))]
namespace SiteWorkPermits.Droid.Platform
{
    public class BiometricAuthentication : IBiometricAuthentication
    {
        static readonly string KEY_NAME = "my_key";
        static readonly byte[] SECRET_BYTE_ARRAY = new byte[] {
            1, 2, 3, 4, 5, 6
        };
        static readonly int REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS = 1;

        /**
     	* If the user has unlocked the device Within the last this number of seconds,
     	* it can be considered as an authenticator.
     	*/
        static readonly int AUTHENTICATION_DURATION_SECONDS = 1;

        KeyguardManager keyguardManager;


        static Context _context;

        public static void Init(Context context)
        {
            _context = context;
        }

        public bool isBiometricSuccess()
        {

            try
            {
                keyguardManager = (KeyguardManager)Forms.Context.GetSystemService(Context.KeyguardService);

                if (!keyguardManager.IsKeyguardSecure)
                {
                    App._isBiometricPass = true;
                    Xamarin.Forms.MessagingCenter.Send("BiometricPrompt", "BiometricResultYes");
                    return true;
                }
                CreateKey();
                TryEncrypt();
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }


        void CreateKey()
        {
            // Generate a key to decrypt payment credentials, tokens, etc.
            // This will most likely be a registration step for the user when they are setting up your app.
            try
            {
                var keyStore = KeyStore.GetInstance("AndroidKeyStore");
                keyStore.Load(null);
                var keyGenerator = KeyGenerator.GetInstance(KeyProperties.KeyAlgorithmAes, "AndroidKeyStore");

                // Set the alias of the entry in Android KeyStore where the key will appear
                // and the constrains (purposes) in the constructor of the Builder
                keyGenerator.Init(new KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyStorePurpose.Encrypt | KeyStorePurpose.Decrypt)
                    .SetBlockModes(KeyProperties.BlockModeCbc)
                    .SetUserAuthenticationRequired(true)
                    // Require that the user has unlocked in the last 30 seconds
                    .SetUserAuthenticationValidityDurationSeconds(AUTHENTICATION_DURATION_SECONDS)
                    .SetEncryptionPaddings(KeyProperties.EncryptionPaddingPkcs7)
                    .Build());
                keyGenerator.GenerateKey();
            }
            catch (System.Exception e)
            {
                throw new SystemException("Failed to create a symmetric key", e);
            }
        }

        bool TryEncrypt()
        {
            try
            {
                var keyStore = KeyStore.GetInstance("AndroidKeyStore");
                keyStore.Load(null);
                IKey secretKey = keyStore.GetKey(KEY_NAME, null);
                var cipher = Cipher.GetInstance(
                    KeyProperties.KeyAlgorithmAes + "/" + KeyProperties.BlockModeCbc + "/"
                    + KeyProperties.EncryptionPaddingPkcs7);

                // Try encrypting something, it will only work if the user authenticated within
                // the last AUTHENTICATION_DURATION_SECONDS seconds.
                cipher.Init(Javax.Crypto.CipherMode.EncryptMode, (IKey)secretKey);
                cipher.DoFinal(SECRET_BYTE_ARRAY);

                // If the user has recently authenticated, you will reach here.
                ShowAlreadyAuthenticated();
                return true;
            }
            catch (UserNotAuthenticatedException)
            {
                // User is not authenticated, let's authenticate with device credentials.
                ShowAuthenticationScreen();
                return false;
            }
            catch (KeyPermanentlyInvalidatedException e)
            {
                // This happens if the lock screen has been disabled or reset after the key was
                // generated after the key was generated.
                Toast.MakeText(_context, "Keys are invalidated after created. Retry the purchase\n"
                    + e.Message, ToastLength.Short).Show();
                return false;
            }
            catch (System.Exception e)
            {
                throw new SystemException("Exception", e);
            }
        }


        void ShowAuthenticationScreen()
        {
            Intent intent = keyguardManager.CreateConfirmDeviceCredentialIntent(App.BiometricAppNameMsg, App.BiometricMsg);
            
            if (intent != null)
            {
                var s = (MainActivity)Forms.Context;
                s.StartActivityForResult(intent, REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS);
            }
        }


        void ShowPurchaseConfirmation()
        {
            

        }
        void ShowAlreadyAuthenticated()
        {
            Toast.MakeText(MainActivity.Instance, "already logged in", ToastLength.Long);
        }
    }
}