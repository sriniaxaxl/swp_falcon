﻿using System;
using System.Threading.Tasks;
using Android.App;
using SiteWorkPermits.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:Dependency(typeof(AlertService))]
namespace SiteWorkPermits.Droid.Renderer
{
    public class AlertService : IAlertService
    {

        public event EventHandler Valueselected;

        public void DisplayAlert(string Title , string Message, string Accept, string Cancel)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Forms.Context);
            alertDialog.SetTitle(Title);
            alertDialog.SetMessage(Message);
            alertDialog.SetPositiveButton(Accept, (sender, e) => 
            {
                if(Valueselected != null)
                {
                    Valueselected.Invoke(this, new EventArgs());
                }
            });          
            if(!string.IsNullOrEmpty(Cancel))
            {
                alertDialog.SetNegativeButton(Cancel, (sender, e) =>
                {
                    if (Valueselected != null)
                    {
                        Valueselected.Invoke(this, new EventArgs());
                    }
                });
            }
            var pr = alertDialog.Show();
            pr.SetCanceledOnTouchOutside(false);
            pr.SetCancelable(false);
            CustomControl.Validation.SetUnSyncAlarm();
        }


    }
}
