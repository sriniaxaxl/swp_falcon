﻿using System;
using System.ComponentModel;
using CustomControl;
using CustomControl.iOS.Renderer;
using iOS.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusAutoCompleteView), typeof(CusAutoCompleteRenderer))]
namespace iOS.Renderer
{
    public class CusAutoCompleteRenderer : ViewRenderer<CusAutoCompleteView, UIKit.UIView>
    {       
        protected override void OnElementChanged(ElementChangedEventArgs<CusAutoCompleteView> e)
		{
            base.OnElementChanged(e);
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
            base.OnElementPropertyChanged(sender, e);
		}
	}
}
