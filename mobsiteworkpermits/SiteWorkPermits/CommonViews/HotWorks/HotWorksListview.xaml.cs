﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class HotWorksListview : ContentView
    {
        public ObservableCollection<ModelPermits> modelPermits;
        public HotWorksListview(ObservableCollection<ModelPermits> lst,bool draft)
        {            
            InitializeComponent();
            if (lst != null)
            {
                if(lst.Count==0){
                    nohotworksLabel.IsVisible = true;
                    return;
                }
                modelPermits = lst;
                Splst.Children.Clear();
                foreach (var d in lst)
                {
                    nohotworksLabel.IsVisible = false;
                    HotWorksView hotview = new HotWorksView(draft);
                    hotview.Collection = d;
                    hotview.BindingContext = d;
                    Splst.Children.Add(hotview);
                }
                Splst.ForceLayout();
            }else{
                nohotworksLabel.IsVisible = true;
            }
        }
    }
}
