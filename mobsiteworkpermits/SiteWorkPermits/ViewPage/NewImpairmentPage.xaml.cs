﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using SiteWorkPermits.Resx;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits
{
    public partial class NewImpairmentPage : ContentPage
    {
        ObservableCollection<TabInfo> TabCollection = new ObservableCollection<TabInfo>();
        NewImpairmentsPageViewModel model;
        public NewImpairmentPage()
        {
            InitializeComponent();
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            TabCollection.Add(new TabInfo { Title = AppResource.IMP_INFORMATION, IsSelected = true });
            TabCollection.Add(new TabInfo { Title = AppResource.IMP_IMP_DETAILS, IsSelected = false });
            TabCollection.Add(new TabInfo { Title = AppResource.IMP_PRE_TAKEN, IsSelected = false });
            foreach (var d in TabCollection)
            {
                TabView tab = new TabView();
                tab.Title = d.Title;
                tab.TabColor = d.IsSelected ? Color.FromHex("#00AFAD") : Color.Transparent;
                tab.GestureRecognizers.Add(new TapGestureRecognizer(TabAction));
                Spworktabs.Children.Add(tab);
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
             {

                 model = (NewImpairmentsPageViewModel)BindingContext;


                 model.ChangeTabEvent += changeTab;

                 var informationview = new CreationImpairmentPanelView { SectionView = new InformationView() { IsEnabled = (model.FromArchieve && !App.isDuplicate) ? false : true } };
                 Xamarin.Forms.ScrollView scInformation = new Xamarin.Forms.ScrollView { Content = informationview };
                 Informationview.Content = new ContentView { Content = scInformation };
                 Informationview.IsVisible = true;
                 var precautionview = new CreationImpairmentPanelView { SectionView = new PreCautionTakenView(model) { IsEnabled = (model.FromArchieve && !App.isDuplicate) ? false : true } };
                 precautionview.RaiseImpairmentEvent += RaiseImpairment;
                 ImpairmentView.RaiseImpairmentEvent += PopupRaiseImpairment;
                  ImpairmentView.CancelImpairmentEvent += PopupCancelImpairment;
                 informationview.RaiseImpairmentEvent += InfoImpTapped;
                 var impairmentDetailsview = new CreationImpairmentPanelView { SectionView = new ImpairmentsDetailView(model) { IsEnabled = (model.FromArchieve  && !App.isDuplicate ) ? false : true } };
                 impairmentDetailsview.RaiseImpairmentEvent += impairmentDetailsTapped;
                 Xamarin.Forms.ScrollView scImpairmentDetails = new Xamarin.Forms.ScrollView { Content = impairmentDetailsview };
                 Xamarin.Forms.ScrollView scPrecautionTaken = new Xamarin.Forms.ScrollView { Content = precautionview };
                 ImpairmentDetailsview.Content = new ContentView { Content = scImpairmentDetails };
                 PrecautionTakenview.Content = new ContentView { Content = scPrecautionTaken };
                 ImpairmentDetailsview.IsVisible = false;
                 PrecautionTakenview.IsVisible = false;
                 return false;
             });
        }

      

        #region Popup Event

        // raised when clicked on precaution tab
        private void RaiseImpairment(object sender, EventArgs e)
        {
            if (App.isImpairementEditorDuplicate == "Archieve")
            {
                //var GdDuplicate = ImpairmentView.FindByName<CusGrid>("Btnduplicate");
                if (model.EditableImpairment.workFlowStatus == "CLOSED")
                {
                    ImpairmentView.GdDuplicate.IsVisible = false;
                }
                else
                {
                    ImpairmentView.GdDuplicate.IsVisible = true;
                }
            }
            GdImpairmentview.IsVisible = true;
        }

        private void InfoImpTapped(object sender, EventArgs e)
        {
            nextviewDisplay();
        }

        private void impairmentDetailsTapped(object sender, EventArgs e)
        {
            nextviewDisplay();
        }

        void nextviewDisplay()
        {
            var PreSel = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).First());
            TabView vw = (TabView)Spworktabs.Children[PreSel + 1];
            SelectionTab(vw);
        }

        //popup wala btn
        private void PopupRaiseImpairment(object sender, EventArgs e)
        {
            model.OnSubmitImpairmentCommand.Execute("");
           
            GdImpairmentview.IsVisible = false;
        }


        private void PopupCancelImpairment(object sender, EventArgs e)
        {           
            GdImpairmentview.IsVisible = false;
        }

        private void ImpairPopupback_Tapped(object sender, System.EventArgs e)
        {
            GdImpairmentview.IsVisible = false;
        }
        #endregion

        #region Tab Section
        private void TabAction(View arg1, object arg2)
        {
            if (model != null)
            {
                SelectionTab(arg1);
            }
        }

        public void SelectionTab(View obj)
        {
            try
            {
                if (obj != null)
                {
                    Deselectview();
                    TabView selv = (TabView)obj;
                    Selectview(selv);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void Deselectview()
        {
            try
            {
                var presel = TabCollection.Where(p => p.IsSelected).First();
                var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).First());
                TabView tab = (TabView)Spworktabs.Children[Selind];
                tab.TabColor = Color.Transparent;
                TabCollection.Where(p => p.IsSelected).First().IsSelected = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void Selectview(TabView v)
        {
            try
            {
                TabView selv = v;
                var seltext = selv.Title;
                selv.TabColor = Color.FromHex("#00AFAD");
                var seltab = TabCollection.Where(s => s.Title == seltext).First();
                seltab.IsSelected = true;
                var selind = TabCollection.IndexOf(TabCollection.Where(s => s.Title == seltext).First());
                switch (selind)
                {
                    case 0:
                        Informationview.IsVisible = true;
                        ImpairmentDetailsview.IsVisible = false;
                        model.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                        model.IsPreviousBtnlabel = false;
                        PrecautionTakenview.IsVisible = false;
                        var Ftab = (TabView)Spworktabs.Children[0];
                        hotworksScroll.ScrollToAsync(Ftab, ScrollToPosition.Start, true);
                        break;
                    case 1:
                        Informationview.IsVisible = false;
                        ImpairmentDetailsview.IsVisible = true;
                        model.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                        model.IsPreviousBtnlabel = true;
                        var pr = (TabView)Spworktabs.Children[1];
                        hotworksScroll.ScrollToAsync(pr, ScrollToPosition.Start, true);
                        PrecautionTakenview.IsVisible = false;
                        break;
                    case 2:
                        Informationview.IsVisible = false;
                        ImpairmentDetailsview.IsVisible = false;
                        model.ContinueBtnlabel = AppResource.CREATE_IMP;
                        model.IsPreviousBtnlabel = true;
                        var Ltab = (TabView)Spworktabs.Children[2];
                        hotworksScroll.ScrollToAsync(Ltab, ScrollToPosition.Start, true);
                        PrecautionTakenview.IsVisible = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }
        #endregion


        private void changeTab(string HWstatus)
        {
            var s = ((NewImpairmentsPageViewModel)BindingContext);
            var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).FirstOrDefault());
            if (HWstatus.Equals("Previous"))
            {
                if (Selind - 1 > 0)
                {
                    s.IsPreviousBtnlabel = true;
                 //   s.IsContinueBtnlabel = true;
                    s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                }
                else
                {
                    s.IsPreviousBtnlabel = false;
                 //   s.IsContinueBtnlabel = true;
                }
                TabView tab = (TabView)Spworktabs.Children[Selind - 1];

                SelectionTab(tab);
            }
            else
            {
                if (Selind + 1 == 4)
                {
                  //  s.ShowSubmitHWPoopup = true;
                }
                else
                {
                    TabView tab = (TabView)Spworktabs.Children[Selind + 1];
                    SelectionTab(tab);

                    if (Selind + 1 == 3)
                    {
                    //    s.IsContinueBtnlabel = true;
                        s.ContinueBtnlabel = AppResource.PRINT_AND_SIGN_LABEL;
                        s.IsPreviousBtnlabel = true;
                    }
                    else
                    {
                        s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                    //    s.IsContinueBtnlabel = true;
                        s.IsPreviousBtnlabel = true;
                    }
                }
            }
        }

    }
}
