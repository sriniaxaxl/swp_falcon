﻿using System;
namespace SiteWorkPermits
{
    public class HotWorkApiRequest
    {
        public string HotWorkId { get; set; }
        public string ToEmailAddress { get; set; }
        public string PaperSize { get; set; }
        public double UtcOffsetInMinute { get; set; }
    }
}
