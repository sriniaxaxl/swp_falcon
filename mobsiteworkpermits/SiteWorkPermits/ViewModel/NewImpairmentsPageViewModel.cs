﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.Model;
using CustomControl;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms.Internals;
using System.Threading.Tasks;
using Xamarin.Forms;
using Microsoft.EntityFrameworkCore;
using SiteWorkPermits.Platform;
using Prism.Common;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using Plugin.Connectivity;
using SiteWorkPermits.DAL.sqlIntegration;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLite;
using SiteWorkPermits.Resx;
using SQLiteNetExtensions.Extensions;

namespace SiteWorkPermits.ViewModel
{
    public class NewImpairmentsPageViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        public Impairment_Sql EditableImpairment;
        public ReporterDetail_sql EditableReportersDetail_Sql;
        private bool _isEdit = false;
        private bool _isDuplicate = false;
        private bool _isDelete = false;
        IReporterDetailService _reporterDetailService;
        IImpairmentService _impairmentService;
        IImpairmentTypeService _impairmentTypeService;
        IImpairmentClassService _impairmentClassService;
        IShutDownReasonService _shutDownReasonService;
        IPrecautionTakenService _precautionTakenService;
        IIncUniqueid _incUniqueid;
        IMajorImpairmentsService _majorImpairmentsService;
        SQLiteDatabase impairmentDatabase = new SQLiteDatabase();
        SQLiteConnection connection;
        string[] Impairtypearray = new string[] { AppResource.IMP_TYPE_1_PLACEHOLDER,
                    AppResource.IMP_TYPE_2_PLACEHOLDER,
                AppResource.IMP_TYPE_3_PLACEHOLDER,
                AppResource.IMP_TYPE_4_PLACEHOLDER,
                AppResource.IMP_TYPE_5_PLACEHOLDER };
        string[] Impairclassarray = new string[] { AppResource.IMP_CLASS_1_PLACEHOLDER,
        AppResource.IMP_CLASS_2_PLACEHOLDER,
        AppResource.IMP_CLASS_3_PLACEHOLDER,
        AppResource.IMP_CLASS_4_PLACEHOLDER,
        AppResource.IMP_CLASS_5_PLACEHOLDER,
        AppResource.IMP_CLASS_6_PLACEHOLDER,
        AppResource.IMP_CLASS_7_PLACEHOLDER,
        AppResource.IMP_CLASS_8_PLACEHOLDER};
        string[] ReasonShutarray = new string[] { AppResource.IMP_SHUTDOWN_REASON_1_PLACEHOLDER ,
        AppResource.IMP_SHUTDOWN_REASON_2_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_3_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_4_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_5_PLACEHOLDER,
        AppResource.IMP_SHUTDOWN_REASON_6_PLACEHOLDER};
        List<string> MajorImpairmnets = new List<string>();
        List<string> MajorPrecation = new List<string>();
        public ObservableCollection<Dropitem> ImpairtypeCollection = new ObservableCollection<Dropitem>();
        public ObservableCollection<Dropitem> ImpairClassCollection = new ObservableCollection<Dropitem>();
        public ObservableCollection<Dropitem> ReasonShutdownCollection = new ObservableCollection<Dropitem>();
        public Dropitem SelImpairmenType = new Dropitem();
        public Dropitem SelImpairmenClass = new Dropitem();
        public Dropitem SelReasonForShutDown = new Dropitem();

        public ObservableCollection<Multiitem> MajorImpairmentCollection = new ObservableCollection<Multiitem>();
        public ObservableCollection<Multiitem> PrecationTakenCollection = new ObservableCollection<Multiitem>();


        public DelegateCommand<string> OnSubmitImpairmentCommand { get; set; }

        public NewImpairmentsPageViewModel(INavigationService navigationService,
                                            IPageDialogService pageDialog,
                                           IReporterDetailService reporterDetailService,
                                           IImpairmentService impairmentService,
                                           IIncUniqueid incUniqueid,
                                           IImpairmentTypeService impairmentTypeService,
                                           IImpairmentClassService impairmentClassService,
                                           IShutDownReasonService shutDownReasonService,
                                           IMajorImpairmentsService majorImpairmentsService,
                                           IPrecautionTakenService precautionTakenService)
        {
            _navigationService = navigationService;
            _incUniqueid = incUniqueid;
            _pageDialog = pageDialog;
            ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
            _reporterDetailService = reporterDetailService;
            _impairmentService = impairmentService;
            _impairmentTypeService = impairmentTypeService;
            _impairmentClassService = impairmentClassService;
            _shutDownReasonService = shutDownReasonService;
            _precautionTakenService = precautionTakenService;
            _majorImpairmentsService = majorImpairmentsService;
            ShowSyncIcon = false;

            connection = impairmentDatabase.GetdbConnection();
            OnPreviousCommand = new DelegateCommand<string>(PreviousTab);
            //Impairtypearray = connection.Table<ImpairmentType_Sql>().ToList().Select(p => p.TypeDescription).ToArray();
           // Impairclassarray = connection.Table<ImpairmentClassType_Sql>().ToList().Select(p => p.ClassDescription).ToArray();
           // ReasonShutarray = connection.Table<ShutDownReason_Sql>().ToList().Select(p => p.ClassDescription).ToArray();


            //Impairtypearray = { AppResource.IMP_TYPE_1_PLACEHOLDER,
            //        AppResource.IMP_TYPE_2_PLACEHOLDER,
            //    AppResource.IMP_TYPE_3_PLACEHOLDER,
            //    AppResource.IMP_TYPE_4_PLACEHOLDER,
            //    AppResource.IMP_TYPE_5_PLACEHOLDER};
            //Impairclassarray = [];
            //ReasonShutarray = [];


            ShowSubmitbtn = true;
            connection = impairmentDatabase.GetdbConnection();
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("CREATE IMPAIRMENT");

            ShowImpDraftBtn = false;
            ShowImpUpdateBtn = false;

            ImpairList = new ObservableCollection<string>(Impairtypearray.ToList());
            ImpairClassList = new ObservableCollection<string>(Impairclassarray.ToList());
            ResonShutdownList = new ObservableCollection<string>(ReasonShutarray.ToList());
            ImpairtypeCollection = new ObservableCollection<Dropitem>(from data in Impairtypearray
                                                                      select new Dropitem
                                                                      {
                                                                          Name = data,
                                                                          Index = Array.IndexOf(Impairtypearray, data) + 1
                                                                      });
            ImpairClassCollection = new ObservableCollection<Dropitem>(from data in Impairclassarray
                                                                       select new Dropitem
                                                                       {
                                                                           Name = data,
                                                                           Index = Array.IndexOf(Impairclassarray, data) + 1
                                                                       });
            ReasonShutdownCollection = new ObservableCollection<Dropitem>(from data in ReasonShutarray
                                                                          select new Dropitem
                                                                          {
                                                                              Name = data,
                                                                              Index = Array.IndexOf(ReasonShutarray, data) + 1
                                                                          });
            OnSubmitImpairmentCommand = new DelegateCommand<string>(SubmitHotwork);

            MajorImpairmnets.Add(AppResource.IMP_MAJOR_IMP_1);
            MajorImpairmnets.Add(AppResource.IMP_MAJOR_IMP_2);
            MajorImpairmnets.Add(AppResource.IMP_MAJOR_IMP_3);
            MajorImpairmnets.Add(AppResource.IMP_MAJOR_IMP_4);

            MajorPrecation.Add(AppResource.IMP_PRECAUTION_1);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_2);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_3);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_4);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_5);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_6);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_7);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_8);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_9);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_10);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_11);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_12);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_13);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_14);
            MajorPrecation.Add(AppResource.IMP_PRECAUTION_15);


            //MajorPrecation.Add("Use Shutoff Tags");
            //MajorPrecation.Add("Discontinue Welding, Cutting, Hot Work");
            //MajorPrecation.Add("Notify Department Head");
            //MajorPrecation.Add("Discontinue Smoking in Area");
            //MajorPrecation.Add("Cease Hazardous Operations");
            //MajorPrecation.Add("Notify Fire Department");
            //MajorPrecation.Add("Charged Hose Lines and Extinguishers");
            //MajorPrecation.Add("Watchman Surveillance");
            //MajorPrecation.Add("Notify Alarm Company");
            //MajorPrecation.Add("Notify Site Emergency Response/Fire Team");
            //MajorPrecation.Add("Work to be Continuous");
            //MajorPrecation.Add("Pipe Plugs/Caps/Etc. Available");
            //MajorPrecation.Add("Emergency Connection Planned");
            //MajorPrecation.Add("Active Smoke/heat detection");
            //MajorPrecation.Add("Other (Explain)");
            //  MajorPrecation.Add("Discontinue Welding, Cutting, Hot Work");

            MajorImpairmentCollection = new ObservableCollection<Multiitem>(from Data in MajorImpairmnets
                                                                            select new Multiitem
                                                                            {
                                                                                Name = Data,
                                                                                Index = Array.IndexOf(MajorImpairmnets.ToArray(), Data) + 1,
                                                                                IsChecked = false
                                                                            });

            PrecationTakenCollection = new ObservableCollection<Multiitem>(from Data in MajorPrecation
                                                                           select new Multiitem
                                                                           {
                                                                               Name = Data,
                                                                               Index = Array.IndexOf(MajorPrecation.ToArray(), Data) + 1,
                                                                               IsChecked = false
                                                                           });
            StartImpairment = DateTime.Now;
            RestorationImpairment = DateTime.Now.AddHours(1);
        }

        public delegate void ChangeTabAction(string HWstatus);
        public event ChangeTabAction ChangeTabEvent;
      //  public event ChangeTabAction ChangeTabEvent;
        private void PreviousTab(String ss)
        {
            ChangeTabEvent?.Invoke("Previous");
        }


        private async void UpdateImpairement(string updateType)
        {
            if (EditableImpairment != null)
            {

                var isDataValide = ImpairmentUtility.IsImpairmentMandatoryDataValid(
                   ReportName, ReportEmail, ReportPhoneNumber, DescribeImpairment,
                   StartImpairment.Date, RestorationImpairment.Date,
                   SelImpairmenType, SelImpairmenClass, SelReasonForShutDown
                   );

                if (!isDataValide.Key)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync(string.Empty, isDataValide.Value, AppResource.OK_LABEL);
                    return;
                }            

                if(PrecationTakenCollection.Where(p => p.IsChecked == true).Count()==0)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.SELECT_PRECAUTIONS, AppResource.OK_LABEL);
                    return;
                }



                //var reporterDetails = ImpairmentUtility.GetReporterDetails
                //    (ReportName, ReportEmail, ReportPhoneNumber);
               EditableReportersDetail_Sql.ReporterName = ReportName;
                EditableReportersDetail_Sql.ReporterEmail = ReportEmail;
                EditableReportersDetail_Sql.ReporterPhone = ReportPhoneNumber;
               
                    connection.Update(EditableReportersDetail_Sql);
                

                EditableImpairment.isEdited = true;
                EditableImpairment.isParent = true;
                EditableImpairment.parentImpairmentId = 0;
                EditableImpairment.isDeleted = false;
                EditableImpairment.impairmentTypeId = SelImpairmenType.Index;
                EditableImpairment.impairmentClassId = SelImpairmenClass.Index;
                EditableImpairment.shutDownReasonId = SelReasonForShutDown.Index;
                EditableImpairment.startDateTime = StartImpairment.ToString();
                EditableImpairment.endDateTime = RestorationImpairment.ToString();
                EditableImpairment.reporterDetail_Sql = EditableImpairment.reporterDetail_Sql;
                EditableImpairment.impairmentDescription = DescribeImpairment;

                if (_isDelete)
                {
                    EditableImpairment.workFlowStatus = "DRAFT";
                    EditableImpairment.isDeleted = true;
                }
                else
                {
                    if (string.IsNullOrEmpty(updateType) && EditableImpairment.workFlowStatus =="DRAFT")
                    {
                        EditableImpairment.workFlowStatus = "DRAFT";
                    }
                    else
                    {

                        if (StartImpairment > DateTime.Now)
                        {
                            EditableImpairment.workFlowStatus = "UPCOMING";
                        }
                        else if (StartImpairment.Ticks <= DateTime.Now.Ticks && RestorationImpairment.Ticks >= DateTime.Now.Ticks)
                        {
                            EditableImpairment.workFlowStatus = "ACTIVE";
                        }
                    }
                }

                var majorImpairmentMaster_Sqls = new List<MajorImpairmentMaster_sql>();

                foreach (var majorImpairment in MajorImpairmentCollection.ToList().Where(p => p.IsChecked == true).ToList())
                {
                    majorImpairmentMaster_Sqls.Add(connection.Table<MajorImpairmentMaster_sql>().Where(p => p.id == majorImpairment.Index).FirstOrDefault());
                }

                var impairment_PrecautionMaster_Sqls = new List<Impairment_PrecautionMaster_sql>();


                foreach (var precautions in PrecationTakenCollection.Where(p => p.IsChecked == true).ToList())
                {
                    string otherDesc = string.Empty;
                    if (precautions.Index == 15)
                    {
                        EditableImpairment.precautionOtherDescription = PrecationExplain;
                    }
                    else
                    {
                        EditableImpairment.precautionOtherDescription = otherDesc;
                    }

                    impairment_PrecautionMaster_Sqls.Add(connection.Table<Impairment_PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.Index).FirstOrDefault());
                }


                if (PrecationTakenCollection.Where(p => p.Index == 15).FirstOrDefault().IsChecked)
                {

                    if (string.IsNullOrEmpty(PrecationExplain))
                    {
                        await _pageDialog.DisplayAlertAsync("", AppResource.PRECAUTIONS_OTHER_EXPLAIN, AppResource.OK_LABEL);
                        return;
                    }
                }

                EditableImpairment.majorImpairmentMaster_sqls.ToList().Clear();
                EditableImpairment.impairment_PrecautionMaster_Sqls.ToList().Clear();

                

                EditableImpairment.majorImpairmentMaster_sqls = majorImpairmentMaster_Sqls;
                EditableImpairment.impairment_PrecautionMaster_Sqls = impairment_PrecautionMaster_Sqls;

                try
                {
                    connection.UpdateWithChildren(EditableImpairment);

                    await Task.Yield();
                    if (_isDelete)
                    {

                        await _pageDialog.DisplayAlertAsync("", AppResource.IMPAIRMENT_DELETE_SUCCESS, AppResource.OK_LABEL);
                        await _navigationService.NavigateAsync("HomePage", null, null, false);
                    }
                    else
                    {
                        App.PrintEditableImpId = EditableImpairment.id;
                        await _pageDialog.DisplayAlertAsync("", AppResource.IMPAIRMENT_UPDATE_SUCCESS, AppResource.OK_LABEL);

                        if (sentToPrint)
                        {
                            sentToPrint = false;
                            MessagingCenter.Send(Xamarin.Forms.Application.Current, "NavigateToPrintImp");
                        }
                        else
                        {
                            await _navigationService.NavigateAsync("HomePage", null, null, false);
                        }
                    }
                }
                catch (Exception ex) {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.IMPAIRMENT_NOT_RAISED, AppResource.OK_LABEL);

                }
            }
        }

        private bool sentToPrint = false;

        private async void SubmitHotwork(string hotworksStatus)
        {

            if (_isEdit)
            {

                if (hotworksStatus == "")
                {
                    sentToPrint = true;
                }

                if (hotworksStatus.Equals("delete"))
                {
                    _isDelete = true;
                    UpdateImpairement("");
                    return;
                }

                if (hotworksStatus == "update")
                {
                    UpdateImpairement("");
                    return;
                }
                else
                {
                    UpdateImpairement("DRAFT_TO_WORKFLOW");
                    return;
                }
            }

            try
            {
                ShowImpDraftBtn = true;
                ShowImpUpdateBtn = false;

                var curpage = (NewImpairmentPage)PageUtilities.GetCurrentPage(Xamarin.Forms.Application.Current.MainPage);
                StackLayout TabPanel = curpage.FindByName<StackLayout>("Spworktabs");
                TabView tab;


                var isDataValide = ImpairmentUtility.IsImpairmentMandatoryDataValid(
                    ReportName, ReportEmail, ReportPhoneNumber, DescribeImpairment,
                    StartImpairment.Date, RestorationImpairment.Date,
                    SelImpairmenType, SelImpairmenClass, SelReasonForShutDown
                    );

                if (!isDataValide.Key)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync(string.Empty, isDataValide.Value, AppResource.OK_LABEL);
                    return;
                }


                if (PrecationTakenCollection.Where(p => p.IsChecked == true).Count() == 0)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.SELECT_PRECAUTIONS, AppResource.OK_LABEL);
                    tab = (TabView)TabPanel.Children[2];
                    curpage.SelectionTab(tab);
                    return;
                }
                
                var reporterDetails = ImpairmentUtility.GetReporterDetails( ReportName, ReportEmail, ReportPhoneNumber);
                Impairment_Sql impairmentSql = new Impairment_Sql();
                if (reporterDetails != null)
                {
                    connection.Insert(reporterDetails);
                    impairmentSql = ImpairmentUtility.GetImpairment( reporterDetails,
                       SelImpairmenType.Index, SelImpairmenClass.Index, SelReasonForShutDown.Index,
                       StartImpairment, RestorationImpairment, DescribeImpairment, hotworksStatus, 0);
                }
                try
                {
                    if (impairmentSql != null)
                    {
                        var majorImpairmentMaster_Sqls = new List<MajorImpairmentMaster_sql>();

                        foreach (var majorImpairment in MajorImpairmentCollection.ToList().Where(p => p.IsChecked == true).ToList())
                        {
                            majorImpairmentMaster_Sqls.Add(connection.Table<MajorImpairmentMaster_sql>().Where(p => p.id == majorImpairment.Index).FirstOrDefault());
                        }

                        var impairment_PrecautionMaster_Sqls = new List<Impairment_PrecautionMaster_sql>();


                        foreach (var precautions in PrecationTakenCollection.Where(p => p.IsChecked == true).ToList())
                        {
                            string otherDesc = string.Empty;
                            if (precautions.Index == 15)
                            {
                                impairmentSql.precautionOtherDescription = PrecationExplain;
                            }
                            else
                            {
                                impairmentSql.precautionOtherDescription = otherDesc;
                            }

                            impairment_PrecautionMaster_Sqls.Add(connection.Table<Impairment_PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.Index).FirstOrDefault());
                        }

                        if (PrecationTakenCollection.Where(p => p.Index == 15).FirstOrDefault().IsChecked)
                        {

                            if (string.IsNullOrEmpty(PrecationExplain))
                            {
                                await _pageDialog.DisplayAlertAsync(string.Empty,AppResource.PRECAUTIONS_OTHER_EXPLAIN, AppResource.OK_LABEL);
                                return;
                            }
                        }

                        connection.Insert(impairmentSql);
                        impairmentSql.majorImpairmentMaster_sqls = majorImpairmentMaster_Sqls;
                        impairmentSql.impairment_PrecautionMaster_Sqls = impairment_PrecautionMaster_Sqls;
                        connection.UpdateWithChildren(impairmentSql);

                        await Task.Yield();
                        App.PrintEditableImpId = impairmentSql.id;
                        if (CrossConnectivity.Current.IsConnected)
                        {
                            await _pageDialog.DisplayAlertAsync(string.Empty, AppResource.IMPAIRMENT_CREATE_SUCCESS, AppResource.OK_LABEL);
                        }
                        else
                        {
                            await _pageDialog.DisplayAlertAsync(string.Empty, CustomControl.Validation.IMPAIRMENT_SAVED_OFFLINE, AppResource.OK_LABEL);
                        }

                        if (hotworksStatus.Equals("Draft"))
                        {
                            await _navigationService.NavigateAsync("HomePage", null, null, false);
                        }
                        else
                        {
                            MessagingCenter.Send(Xamarin.Forms.Application.Current, "NavigateToPrintImp");
                        }

                    }
                }
                catch (Exception ex)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.IMPAIRMENT_NOT_RAISED, AppResource.OK_LABEL);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("IOException source: {0}", e.Source);
            }
        }

        public DelegateCommand<string> OnPreviousCommand { get; set; }
        #region Properties

        private string _PrecationExplain;
        public string PrecationExplain
        {
            get { return _PrecationExplain; }
            set { SetProperty(ref _PrecationExplain, value); }
        }
        
        private string _continueBtnlabel;
        public string ContinueBtnlabel
        {
            get { return _continueBtnlabel; }
            set { SetProperty(ref _continueBtnlabel, value); }
        }

        private bool _isPreviousBtnlabel;
        public bool IsPreviousBtnlabel
        {
            get { return _isPreviousBtnlabel; }
            set { SetProperty(ref _isPreviousBtnlabel, value); }
        }

        private string _ReportName;
        public string ReportName
        {
            get { return _ReportName; }
            set { SetProperty(ref _ReportName, value); }
        }


        private string _ReportEmail;
        public string ReportEmail
        {
            get { return _ReportEmail; }
            set { SetProperty(ref _ReportEmail, value); }
        }

        private string _ReportPhoneNumber;
        public string ReportPhoneNumber
        {
            get { return CustomControl.Validation.ToCulturePhone(_ReportPhoneNumber); }
            set { SetProperty(ref _ReportPhoneNumber, CustomControl.Validation.ToCulturePhone(value)); }
        }


        private string _DescribeImpairment;
        public string DescribeImpairment
        {
            get { return _DescribeImpairment; }
            set { SetProperty(ref _DescribeImpairment, value); }
        }


        private ObservableCollection<string> _ImpairList;
        public ObservableCollection<string> ImpairList
        {
            get { return _ImpairList; }
            set { SetProperty(ref _ImpairList, value); }
        }


        private ObservableCollection<string> _ImpairClassList;
        public ObservableCollection<string> ImpairClassList
        {
            get { return _ImpairClassList; }
            set { SetProperty(ref _ImpairClassList, value); }
        }

        private ObservableCollection<string> _ResonShutdownList;
        public ObservableCollection<string> ResonShutdownList
        {
            get { return _ResonShutdownList; }
            set { SetProperty(ref _ResonShutdownList, value); }
        }


        private DateTime _StartImpairment;
        public DateTime StartImpairment
        {
            get { return _StartImpairment; }
            set { SetProperty(ref _StartImpairment, value); }
        }

        private bool _showImpUpdateBtn;
        public bool ShowImpUpdateBtn
        {
            get { return _showImpUpdateBtn; }
            set { SetProperty(ref _showImpUpdateBtn, value); }
        }

        private bool _showImpDraftBtn;
        public bool ShowImpDraftBtn
        {
            get { return _showImpDraftBtn; }
            set { SetProperty(ref _showImpDraftBtn, value); }
        }


        private bool _showImpDeleteBtn;
        public bool ShowImpDeleteBtn
        {
            get { return _showImpDeleteBtn; }
            set { SetProperty(ref _showImpDeleteBtn, value); }
        }


        private DateTime _RestorationImpairment;
        public DateTime RestorationImpairment
        {
            get { return _RestorationImpairment; }
            set { SetProperty(ref _RestorationImpairment, value); }
        }
        #endregion

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }


        private bool _FromArchieve = false;
        public bool FromArchieve
        {
            get { return _FromArchieve; }
            set { SetProperty(ref _FromArchieve, value); }
        }

        private bool _showSubmitbtn;
        public bool ShowSubmitbtn
        {
            get { return _showSubmitbtn; }
            set { SetProperty(ref _showSubmitbtn, value); }
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            try
            {
                if (parameters.Count > 0)
                {
                    if (App.isImpairementEditorDuplicate == "Edit")
                    {
                        ShowImpDraftBtn = false;
                        ShowImpUpdateBtn = true;
                        this._isEdit = true;
                        this._isDuplicate = false;
                        EditableImpairment = new Impairment_Sql();
                        EditableImpairment = (Impairment_Sql)parameters["impairmentModel"];
                        if (EditableImpairment.workFlowStatus == "DRAFT")
                        {
                            ShowImpDeleteBtn = true;
                        }
                        else
                        {
                            ShowImpDeleteBtn = false;
                        }
                    }
                    else if (App.isImpairementEditorDuplicate == "Duplicate")
                    {
                        ShowImpDeleteBtn = false;
                        ShowImpDraftBtn = true;
                        ShowImpUpdateBtn = false;
                        this._isEdit = false;
                        this._isDuplicate = true;
                        EditableImpairment = (Impairment_Sql)parameters["impairmentModel"];
                    }
                    else
                    {
                        if (parameters.Where(s => s.Key == "impairmentArchieveModel").Count() > 0)
                        {
                            EditableImpairment = (Impairment_Sql)parameters["impairmentArchieveModel"];
                        }
                        else
                        {
                            EditableImpairment = (Impairment_Sql)parameters["impairmentModel"];
                        }

                        if (App.isDuplicate)
                        {
                            ShowImpDeleteBtn = false;
                            ShowImpDraftBtn = true;
                            ShowImpUpdateBtn = false;
                            this._isEdit = false;
                            this._isDuplicate = true;

                        }
                        else
                        {
                            ShowImpDeleteBtn = false;
                            ShowImpDraftBtn = false;
                            ShowImpUpdateBtn = false;
                            this._isEdit = false;
                            this._isDuplicate = false;
                        }
                        FromArchieve = true;
                    }

                    if (FromArchieve && !App.isDuplicate)
                    {
                        ShowSubmitbtn = false;
                    }
                    else
                    {
                        ShowSubmitbtn = true;
                    }
                    var ss = connection;


                    if (EditableImpairment.reporterDetail_sql_id == 0) {

                        EditableReportersDetail_Sql = EditableImpairment.reporterDetail_Sql;
                    } else {
                        try
                        {
                            EditableReportersDetail_Sql = connection.GetWithChildren<ReporterDetail_sql>(EditableImpairment.reporterDetail_sql_id, recursive: true);
                        }
                        catch (Exception ex)
                        {
                            EditableReportersDetail_Sql = connection.GetWithChildren<ReporterDetail_sql>(EditableImpairment.reporterDetail_Sql.id, recursive: true);
                        }
                    }


                 

                   // EditableReportersDetail_Sql = connection.GetWithChildren<ReporterDetail_sql>(EditableImpairment.reporterDetail_sql_id, recursive: true);

                 //   var reportDetail = EditableImpairment.reporterDetail_Sql;
                    ReportName = EditableReportersDetail_Sql.ReporterName;
                    ReportEmail = EditableReportersDetail_Sql.ReporterEmail;
                    ReportPhoneNumber = EditableReportersDetail_Sql.ReporterPhone;
                    SelImpairmenType = ImpairtypeCollection.Where(s => s.Index == EditableImpairment.impairmentTypeId).FirstOrDefault();
                    SelImpairmenClass = ImpairClassCollection.Where(s => s.Index == EditableImpairment.impairmentClassId).FirstOrDefault();
                    SelReasonForShutDown = ReasonShutdownCollection.Where(s => s.Index == EditableImpairment.shutDownReasonId).FirstOrDefault();
                    if (App.isDuplicate)
                    {
                        StartImpairment = DateTime.Now;
                        RestorationImpairment = DateTime.Now.AddDays(1);
                    }
                    else
                    {
                        StartImpairment = Convert.ToDateTime(EditableImpairment.startDateTime);
                        RestorationImpairment = Convert.ToDateTime(EditableImpairment.endDateTime);
                    }
                    //    MajorImpairmentCollection = new ObservableCollection<Multiitem>(MajorImpairmentCollection.Where((cat, ind) => ind % 2 != 0));
                    //PrecationTakenCollection = new ObservableCollection<Multiitem>(PrecationTakenCollection.Where((cat, ind) => ind % 2 != 0));
                    DescribeImpairment = EditableImpairment.impairmentDescription;

                    MajorImpairmentCollection.ToList().ForEach(s =>
                    {
                        var pr = EditableImpairment.majorImpairmentMaster_sqls.Select(p => p.id).ToList();
                        s.IsChecked = pr.Where(p => p.Equals(s.Index)).Count() > 0 ? true : false;
                    });

                    PrecationTakenCollection.ToList().ForEach(s =>
                    {
                        var pr = EditableImpairment.impairment_PrecautionMaster_Sqls.Select(p => p.id).ToList();
                        s.IsChecked = pr.Where(p => p.Equals(s.Index)).Count() > 0 ? true : false;
                    });
                    PrecationExplain = EditableImpairment.precautionOtherDescription;

                    // PrecationExplain = "";
                }
                else
                {
                    ShowImpDeleteBtn = false;
                    ShowImpDraftBtn = true;
                    ShowImpUpdateBtn = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        public class Dropitem
        {
            public string Name { get; set; }

            public int Index { get; set; }
        }

        public class Multiitem
        {
            public string Name { get; set; }
            public int Index { get; set; }
            public bool IsChecked { get; set; }
        }
    }
}

