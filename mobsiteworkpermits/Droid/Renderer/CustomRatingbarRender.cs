using System;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using CustomControl;
using System.ComponentModel;
using Android.Renderer;

[assembly: ExportRenderer(typeof(CustomRatingBar), typeof(CustomRatingbarRender))]
namespace Android.Renderer
{
    public class CustomRatingbarRender : ViewRenderer<CustomRatingBar, RatingBar>
    {
        RatingBar Cusrating;
        CustomRatingBar CusFormrating;
        protected override void OnElementChanged(ElementChangedEventArgs<CustomRatingBar> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            Cusrating = new RatingBar(Context);
            if (Control == null)
            {
                CusFormrating = Element as CustomRatingBar;
                Cusrating.NumStars = Convert.ToInt32(CusFormrating.Maximum);
                Cusrating.StepSize = 1.0f;
                Cusrating.RatingBarChange += Cusrating_RatingBarChange;                
                SetNativeControl(Cusrating);
            }
        }

        private void Cusrating_RatingBarChange(object sender, RatingBar.RatingBarChangeEventArgs e)
        {
            CusFormrating.Value = Convert.ToDouble(e.Rating);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
    }
}