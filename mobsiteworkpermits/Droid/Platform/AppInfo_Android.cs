﻿using System;
using Android.Content.PM;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Platform;

[assembly: Xamarin.Forms.Dependency(typeof(AppInfo_Android))]
namespace SiteWorkPermits.Droid.Platform
{
    public class AppInfo_Android : IAppInfo
    {
        public string GetVersion()
        {
            var context = global::Android.App.Application.Context;

            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionName;
        }

        public int GetBuild()
        {
            var context = global::Android.App.Application.Context;
            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionCode;
        }
    }
}