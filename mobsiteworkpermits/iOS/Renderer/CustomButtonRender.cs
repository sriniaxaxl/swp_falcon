﻿using System;
using System.ComponentModel;
using CustomControl;
using CustomControl.iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusButton), typeof(CustomButtonRender))]
namespace CustomControl.iOS.Renderer
{
    public class CustomButtonRender : ButtonRenderer
    {
        CusButton FormBtn;
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            FormBtn = Element as CusButton;
            SetPadding();
            if (FormBtn.PressedBackgroundColor != Color.Default)
            {
                Control.TouchUpInside += Control_TouchUpInside;
                Control.TouchDown += Control_TouchDown;
            }
        }

        void Control_TouchDown(object sender, EventArgs e)
        {          
            if (FormBtn.PressedBackgroundColor != Color.Default)
            {
                Control.BackgroundColor = FormBtn.PressedBackgroundColor.ToUIColor();
            }
        }

        void Control_TouchUpInside(object sender, EventArgs e)
        {           
            if (FormBtn.PressedBackgroundColor != Color.Default)
            {
                Control.BackgroundColor = FormBtn.BackgroundColor.ToUIColor();
            }        
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;            
            FormBtn = Element as CusButton;
            if (e.PropertyName == CusButton.PaddingProperty.PropertyName)
            {
                SetPadding();
            }
        }

        void SetPadding()
        {
            var lpadding =(nfloat) FormBtn.Padding.Left;
            var rpadding = (nfloat)FormBtn.Padding.Right;
            var tpadding =(nfloat) FormBtn.Padding.Top;
            var bpadding =(nfloat) FormBtn.Padding.Bottom;
            Control.ContentEdgeInsets = new UIEdgeInsets(tpadding, lpadding,bpadding, rpadding);
        }
    }
}
