﻿using System;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using SiteWorkPermits.Platform;
using Xamarin.Forms;
using System.Threading.Tasks;
using Prism.Services;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits.ViewModel
{
    public class DownloadViewModel : BindableBase, INavigatedAware
    {
        public DelegateCommand<string> OnNavigateCommand { get; set; }
        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        public DownloadViewModel(INavigationService navigationService,
                                        IPageDialogService pageDialog)
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            OnNavigateCommand = new DelegateCommand<string>(NavigateAsync);
            SelFormat = "USLETTER";
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("DOWNLOADS");
        }

        private string _selFormat;
        public string SelFormat
        {
            get { return _selFormat; }
            set { SetProperty(ref _selFormat, value); }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }
        private async void NavigateAsync(string page)
        {
            bool isDownloadedSuccess = false;
            if (page.Equals("HotWorks"))
            {
                if (SelFormat == "A4")
                {
                    isDownloadedSuccess = Xamarin.Forms.DependencyService.Get<IDBPath>().DownloadOfflinePDFPath("HotWorksPermitFormA4.pdf");
                }
                else
                {
                    isDownloadedSuccess = Xamarin.Forms.DependencyService.Get<IDBPath>().DownloadOfflinePDFPath("HotWorksPermitFormUSLetter.pdf");
                }
            }
            else if (page.Equals("Impairment"))
            {
                if (SelFormat == "A4")
                {
                    isDownloadedSuccess = Xamarin.Forms.DependencyService.Get<IDBPath>().DownloadOfflinePDFPath("ImpairmentsFormA4.pdf");
                }
                else
                {
                    isDownloadedSuccess = Xamarin.Forms.DependencyService.Get<IDBPath>().DownloadOfflinePDFPath("ImpairmentsFormUSLetter.pdf");
                }
            }

            if (isDownloadedSuccess)
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.DOWNLOAD_SUCCESS, AppResource.OK_LABEL);
                        string filePath = "";
                        if (Application.Current.Properties.ContainsKey("iOSDownloadedFile"))
                        {
                            filePath = Application.Current.Properties["iOSDownloadedFile"] as string;
                        }
                        await Xamarin.Forms.DependencyService.Get<IShare>().Show("title", "Hot Works Permit", filePath);
                        break;
                    case Device.Android:
                        await _pageDialog.DisplayAlertAsync(string.Empty, AppResource.DOWNLOAD_SUCCESS, AppResource.OK_LABEL);
                        break;
                }
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
