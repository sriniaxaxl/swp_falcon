﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusGrid : Grid
    {
        #region Constructor
        public CusGrid() : base()
        {
            this.RowSpacing = 0;
            this.ColumnSpacing = 0;            
        }
      
        #endregion       

        #region Properties   

        public static BindableProperty ShadowColorProperty = BindableProperty.Create<CusGrid, Color>(o => o.ShadowColor, Color.Transparent);

        public Color ShadowColor
        {
            get { return (Color)GetValue(ShadowColorProperty); }
            set { SetValue(ShadowColorProperty, value); }
        }

        public static BindableProperty ShadowWidthProperty = BindableProperty.Create<CusGrid, float>(o => o.BorderWidth, 0);

        public float ShadowWidth
        {
            get { return (float)GetValue(ShadowWidthProperty); }
            set { SetValue(ShadowWidthProperty, value); }
        }

        public static BindableProperty ShadowRadiusProperty = BindableProperty.Create<CusGrid, Thickness>(o => o.ShadowRadius, new Thickness(0));

        public Thickness ShadowRadius
        {
            get { return (Thickness)GetValue(ShadowRadiusProperty); }
            set { SetValue(ShadowRadiusProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusGrid, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusGrid, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusGrid, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusGrid, Thickness>(o => o.BorderRadius, new Thickness(5));

        public Thickness BorderRadius
        {
            get { return (Thickness)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        public static BindableProperty WidthValueProperty = BindableProperty.Create<CusGrid, int>(o => o.WidthValue, 100);

        public int WidthValue
        {
            get { return (int)GetValue(WidthValueProperty); }
            set { SetValue(WidthValueProperty, value); }
        }

        public static BindableProperty HeightValueProperty = BindableProperty.Create<CusGrid, int>(o => o.HeightValue, 50);

        public int HeightValue
        {
            get { return (int)GetValue(HeightValueProperty); }
            set { SetValue(HeightValueProperty, value); }
        }


        public static BindableProperty TagProperty = BindableProperty.Create<CusGrid, object>(o => o.Tag, string.Empty);

        public object Tag
        {
            get { return (object)GetValue(TagProperty); }
            set { SetValue(TagProperty, value); }
        }

        public static BindableProperty KMarginProperty = BindableProperty.Create<CusGrid, object>(o => o.KMargin, new Thickness());

        public Thickness KMargin
        {
            get { return (Thickness)GetValue(KMarginProperty); }
            set { SetValue(KMarginProperty, value); }
        }

        #endregion
    }
}
