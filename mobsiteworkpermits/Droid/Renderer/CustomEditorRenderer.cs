using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Text;
using System.ComponentModel;
using CustomControl;
using Android.Views.InputMethods;
using Android.Renderer;
using Android.Renderer.CommanRender;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace Android.Renderer
{
    public class CustomEditorRenderer : EditorRenderer
    {

        #region Private fields and properties

        private BorderRenderer _renderer;
        private const GravityFlags DefaultGravity = GravityFlags.CenterVertical;
        #endregion

        #region Parent override

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {

            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;

            var entryEx = Element as CustomEditor;
            Control.Gravity = GravityFlags.Top;
            Control.Hint = entryEx.Placeholder;
            Control.SetHintTextColor(entryEx.PlaceholderColor.ToAndroid());
            if (entryEx.IsMultiline == true)
            {
                Control.InputType = InputTypes.TextFlagMultiLine;
                Control.SetSingleLine(false);                
            }            
            UpdateBackground(entryEx);
            UpdatePadding(entryEx);

            //UpdateTextAlighnment(entryEx);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            var entryEx = Element as CustomEditor;
            if (e.PropertyName == CustomEditor.BorderWidthProperty.PropertyName ||
                e.PropertyName == CustomEditor.BorderColorProperty.PropertyName ||
                e.PropertyName == CustomEditor.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CustomEditor.BackgroundColorProperty.PropertyName)
            {
                UpdateBackground(entryEx);
            }
            else if (e.PropertyName == CustomEditor.PaddingProperty.PropertyName)
            {
                UpdatePadding(entryEx);
            }
            else if (e.PropertyName == Entry.HorizontalTextAlignmentProperty.PropertyName)
            {
                UpdateTextAlighnment(entryEx);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (_renderer != null)
                {
                    _renderer.Dispose();
                    _renderer = null;
                }
            }
        }

        #endregion

        #region Utility methods

        private void UpdateBackground(CustomEditor entryEx)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }
            _renderer = new BorderRenderer();
            if (entryEx.InnerBackground != null)
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.InnerBackground, entryEx.BorderWidth, entryEx.BorderRadius);
            }
            else
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.BackgroundColor, entryEx.BorderWidth, entryEx.BorderRadius);
            }

        }

        private void UpdatePadding(CustomEditor entryEx)
        {
            Control.SetPadding((int)Forms.Context.ToPixels(entryEx.Padding.Left), (int)Forms.Context.ToPixels(entryEx.Padding.Top), (int)Forms.Context.ToPixels(entryEx.Padding.Right), (int)Forms.Context.ToPixels(entryEx.Padding.Bottom));
        }

        private void UpdateTextAlighnment(CustomEditor entryEx)
        {
            var gravity = GravityFlags.Top | GravityFlags.Start;
            //switch (entryEx.HorizontalTextAlignment)
            //{
            //    case Xamarin.Forms.TextAlignment.Start:
            //        gravity |= GravityFlags.Start;
            //        break;
            //    case Xamarin.Forms.TextAlignment.Center:
            //        gravity |= GravityFlags.CenterHorizontal;
            //        break;
            //    case Xamarin.Forms.TextAlignment.End:
            //        gravity |= GravityFlags.End;
            //        break;
            //}
            //switch (entryEx.VerticaltextAlignment)
            //{
            //    case Xamarin.Forms.TextAlignment.Start:
            //        gravity |= GravityFlags.Top;
            //        break;
            //    case Xamarin.Forms.TextAlignment.Center:
            //        gravity |= GravityFlags.CenterVertical;
            //        break;
            //    case Xamarin.Forms.TextAlignment.End:
            //        gravity |= GravityFlags.Bottom;
            //        break;
            //}

            Control.Gravity = gravity;
        }

        #endregion
    }
}