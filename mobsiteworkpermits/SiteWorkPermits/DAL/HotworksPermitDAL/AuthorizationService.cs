﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class AuthorizationService:IAuthorizationService
    {
        private readonly DatabaseContext _databaseContext;
        public AuthorizationService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public  bool AddAuthorizationsAsync(Authorizations authorizations)
        {
            try
            {
                var authorization =  _databaseContext.Authorizations.Add(authorizations);
                 _databaseContext.SaveChanges();
                var isAdded = authorization.State == EntityState.Unchanged;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public  IEnumerable<Authorizations> GetAuthorizationsAsync()
        {
            try
            {
                var authorizations =  _databaseContext.Authorizations.ToList();
                return authorizations;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Authorizations GetAuthorizationsByIdAsync(int id)
        {
            try
            {
                var scope = _databaseContext.Authorizations.Find(id);
                return scope;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool RemoveAuthorizationsAsync(int id)
        {
            throw new NotImplementedException();
        }

        public  bool UpdateAuthorizationsAsync(Authorizations authorizations)
        {
            try
            {
                var scopeStatus = _databaseContext.Authorizations.Update(authorizations);
                 _databaseContext.SaveChanges();
                var isUpdated = scopeStatus.State == EntityState.Unchanged;
                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<Authorizations>();
            _databaseContext.Authorizations.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
