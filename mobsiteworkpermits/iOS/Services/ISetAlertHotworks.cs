﻿using System;
using System.Diagnostics;
using System.Linq;
using SiteWorkPermits.iOS.Services;
using SiteWorkPermits.Platform;
using EventKit;
using Foundation;
using UIKit;
using Xamarin.Forms;
using AudioToolbox;
using UserNotifications;

[assembly: Dependency(typeof(AlarmService))]
namespace SiteWorkPermits.iOS.Services
{
    public class AlarmService : ISetAlertHotworks
    {
        public void SetStartAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime startTime)
        {
            SetAlarm(hotworkTitle, hotworkDescription, alertId, startTime);
        }

        public void SetEndAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            SetAlarm(hotworkTitle, hotworkDescription, alertId, endTime);
        }

        public void SetGoingToEndAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            SetAlarm(hotworkTitle, hotworkDescription, alertId, endTime);
        }

        public void CancelAlertNotification(int alertId)
        {
            Deletereminder(alertId);
        }

        public void Deletereminder(int alertId)
        {
            var notificaitons = UIApplication.SharedApplication.ScheduledLocalNotifications;
            foreach (var d in notificaitons)
            {
                var dr = d.UserInfo.FirstOrDefault().Value;
                if (dr.ToString() == alertId.ToString())
                {
                    UIApplication.SharedApplication.CancelLocalNotification(d);
                }
                Debug.WriteLine(dr.ToString());               
            }                     
        }

        public void SetAlarm(string AlarmTitle, string AlarmDescription, int alertId, DateTime AlarmTime)
        {
            try
            {
                var notificaitons = UIApplication.SharedApplication.ScheduledLocalNotifications;
                foreach (var d in notificaitons)
                {
                    var dr = d.UserInfo.FirstOrDefault().Value;
                    if (dr.ToString() == alertId.ToString())
                    {
                        UIApplication.SharedApplication.CancelLocalNotification(d);
                        break;
                    }
                }

                var notification = new UILocalNotification();              
                notification.FireDate = (NSDate)DateTime.SpecifyKind(AlarmTime, DateTimeKind.Local);
                notification.Category = "HotWorksAlert";
                notification.AlertAction = "Workour";
                notification.AlertBody = AlarmDescription;
                notification.AlertTitle = AlarmTitle;
                //var dict = new NSDictionary(alertId.ToString());
                var dict = NSDictionary.FromObjectAndKey(new NSString(alertId.ToString()), new NSString("notification"));
                try
                {
                    notification.UserInfo = dict;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                UIApplication.SharedApplication.ScheduleLocalNotification(notification);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Notification Error : " + ex.InnerException.Message);
            }
        }
        public void SetFirewatcher1AlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            SetAlarm(hotworkTitle, hotworkDescription, alertId, endTime);
        }

        public void SetFirewatcher2AlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            SetAlarm(hotworkTitle, hotworkDescription, alertId, endTime);
        }
    }
}
