﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SiteWorkPermits.Platform
{
    public interface IFileUtility
    {
        //event EventHandler<MediaEventArgs> OnMediaAssetLoaded;
        //bool IsLoading { get; }
        //Task<IList<MediaAssest>> RetrieveMediaAssetsAsync(CancellationToken? token = null);
        //Task<string> StoreProfileImage(string path);
        Task<byte[]> GetImageWithCamera();
    }
}
