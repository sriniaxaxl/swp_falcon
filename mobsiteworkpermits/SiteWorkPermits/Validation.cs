﻿using System;
using System.Text.RegularExpressions;
using System.Linq;
using SiteWorkPermits.Platform;
using SiteWorkPermits.Resx;
using System.Collections.ObjectModel;
using SiteWorkPermits.Model;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.DAL.ImpairmentDAL;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SiteWorkPermits.DAL.sqlIntegration;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.Model.SyncHotWorks;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model.SyncImpairements;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits;
using Xamarin.Forms;
using System.Globalization;

namespace CustomControl
{
    public static class Validation
    {
        public static string CurrentAndroidVersion = Xamarin.Forms.DependencyService.Get<IAppInfo>().GetVersion();
        public static string CurrentIOSVersion = Xamarin.Forms.DependencyService.Get<IAppInfo>().GetVersion();

        public static string DownTimeTitle = "Application Down";
        public static string UpgradeTitle = "Upgrade Alert";
        public static string UpgradeMandatoryMessage = AppResource.APP_UPDATE;
        public static string UpgradeOptionalMessage = AppResource.APP_UPDATE;
        

        public static string INVALID_CREDENTIALS = AppResource.INVALID_ACCOUNT_LOCATION_NUMBER;
        public static string MANDATORY_FIELDS = AppResource.PLEASE_ENTER_MANDATORY_FIELDS;
        public static string HOTWORKS_NOT_CREATE = AppResource.HOTWORK_NOT_SAVED;
        public static string HOTWORKS_SUCCESS_CREATE = AppResource.HOTWORK_CREATED_SUCCESSFULLY;
        public static string HOTWORKS_SUCCESS_UPDATE = AppResource.HOTWORK_UPDATED_SUCCESSFULLY;
        public static string HOTWORKS_SUCCESS_DELETE = AppResource.HOTWORK_DELETED_SUCCESSFULLY;
        public static string SOMETHING_WENT_WRONG = AppResource.SOMETHING_WENT_WRONG;
        public static string EMPTY_CREDENTIALS = AppResource.ACCOUNT_LOCATION_NUMBER_MANDATORY;
        public static string START_END_DATE_SAME = AppResource.START_END_DATE_SAME;
        public static string FIRE_HOUSE_EXTINGUSER_CANNOT_BLANK = AppResource.FIRE_HOUSE_EXTINGUSER_CANNOT_BLANK;
        public static string HIGH_HAZARD_DETAIL_MANDATORY = AppResource.HIGH_HAZARD_DETAIL_MANDATORY;


        public static string Time_Alert_During = AppResource.Time_Alert_During;
        public static string Time_Alert_After = AppResource.Time_Alert_During;
        public static string IMPAIRMENT_SAVED_OFFLINE = AppResource.IMP_SUBMITTED_OFFLINE;
        public static bool Emailvalid(string Value)
        {
            string email = Value;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            return match.Success;
        }

        public static bool Numericvalid(string Value)
        {
            return true;
        }

        public static string RemoveWhiteSpace(this string self)
        {
            return new string(self.Where(c => !Char.IsWhiteSpace(c)).ToArray());
        }


        public static string ToCultureDate(DateTime dt) {
            var currentCulture = System.Globalization.CultureInfo.CurrentCulture.Name;
            return dt.ToString(new CultureInfo(currentCulture)).Split(' ')[0];
        }


        public static string ToCultureTime(DateTime dt)
        {
            var currentCulture = System.Globalization.CultureInfo.CurrentCulture.Name;
            return dt.ToString(new CultureInfo(currentCulture)).Split(' ')[1];
        }

        public static string ToCulturePhone(string phoneNumber) {

            try
            {
               
                if (string.IsNullOrEmpty(phoneNumber.Trim()))
                {
                    return "";
                }
                var currentCulture = System.Globalization.CultureInfo.CurrentCulture.Name;
                switch (currentCulture.Split('-')[0].ToUpper())
                {
                    case "DE":
                        string phoneFormat_DE = "####-######";
                        return Convert.ToInt64(Validation.RemoveWhiteSpace(phoneNumber)).ToString(phoneFormat_DE);
                    case "FR":
                        string phoneFormat_FR = "# ## ## ## ##";
                        return Convert.ToInt64(Validation.RemoveWhiteSpace(phoneNumber)).ToString(phoneFormat_FR);
                    case "ES":
                        string phoneFormat_ES = "### ### ###";
                        return Convert.ToInt64(Validation.RemoveWhiteSpace(phoneNumber)).ToString(phoneFormat_ES);
                    case "IT":
                        string phoneFormat_IT = "# ########";
                        return Convert.ToInt64(Validation.RemoveWhiteSpace(phoneNumber)).ToString(phoneFormat_IT);
                    case "PT":
                        string phoneFormat_PT = "### ### ###";
                        return Convert.ToInt64(Validation.RemoveWhiteSpace(phoneNumber)).ToString(phoneFormat_PT);
                    default:
                        return phoneNumber;
                }
            }
            catch (Exception ex) {
                return "";
            }
            
        }




        public static string GetHandHeldId()
        {
            string handHeldID = string.Empty;
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("HandHeldId"))
            {
                return Convert.ToString(Xamarin.Forms.Application.Current.Properties["HandHeldId"]);
            }
            else
            {
                return "4321";
            }
        }

        public static void SetUnSyncAlarm() {
            DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification(AppResource.APP_NAME, 
                AppResource.APP_NOT_SYNC_IN_24HRS, 
                11991, 
                DateTime.Now.AddHours(24));

        }

        public static void SetAlarm(HotWorkPermit_sql hotWorkPermit_Sql) {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();

            if (hotWorkPermit_Sql.workFlowStatus.ToLower() == "upcoming")
            {
                Scope_sql scope;

                try {
                    scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql.id, recursive: true);
                }
                catch (Exception ex) {
                    scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql_id, recursive: true);
                }


                //   var scope = connection.GetAllWithChildren<> Scopes.Where(p => p.ScopeId == hotWorkPermit_Sql.Scopes.ScopeId).First();


                if (Convert.ToDateTime(hotWorkPermit_Sql.scope_Sql.StartDateTime) >= DateTime.Now)
                {
                    DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotWorkPermit_Sql.id));
                    DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification("SiteWorkPermits Notification", "Hot Works Permit " + 
                        hotWorkPermit_Sql.id + " has started.", CustomControl.Validation.AlarmStartUniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.StartDateTime));
                }
                if (Convert.ToDateTime(hotWorkPermit_Sql.scope_Sql.EndDateTime) >= DateTime.Now)
                {
                    DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotWorkPermit_Sql.id));
                    DependencyService.Get<ISetAlertHotworks>().SetEndAlertNotification("SiteWorkPermits Notification", "Hot Works Permit " + 
                        hotWorkPermit_Sql.id + " has ended.", CustomControl.Validation.AlarmEndUniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.EndDateTime));
                }

                if (Convert.ToDateTime(hotWorkPermit_Sql.scope_Sql.EndDateTime).AddHours(1) >= DateTime.Now)
                {
                    DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotWorkPermit_Sql.id));
                    DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("SiteWorkPermits Notification", "First Fire Watch for  Hot Works Permit " 
                        + hotWorkPermit_Sql.id + " is scheduled now.", CustomControl.Validation.FireWatcher1UniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.EndDateTime).AddHours(1));
                }

                if (Convert.ToDateTime(hotWorkPermit_Sql.scope_Sql.EndDateTime).AddHours(2) >= DateTime.Now)
                {
                    DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotWorkPermit_Sql.id));
                    DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("SiteWorkPermits Notification", 
                        "Second Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.id + " is scheduled now.", 
                        CustomControl.Validation.FireWatcher2UniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.EndDateTime).AddHours(2));
                }


                //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("SiteWorkPermits Notification", "First Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.HandHeldId + " is scheduled now.", Validation.FireWatcher1UniqueId(_databaseContext.hotWorkPermit_Sql.LastOrDefault().hotWorkPermit_SqlId), scope.EndDate.AddHours(1));
                //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("SiteWorkPermits Notification", "Second Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.HandHeldId + " is scheduled now.", Validation.FireWatcher2UniqueId(_databaseContext.hotWorkPermit_Sql.LastOrDefault().hotWorkPermit_SqlId), scope.EndDate.AddHours(2));
            }
            if (hotWorkPermit_Sql.isDeleted)
            {
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotWorkPermit_Sql.id));
            }

        }

        public static void UpdateAlarm(HotWorkPermit_sql hotWorkPermit_Sql) {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();

            if (hotWorkPermit_Sql.workFlowStatus.ToLower() == "upcoming")
            {
                Scope_sql scope;

                try
                {
                    scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql.id, recursive: true);
                }
                catch (Exception ex)
                {
                    scope = connection.GetWithChildren<Scope_sql>(hotWorkPermit_Sql.scope_Sql_id, recursive: true);
                }

              //  var scope = _databaseContext.Scopes.Where(p => p.ScopeId == hotWorkPermit_Sql.Scopes.ScopeId).First();
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification("MySite Notification", "Hot Works Permit " + hotWorkPermit_Sql.id + " has started.", CustomControl.Validation.AlarmStartUniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.StartDateTime));
                DependencyService.Get<ISetAlertHotworks>().SetEndAlertNotification("MySite Notification", "Hot Works Permit " + hotWorkPermit_Sql.id + " has ended.", CustomControl.Validation.AlarmEndUniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.EndDateTime));

                DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("MySite Notification", "First Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.id + " is scheduled now.", CustomControl.Validation.FireWatcher1UniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.EndDateTime).AddHours(1));
                DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("MySite Notification", "Second Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.id + " is scheduled now.", CustomControl.Validation.FireWatcher2UniqueId(hotWorkPermit_Sql.id), Convert.ToDateTime(scope.EndDateTime).AddHours(2));
                //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher1AlertNotification("MySite Notification", "First Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.HandHeldId + " is scheduled now.", Validation.FireWatcher1UniqueId(_databaseContext.hotWorkPermit_Sqls.LastOrDefault().hotWorkPermit_SqlsId), scope.EndDate.AddHours(1));
                //DependencyService.Get<ISetAlertHotworks>().SetFirewatcher2AlertNotification("MySite Notification", "Second Fire Watch for  Hot Works Permit " + hotWorkPermit_Sql.HandHeldId + " is scheduled now.", Validation.FireWatcher2UniqueId(_databaseContext.hotWorkPermit_Sqls.LastOrDefault().hotWorkPermit_SqlsId), scope.EndDate.AddHours(2));
            }
            if (hotWorkPermit_Sql.isDeleted)
            {
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmStartUniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.AlarmEndUniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher1UniqueId(hotWorkPermit_Sql.id));
                DependencyService.Get<ISetAlertHotworks>().CancelAlertNotification(CustomControl.Validation.FireWatcher2UniqueId(hotWorkPermit_Sql.id));
            }

        }


        public static string HotworksUniqueID(int input)
        {

            string handHeldID = string.Empty;
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("HandHeldId"))
            {
                var resulta = input.ToString().PadLeft(4, '0');
                var ssss = Xamarin.Forms.Application.Current.Properties["HandHeldId"];
                return (ssss + "" + resulta);

            }
            else
            {
                var resultaa = input.ToString().PadLeft(4, '0');
                return "4321" + resultaa;
            }

        }

        // start unique ID
        public static int AlarmStartUniqueId(int hotworkId)
        {
            return Convert.ToInt32(hotworkId + "" + 118);
        }


        public static int AlarmToBeEndUniqueId(int hotworkId)
        {
            return Convert.ToInt32(hotworkId + "" + 116);
        }

        // end unique ID
        public static int AlarmEndUniqueId(int hotworkId)
        {
            return Convert.ToInt32(hotworkId + "" + 117);
        }


        // firewatcher 1 unique ID
        public static int FireWatcher1UniqueId(int hotworkId)
        {
            return Convert.ToInt32(hotworkId + "" + 114);
        }

        // firewatcher 2 unique ID
        public static int FireWatcher2UniqueId(int hotworkId)
        {
            return Convert.ToInt32(hotworkId + "" + 115);
        }

        //public static DateTime ChangeTime(this DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        //{
        //    return new DateTime(
        //        dateTime.Year,
        //        dateTime.Month,
        //        dateTime.Day,
        //        hours,
        //        minutes,
        //        seconds,
        //        milliseconds,
        //        dateTime.Kind);
        //}


        public static bool Mobilevalid(string Value)
        {
            Value = Value != null ? Value : string.Empty;
            var checknumeric = Numericvalid(Value);
            if (checknumeric)
            {
                if (Value.Length == 10)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public class ListViewElement
        {
            public int ElementId { get; set; }
            public string ElementCategory { get; set; } 
            public string ElementCreatedAt { get; set; }
            public string ElementDescription { get; set; } 
            public string ElementStartDate { get; set; } 
            public string ElementEndDate { get; set; } 
            public string ElementStatus { get; set; }
            public string ElementEvent { get; set; }
            public string ElementType { get; set; }
            public bool showCloseStatus { get; set; }
        }


        public static string GetElementEventName(string ElementWorkFlowStatus)
        {
            if (ElementWorkFlowStatus.ToLower().Equals("active") || ElementWorkFlowStatus.ToLower().Equals("upcoming")
                || ElementWorkFlowStatus.ToLower().Equals("expired"))
            {
                return AppResource.CLOSE_IMPAIRMENT;
            }
            else
            {
                return AppResource.DUPLICATE_IMP;
            }
        }



        public static HotWorkPermit_sql EditPermitByID(int permitID)
        {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();
            var hotWorkPermit_Sql = connection.GetWithChildren<HotWorkPermit_sql>(permitID, true);
            return hotWorkPermit_Sql;
            //var navigationParams = new NavigationParameters();
            //navigationParams.Add("hotworkPermit_sql", hotWorkPermit_Sql);
            // await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
        }


        public static Impairment_Sql EditImpairmentByID(int impairmentID)
        {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();
            var impairment_Sql = connection.GetWithChildren<Impairment_Sql>(impairmentID, true);
            return impairment_Sql;
            //var navigationParams = new NavigationParameters();
            //navigationParams.Add("hotworkPermit_sql", hotWorkPermit_Sql);
            // await HomeNavigation.NavigateAsync("NewHotWorksPage", navigationParams, null, false);
        }

        public static async Task<HotWork> GetServerHotwork(HotWorkPermit_sql hotworkspermit) {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();

            var scope = connection.GetWithChildren<Scope_sql>(hotworkspermit.scope_Sql.id, recursive: true);
            HotWork serverPermit = new HotWork();

            serverPermit.HotWorkMobileId = hotworkspermit.handHeldId;
            serverPermit.IsParent = false;
            serverPermit.GUID = hotworkspermit.hotworkGUID;
            serverPermit.ParentHotWorkId = 0;
            serverPermit.Id = hotworkspermit.server_id;

            if (hotworkspermit.CloseAuthorizedBy == null || hotworkspermit.CloseAuthorizedBy == "") { } else {
                serverPermit.CloseAuthorizedBy = hotworkspermit.CloseAuthorizedBy;
            }

            serverPermit.ScannedPaperPermitGUID = hotworkspermit.ScannedPaperPermitGUID;
            //serverPermit.ScannedPermits = hotworkspermit.ScannedPaperPermit;


            if (hotworkspermit.isDeleted)
            {
                serverPermit.isDeleted = true;
            }
            else
            {
                serverPermit.isDeleted = false;
            }
            serverPermit.IsScannedPermits = hotworkspermit.IsScannedPermits;
            serverPermit.WorkFlowStatus = hotworkspermit.workFlowStatus + "";


            if (hotworkspermit.hotWorkCloseDate == null || hotworkspermit.hotWorkCloseDate == "")
            {
            }
            else
            {
                serverPermit.CloseDateTime = Convert.ToDateTime(hotworkspermit.hotWorkCloseDate);

            }


            #region map scope

            //scope
            serverPermit.Scope = new Scope
            {
                WorkDetail = scope.WorkDetail,
                WorkLocation = scope.WorkLocation,
                CompanyName = scope.CompanyName,
                StartDateTime = Convert.ToDateTime(scope.StartDateTime).ToUniversalTime(),
                EndDateTime = Convert.ToDateTime(scope.EndDateTime).ToUniversalTime(),
                EquipmentDescription = scope.EquipmentDescription
            };

            serverPermit.Scope.ScopePersons = new List<ScopePerson>();

            foreach (var personName in scope.scopePerson_Sqls)
            {
                serverPermit.Scope.ScopePersons.Add(new ScopePerson { PersonName = personName.PersonName });
            }

            serverPermit.Scope.ScopeWorkOrders = new List<SiteWorkPermits.Model.SyncHotWorks.ScopeWorkOrder>();

            foreach (WorkOrderMaster_sql workOrderMaster_Sql in scope.workOrderMaster_Sqls)
            {
                serverPermit.Scope.ScopeWorkOrders.Add(
                    new SiteWorkPermits.Model.SyncHotWorks.ScopeWorkOrder
                    {
                        WorkOrderId = workOrderMaster_Sql.id
                    });
            }
            #endregion

            #region map fire prevention
            //fire watcher

            var firePrevention_sql = connection.GetWithChildren<FirePrevention_sql>(hotworkspermit.firePrevention_sql.id, recursive: true);

            FirePrevention firePrevention = new FirePrevention();
            firePrevention.FireWatchPreventions = new List<FireWatchPrevention>();
            firePrevention.NearestFireAlarmLocation = firePrevention_sql.NearestFireAlarmLocation;
            firePrevention.NearestPhone = firePrevention_sql.NearestPhone;
            //adding fire watcher after
            if (firePrevention_sql.fireWatcherPrevention_Sqls != null)
            {
                foreach (FireWatcherPrevention_sql fireWatcher_sql in firePrevention_sql.fireWatcherPrevention_Sqls)
                {

                    var fireWatcher = new FireWatchPrevention
                    {
                        PersonName = fireWatcher_sql.PersonName,
                        StartTime = fireWatcher_sql.StartTime,
                        EndTime = fireWatcher_sql.EndTime,
                        FireWatchType = fireWatcher_sql.FireWatchType == "AFTER" ? "AFTER" : "DURING"
                    };

                    firePrevention.FireWatchPreventions.Add(fireWatcher);
                }
            }

            serverPermit.FirePrevention = firePrevention;
            #endregion

            #region precaution measure
            // precautions


            var PermitPreventionMaster = connection.GetAllWithChildren<PrecautionMaster_sql>().ToList();


            List<PrecautionList> precautionList = new List<PrecautionList>();

           

            foreach (PrecautionMaster_sql permitPrecaution in PermitPreventionMaster)
            {
                if (hotworkspermit.precautionMaster_Sqls.Find(p => p.id == permitPrecaution.id) != null)
                {
                    var percaution = new PrecautionList
                    {
                        MeasureId = permitPrecaution.id,
                        MeasureStatus = "YES",
                        FireHoseCount = permitPrecaution.id == 6 ? hotworkspermit.fireHoseCount : 0,
                        FireExtinguisherCount = permitPrecaution.id == 6 ? hotworkspermit.fireExtinguisherCount : 0,
                    };
                    precautionList.Add(percaution);
                }
                else {
                    var percaution = new PrecautionList
                    {
                        MeasureId = permitPrecaution.id,
                        MeasureStatus = "SKIP",
                        FireHoseCount =  0,
                        FireExtinguisherCount = 0,
                    };
                    precautionList.Add(percaution);
                }
            }

            serverPermit.PrecautionList = precautionList;

            var auth = hotworkspermit.authorization_sql;

            #endregion

            #region authorization
            // authorization 
            Authorization authorization = new Authorization
            {
                AuthorizerName = auth.AuthorizerName,
                Department = auth.Department,
                Location = auth.Location,
                AuthorizationDateTime = (auth.AuthorizationDateTime == null) ? DateTime.UtcNow : Convert.ToDateTime(auth.AuthorizationDateTime),
            };

            if (!auth.IsHighHazardArea)
            {
                authorization.IsHighHazardArea = "NA";
                authorization.AreaSupervisorName = "";
                authorization.AreaSupervisorDepartment = "";
            }
            else {
                authorization.IsHighHazardArea = "YES";
                authorization.AreaSupervisorName = auth.AreaSupervisorName;
                authorization.AreaSupervisorDepartment = auth.AreaSupervisorDepartment;

            }

            serverPermit.Authorization = authorization;
            #endregion


            return serverPermit;
        }

        public static async Task<SyncImpairements> GetServerImpaierment(Impairment_Sql impairement_sql) {

            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            var connection = hotworkDatabase.GetdbConnection();

            SyncImpairements serverImpairement = new SyncImpairements();

            serverImpairement.ImpairmentMobileId = impairement_sql.impairmentMobileId;
            serverImpairement.IsParent = false;
            serverImpairement.ParentImpairmentId = 0;
            if (impairement_sql.isDeleted)
            {
                serverImpairement.IsDeleted = true;
            }
            else
            {
                serverImpairement.IsDeleted = false;
            }
            serverImpairement.Id = impairement_sql.server_id;
            serverImpairement.WorkFlowStatus = impairement_sql.workFlowStatus;
            serverImpairement.ImpairmentTypeId = impairement_sql.impairmentTypeId;
            serverImpairement.ImpairmentClassId = impairement_sql.impairmentClassId;
            serverImpairement.ShutDownReasonId = impairement_sql.shutDownReasonId;
            serverImpairement.ImpairmentDescription = impairement_sql.impairmentDescription;
            serverImpairement.GUID = impairement_sql.impairmentGUID;
            serverImpairement.StartDateTime = Convert.ToDateTime(impairement_sql.startDateTime).ToUniversalTime();
            serverImpairement.EndDateTime = Convert.ToDateTime(impairement_sql.endDateTime).ToUniversalTime();
            serverImpairement.IsDeleted = impairement_sql.isDeleted;

            if (impairement_sql.impairmentCloseDateTime == null || impairement_sql.impairmentCloseDateTime == "")
            {
            }
            else {
                serverImpairement.CloseDateTime = Convert.ToDateTime(impairement_sql.impairmentCloseDateTime);

            }


            //var reporter =   _connection.GetWithChildren<ReporterDetail_sql>(impairement_sql.reporterDetail_sql_id, true);
            ReporterDetail_sql reporter;
            try
            {
                reporter = connection.GetWithChildren<ReporterDetail_sql>(impairement_sql.reporterDetail_sql_id, recursive: true);
            }
            catch (Exception ex)
            {
                reporter = connection.GetWithChildren<ReporterDetail_sql>(impairement_sql.reporterDetail_Sql.id, recursive: true);
            }


            serverImpairement.ReporterDetail = new SiteWorkPermits.Model.SyncImpairements.ReporterDetail
            {
                Name = reporter.ReporterName,
                Email = reporter.ReporterEmail,
                Phone = reporter.ReporterPhone,
            };

            List<ImpairmentPrecautionList> impairmentPrecautionLists = new List<ImpairmentPrecautionList>();

            foreach (var impairmentPreTaken in impairement_sql.impairment_PrecautionMaster_Sqls)
            {
                impairmentPrecautionLists.Add(new ImpairmentPrecautionList
                {
                    ImpairmentPrecautionMasterId = impairmentPreTaken.id,
                    OtherDescription = impairement_sql.precautionOtherDescription
                });
            }
            serverImpairement.ImpairmentPrecautionList = impairmentPrecautionLists;

            List<int> ImpairmentMeasureMasterIdLists = new List<int>();

            foreach (var impairmentImpairmentMeasureMaster in impairement_sql.majorImpairmentMaster_sqls)
            {
                ImpairmentMeasureMasterIdLists.Add(impairmentImpairmentMeasureMaster.id);
            }

            serverImpairement.ImpairmentMeasureMasterIdList = ImpairmentMeasureMasterIdLists;
            return serverImpairement;
        }

        public static async Task<HotWorkPermit_sql> GetLocalHotwork_Sql(HotWork hotWork)
        {
            try
            {
                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();

                var personListq = new List<ScopePerson_sql>();

                if (hotWork.Scope.ScopePersons != null)
                {
                    foreach (var personHotWork in hotWork.Scope.ScopePersons)
                    {
                        personListq.Add(new ScopePerson_sql() { PersonName = personHotWork.PersonName });
                    }
                }

                var workorderList = new List<WorkOrderMaster_sql>();
                foreach (var workOrder in hotWork.Scope.ScopeWorkOrders)
                {
                    workorderList.Add(connection.Table<WorkOrderMaster_sql>().Where(p => p.id == workOrder.WorkOrderId).FirstOrDefault());
                }

                

                var firewatcherList = new List<FireWatcherPrevention_sql>();

                if (hotWork.FirePrevention.FireWatchPreventions.Count() > 0) {
                    foreach (var firewatcher in hotWork.FirePrevention.FireWatchPreventions)
                    {
                        string watcherType = string.Empty;

                        watcherType = firewatcher.FireWatchType;

                        firewatcherList.Add(new FireWatcherPrevention_sql
                        {
                            PersonName = firewatcher.PersonName,
                            StartTime = firewatcher.StartTime.ToString(),
                            EndTime = firewatcher.EndTime.ToString(),
                            FireWatchType = watcherType,
                        });
                    }

                }

                var precautionMaster_sql = new List<PrecautionMaster_sql>();
                int firehouses = 0;
                int fireextinguisher = 0;
                if (hotWork.PrecautionList != null)
                {
                    foreach (var precautions in hotWork.PrecautionList)
                    {

                        if (precautions.MeasureId == 6 && precautions.MeasureStatus == "YES")
                        {
                            firehouses = Convert.ToInt32(hotWork.PrecautionList.Where(p => p.MeasureId == 6).FirstOrDefault().FireHoseCount);
                            fireextinguisher = Convert.ToInt32(hotWork.PrecautionList.Where(p => p.MeasureId == 6).FirstOrDefault().FireExtinguisherCount);
                        }

                        if (precautions.MeasureStatus == "YES")
                        {
                            precautionMaster_sql.Add(connection.Table<PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.MeasureId).FirstOrDefault());
                        }

                    }
                }

                var ishighHazard = (hotWork.Authorization.IsHighHazardArea == null || hotWork.Authorization.IsHighHazardArea == "NA") ? false : true;

                var SCOPE = HotWorkUtility.CreateScopeInstanceSQL(
                    hotWork.Scope.WorkDetail == null ? "": hotWork.Scope.WorkDetail, 
                    hotWork.Scope.WorkLocation==null?"": hotWork.Scope.WorkLocation, 
                    hotWork.Scope.CompanyName==null?"": hotWork.Scope.CompanyName, 
                    hotWork.Scope.EquipmentDescription==null?"": hotWork.Scope.EquipmentDescription,
                    hotWork.Scope.StartDateTime.ToLocalTime().ToString(), hotWork.Scope.EndDateTime.ToLocalTime().ToString(), 
                    personListq, workorderList,
                    connection);

                var FIRE_PREVENTION = HotWorkUtility.CreateFirePreventionInstanceSQL(hotWork.FirePrevention.NearestPhone == null ? "" : hotWork.FirePrevention.NearestPhone,
                    hotWork.FirePrevention.NearestFireAlarmLocation == null ? "" : hotWork.FirePrevention.NearestFireAlarmLocation,
                    firewatcherList, connection);

                var AUTHORIZATION = HotWorkUtility.CreateAuthorizationInstanceSQL
                    (hotWork.Authorization.AuthorizerName==null?"":hotWork.Authorization.AuthorizerName,
                    hotWork.Authorization.Department==null?"": hotWork.Authorization.Department,
                    hotWork.Authorization.Location==null?"": hotWork.Authorization.Location, 
                    hotWork.Authorization.AuthorizationDateTime.ToString(),
                    ishighHazard, hotWork.Authorization.AreaSupervisorName==null?"": hotWork.Authorization.AreaSupervisorName,
                    hotWork.Authorization.Department==null?"": hotWork.Authorization.Department, connection);

                if (SCOPE != null && FIRE_PREVENTION != null && AUTHORIZATION != null)
                {
                    var hwinstance = HotWorkUtility.CreateHotWorkInstanceSQL(
                                               false, true, false, false, hotWork.WorkFlowStatus, "", App.SiteId, hotWork.Id,
                                               firehouses,
                                               0,
                                               fireextinguisher,
                                            SCOPE, FIRE_PREVENTION, precautionMaster_sql, AUTHORIZATION, connection);

                    hwinstance.ScannedPaperPermit = hotWork.ScannedPermits;
                    hwinstance.ScannedPaperPermitGUID = hotWork.ScannedPaperPermitGUID;
                    hwinstance.IsScannedPermits = hotWork.IsScannedPermits;
                    if (hwinstance != null)
                    {
                        hwinstance.hotworkGUID = hotWork.GUID;

                        if (hotWork.CloseDateTime!=null)
                        hwinstance.hotWorkCloseDate = hotWork.CloseDateTime.Value.ToLocalTime().ToString();
                        if (hotWork.CloseAuthorizedBy == null || hotWork.CloseAuthorizedBy == "") { }
                        else
                        {
                            hwinstance.CloseAuthorizedBy = hotWork.CloseAuthorizedBy;
                        }

                        connection.UpdateWithChildren(hwinstance);
                        return hwinstance;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        public static async Task<Impairment_Sql> GetLocalImpairement_Sql(SyncImpairements SyncImpairements)
        {

            try
            {
                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();
                var reporterDetails = ImpairmentUtility.GetReporterDetails(SyncImpairements.ReporterDetail.Name, SyncImpairements.ReporterDetail.Email, SyncImpairements.ReporterDetail.Phone);
                Impairment_Sql impairmentSql = new Impairment_Sql();

                if (reporterDetails != null)
                {
                    connection.Insert(reporterDetails);
                    impairmentSql = ImpairmentUtility.GetImpairment(reporterDetails,
                       SyncImpairements.ImpairmentTypeId, SyncImpairements.ImpairmentClassId, SyncImpairements.ShutDownReasonId,
                       SyncImpairements.StartDateTime.ToLocalTime(),
                       SyncImpairements.EndDateTime.ToLocalTime(),
                       SyncImpairements.ImpairmentDescription, SyncImpairements.WorkFlowStatus, SyncImpairements.Id);

                    impairmentSql.impairmentGUID = SyncImpairements.GUID;


                    impairmentSql.impairmentCloseDateTime = SyncImpairements.CloseDateTime == null ? null :
                        SyncImpairements.CloseDateTime.Value.ToLocalTime().ToString();


                }

                impairmentSql.workFlowStatus = SyncImpairements.WorkFlowStatus;

                if (impairmentSql != null) {

                    connection.Insert(impairmentSql);
                    var majorImpairmentMaster_Sqls = new List<MajorImpairmentMaster_sql>();

                    foreach (var majorImpairment in SyncImpairements.ImpairmentMeasureMasterIdList)
                    {
                        majorImpairmentMaster_Sqls.Add(connection.Table<MajorImpairmentMaster_sql>().Where(p => p.id == majorImpairment).FirstOrDefault());
                    }

                    var impairment_PrecautionMaster_Sqls = new List<Impairment_PrecautionMaster_sql>();
                    
                    foreach (var precautions in SyncImpairements.ImpairmentPrecautionList)
                    {
                        string otherDesc = string.Empty;
                        if (precautions.ImpairmentPrecautionMasterId == 15)
                        {
                            impairmentSql.precautionOtherDescription = precautions.OtherDescription;
                        }
                        else
                        {
                            impairmentSql.precautionOtherDescription = otherDesc;
                        }

                        impairment_PrecautionMaster_Sqls.Add(connection.Table<Impairment_PrecautionMaster_sql>().ToList().Where(p => p.id == precautions.ImpairmentPrecautionMasterId).FirstOrDefault());
                    }

                    impairmentSql.majorImpairmentMaster_sqls = majorImpairmentMaster_Sqls;
                    impairmentSql.impairment_PrecautionMaster_Sqls = impairment_PrecautionMaster_Sqls;
                    connection.UpdateWithChildren(impairmentSql);

                }

                return impairmentSql;
            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }


        public static List<HotWork> HotWorks { get; set; }
        public static List<SiteWorkPermits.Model.SyncImpairements.SyncImpairements> Impairments { get; set; }

        public static async Task<bool> IsSyncDone() {
            try
            {
                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                var connection = hotworkDatabase.GetdbConnection();
                
                var hotwork_sql_List = connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();
                var impairment_sql_List = connection.GetAllWithChildren<Impairment_Sql>().ToList();

                

                if (hotwork_sql_List.Count() > 0)
                {
                    HotWorks = new List<HotWork>();
                    foreach (HotWorkPermit_sql hotWorkPermit_sql in hotwork_sql_List)
                    {
                        if(hotWorkPermit_sql.server_id==0 || hotWorkPermit_sql.isEdited || hotWorkPermit_sql.isNewlyCreated || hotWorkPermit_sql.isDeleted)
                        HotWorks.Add(await GetServerHotwork(hotWorkPermit_sql));
                    }

                }
                

                if (impairment_sql_List.Count() > 0)
                {
                    Impairments = new List<SyncImpairements>();
                    foreach (Impairment_Sql impairment_Sql in impairment_sql_List)
                    {
                        if (impairment_Sql.server_id == 0 || impairment_Sql.isEdited)
                        Impairments.Add(await GetServerImpaierment(impairment_Sql));
                    }

                }


              SiteWorkPermits.Model.SyncHotWorks.Data syncHotWorksRequest = new SiteWorkPermits.Model.SyncHotWorks.Data
                {
                    HotWorks = HotWorks,
                    Impairments = Impairments
              };

                

                var syncHotWorksResponse = await RestApiHelper<RootObject>.SyncUserDate_Post(syncHotWorksRequest);
              
                connection.DeleteAll<HotWorkPermit_sql>();
                connection.DeleteAll<Scope_sql>();
                connection.DeleteAll<ScopePerson_sql>();
                connection.DeleteAll<FirePrevention_sql>();
                connection.DeleteAll<FireWatcherPrevention_sql>();
                connection.DeleteAll<Authorization_sql>();

                connection.DeleteAll<Impairment_Sql>();
                connection.DeleteAll<ReporterDetail_sql>();

               

                for (int i = 0; i < syncHotWorksResponse.Data.HotWorks.Count(); i++) {
                    await GetLocalHotwork_Sql(syncHotWorksResponse.Data.HotWorks[i]);
                }

                for (int i = 0; i < syncHotWorksResponse.Data.Impairments.Count(); i++)
                {
                    await GetLocalImpairement_Sql(syncHotWorksResponse.Data.Impairments[i]);
                }

                return true;

            }
            catch (Exception ex) {
                return false;
            }            
        }
                     
        public static bool Lengthvalid(string Value, int length)
        {
            Value = Value != null ? Value : string.Empty;
            if (Value.Length == length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool PasswordValidate(string Value)
        {
            var hasUpperChar = new Regex(@"[A-Z]+");
            var haslowerChar = new Regex(@"[a-z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");
            var isValidated = haslowerChar.IsMatch(Value) && hasUpperChar.IsMatch(Value) && hasMinimum8Chars.IsMatch(Value);
            return isValidated;
        }

        public static ObservableCollection<WorkOrders> GetWorkOrdersFromDB()
        {

            return new ObservableCollection<WorkOrders>{
                new WorkOrders { WorkOrderId = 1, Description = "Thawing", IsSelected = false },
                new WorkOrders { WorkOrderId = 2, Description = "Grinding", IsSelected = false },
                new WorkOrders { WorkOrderId = 3, Description = "Brazing", IsSelected = false },
                new WorkOrders { WorkOrderId = 4, Description = "Cutting", IsSelected = false },
                new WorkOrders { WorkOrderId = 5, Description = "Welding", IsSelected = false },
                new WorkOrders { WorkOrderId = 6, Description = "Other", IsSelected = false }
                };
        }

        public static ObservableCollection<Precautions> GetPrecautionsFromDB()
        {

            return new ObservableCollection<Precautions>
            {
                new Precautions { Id = 1, Description = "Alternative means to hot work or moving the hot work to a safe area have been considered but cannot be achieved." },
                new Precautions { Id = 2, Description = "Measures have been taken to limit the hot work to the permitted location and the tools specified in this permit." },
                new Precautions { Id = 3, Description = "Automatic sprinkler protection, where provided are operational and will not be taken out of service at any time during the hot work." },
                new Precautions { Id = 4, Description = "Surrounding floors have been swept clean and combustible floors have been wet down or covered with wet sand (if possible) or fire retardant tarpaulins." },
                    new Precautions
                    {
                        Id = 5,
                    Description = "There are no combustible fibers, dusts, vapors, gases or liquids in the area of the hot work. Tanks and equipment previously containing such have been cleaned and purged. The absence of flammable gases and vapors has been verified by portable gas detectors. If there is the possibility of a leak developing in nearby piping, equipment or tanks, the area is continuously monitored with portable gas detectors."
                    },
                    new Precautions
                    {
                        Id = 6,
                    Description = "All combustibles have been relocated 11 m or 35 ft (further away for elevated work) from the location of the hot work including areas on opposite sides of walls if heat can be transferred through thermally conductive material through the wall. Any material that cannot be moved is protected with metal guards or fire retardant tarpaulins."
                    },

                new Precautions { Id = 7, Description = "All floor and wall openings within 11 m or 35 ft of the area of hot work have been tightly covered or closed." },
                new Precautions { Id = 8, Description = "All walls, floors or ceilings being worked on are of noncombustible construction, including any internal insulation or external facade." },
                new Precautions { Id = 9, Description = "The equipment for performing the hot work is in proper working condition." },
                    new Precautions { Id = 10, Description = "A sufficient number of portable fire extinguishers and fire hoses have been provided." },
                    new Precautions
                    {
                        Id = 11,
                    Description = "A fire watch has been assigned to watch for fires or the potential for fires in the work area as well as the floors above and below and the opposite side of walls. The fire watch will continue during any break period and for at least one hour after the hot work has been completed. Patrols will begin on regular intervals over the next 3 hours after the fire watch has been released."
                    }
                };
        }

        public static ObservableCollection<ImpairmentType> GetImpairmentTypeFromDB()
        {
            return new ObservableCollection<ImpairmentType>
            {
                new ImpairmentType { Id=1, TypeDescription = "Planned"},
                new ImpairmentType { Id=2, TypeDescription = "Emergency"},
                new ImpairmentType {  Id=3,TypeDescription = "Hidden/Discovered" },
                new ImpairmentType {  Id=4,TypeDescription = "Long Term" },
                new ImpairmentType {  Id=5,TypeDescription = "Ongoing"},
            };
        }

        public static ObservableCollection<ImpairmentClassType> GetImpairmentClassTypesFromDB()
        {
            return new ObservableCollection<ImpairmentClassType>
            {
                new ImpairmentClassType { Id=1, ClassDescription = "Automatic Sprinkler System(s)"},
                new ImpairmentClassType {  Id=2,ClassDescription = "Special Extinguishing System"},
                new ImpairmentClassType {  Id=3,ClassDescription = "Fire pump(s)" },
                new ImpairmentClassType {  Id=4,ClassDescription = "Firewater Tank" },
                new ImpairmentClassType { Id=5, ClassDescription = "Public Water Supply"},
                new ImpairmentClassType { Id=6,ClassDescription = "Fire Hydrant(s)"},
                new ImpairmentClassType { Id=7, ClassDescription = "Fire Alarm System"},
                new ImpairmentClassType {Id=8,  ClassDescription = "Other"},
            };
        }

        public static ObservableCollection<ShutDownReason> GetshutDownReasonsFromDB()
        {
            return new ObservableCollection<ShutDownReason>
            {
                new ShutDownReason {  Id=1,ClassDescription = "System Modification"},
                new ShutDownReason { Id=2,ClassDescription = "System Repairs (result of loss)"},
                new ShutDownReason {  Id=3,ClassDescription = "System Repairs (not as result of loss)" },
                new ShutDownReason {  Id=4,ClassDescription = "Routine Maintenance" },
                new ShutDownReason {  Id=5,ClassDescription = "Building Renovation"},
                new ShutDownReason {  Id=6,ClassDescription = "Other"},
            };
        }

        public static ObservableCollection<ImpairmentMeasureMaster> GetMajorImpairmentsFromDB()
        {
            return new ObservableCollection<ImpairmentMeasureMaster>
            {
                new ImpairmentMeasureMaster { ImpairmentMeasureMasterId=1, ImpairmentMeasureDescription = "More than one sprinkler system is shutdown."},
                new ImpairmentMeasureMaster { ImpairmentMeasureMasterId=2,ImpairmentMeasureDescription = "Duration expected to be more than 24 hours."},
                new ImpairmentMeasureMaster { ImpairmentMeasureMasterId=3, ImpairmentMeasureDescription = "Entire water supply is shutdown (affecting sprinklers and/or fire hydrant supply)." },
                new ImpairmentMeasureMaster {  ImpairmentMeasureMasterId=4,ImpairmentMeasureDescription = "Hot work required inside impaired area (not recommended)." },
            };
        }
        
        public static ObservableCollection<PrecautionTaken> GetprecautionTakenFromDB()
        {
            return new ObservableCollection<PrecautionTaken>
            {
                new PrecautionTaken { ImpairmentPrecautionId=1, Description = "Use Shutoff Tags"},
                new PrecautionTaken { ImpairmentPrecautionId=2, Description = "Discontinue Welding, Cutting, Hot Work"},
                new PrecautionTaken { ImpairmentPrecautionId=3, Description = "Notify Department Head" },
                new PrecautionTaken { ImpairmentPrecautionId=4, Description = "Discontinue Smoking in Area" },
                new PrecautionTaken { ImpairmentPrecautionId=5, Description = "Cease Hazardous Operations"},
                new PrecautionTaken { ImpairmentPrecautionId=6, Description = "Notify Fire Department"},
                new PrecautionTaken { ImpairmentPrecautionId=7, Description = "Charged Hose Lines and Extinguishers"},
                new PrecautionTaken {  ImpairmentPrecautionId=8,Description = "Watchman Surveillance"},
                new PrecautionTaken { ImpairmentPrecautionId=9, Description = "Notify Alarm Company"},
                new PrecautionTaken { ImpairmentPrecautionId=10, Description = "Notify Site Emergency Response/Fire Team"},
                new PrecautionTaken { ImpairmentPrecautionId=11, Description = "Work to be Continuous"},
                new PrecautionTaken { ImpairmentPrecautionId=12, Description = "Pipe Plugs/Caps/Etc. Available"},
                new PrecautionTaken { ImpairmentPrecautionId=13, Description = "Emergency Connection Planned"},
                new PrecautionTaken { ImpairmentPrecautionId=14, Description = "Active Smoke/heat detection"},
                new PrecautionTaken { ImpairmentPrecautionId=15, Description = "Other (Explain)"},
            };
        }

        
    }
}
