﻿using System;
using System.ComponentModel;
using CoreGraphics;
using CustomControl;
using CustomControl.iOS.Renderer;
using Foundation;
using iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusFloatingButton), typeof(CustomFloatingButtonRender))]
namespace iOS.Renderer
{
    [Preserve]
    public class CustomFloatingButtonRender : ButtonRenderer
    {       
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;
                           
            Element.HeightRequest = Element.WidthRequest;
            Element.BorderRadius = (int)Element.WidthRequest / 2;
            Element.BorderWidth = 0;
            Element.Text = null;

            Control.BackgroundColor = ((CusFloatingButton)Element).ButtonColor.ToUIColor();
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            // add shadow
            Layer.ShadowRadius = 2.0f;
            Layer.ShadowColor = UIColor.Black.CGColor;
            Layer.ShadowOffset = new CGSize(1, 1);
            Layer.ShadowOpacity = 0.80f;
            Layer.ShadowPath = UIBezierPath.FromOval(Layer.Bounds).CGPath;
            Layer.MasksToBounds = false;

        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == CusFloatingButton.ButtonColorProperty.PropertyName)
            {
                Control.BackgroundColor = ((CusFloatingButton)Element).ButtonColor.ToUIColor();
            }
        }

    }
}
