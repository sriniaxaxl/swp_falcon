﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IWorkOrdersService
    {
        IEnumerable<WorkOrders> GetWorkOrdersAsync();

        WorkOrders GetWorkOrdersByIdAsync(int id);

        bool AddWorkOrdersAsync(WorkOrders workOrder);
        void RemoveAllData();

    }
}
