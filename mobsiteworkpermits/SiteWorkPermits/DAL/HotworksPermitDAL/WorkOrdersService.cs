﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using SiteWorkPermits.Platform;
using System.Linq;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class WorkOrdersService:IWorkOrdersService
    {
        private readonly DatabaseContext _databaseContext;
        public WorkOrdersService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public  bool AddWorkOrdersAsync(WorkOrders workOrder)
        {
            try
            {
                var scopeStatus =  _databaseContext.WorkOrders.Add(workOrder);
                _databaseContext.SaveChanges();
                var isAdded = scopeStatus.State == EntityState.Added;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<WorkOrders> GetWorkOrdersAsync()
        {
            try
            {
                var workOrders = _databaseContext.WorkOrders.ToList();
                if(workOrders!=null && workOrders.Count>0)
                return workOrders;
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public WorkOrders GetWorkOrdersByIdAsync(int id)
        {
            var hotworksPermits = _databaseContext.WorkOrders.Where(p => p.WorkOrderId == id);
            return hotworksPermits.FirstOrDefault();
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<WorkOrders>();
            _databaseContext.WorkOrders.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
