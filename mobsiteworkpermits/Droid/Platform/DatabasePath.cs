﻿//using System;
//using System.IO;
using System;
using System.IO;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Widget;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Platform;
using Java.IO;
using Java.Nio.Channels;
using SQLite.Net.Cipher.Interfaces;
using Xamarin.Forms;
using Android.Util;
using Android.Arch.Lifecycle;

[assembly: Dependency(typeof(DatabasePath))]
namespace SiteWorkPermits.Droid.Platform
{
    public class DatabasePath : IDBPath
    {
        private static int REQUEST_EXTERNAL_STORAGE = 1;
        private static string[] PERMISSIONS_STORAGE = {
            Manifest.Permission.ReadExternalStorage,
            Manifest.Permission.WriteExternalStorage
        };

        static Context _context;

        public static void Init(Context context)
        {
            _context = context;
        }

        public string GetDbPath()
        {
            //exportDB();
            //var dbPath = Path.Combine(exportDB(), "CLPCoreDB.db");
            var dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CLPCoreDB.db");

            App.connection = new SQLite.SQLiteConnection(dbPath);

            var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
            //  ISecureDatabase secureDatabase = new SQLiteDatabase(platform, dbPath);

            //string password = "#HM#=3#s2ydEC_+";

            if (!string.IsNullOrEmpty(AppConfig.passwordKey))
            {
                App.connection.Query<int>("PRAGMA key='?'", AppConfig.passwordKey);
            }

            return dbPath;
        }

        public string GetPDFPath()
        {
            try
            {
                string path = string.Empty;
                Java.IO.File sd = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
                path = sd.AbsolutePath;
                return path;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private string exportDB()
        {
            try
            {
                Java.IO.File sd = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
                var path = sd.AbsolutePath;
                return path;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool DownloadOfflinePDFPath(string fileName)
        {

            if (ContextCompat.CheckSelfPermission((Activity)_context, Manifest.Permission.WriteExternalStorage)
                != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions((Activity)_context, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }

            try
            {
                AssetManager assets = ((MainActivity)_context).Assets;
                //string fileName1 = fileName;  
                var localFolder = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/Download";
                var MyFilePath = System.IO.Path.Combine(localFolder, fileName);
                Java.IO.File file = new Java.IO.File(MyFilePath);
                if (file.Exists())
                {
                    file.Delete();
                }
                using (var streamReader = new StreamReader(assets.Open(fileName)))
                {
                    using (var memstream = new MemoryStream())
                    {
                        try
                        {
                            streamReader.BaseStream.CopyTo(memstream);
                            var bytes = memstream.ToArray();
                            //write to local storage
                            System.IO.File.WriteAllBytes(MyFilePath, bytes);

                            // var contentUri = FileProvider.GetUriForFile(_context.ApplicationContext, _context.ApplicationContext.PackageName + ".fileProvider", new Java.IO.File(MyFilePath));
                            var contentUri = Android.Net.Uri.Parse(MyFilePath);
                            var intent = new Intent(Intent.ActionView);
                            intent.SetDataAndType(contentUri, "application/pdf");
                            intent.SetFlags(ActivityFlags.ClearWhenTaskReset |   ActivityFlags.NewTask | ActivityFlags.GrantReadUriPermission | ActivityFlags.NewTask | ActivityFlags.ClearTop);
                            Intent intentchooser = Intent.CreateChooser(intent, "Open File");
                            intentchooser.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask | ActivityFlags.GrantReadUriPermission | ActivityFlags.NewTask | ActivityFlags.ClearTop);
                            _context.ApplicationContext.StartActivity(intentchooser);
                            Xamarin.Forms.Application.Current.Properties["iOSDownloadedFile"] = contentUri;
                        }
                        catch (Exception e)
                        {
                            Log.Error("Error", e.ToString());
                            Toast.MakeText(_context, "Please install PDF Viewer to view the file.", ToastLength.Long).Show();
                        }
                    }
                }
                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }
        }
    }
}