﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CustomRatingBar : View
    {
        public CustomRatingBar() : base()
        {

        }

        public static BindableProperty ValueProperty = BindableProperty.Create<CustomRatingBar, double>(o => o.Value, 0.0);
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }


        public static BindableProperty RatingcolorProperty = BindableProperty.Create<CustomRatingBar, Color>(o => o.Ratingcolor, Color.Transparent);
        public Color Ratingcolor
        {
            get { return (Color)GetValue(RatingcolorProperty); }
            set { SetValue(RatingcolorProperty, value); }
        }

        public static BindableProperty MinimumProperty = BindableProperty.Create<CustomRatingBar, double>(o => o.Minimum, 0.0);
        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public static BindableProperty MaximumProperty = BindableProperty.Create<CustomRatingBar, double>(o => o.Maximum, 0.0);
        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }
    }
}
