﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;
using SiteWorkPermits.Resx;
using SiteWorkPermits.ViewModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class FirePreventionView : ContentView
    {
        public event EventHandler LastElementFocus;
        public event EventHandler LastElementUnFocus;
        public FirePreventionView(NewHotWorksPageViewModel vm = null)
        {
            InitializeComponent();
            Lblnote.Text = AppResource.FIRE_PREVENTION_NOTE;
            if (vm != null)
            {
                FireDuringView.DurationDisplay = true;
                FireAfterView.DurationDisplay = true;
                FireDuringView.EditValcollection = vm.FirewatchDuringHotWork;
                FireAfterView.EditValcollection = vm.FirewatchAfterHotWork;
            }

            //TxtDesLocation.EntryFocuesed +=TxtLast_Focused;

        }

        public void TxtLast_Focused(object sender, EventArgs e)
        {
            if (LastElementFocus != null)
            {
                LastElementFocus.Invoke(this, new EventArgs());
            }
        }
                       
        public void TxtLast_UnFocused(object sender, System.EventArgs e)
        {
            if (LastElementUnFocus != null)
            {
                LastElementUnFocus.Invoke(this, new EventArgs());
            }
        }
    }
}
