﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiteWorkPermits.Model
{
    public class ScannedPermitModel
    {
        int id { get; set; }
        public byte[] ScannedPaperPermit { get; set; }
        public string ScannedPaperPermitGUID { get; set; }
    }
}
