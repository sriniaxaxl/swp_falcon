﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IFirePreventionMeasuresService
    {
        
        IEnumerable<FirePreventionMeasures> GetFirePreventionMeasuresAsync();

        FirePreventionMeasures GetFirePreventionMeasuresByIdAsync(int id);

        bool AddFirePreventionMeasuresAsync(FirePreventionMeasures firePreventionMeasures);

        bool UpdateFirePreventionMeasuresAsync(FirePreventionMeasures firePreventionMeasures);

        bool RemoveFirePreventionMeasuresAsync(int id);
        void RemoveAllData();
    }
}
