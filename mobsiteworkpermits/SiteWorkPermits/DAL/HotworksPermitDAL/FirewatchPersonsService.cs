﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class FirewatchPersonsService:IFirewatchPersonsService
    {
        private readonly DatabaseContext _databaseContext;
        public FirewatchPersonsService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public  bool AddFirewatchPersonsAsync(FirewatchPersons firewatchPersons)
        {
            try
            {
                var fireWatchStatus =  _databaseContext.FirewatchPersons.Add(firewatchPersons);
                 _databaseContext.SaveChanges();
                var isAdded = fireWatchStatus.State == EntityState.Unchanged;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public  IEnumerable<FirewatchPersons> GetFirewatchPersonsAsync()
        {
            try
            {
                var firewatchPersons =  _databaseContext.FirewatchPersons.ToList();
                return firewatchPersons;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public  FirewatchPersons GetFirewatchPersonsByIdAsync(int id)
        {
            try
            {
                var firewatchPerson =  _databaseContext.FirewatchPersons.Find(id);
                return firewatchPerson;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public  bool RemoveFirewatchPersonsAsync(int id)
        {
            try
            {
                var firewatchPerson =  _databaseContext.FirewatchPersons.Find(id);
                var firewatchPersonsStatus = _databaseContext.FirewatchPersons.Remove(firewatchPerson);
                 _databaseContext.SaveChanges();
                var isDeleted = firewatchPersonsStatus.State == EntityState.Deleted;
                return isDeleted;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public  bool UpdateFirewatchPersonsAsync(FirewatchPersons firewatchPersons)
        {
            try
            {
                var firewatchPersonStatus = _databaseContext.Update(firewatchPersons);
                 _databaseContext.SaveChanges();
                var isUpdated = firewatchPersonStatus.State == EntityState.Unchanged;
                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<FirewatchPersons>();
            _databaseContext.FirewatchPersons.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
