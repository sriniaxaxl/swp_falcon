﻿using System;
namespace SiteWorkPermits
{
    public class ImpairmentApiRequest
    {
        public string ImpairmentId { get; set; }
        public string ToEmailAddress { get; set; }
        public string PaperSize { get; set; }
        public double UtcOffsetInMinute { get; set; }
        public string TimeZone { get; set; }
    }
}
