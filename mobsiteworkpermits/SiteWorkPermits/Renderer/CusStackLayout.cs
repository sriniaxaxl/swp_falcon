﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusStackLayout : StackLayout
    {
        #region Constructor
        public CusStackLayout() : base()
        {
            this.Spacing = 0;           
        }

        private void TapGesRecongnize_Tapped(object sender, EventArgs e)
        {
        }

        #endregion

        //#region Event Handler

        //public event EventHandler Tapped;
        //public event EventHandler<PinchGestureUpdatedEventArgs> Pinched;
        //void OnGdTapped(object sender, EventArgs args)
        //{
        //    EventHandler eh = Tapped;
        //    if (eh != null)
        //    {
        //        eh(this, args);
        //    }
        //}

        //private void PinchGesrecognize_PinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
        //{
        //    EventHandler<PinchGestureUpdatedEventArgs> eh = Pinched;
        //    if (eh != null)
        //    {
        //        eh(this, e);
        //    }
        //}

        //#endregion

        #region Properties   

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusStackLayout, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusStackLayout, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusStackLayout, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusStackLayout, float>(o => o.BorderRadius, 0);

        public float BorderRadius
        {
            get { return (float)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        #endregion

    }
}
