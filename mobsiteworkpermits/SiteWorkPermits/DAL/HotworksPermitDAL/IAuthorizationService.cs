﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IAuthorizationService
    {
        IEnumerable<Authorizations> GetAuthorizationsAsync();

        Authorizations GetAuthorizationsByIdAsync(int id);

        bool AddAuthorizationsAsync(Authorizations authorizations);

        bool UpdateAuthorizationsAsync(Authorizations authorizations);

        bool RemoveAuthorizationsAsync(int id);
        void RemoveAllData();
    }
}
