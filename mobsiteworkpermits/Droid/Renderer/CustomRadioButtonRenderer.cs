using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using CustomControl;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Android.Content.Res;
using Android.Renderer;

[assembly: ExportRenderer(typeof(CustomRadioButton), typeof(CustomRadioButtonRenderer))]
namespace Android.Renderer
{
    public class CustomRadioButtonRenderer : ViewRenderer<CustomRadioButton, RadioButton>
    {
        RadioButton Cusradio;
        CustomRadioButton CusFormradio;        
        protected override void OnElementChanged(ElementChangedEventArgs<CustomRadioButton> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            Cusradio = new RadioButton(Context);
            if (Control == null)
            {
                CusFormradio = Element as CustomRadioButton;
                Cusradio.Text = CusFormradio.Text;
                Cusradio.SetTextColor(CusFormradio.TextColor.ToAndroid());                
                Cusradio.CheckedChange += Cusradio_CheckedChange;
                SetNativeControl(Cusradio);
            }                        
        }

        private void Cusradio_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            var Rdbtn = Element as CustomRadioButton;
            Rdbtn.Checked = e.IsChecked;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            var Rdbtn = Element as CustomRadioButton;
            if (e.PropertyName == CustomRadioButton.CheckedProperty.PropertyName)            
            {
                Control.Checked = Rdbtn.Checked;
            }
        }
    }
}