﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class ImpairmentsView : ContentView
    {
        public static bool Draft;
        public ModelPermits con;
        public ImpairmentsView(bool draft)
        {
            InitializeComponent();
            Draft = draft;
        }


        public static BindableProperty CollectionProperty = BindableProperty.Create<ImpairmentsView, ModelPermits>(o => o.Collection, null, BindingMode.TwoWay, null, CollectionChanged);

        public ModelPermits Collection
        {
            get { return (ModelPermits)GetValue(CollectionProperty); }
            set { SetValue(CollectionProperty, value); }
        }

        static void CollectionChanged(BindableObject bindable, ModelPermits oldValue, ModelPermits newValue)
        {
            var control = (ImpairmentsView)bindable;
            control.LblMonth.Text = newValue.Month;
            foreach (var pr in newValue.WorkCollection)
            {                
                control.SpHotWork.Children.Add(new WhiteView(Draft) { HWStatus = pr.HWStatus ?? HWStatus.EXPIRED, 
                    PermitId = pr.PermitId, CategoryName = pr.Category, Description = pr.Description, 
                ExpiryDate = pr.ExpiryDate, Margin = new Thickness(0, 10),
                    ActionTitle=(pr.HWStatus == HWStatus.CLOSED || pr.HWStatus == HWStatus.DRAFT)? "DUPLICATE IMPAIRMENT" :
                        "CLOSE AND SUBMIT IMPAIRMENT", BindingContext = pr, InfoType = "Impairment", FromArchieve = pr.FromArchieve });
            }
        }
    }
}
