﻿using System;
using System.ComponentModel;
using CoreGraphics;
using CustomControl;
using iOS.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CusFloatButton), typeof(CustomFloatButtonRender))]
namespace iOS.Renderer
{
    public class CustomFloatButtonRender : ViewRenderer<CusFloatButton, UIView>
    {
        CusFloatButton Fabbtn;  
        protected override void OnElementChanged(ElementChangedEventArgs<CusFloatButton> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;
            Fabbtn = (CusFloatButton)Element;
            //Element.HeightRequest = Element.WidthRequest;
            //Element.BorderRadius = (int)Element.WidthRequest / 2;
            //Element.BorderWidth = 0;
            //Element.Text = null; 
            Element.Children.Add(new Image { Source = "ic_add_white.png", HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand });
            
            //UIImage imgerror = UIImage.FromBundle("ic_add_white.png");
            //UIImageView vimg = new UIImageView(new CGRect(0, 0, 30, 30));
            //vimg.Image = imgerror;

            Layer.CornerRadius = (int)Element.WidthRequest / 2;
            Layer.BackgroundColor = ((CusFloatButton)Element).ButtonColor.ToCGColor();
            UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(HandleAction);
            AddGestureRecognizer(tapGesture);           
        }

        void HandleAction()
        {
            Fabbtn.Clicked.Invoke(Fabbtn, new EventArgs());
        }


        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            // add shadow
            Layer.ShadowRadius = 2.0f;
            Layer.ShadowColor = UIColor.Black.CGColor;
            Layer.ShadowOffset = new CGSize(1, 1);
            Layer.ShadowOpacity = 0.80f;
            Layer.ShadowPath = UIBezierPath.FromOval(Layer.Bounds).CGPath;
            Layer.MasksToBounds = false;

        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == CusFloatButton.ButtonColorProperty.PropertyName)
            {
                Control.BackgroundColor = ((CusFloatButton)Element).ButtonColor.ToUIColor();
            }
        }
    }
}
