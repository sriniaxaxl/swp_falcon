﻿using System;
namespace SiteWorkPermits.Platform
{
    public interface IDBPath
    {
        string GetDbPath();
        string GetPDFPath();
        bool DownloadOfflinePDFPath(string filePath);
    }
}
