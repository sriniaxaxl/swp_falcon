﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Platform;
using SiteWorkPermits.Resx;
using Plugin.Fingerprint;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using SiteWorkPermits.Services;
using System.Threading.Tasks;

namespace SiteWorkPermits.ViewPage
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            loginbtn.IsEnabled = false;

            if (Device.RuntimePlatform
                 == Device.Android)
            {
                appVersion.Text = CustomControl.Validation.CurrentAndroidVersion;
            }
            else {
                appVersion.Text = CustomControl.Validation.CurrentIOSVersion;
            }

            
        }

        protected override void OnAppearing()
        {
            //lblticket.Text = SiteWorkPermits.AppConfig.BASE_URL;
            App._isBiometricPass = false;
            App.PermitFlow = "new";
            tcImage.Source = "checkbox_off.png";
            loginbtn.IsEnabled = false;
            loginbtn.InnerBackground = Color.FromHex("#D6EAF8");
            base.OnAppearing();
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            if ((Xamarin.Forms.FileImageSource)tcImage.Source == "checkbox_off.png")
            {
                tcImage.Source = "checkbox_on.png";
                loginbtn.IsEnabled = true;
                loginbtn.InnerBackground = Color.FromHex("#003366");
            }
            else
            {
                tcImage.Source = "checkbox_off.png";
                loginbtn.IsEnabled = false;
                loginbtn.InnerBackground = Color.FromHex("#D6EAF8");
            }
        }
    }
}