﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits.ViewPage
{
    public partial class PrintImpairmentPage : ContentPage
    {
        public string note1 = AppResource.PRINT_LANDSCAPE_MODE_NOTE;
        public string note2 = AppResource.EMAIL_LABEL;
        public PrintImpairmentPage()
        {
            InitializeComponent();
            //lblNote.Text = "";
            //lblNote1.Text = note2;
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        void ReadioSel_Tapped(object sender, System.EventArgs e)
        {
            var md = (PrintImpairmentViewModel)this.BindingContext;
            CusGrid Selgd = (CusGrid)sender;
            Label sellbl =(Label)Selgd.Children[1];
            if(sellbl.Text == "A4")
            {
                md.SelFormat = "A4";
                ImgA4.Source = "radio_on.png";
                ImgUs.Source = "radio_off.png";
            }
            else
            {
                md.SelFormat = "USLETTER";
                ImgA4.Source = "radio_off.png";
                ImgUs.Source = "radio_on.png";
            }

            Debug.WriteLine(md.SelFormat);
        }
    }
}
