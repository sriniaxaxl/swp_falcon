﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SiteWorkPermits.Model;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class HotWorksView : ContentView
    {

        public static bool Draft;
        public ModelPermits con;
        public HotWorksView(bool draft)
        {
            InitializeComponent();
            Draft = draft;

        }

        public static BindableProperty CollectionProperty = BindableProperty.Create<HotWorksView, ModelPermits>(o => o.Collection, null, BindingMode.TwoWay, null, CollectionChanged);

        public ModelPermits Collection
        {
            get { return (ModelPermits)GetValue(CollectionProperty); }
            set { SetValue(CollectionProperty, value); }
        }

        static void CollectionChanged(BindableObject bindable, ModelPermits oldValue, ModelPermits newValue)
        {
            var control = (HotWorksView)bindable;
            control.LblMonth.Text = newValue.Month;
            foreach (var pr in newValue.WorkCollection)
            {
                control.SpHotWork.Children.Add(new WhiteView(Draft) { HWStatus = pr.HWStatus ?? HWStatus.EXPIRED, PermitId = pr.PermitId, CategoryName = pr.Category, Description = pr.Description, ExpiryDate = pr.ExpiryDate, Margin = new Thickness(0, 10), ActionTitle = "DUPLICATE PERMIT", BindingContext = pr, InfoType = "HotWorks", FromArchieve = pr.FromArchieve });
            }
        }
    }

    public class ModelPermits
    {
        public string Month { get; set; }

        public ObservableCollection<ModelWorkPermit> WorkCollection { get; set; }
    }

    public class ModelWorkPermit
    {
        public int PermitId { get; set; }
        public string Category { get; set; } // workorder
        public string CreatedAt { get; set; }
        public string Description { get; set; } // location
        public string HotworkDate { get; set; } // end date
        public string StartDate { get; set; } // end date
        public string ExpiryDate { get; set; } // end date
        public HWStatus? HWStatus { get; set; }
        private bool _FromArchieve =false;
        public bool FromArchieve
        {
            get
            {
                return _FromArchieve;
            }
            set
            {
                _FromArchieve = value;
            }
        }
    }
}
