﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SiteWorkPermits.ViewModel;
using Xamarin.Forms;
using SiteWorkPermits.Model;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using System.Diagnostics;
using CustomControl;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits
{
    public partial class NewHotWorksPage : ContentPage
    {
        ObservableCollection<TabInfo> TabCollection = new ObservableCollection<TabInfo>();
        ObservableCollection<PrecautionsMasterLocal> SectionCollection = new ObservableCollection<PrecautionsMasterLocal>();
        StackLayout SpScope;
        StackLayout SpFire;
        StackLayout SpToken;
        StackLayout SpAuthorization;
        public List<TabView> _tabViews = new List<TabView>();
        public string _currentTab = string.Empty;
        public NewHotWorksPage()
        {
            InitializeComponent();
            if (!App.FromScanPermit)
            {

                Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
                On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
                TabCollection.Add(new TabInfo { Title = AppResource.SCOPE_LABEL, IsSelected = true, view = null });
                TabCollection.Add(new TabInfo { Title = AppResource.FIRE_PREVENTION_LABEL, IsSelected = false, view = null });
                TabCollection.Add(new TabInfo { Title = AppResource.PRECAUTION_LABEL, IsSelected = false, view = null });
                TabCollection.Add(new TabInfo { Title = AppResource.AUTHORIZATION_LABEL, IsSelected = false, view = null });
                foreach (var d in TabCollection)
                {
                    TabView tab = new TabView();
                    tab.Title = d.Title;
                    tab.TabColor = d.IsSelected ? Color.FromHex("#00AFAD") : Color.Transparent;
                    tab.GestureRecognizers.Add(new TapGestureRecognizer(TabAction));
                    _tabViews.Add(tab);
                    Spworktabs.Children.Add(tab);
                }

               
                Pageview.IsVisible = true;
                FirePreventionview.IsVisible = false;
                Precautionview.IsVisible = false;
                Authorizationview.IsVisible = false;
                loaderLayout.IsVisible = true;
                //vm = ((NewHotWorksPageViewModel)BindingContext);
                //vm.LoaderLayout = true;
            }
        }

        Xamarin.Forms.ScrollView scscope;
        Xamarin.Forms.ScrollView scfire;
        Xamarin.Forms.ScrollView sctoken;
        Xamarin.Forms.ScrollView scauthorization;
        NewHotWorksPageViewModel vm;
        double FrscrlSize = 0;
        protected override void OnAppearing()
        {
            try
            {
                App.PermitLoadingDone = false;
                if (!App.FromScanPermit)
                {
                    base.OnAppearing();
                    Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        vm = ((NewHotWorksPageViewModel)BindingContext);
                        savebtn.Opacity = 0.3;
                        savebtn.IsEnabled = false;
                        Spworktabs.Children[1].Opacity = 0.5;
                        Spworktabs.Children[2].Opacity = 0.5;
                        Spworktabs.Children[3].Opacity = 0.5;





                        if (vm.ActionHotWork == HotWorkAction.Edit)
                        {
                            scscope = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = new ScopeView(vm) } };
                           

                        }
                        else
                        {
                            scscope = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = new ScopeView() } };
                            App.PermitLoadingDone = true;
                        }
                        Pageview.Content = new ContentView { Content = scscope };
                        Pageview.IsVisible = true;
                    });
                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                    {
                        vm.ChangeTabEvent += changeTab;
                        SpToken = new StackLayout();
                        try
                        {
                            SectionCollection = ((NewHotWorksPageViewModel)BindingContext).PrecautionCollectionLocal;
                        }
                        catch (Exception ex)
                        {

                        }

                        if (vm.ActionHotWork == HotWorkAction.Edit)
                        {
                            for (int i = 0; i < SectionCollection.Count; i++)
                            {
                                TokenView tokenView = new TokenView(SectionCollection[i])
                                {
                                    Title = SectionCollection[i].Description,
                                    NoteVisible = i == 0 ? true : false,
                                    Index = (i + 1).ToString(),
                                    Total = SectionCollection.Count.ToString(),
                                    Action = SectionCollection[i].PrecautionActions
                                };

                                if (SectionCollection[i].PrecautionID == 6)
                                {
                                    tokenView.IsAdditional = true;
                                    vm.NoOfFireHouse = Convert.ToInt32(SectionCollection[i].NoOfFirehouses);
                                    vm.NoOfFireExtinguiser = Convert.ToInt32(SectionCollection[i].NoOfFireExtinguiser);
                                }

                                tokenView.BindingContext = SectionCollection[i];
                                SpToken.Children.Add(tokenView);
                            }
                            var firepv = new FirePreventionView(vm);
                            firepv.LastElementFocus += Firepv_LastElementFocus;

                            scfire = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = firepv } };
                            sctoken = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = SpToken } };
                            scauthorization = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = new AuthorizationView(vm) } };
                        }
                        else
                        {
                            if (SectionCollection != null)
                            {
                                for (int i = 0; i < SectionCollection.Count; i++)
                                {
                                    TokenView tokenView = new TokenView
                                    {
                                        Title = SectionCollection[i].Description,
                                        NoteVisible = i == 0 ? true : false,
                                        Index = (i + 1).ToString(),
                                        Total = SectionCollection.Count.ToString(),
                                        Action = SectionCollection[i].PrecautionActions
                                    };
                                    if (i == 5)
                                    {
                                        tokenView.IsAdditional = true;
                                    }
                                    tokenView.BindingContext = SectionCollection[i];
                                    SpToken.Children.Add(tokenView);
                                }
                            }
                            var firepv = new FirePreventionView();
                            firepv.LastElementFocus += Firepv_LastElementFocus;
                            firepv.LastElementUnFocus += Firepv_LastElementUnFocus;
                            scfire = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = firepv } };

                            sctoken = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = SpToken } };
                            scauthorization = new Xamarin.Forms.ScrollView { Content = new CreationPanelView { SectionView = new AuthorizationView() } };
                        }

                        if (App.isImpairementEditorDuplicate == "Archieve")
                        {
                            savebtn.IsVisible = false;
                            scscope.Content.IsEnabled = false;
                            scfire.Content.IsEnabled = false;
                            sctoken.Content.IsEnabled = false;
                            scauthorization.Content.IsEnabled = false;
                        }
                        else
                        {
                            savebtn.IsVisible = true;
                            scscope.Content.IsEnabled = true;
                            scfire.Content.IsEnabled = true;
                            sctoken.Content.IsEnabled = true;
                            scauthorization.Content.IsEnabled = true;
                        }
                        //scfire.Scrolled += Scfire_Scrolled;
                        Pageview.Content = new ContentView { Content = scscope };
                        FirePreventionview.Content = new ContentView { Content = scfire };
                        Precautionview.Content = new ContentView { Content = sctoken };
                        Authorizationview.Content = new ContentView { Content = scauthorization };
                        loaderLayout.IsVisible = false;
                        FrscrlSize = FirePreventionview.Content.Height;
                        App.PermitLoadingDone = true;
                        return false; // True = Repeat again, False = Stop the timer
                    });
                  
                }

            }
            catch (Exception ex)
            {

            }
            finally {
               
            }
            }
         
        private void Firepv_LastElementFocus(object sender, EventArgs e)
        {            
            if (Device.RuntimePlatform == Device.Android)
            {
                var pr = (CreationPanelView)scfire.Content;
                CusGrid Gdcont = (CusGrid)pr.Content;
                var CusStatic = Gdcont.Children[1];
                CusStatic.IsVisible = true;
            }
        }

        private void Firepv_LastElementUnFocus(object sender, EventArgs e)
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                var pr = (CreationPanelView)scfire.Content;
                CusGrid Gdcont = (CusGrid)pr.Content;
                var CusStatic = Gdcont.Children[1];
                CusStatic.IsVisible = false;
            }
        }
               
        double Scrlht = 0;
        private void Scfire_Scrolled(object sender, ScrolledEventArgs e)
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                Xamarin.Forms.ScrollView FirescrollView = (Xamarin.Forms.ScrollView)sender;
                var CurrentPosition = FirescrollView.Height + e.ScrollY + 5;
                if (Scrlht == 0)
                {
                    Scrlht = FirescrollView.ContentSize.Height;
                }
                if (CurrentPosition > Scrlht)
                {
                    if (FirescrollView.Content.HeightRequest <= (Scrlht + FirescrollView.Height * 0.3))
                    {
                        FirescrollView.Content.HeightRequest = Scrlht + FirescrollView.Height * 0.3;
                    }
                }
                else
                {
                    FirescrollView.Content.HeightRequest = Scrlht;
                }
                Debug.WriteLine("Scroll Content : " + FirescrollView.ContentSize.Height);
                Debug.WriteLine("Scroll Height : " + FirescrollView.Height);
                Debug.WriteLine("Current Scroll Y Point : " + e.ScrollY);
            }
        }


        #region Tab Selection
        private void changeTab(string HWstatus)
        {
            var ss = TabCollection;
            var s = ((NewHotWorksPageViewModel)BindingContext);
            var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).FirstOrDefault());

            //(TabCollection[1] as TabInfo)
           
            if (HWstatus.Equals("Previous"))
            {


               




                if (Selind - 1 > 0)
                {

                    if (Selind - 1 == 1)
                    {
                        Spworktabs.Children[1].Opacity = 1;
                        Spworktabs.Children[2].Opacity = 0.5;
                        Spworktabs.Children[3].Opacity = 0.5;
                    }
                    else if (Selind - 1 == 2)
                    {
                        Spworktabs.Children[1].Opacity = 1;
                        Spworktabs.Children[2].Opacity = 1;
                        Spworktabs.Children[3].Opacity = 0.5;
                    }


                    s.IsPreviousBtnlabel = true;
                    s.IsContinueBtnlabel = true;
                    s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                    savebtn.Opacity = 0.3;
                    savebtn.IsEnabled = false;
                }
                else
                {
                    s.IsPreviousBtnlabel = false;
                    s.IsContinueBtnlabel = true;
                    savebtn.Opacity = 0.3;
                    savebtn.IsEnabled = false;
                    Spworktabs.Children[1].Opacity = 0.5;
                    Spworktabs.Children[2].Opacity = 0.5;
                    Spworktabs.Children[3].Opacity = 0.5;
                }
                TabView tab = (TabView)Spworktabs.Children[Selind - 1];

                SelectTabCommon(tab);
            }
            else
            {
                if (Selind + 1 == 4)
                {
                    s.ShowSubmitHWPoopup = true;
                    savebtn.Opacity = 1;
                    savebtn.IsEnabled = true;
                    Spworktabs.Children[1].Opacity = 1;
                    Spworktabs.Children[2].Opacity = 1;
                    Spworktabs.Children[3].Opacity = 1;
                }
                else
                {
                    TabView tab = (TabView)Spworktabs.Children[Selind + 1];
                    SelectTabCommon(tab);


                    if (Selind + 1 == 1) {
                        Spworktabs.Children[1].Opacity = 1;
                        Spworktabs.Children[2].Opacity = 0.5;
                        Spworktabs.Children[3].Opacity = 0.5;
                    }
                    else if (Selind + 1 == 2) {
                        Spworktabs.Children[1].Opacity = 1;
                        Spworktabs.Children[2].Opacity = 1;
                        Spworktabs.Children[3].Opacity = 0.5;
                    }


                    if (Selind + 1 == 3)
                    {
                        s.IsContinueBtnlabel = true;
                        s.ContinueBtnlabel = AppResource.PRINT_AND_SIGN_LABEL;
                        s.IsPreviousBtnlabel = true;
                        savebtn.Opacity = 1;
                        savebtn.IsEnabled = true;
                        Spworktabs.Children[1].Opacity = 1;
                        Spworktabs.Children[2].Opacity = 1;
                        Spworktabs.Children[3].Opacity = 1;
                    }
                    else
                    {
                        s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                        s.IsContinueBtnlabel = true;
                        s.IsPreviousBtnlabel = true;
                        savebtn.Opacity = 0.3;
                        savebtn.IsEnabled = false;
                    }
                }
            }
        }


        public void SelectTabCommon(View obj) {

            var s = ((NewHotWorksPageViewModel)BindingContext);
            Deselectview();
            TabView selv = (TabView)obj;
            _currentTab = selv.Title;
            Selectview(selv);
        }

        public void SelectionTab(View obj)
        {
            if (App.PermitFlow == "edit")
            {

                var s = ((NewHotWorksPageViewModel)BindingContext);
                Deselectview();
                TabView selv = (TabView)obj;
                _currentTab = selv.Title;

                if (selv.Title.ToUpper() == "FIRE PREVENTION MEASURES")
                {
                    s.IsContinueBtnlabel = true;
                    s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                    s.IsPreviousBtnlabel = true;
                    savebtn.Opacity = 0.3;
                    savebtn.IsEnabled = false;
                    //if (Device.Idiom == TargetIdiom.Phone)
                    //{
                    //    await hotworksScroll.ScrollToAsync(0, 0, true);
                    //}
                }
                else if (selv.Title.ToUpper() == "PRECAUTIONS TAKEN")
                {
                    s.IsContinueBtnlabel = true;
                    s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                    s.IsPreviousBtnlabel = true;
                    savebtn.Opacity = 0.3;
                    savebtn.IsEnabled = false;
                    //if (Device.Idiom == TargetIdiom.Phone)
                    //{
                    //    await hotworksScroll.ScrollToAsync(hotworksScroll.ContentSize.Width, 0, true);
                    //}
                }
                else if (selv.Title.ToUpper() == "SCOPE")
                {
                    s.IsContinueBtnlabel = true;
                    s.ContinueBtnlabel = AppResource.CONTINUE_LABEL.ToUpper();
                    s.IsPreviousBtnlabel = false;
                    savebtn.Opacity = 0.3;
                    savebtn.IsEnabled = false;
                }
                else if (selv.Title.ToUpper() == "AUTHORIZATION")
                {
                    s.IsContinueBtnlabel = true;
                    s.ContinueBtnlabel = AppResource.PRINT_AND_SIGN_LABEL;
                    s.IsPreviousBtnlabel = true;
                    savebtn.Opacity = 1;
                    savebtn.IsEnabled = true;
                }

                Selectview(selv);

            }
            
        }

        private void TabAction(View arg1, object arg2)
        {
            SelectionTab(arg1);
        }

        private void Deselectview()
        {
            var presel = TabCollection.Where(p => p.IsSelected).FirstOrDefault();
            var Selind = TabCollection.IndexOf(TabCollection.Where(p => p.IsSelected).FirstOrDefault());
            TabView tab = (TabView)Spworktabs.Children[Selind];
            tab.TabColor = Color.Transparent;
            TabCollection.Where(p => p.IsSelected).FirstOrDefault().IsSelected = false;
        }

        void Selectview(TabView v)
        {
            TabView selv = v;
            var seltext = selv.Title;
            selv.TabColor = Color.FromHex("#00AFAD");
            var seltab = TabCollection.Where(s => s.Title == seltext).FirstOrDefault();
            seltab.IsSelected = true;
            var selind = TabCollection.IndexOf(TabCollection.Where(s => s.Title == seltext).FirstOrDefault());
            switch (selind)
            {
                case 0:
                    Pageview.IsVisible = true;
                    FirePreventionview.IsVisible = false;
                    Precautionview.IsVisible = false;
                    Authorizationview.IsVisible = false;
                    break;
                case 1:
                    Pageview.IsVisible = false;
                    FirePreventionview.IsVisible = true;
                    Precautionview.IsVisible = false;
                    Authorizationview.IsVisible = false;
                    break;
                case 2:
                    Pageview.IsVisible = false;
                    FirePreventionview.IsVisible = false;
                    Precautionview.IsVisible = true;
                    Authorizationview.IsVisible = false;
                    break;
                case 3:
                    Pageview.IsVisible = false;
                    FirePreventionview.IsVisible = false;
                    Precautionview.IsVisible = false;
                    Authorizationview.IsVisible = true;
                    break;
            }
            hotworksScroll.ScrollToAsync(selv, ScrollToPosition.Start, true);

            //if (TabCollection.Where(s => s.IsSelected).First().view != null)
            //{
            //    Pageview.Content = TabCollection.Where(s => s.IsSelected).First().view.Content;
            //}
        }
        #endregion

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
           // var vieww = (CusGrid)sender;
            vm.showHWScanPopup = !vm.showHWScanPopup;
            //vm.showHWClosePopup = !vm.showHWClosePopup;
            //vm.LoaderLayout = !vm.LoaderLayout;
        }

        private void TapGestureRecognizer_Tapped1(object sender, EventArgs e)
        {
            // var vieww = (CusGrid)sender;
            //vm.showHWScanPopup = !vm.showHWScanPopup;
            vm.showHWClosePopup = !vm.showHWClosePopup;
           // vm.LoaderLayout = !vm.LoaderLayout;
        }

        private void TapGestureRecognizer_Tapped3(object sender, EventArgs e)
        {
            // var vieww = (CusGrid)sender;
            //vm.showHWScanPopup = !vm.showHWScanPopup;
          //  vm.showHWClosePopup = !vm.showHWClosePopup;
            // vm.LoaderLayout = !vm.LoaderLayout;
        }

        private void TapGestureRecognizer_Tapped2(object sender, EventArgs e)
        {
            // var vieww = (CusGrid)sender;
           // vm.showHWScanPopup = !vm.showHWScanPopup;
           // vm.showHWClosePopup = !vm.showHWClosePopup;
           // vm.LoaderLayout = !vm.LoaderLayout;
        }
    }

    public class ModelQuestion
    {
        public string Title { get; set; }
        public TokenAction Action { get; set; }
        public string NoOfFirehouses { get; set; }
        public string NoOfFireExtinguiser { get; set; }
        public enum TokenAction
        {
            Skip,
            NA,
            Yes
        }
    }
}




