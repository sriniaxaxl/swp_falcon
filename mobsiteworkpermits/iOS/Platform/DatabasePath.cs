﻿using System;
using System.IO;
using System.Reflection;
using SiteWorkPermits.iOS.Platform;
using SiteWorkPermits.Platform;
using Foundation;

[assembly: Xamarin.Forms.Dependency(typeof(DatabasePath))]
namespace SiteWorkPermits.iOS.Platform
{
    public class DatabasePath : IDBPath
    {
        public string GetDbPath()
        {
            var dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                      "..", "Library", "CLPCoreDB.db");

            App.connection = new SQLite.SQLiteConnection(dbPath);
            //string password = "#HM#=3#s2ydEC_+";

            if (!string.IsNullOrEmpty(AppConfig.passwordKey))
            {
                App.connection.Query<int>("PRAGMA key='?'", AppConfig.passwordKey);
            }

            return dbPath;
            
        }

        public string GetPDFPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); 
        }

        public bool DownloadOfflinePDFPath(string fileName)
        {
            try
            {
               // AssetManager assets = ((MainActivity)Forms.Context).Assets;
                string fileName1 = fileName;

                var localFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                var MyFilePath = System.IO.Path.Combine(localFolder, fileName1);
                var filePath = NSBundle.MainBundle.PathForResource(fileName.Split('.')[0], "pdf");
                using (var streamReader = new StreamReader(filePath))
                {
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        var bytes = memstream.ToArray();
                        //write to local storage
                        System.IO.File.WriteAllBytes(MyFilePath, bytes);
                        MyFilePath = $"file://{localFolder}/{fileName}";
                    }
                }
                Xamarin.Forms.Application.Current.Properties["iOSDownloadedFile"] = MyFilePath;
                return true;
            }
            catch (System.Exception e)
            {
                return false;
            }
        }
    }
}