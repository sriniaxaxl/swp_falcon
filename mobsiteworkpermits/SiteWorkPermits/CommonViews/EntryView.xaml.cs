﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using CustomControl;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class EntryView : ContentView, INotifyPropertyChanged
    {
        public event EventHandler EntryFocuesed;
        public event EventHandler EntryUnFocuesed;
        public static BindableProperty TitleProperty = BindableProperty.Create<EntryView, string>(o => o.Title,string.Empty, BindingMode.TwoWay, null, HandleBindingPropertyChangedDelegate);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set
            {
                SetValue(TitleProperty, value);
                OnPropertyChanged();
            }
        }

        static void HandleBindingPropertyChangedDelegate(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (EntryView)bindable;
            control.Lbltitle.Text = newValue;
        }
          
        public static readonly BindableProperty MaxLengthProperty = BindableProperty.Create<EntryView, int>(o => o.MaxLength, 100, BindingMode.TwoWay, null, null);

        public int MaxLength
        {
            get
            {
                return (int)GetValue(MaxLengthProperty);
            }
            set
            {
                SetValue(MaxLengthProperty, value);
                OnPropertyChanged();
            }
        }

        public static readonly BindableProperty IsEmailProperty
        = BindableProperty.Create<EntryView, bool>(o => o.IsEmail, false, BindingMode.TwoWay, null, null);

        public bool IsEmail
        {
            get
            {
                return (bool)GetValue(IsEmailProperty);
            }
            set
            {
                SetValue(IsEmailProperty, value);
                OnPropertyChanged();
            }
        }



        public static BindableProperty ValueProperty = BindableProperty.Create<EntryView, string>(o => o.Value, string.Empty, BindingMode.TwoWay, null, ValuePropertyChanged);

        public string Value
        {
            get
            {
                return (string)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
                OnPropertyChanged();
            }
        }

        public static BindableProperty KeyboardProperty = BindableProperty.Create<EntryView, Keyboard>(o => o.Keyboard, Keyboard.Text, BindingMode.TwoWay, null, KeyboardPropertyChanged);

        private static void KeyboardPropertyChanged(BindableObject bindable, Keyboard oldValue, Keyboard newValue)
        {
            var control = (EntryView)bindable;
            control.txtval.Keyboard = newValue;
        }

        public Keyboard Keyboard
        {
            get
            {
                return (Keyboard)GetValue(KeyboardProperty);
            }
            set
            {
                SetValue(KeyboardProperty, value);
                OnPropertyChanged();
            }
        }


        public static void ValuePropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            if (!string.IsNullOrEmpty(newValue))
            {
                var control = (EntryView)bindable;
                if (newValue.Length <= control.MaxLength)
                {
                    control.txtval.Text = newValue;
                    Debug.WriteLine("New Value : " + newValue);
                }
                else
                {
                    var oldval = control.txtval.Text.Substring(0, control.txtval.Text.Length - 1);
                    control.Value = oldval;
                    return;
                    //control.txtval.Text = control.txtval.Text.Substring(0, control.txtval.Text.Length - 1);
                }

                if (control.IsEmail)
                {
                    control.Value = Regex.Replace(control.Value, "[^\\w\\@_. ]", "");
                }
                else
                {
                    control.Value = Regex.Replace(control.Value, "[^\\w\\ _]", "");
                }
            }
        }

        public static BindableProperty PlaceHolderProperty = BindableProperty.Create<EntryView, string>(o => o.PlaceHolder, string.Empty, BindingMode.TwoWay, null, PlaceHolderPropertyChanged);

        public string PlaceHolder
        {
            get { return (string)GetValue(PlaceHolderProperty); }
            set { SetValue(PlaceHolderProperty, value); }
        }

        static void PlaceHolderPropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (EntryView)bindable;
            control.txtval.Placeholder = newValue;
        }

        public EntryView()
        {
            InitializeComponent();
            //txtval.Focused +=Txtval_Focused;
            txtval.PropertyChanged +=Txtval_PropertyChanged;
        }

        private void Txtval_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == CusEntry.IsFocusedProperty.PropertyName)
            {
                if(txtval.IsFocused)
                {
                    if (EntryFocuesed != null)
                    {
                        EntryFocuesed.Invoke(this, new EventArgs());
                    }
                }
                else
                {
                    if (EntryUnFocuesed != null)
                    {
                        EntryUnFocuesed.Invoke(this, new EventArgs());
                    }
                }
                Debug.WriteLine("Last Element Focused : " + txtval.IsFocused);
            }
        }

              
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
