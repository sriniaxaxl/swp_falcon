﻿using System;
using System.Collections.Generic;
using Android.OS;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Platform;
using Firebase.Analytics;
using Xamarin.Forms;

[assembly: Dependency(typeof(AnalyticsServiceDroid))]
namespace SiteWorkPermits.Droid.Platform
{
    public class AnalyticsServiceDroid : IAnalyticsService
    {
        public AnalyticsServiceDroid()
        {
        }

        public void LogEvent(string eventId)
        {
            LogEvent(eventId, null);
        }

        public void LogEvent(string eventId, string paramName, string value)
        {
            LogEvent(eventId, new Dictionary<string, string>
            {
                {paramName, value}
            });
        }

        public void LogEvent(string eventId, IDictionary<string, string> parameters)
        {
            var fireBaseAnalytics = FirebaseAnalytics.GetInstance(Forms.Context);

            if (parameters == null)
            {
                fireBaseAnalytics.LogEvent(eventId, null);
                return;
            }
            var bundle = new Bundle();
            foreach (var item in parameters)
            {
                bundle.PutString(FirebaseAnalytics.Param.ItemId, item.Key);
                bundle.PutString(FirebaseAnalytics.Param.ItemName, item.Value);
            }
            fireBaseAnalytics.LogEvent(FirebaseAnalytics.Event.SelectContent, bundle);
        }

        public void LogScreen(string screenName)
        {
            var fireBaseAnalytics = FirebaseAnalytics.GetInstance(Forms.Context);
            //var bundle = new Bundle();          
            //bundle.PutString(FirebaseAnalytics.Param.ItemCategory, "Screen");
            //bundle.PutString(FirebaseAnalytics.Param.ItemName, screenName);


            //for inbuilt firebase event
            //fireBaseAnalytics.SetCurrentScreen((MainActivity)Forms.Context, screenName, null);


            // Screen View Implementation GTM (for logging GA events through GTM)
            Bundle screenLog = new Bundle();
            screenLog.PutString("screen_name", screenName);
            fireBaseAnalytics.LogEvent("screenview", screenLog);
        }
    }
}
