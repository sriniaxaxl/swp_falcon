﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public class TabInfo : INotifyPropertyChanged
    {
        public string Title { get; set; }
        public bool IsSelected { get; set; }
        public ContentView view { get; set; }

        public ObservableCollection<ModelPermits> Collection { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
