﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IMajorImpairmentsService
    {
        IEnumerable<ImpairmentMeasureMaster> GetImpairmentMeasureMaster();

        ImpairmentMeasureMaster GetImpairmentMeasureMasterById(int id);

        bool AddImpairmentMeasureMaster(ImpairmentMeasureMaster impairmentMeasureMaster);

        void RemoveAllData();
    }
}
