﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using SiteWorkPermits.Model;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Prism.Common;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class AddingView : ContentView
    {
        public static BindableProperty ValcollectionProperty = BindableProperty.Create<AddingView, ObservableCollection<FirewatchPersonsLocal>>(o => o.Valcollection, null,
                                                                                                                                         BindingMode.TwoWay, null, ValcollectionChanged);

        public ObservableCollection<FirewatchPersonsLocal> Valcollection
        {
            get
            {
                return (ObservableCollection<FirewatchPersonsLocal>)GetValue(ValcollectionProperty);
            }
            set
            {
                SetValue(ValcollectionProperty, value);
                OnPropertyChanged();
            }
        }


        public static BindableProperty EditValcollectionProperty = BindableProperty.Create<AddingView, ObservableCollection<FirewatchPersonsLocal>>(o => o.EditValcollection, null,
                                                                                                                                               BindingMode.TwoWay, null, EditValcollectionChanged);

        public ObservableCollection<FirewatchPersonsLocal> EditValcollection
        {
            get
            {
                return (ObservableCollection<FirewatchPersonsLocal>)GetValue(EditValcollectionProperty);
            }
            set
            {
                SetValue(EditValcollectionProperty, value);
                OnPropertyChanged();
            }
        }

        private static void EditValcollectionChanged(BindableObject bindable, ObservableCollection<FirewatchPersonsLocal> oldValue, ObservableCollection<FirewatchPersonsLocal> newValue)
        {
            var control = (AddingView)bindable;
            control.Valcollection = newValue;
            foreach (var d in newValue)
            {
                control.viewadd(d);
            }
        }

        private void viewadd(FirewatchPersonsLocal d)
        {
            ListAddview adview = new ListAddview()
            {
                Title = d.PersonName,
                DurationDisplay = DurationDisplay,
               // StratTime = d.StartTime==DateTime.MinValue?DateTime.Now: d.StartTime,
             //   EndTime = d.EndTime == DateTime.MinValue ? DateTime.Now : d.StartTime
            };
            adview.PropertyChanged += Adview_PropertyChanged;
            adview.DeleteEvent += DeleteEvent;
            Spval.Children.Add(adview);
        }

        public AddingView()
        {
            InitializeComponent();
            DurationDisplay = false;
            StartTimePicker.Timeselected += StartTimePicker_Timeselected;
            EndTimePicker.Timeselected += EndTimePicker_Timeselected;
            Txtval.TextChanged += Txtval_TextChanged;
            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "ClearFireWatcher",  (sender) =>
            {
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                if(Valcollection!=null){
                    Valcollection.Clear();
                }
                if(Spval!=null){
                    Spval.Children.Clear();    
                }
            });
        }

        private void Txtval_TextChanged(object sender, TextChangedEventArgs e)
        {
            var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            if (curpage.BindingContext is NewHotWorksPageViewModel)
            {
                var model = (NewHotWorksPageViewModel)curpage.BindingContext;
                if (FireType == FireType.Person)
                {
                    model.CurrentHotWorkPerson.PersonName = e.NewTextValue;
                }
                else if (FireType == FireType.During)
                {
                    model.CurrentFireWatchDuringPerson.PersonName = e.NewTextValue;
                }
                else
                {
                    model.CurrentFireWatchAfterPerson.PersonName = e.NewTextValue;
                }
            }
        }


        private void StartTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            
            if (EndTime.TimeOfDay >= e.NewTime.TimeOfDay)
            {
                StartTimePicker.Text = e.NewTime.ToString("HH:mm");
                StratTime = e.NewTime;
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                if (curpage.BindingContext is NewHotWorksPageViewModel)
                {
                    var model = (NewHotWorksPageViewModel)curpage.BindingContext;

                    if (FireType == FireType.During)
                    {
                        if (StratTime.TimeOfDay < model.StartHotWork.TimeOfDay)
                        {
                            StratTime = model.StartHotWork;
                            EndTime = model.EndHotWork;
                            curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_During, "Ok");
                        }
                        else
                        {
                            model.CurrentFireWatchDuringPerson.StartTime = StratTime;
                        }
                    }
                    else
                    {
                        if (model.EndHotWork.TimeOfDay > StratTime.TimeOfDay)
                        {
                            StratTime = model.EndHotWork;
                            EndTime = StratTime.AddHours(1);
                            curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_After, "Ok");
                        }
                        else
                        {
                            model.CurrentFireWatchAfterPerson.StartTime = StratTime;
                        }
                    }
                }
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_During , "Ok");
            }
        }       

        private void EndTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            if (e.NewTime.TimeOfDay >= StratTime.TimeOfDay)
            {
                EndTimePicker.Text = e.NewTime.ToString("HH:mm");
                EndTime = e.NewTime;
                var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
                if (curpage.BindingContext is NewHotWorksPageViewModel)
                {
                    var model = (NewHotWorksPageViewModel)curpage.BindingContext;
                    if (FireType == FireType.During)
                    {
                        if (EndTime.TimeOfDay > model.EndHotWork.TimeOfDay)
                        {
                            EndTime = model.EndHotWork;
                            curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_During, "Ok");
                        }
                        else
                        {
                            model.CurrentFireWatchDuringPerson.EndTime = EndTime;
                        }
                    }
                    else
                    {
                        if (model.EndHotWork.TimeOfDay > EndTime.TimeOfDay)
                        {
                            EndTime = model.EndHotWork.AddHours(1);
                            curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_After, "Ok");
                        }
                        else
                        {
                            model.CurrentFireWatchAfterPerson.EndTime = EndTime;
                        }

                    }
                }
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_During, "Ok");
            }
        }

        private static void ValcollectionChanged(BindableObject bindable, ObservableCollection<FirewatchPersonsLocal> oldValue, ObservableCollection<FirewatchPersonsLocal> newValue)
        {
            var control = (AddingView)bindable;
        }

        public string Title
        {
            get
            {
                return Lbltitle.Text;
            }
            set
            {
                Lbltitle.Text = value;
            }
        }

        public static BindableProperty FireTypeProperty = BindableProperty.Create<AddingView, FireType>(o => o.FireType, FireType.None, BindingMode.TwoWay, null, FireTypePropertyChanged);

        public FireType FireType
        {
            get { return (FireType)GetValue(FireTypeProperty); }
            set { SetValue(FireTypeProperty, value); }
        }

        static void FireTypePropertyChanged(BindableObject bindable, FireType oldValue, FireType newValue)
        {
            var control = (AddingView)bindable;
            var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            NewHotWorksPageViewModel model = null;
            if (curpage.BindingContext is NewHotWorksPageViewModel)
            {
                model = (NewHotWorksPageViewModel)curpage.BindingContext;
            }
            if (model != null)
            {

                if (newValue == FireType.Person)
                {
                    control.GdTimeDisplay.IsVisible = false;
                }
                else
                {
                    if (newValue == FireType.During)
                    {
                        control.StratTime = model.StartHotWork;
                        control.EndTime = model.EndHotWork;
                        model.CurrentFireWatchDuringPerson.StartTime = model.StartHotWork;
                        model.CurrentFireWatchDuringPerson.EndTime = model.EndHotWork;
                    }
                    else
                    {
                        control.StratTime = model.EndHotWork;
                        control.EndTime = model.EndHotWork.AddHours(1);
                        model.CurrentFireWatchAfterPerson.StartTime = model.EndHotWork;
                        model.CurrentFireWatchAfterPerson.EndTime = model.EndHotWork.AddHours(1);
                    }
                    control.GdTimeDisplay.IsVisible = true;
                }
            }
            }


        public BindableProperty StartTimeProperty = BindableProperty.Create<AddingView, DateTime>(o => o.StratTime, DateTime.Now, BindingMode.TwoWay, null, StartTimePropertyChanged);

        public DateTime StratTime
        {
            get { return (DateTime)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        static void StartTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var control = (AddingView)bindable;
            control.StratTime = newValue;
            control.StartTimePicker.Text = Helper.TimeFormatSet(newValue);

        }

        public BindableProperty EndTimeProperty = BindableProperty.Create<AddingView, DateTime>(o => o.EndTime, DateTime.Now, BindingMode.TwoWay, null, EndTimePropertyChanged);

        public DateTime EndTime
        {
            get { return (DateTime)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }

        static void EndTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var control = (AddingView)bindable;
            control.EndTime = newValue;
            control.EndTimePicker.Text = Helper.TimeFormatSet(newValue);
        }

        public bool DurationDisplay { get; set; }

        public string AddImg
        {
            get
            {
                return ((FileImageSource)imgadd.Source).File;
            }
            set
            {
                imgadd.Source = value;
            }
        }

        private void Person_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            CusEntry CurEntry = (CusEntry)sender;
            CurEntry.Text = Regex.Replace(CurEntry.Text, "[^\\w\\ _]", "");
        }

        private void Val_Tapped(object sender, System.EventArgs e)
        {
            if (Valcollection == null)
            {
                Valcollection = new ObservableCollection<FirewatchPersonsLocal>();
            }

            NewHotWorksPageViewModel model = null;
            var curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            if (curpage.BindingContext is NewHotWorksPageViewModel)
            {
                model = (NewHotWorksPageViewModel)curpage.BindingContext;
            }

            if(FireType == FireType.During){
                StratTime = model.StartHotWork;
                EndTime = model.EndHotWork;    
            }else{
                StratTime = model.EndHotWork;
                EndTime = model.EndHotWork.AddHours(1);
            }


            var FireStartDate = new DateTime();
            var FireEndDate = new DateTime();
            FireStartDate = StratTime;
            FireEndDate = EndTime;
            Valcollection.Add(new FirewatchPersonsLocal
            {
                PersonName = Convert.ToString(Txtval.Text),
                StartTime = FireStartDate,
                EndTime = FireEndDate
            });

            ListAddview adview = new ListAddview();
            adview.Title = Txtval.Text;
            adview.DurationDisplay = DurationDisplay;
            adview.StratTime = FireStartDate;
            adview.EndTime = FireEndDate;

            //{ Title = Txtval.Text, DurationDisplay = DurationDisplay, StratTime = FireStartDate, EndTime = FireEndDate };
            adview.PropertyChanged += Adview_PropertyChanged;
            adview.DeleteEvent += DeleteEvent;

            Spval.Children.Add(adview);
            Txtval.Text = string.Empty;
            if (FireType == FireType.Person)
            {
                GdTimeDisplay.IsVisible = false;
            }
            else
            {
                if (FireType == FireType.During)
                {
                    StratTime = model.StartHotWork;
                    EndTime = model.EndHotWork;
                }
                else
                {
                    StratTime = model.EndHotWork;
                    EndTime = model.EndHotWork.AddHours(1);
                }
                GdTimeDisplay.IsVisible = true;
            }
        }

        public void Adview_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            ListAddview listAddview = (ListAddview)sender;
            var Curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            if (Curpage.BindingContext is NewHotWorksPageViewModel)
            {
                var model = (NewHotWorksPageViewModel)Curpage.BindingContext;
                if (e.PropertyName == "StratTime")
                {
                    var sttime = listAddview.StratTime;
                    if (FireType == FireType.During)
                    {
                        var FWDuring = sttime.TimeOfDay;
                        if (model.StartHotWork.TimeOfDay <= FWDuring && FWDuring < model.EndHotWork.TimeOfDay)
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().StartTime = sttime;
                        }
                        else
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().StartTime = model.StartHotWork;
                            Curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_During, "Ok");
                        }
                    }
                    else
                    {
                        var FWAfter = sttime.TimeOfDay;
                        if (model.EndHotWork.TimeOfDay <= FWAfter)
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().StartTime = sttime;
                        }
                        else
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().StartTime = model.EndHotWork;
                            Curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_After, "Ok");
                        }
                    }
                }
                else if (e.PropertyName == "EndTime")
                {
                    var endtime = listAddview.EndTime;
                    if (FireType == FireType.During)
                    {
                        var FWDuring = endtime.TimeOfDay;
                        if (model.StartHotWork.TimeOfDay < FWDuring && FWDuring <= model.EndHotWork.TimeOfDay)
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().EndTime = endtime;
                        }
                        else
                        {
                            //listAddview.EndTime = model.EndHotWork;
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().EndTime = model.EndHotWork;
                            Curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_During, "Ok");
                            return;
                        }
                    }
                    else
                    {
                        var FWAfter = endtime.TimeOfDay;
                        if (model.EndHotWork.TimeOfDay < FWAfter && listAddview.StratTime.TimeOfDay < FWAfter)
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().EndTime = endtime;
                        }
                        else
                        {
                            Valcollection.Where(s => s.PersonName == listAddview.Title).First().EndTime = model.EndHotWork.AddHours(1);
                            Curpage.DisplayAlert(string.Empty, CustomControl.Validation.Time_Alert_After, "Ok");
                        }
                    }
                }
            }
        }

        private void DeleteEvent(object sender, ValueArgs e)
        {
            var val = e.value;
            var DelItem = Valcollection.Where(s => s.PersonName == val).FirstOrDefault();
            if (DelItem != null)
            {
                Valcollection.Remove(DelItem);
            }
            Spval.Children.Remove((ListAddview)sender);
        }
    }


    public enum FireType
    {
        Person,
        During,
        After,
        None
    }
}
