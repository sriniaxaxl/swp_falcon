﻿using System;
using System.ComponentModel;
using CoreGraphics;
using CustomControl;
using Foundation;
using iOS.Renderer;
using SLCheckBoxFinal;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomCheckbox), typeof(CustomCheckboxRenderer))]
namespace iOS.Renderer
{
    public class CustomCheckboxRenderer : ViewRenderer<CustomCheckbox, BEMCheckBox>, IBEMCheckBoxDelegate
    {
        CustomCheckbox CusFormCheckbox;
        BEMCheckBox checkBox;
        protected override void OnElementChanged(ElementChangedEventArgs<CustomCheckbox> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
                return;            

            CusFormCheckbox = Element as CustomCheckbox;
                       

            if (CusFormCheckbox == null) 
                return;


            checkBox = new BEMCheckBox();
            //checkBox.Frame = new CGRect(0, 0, 16, 16);

            checkBox.BoxType = BEMBoxType.Square;
            checkBox.OnFillColor = Color.Transparent.ToUIColor();
            checkBox.OffFillColor = Color.Transparent.ToUIColor();
            checkBox.OnCheckColor = Color.Gray.ToUIColor();
            checkBox.WeakDelegate = this;
            checkBox.On = CusFormCheckbox.Checked;

            //checkBox.Layer.BorderColor = Color.Gray.ToCGColor();
            checkBox.OnTintColor = Color.Gray.ToUIColor();
            //checkBox.TintColor =Color.Gray.ToUIColor();
            SetNativeControl(checkBox);
            //CusFormCheckbox.PropertyChanged +=CusFormCheckbox_PropertyChanged;
            //this.AddSubview(checkBox);
            //this.Layer.BackgroundColor = UIColor.Yellow.CGColor;
        }

        //void CusFormCheckbox_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == CustomCheckbox.CheckedProperty.PropertyName)
        //    {
        //        checkBox.On = CusFormCheckbox.Checked;
        //    }
        //}
              

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            if (e.PropertyName == CustomCheckbox.CheckedProperty.PropertyName)
            {
                checkBox.On = CusFormCheckbox.Checked;
            }
        }
                       
        [Export("didTapCheckBox:")]
        public void DidTapCheckBox(BEMCheckBox checkBox)
        {
            CheckedArgs args = new CheckedArgs();
            args.Checked = checkBox.On;
            if (CusFormCheckbox.Checked_Changed != null)
            {
                CusFormCheckbox.Checked_Changed.Invoke(CusFormCheckbox, args);
            }
        }
    }
}
