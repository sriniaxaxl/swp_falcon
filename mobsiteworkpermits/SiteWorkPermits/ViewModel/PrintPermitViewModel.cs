﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.DAL.HotworksPermitDAL;
using SiteWorkPermits.DAL.RestHelper;
using SiteWorkPermits.Model;
using SiteWorkPermits.Model.SyncHotWorks;
using CustomControl;
using Plugin.Connectivity;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using SiteWorkPermits.Platform;
using SiteWorkPermits.DAL.sqlIntegration.HotworkDatabase;
using SQLite;
using SiteWorkPermits.DAL.sqlIntegration;
using SQLiteNetExtensions.Extensions;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits.ViewModel
{
    public class PrintPermitViewModel : BindableBase, INavigatedAware
    {
        public DelegateCommand<string> OnEmailCommand { get; set; }

        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        IWorkOrdersService _workOrdersService;
        IHotworkPermitService _hotworkPermitService;
        IScopeService _scopeService;
        IAuthorizationService _authorizationService;
        IFirePreventionMeasuresService _firePreventionMeasuresService;
        IFirewatchPersonsService _firewatchPersonsService;
        IPrecautionsService _precautionsService;
        IHighHazardAreasService _highHazardAreasService;
       
        public PrintPermitViewModel(INavigationService navigationService,
                                 IPageDialogService pageDialog,
                                 IHotworkPermitService hotworkPermitService,
                                 IWorkOrdersService workOrdersService,
                                 IScopeService scopeService, IAuthorizationService authorizationService,
                                 IFirePreventionMeasuresService firePreventionMeasuresService,
                                 IPrecautionsService precautionsService,
                                 IFirewatchPersonsService firewatchPersonsService,
                                 IHighHazardAreasService highHazardAreasService)
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            _workOrdersService = workOrdersService;
            _scopeService = scopeService;
            _hotworkPermitService = hotworkPermitService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _authorizationService = authorizationService;
            _firewatchPersonsService = firewatchPersonsService;
            _precautionsService = precautionsService;
            _scopeService = scopeService;
            _firePreventionMeasuresService = firePreventionMeasuresService;
            _highHazardAreasService = highHazardAreasService;
            _authorizationService = authorizationService;
            ShowActivityLoader = false;
            OnEmailCommand = new DelegateCommand<string>(SendEmail);
            ShowSyncIcon = false;
            SelFormat = "USLETTER";
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("PRINT OR EMAIL HOT WORKS PERMIT");
        }

        private bool _showActivityLoader;
        public bool ShowActivityLoader
        {
            get { return _showActivityLoader; }
            set { SetProperty(ref _showActivityLoader, value); }
        }

        private string _selFormat;
        public string SelFormat
        {
            get { return _selFormat; }
            set { SetProperty(ref _selFormat, value); }
        }

        private async void SendEmail(string obj)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await _pageDialog.DisplayAlertAsync("", AppResource.SIGN_MANDATORY_TEXT_PRINT, AppResource.OK_LABEL);
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowActivityLoader = true;
                });
                //  return;
            }
            else {

                await Task.Yield();
                await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                return;

            }

            if (obj.Equals("Print"))
            {
                try
                {
                    var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                    analyticsService.LogEvent("", "HOT WORKS PERMIT PRINT", "HOT WORKS PERMIT PRINT");
                    string hwguid = string.Empty;
                    int hwserverid=0;
                    if (printablePermit.server_id == 0)
                    {
                        var hotWork = await CustomControl.Validation.GetServerHotwork(printablePermit);
                        var createHwResponse = await RestApiHelper<CreateHWResponse>.CreateHW_Post(hotWork);
                        await Task.Yield();
                        if (createHwResponse == null)
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.PRINT_NOT_SUCCESS, AppResource.OK_LABEL);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            return;
                        }
                        else if (createHwResponse.Data != null)
                        {
                            hwguid = createHwResponse.Data.GUID;
                            hwserverid = createHwResponse.Data.Id;
                        }
                        else if (createHwResponse.Status != null)
                        {
                            if (createHwResponse.Status.StatusCode == 401)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", createHwResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                foreach (string key in keyList.ToList())
                                {
                                    if (!key.Equals("WelcomeShow"))
                                    {
                                        Application.Current.Properties.Remove(key);
                                    }
                                }
                                await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                ShowActivityLoader = false;
                                return;
                            }
                        }
                        printablePermit.hotworkGUID = hwguid;
                        printablePermit.server_id = hwserverid;
                        SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                        var connection = hotworkDatabase.GetdbConnection();
                        connection.UpdateWithChildren(printablePermit);
                    }
                    else
                    {
                        hwguid = printablePermit.hotworkGUID;
                    }
                    var pdfPath = await RestApiHelper<Task<string>>.DownloadHwPermitPDF_Post(hwguid, EmailIdEntry, SelFormat);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                    });
                    if (string.IsNullOrEmpty(pdfPath))
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.PRINT_NOT_SUCCESS, AppResource.OK_LABEL);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = false;
                        });
                    }
                    else
                    {
                        if (pdfPath == "Session Out")
                        {
                            await Task.Yield();
                            await _pageDialog.DisplayAlertAsync("", AppResource.UNABLE_LOGIN,AppResource.OK_LABEL);
                            var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                            // Xamarin.Forms.Application.Current.Properties.Clear();

                            foreach (string key in keyList.ToList())
                            {
                                if (!key.Equals("WelcomeShow"))
                                {
                                    Application.Current.Properties.Remove(key);
                                }
                            }
                            await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                            ShowActivityLoader = false;
                            return;
                        }
                        else
                        {
                            Xamarin.Forms.DependencyService.Get<IShare>().Show("title", "Hot Works Permit", pdfPath);
                        }
                    }
                    // await _navigationService.NavigateAsync("HomePage");
                }
                catch (Exception e)
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.PRINT_NOT_SUCCESS, AppResource.OK_LABEL);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ShowActivityLoader = false;
                        Console.WriteLine("IOException source: {0}", e.Source);
                    });
                }

            }
            else
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (string.IsNullOrEmpty(EmailIdEntry))
                    {
                        await Task.Yield();
                        await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_BLANK,AppResource.OK_LABEL);
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowActivityLoader = true;
                        });
                        try
                        {
                            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
                            analyticsService.LogEvent("", "HOT WORKS PERMIT EMAIL", "HOT WORKS PERMIT EMAIL");

                            string  hwguid = string.Empty;
                            int hwserverid = 0;
                            if (printablePermit.server_id == 0)
                            {
                                var hotWork =  CustomControl.Validation.GetServerHotwork(printablePermit);
                                var createHwResponse = await RestApiHelper<CreateHWResponse>.CreateHW_Post(hotWork.Result);
                                await Task.Yield();
                                if (createHwResponse == null)
                                {
                                    await Task.Yield();
                                    await _pageDialog.DisplayAlertAsync("",AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        ShowActivityLoader = false;
                                    });
                                    return;
                                }
                                else if (createHwResponse.Data != null)
                                {
                                    hwguid = createHwResponse.Data.GUID;
                                    hwserverid = createHwResponse.Data.Id;
                                }
                                else if (createHwResponse.Status != null)
                                {
                                    if (createHwResponse.Status.StatusCode == 401)
                                    {

                                        await Task.Yield();
                                        await _pageDialog.DisplayAlertAsync("", createHwResponse.Status.StatusMessage, "ok");
                                        var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                        // Xamarin.Forms.Application.Current.Properties.Clear();

                                        foreach (string key in keyList.ToList())
                                        {
                                            if (!key.Equals("WelcomeShow"))
                                            {
                                                Application.Current.Properties.Remove(key);
                                            }
                                        }
                                        await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                        ShowActivityLoader = false;
                                        return;
                                    }
                                }

                                printablePermit.hotworkGUID = hwguid;
                                printablePermit.server_id = hwserverid;
                                SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
                                var connection = hotworkDatabase.GetdbConnection();

                                connection.UpdateWithChildren(printablePermit);
                            }
                            else
                            {
                                hwguid = printablePermit.hotworkGUID;
                            }
                            await Task.Yield();
                            var isEmailSentResponse = await RestApiHelper<EmailHWResponse>.SendEmail_Post(hwguid, EmailIdEntry, SelFormat);

                            //bool isEmailSent;
                            if (isEmailSentResponse == null)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    ShowActivityLoader = false;
                                });
                                return;
                            }
                            else if (isEmailSentResponse.Status != null)
                            {
                                if (isEmailSentResponse.Status.StatusCode == 401)
                                {
                                    await Task.Yield();
                                    await _pageDialog.DisplayAlertAsync("", isEmailSentResponse.Status.StatusMessage, AppResource.OK_LABEL);
                                    var keyList = Xamarin.Forms.Application.Current.Properties.Keys;
                                    // Xamarin.Forms.Application.Current.Properties.Clear();

                                    foreach (string key in keyList.ToList())
                                    {
                                        if (!key.Equals("WelcomeShow"))
                                        {
                                            Application.Current.Properties.Remove(key);
                                        }
                                    }
                                    await _navigationService.NavigateAsync("/NavigationPage/LoginPage", null, null, false);
                                    ShowActivityLoader = false;
                                    return;
                                }
                            }

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            if (isEmailSentResponse.Data.Value)
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_SUCCESS, AppResource.OK_LABEL);
                                // await _navigationService.NavigateAsync("HomePage");
                            }
                            else
                            {
                                await Task.Yield();
                                await _pageDialog.DisplayAlertAsync("", AppResource.EMAIL_NOT_SENT, AppResource.OK_LABEL);
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    ShowActivityLoader = false;
                                });
                            }

                        }
                        catch (Exception e)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ShowActivityLoader = false;
                            });
                            Console.WriteLine("IOException source: {0}", e.Source);
                        }
                    }

                }
                else
                {
                    await Task.Yield();
                    await _pageDialog.DisplayAlertAsync("", AppResource.NO_INTERNET_AVAILABLE, AppResource.OK_LABEL);
                }
            }
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }
        private string _emailIdEntry;
        public string EmailIdEntry
        {
            get { return _emailIdEntry; }
            set { SetProperty(ref _emailIdEntry, value); }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            
        }


        private HotWorkPermit_sql printablePermit;

        SQLiteConnection _connection;

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            SQLiteDatabase hotworkDatabase = new SQLiteDatabase();
            _connection = hotworkDatabase.GetdbConnection();
            printablePermit = _connection.GetWithChildren<HotWorkPermit_sql>(App.PermitEditableId, true);

            var dd = _connection.GetAllWithChildren<HotWorkPermit_sql>().ToList();

            Scope_sql scope = new Scope_sql();
            try {
                scope = _connection.GetWithChildren<Scope_sql>(printablePermit.scope_Sql.id, recursive: true);

            }
            catch (Exception ex) {
                scope = _connection.GetWithChildren<Scope_sql>(printablePermit.scope_Sql_id, recursive: true);
            }
            


            printablePermit.scope_Sql = scope;
        }

        


        private string _uniqueId;

        private void SetHotworksNumber()
        {
            var hw = _hotworkPermitService.GetHotworksAsync();
            if (hw != null)
            {
                _uniqueId = CustomControl.Validation.HotworksUniqueID(hw.Count());
            }
            else
            {
                _uniqueId = CustomControl.Validation.HotworksUniqueID(0);
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
