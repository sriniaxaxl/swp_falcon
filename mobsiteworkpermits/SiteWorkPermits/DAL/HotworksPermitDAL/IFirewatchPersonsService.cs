﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IFirewatchPersonsService
    {
        IEnumerable<FirewatchPersons> GetFirewatchPersonsAsync();

        FirewatchPersons GetFirewatchPersonsByIdAsync(int id);

        bool AddFirewatchPersonsAsync(FirewatchPersons firewatchDuringHtPersons);

        bool UpdateFirewatchPersonsAsync(FirewatchPersons firewatchDuringHtPersons);

        bool RemoveFirewatchPersonsAsync(int id);
        void RemoveAllData();
    }
}
