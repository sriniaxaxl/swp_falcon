﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IImpairmentTypeService
    {
        IEnumerable<ImpairmentType> GetImpairmentType();

        ImpairmentType GetImpairmentTypeById(int id);

        bool AddImpairmentType(ImpairmentType impairmentType);

        void RemoveAllData();
    }
}
