﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SiteWorkPermits.ViewModel;
using CustomControl;
using Prism.Common;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class ListAddview : ContentView
    {

        public event EventHandler<ValueArgs> DeleteEvent;

        public string Title
        {
            get
            {
                return Lblval.Text;
            }
            set
            {
                Lblval.Text = value;
            }
        }

        public bool DurationDisplay
        {
            get
            {
                return GdDuration.IsVisible;
            }
            set
            {
                GdDuration.IsVisible = value;
            }
        }


        public BindableProperty StartTimeProperty = BindableProperty.Create<ListAddview, DateTime>(o => o.StratTime, DateTime.Now, BindingMode.TwoWay, null, StartTimePropertyChanged);

        public DateTime StratTime
        {
            get { return (DateTime)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        static void StartTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var Curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            var model = (NewHotWorksPageViewModel)Curpage.BindingContext;
            if(newValue<model.StartHotWork){
                var control = (ListAddview)bindable;
                //if (control.EndTime > control.StratTime)
                //{
                control.StratTime = oldValue;
                control.StartTimePicker.Text = Helper.TimeFormatSet(oldValue);
                //}
                //else
                //{
                //    var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                //    curpage.DisplayAlert(string.Empty, "Start Time should be lower than End Time", "Ok");
                //    //control.StratTime = oldValue;
                //}
            }else{
                var control = (ListAddview)bindable;
                //if (control.EndTime > control.StratTime)
                //{
                control.StratTime = newValue;
                control.StartTimePicker.Text = Helper.TimeFormatSet(newValue);
                //}
                //else
                //{
                //    var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                //    curpage.DisplayAlert(string.Empty, "Start Time should be lower than End Time", "Ok");
                //    //control.StratTime = oldValue;
                //}    
            }


        }

        public BindableProperty EndTimeProperty = BindableProperty.Create<ListAddview, DateTime>(o => o.EndTime, DateTime.Now, BindingMode.TwoWay, null, EndTimePropertyChanged);

        public DateTime EndTime
        {
            get { return (DateTime)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }
        //ss
        static void EndTimePropertyChanged(BindableObject bindable, DateTime oldValue, DateTime newValue)
        {
            var Curpage = PageUtilities.GetCurrentPage(App.Current.MainPage);
            var model = (NewHotWorksPageViewModel)Curpage.BindingContext;

            if(newValue>model.EndHotWork){
                var control = (ListAddview)bindable;
                //if (control.EndTime > control.StratTime)
                //{
                //control.EndTime = model.EndHotWork;
                //control.EndTimePicker.Text = Helper.TimeFormatSet(model.EndHotWork);

                control.EndTime = newValue;
                control.EndTimePicker.Text = Helper.TimeFormatSet(newValue);

            }else{
                var control = (ListAddview)bindable;
                //if (control.EndTime > control.StratTime)
                //{
                control.EndTime = newValue;
                control.EndTimePicker.Text = Helper.TimeFormatSet(newValue);
                //}
                //else
                //{
                //    var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                //    curpage.DisplayAlert(string.Empty, "EndTime Should be higher than StartTime", "Ok");
                //    //control.EndTime = oldValue;
                //}    
            }
        }

        private void Person_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            CusEntry CurEntry = (CusEntry)sender;
            CurEntry.Text = Regex.Replace(CurEntry.Text, "[^\\w\\ _]", "");
        }

        //static void HandleBindingPropertyChangedDelegate(BindableObject bindable, string oldValue, string newValue)
        //{
        //    var control = (Grayview)bindable;
        //    control.Lbltxt.Text = newValue;
        //}

        public ListAddview()
        {
            InitializeComponent();
            DurationDisplay = false;
            StartTimePicker.Timeselected += StartTimePicker_Timeselected;
            EndTimePicker.Timeselected += EndTimePicker_Timeselected;
        }

        private void StartTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {
            if (EndTime.TimeOfDay >= e.NewTime.TimeOfDay)
            {
                StartTimePicker.Text = e.NewTime.ToString("HH:mm");
                StratTime = e.NewTime;
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                var model = (NewHotWorksPageViewModel)curpage.BindingContext;
                //StartTimePicker.Text = e.OldTime.ToString("HH:mm");
                //StratTime = e.OldTime;
                curpage.DisplayAlert(string.Empty, "Start Time should be before End Time", "Ok");
            }
        }

        private void EndTimePicker_Timeselected(object sender, CustomControl.TimeChangeArgs e)
        {            
            if (e.NewTime.TimeOfDay >= StratTime.TimeOfDay)
            {
                EndTimePicker.Text = e.NewTime.ToString("HH:mm");
                EndTime = e.NewTime;
            }
            else
            {
                var curpage = PageUtilities.GetCurrentPage(Application.Current.MainPage);
                //EndTimePicker.Text = e.OldTime.ToString("HH:mm");
                //EndTime = e.OldTime;
                curpage.DisplayAlert(string.Empty, "End Time Should be after Start Time", "Ok");
            }
        }

        private void ValDeleted(object sender, System.EventArgs e)
        {
            DeleteEvent.Invoke(this, new ValueArgs() { value = Lblval.Text });

            //Label lbl = (Label)sender;
            //var lbltitle = (Label)(((CusGrid)lbl.Parent).Children[0]);
            //var val = lbltitle.Text;
            //valcollection.Add(val);
        }
    }

    public class ValueArgs : EventArgs
    {
        public string value { get; set; }
    }
}
