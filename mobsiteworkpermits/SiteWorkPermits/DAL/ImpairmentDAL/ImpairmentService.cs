﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public class ImpairmentService:IImpairmentService
    {
        private readonly DatabaseContext _databaseContext;
        public ImpairmentService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddImpairment(Impairment impairment)
        {
            try
            {
                var status = _databaseContext.Impairment.Add(impairment);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Impairment GetImpairmentById(int id)
        {
            try
            {
                //var impairments = _databaseContext.Impairment.Where(p => p.ImpairmentId == id)
                                                  //.Include(p => p.ReporterDetail).
                                                  //Include(p => p.ImpairmentImpairmentMeasureMasters)
                                                  //.Include(p => p.ImpairmentPrecautionTakens);

                var impairments = _databaseContext.Impairment.Where(p => p.ImpairmentId == id)
                                                  .Include(p => p.ReporterDetail).
                                                  Include(p=>p.ImpairmentMeasureMasters).
                                                  Include(p => p.ImpairmentImpairmentMeasureMasters)
                                                  .Include(p => p.ImpairmentPrecautionTakens);

                return impairments.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<Impairment> GetImpairments()
        {
            try
            {
                var impairments = _databaseContext.Impairment.Include(p=>p.ReporterDetail).
                                                  Include(p=>p.ImpairmentImpairmentMeasureMasters)
                                                  .Include(p => p.ImpairmentPrecautionTakens).Where(p => p.SiteId == App.SiteId).ToList();
                return impairments;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<Impairment>();
            _databaseContext.Impairment.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }

        public bool RemoveImpairment(int id)
        {
            try
            {
                var impairment = _databaseContext.Impairment.Find(id);
                var status = _databaseContext.Impairment.Remove(impairment);
                _databaseContext.SaveChanges();
                var isDeleted = status.State == EntityState.Deleted;
                return isDeleted;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateImpairment(Impairment impairment)
        {
            try
            {
                var status = _databaseContext.Impairment.Update(impairment);
                _databaseContext.SaveChanges();
                var isUpdated = status.State == EntityState.Unchanged;
                return isUpdated;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
