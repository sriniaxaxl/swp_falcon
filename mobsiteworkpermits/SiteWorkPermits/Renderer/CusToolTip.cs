﻿using System;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusToolTip : Grid
    {       
        public static BindableProperty ToolTipTextProperty = BindableProperty.Create<CusToolTip, string>(o => o.ToolTipText, string.Empty);

        public string ToolTipText
        {
            get
            {
                return (string)GetValue(ToolTipTextProperty);
            }
            set
            {
                SetValue(ToolTipTextProperty, value);
            }
        }
    }
}
