﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using System.Linq;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class PrecautionsService:IPrecautionsService
    {
        private  DatabaseContext _databaseContext;
        public PrecautionsService()
        {
            _databaseContext = new DatabaseContext(DependencyService.Get<IDBPath>().GetDbPath());
        }

        public  bool AddPrecautionsAsync(Precautions precautions)
        {
            //_databaseContext = DataContextEntity.Instance();
            try
            {
                var precautionStatus =  _databaseContext.Precautions.Add(precautions);
                _databaseContext.SaveChanges();
                var isAdded = precautionStatus.State == EntityState.Added;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Precautions> GetPrecautionsAsync()
        {
            try
            {
                var precautions =  _databaseContext.Precautions.ToList();
                return precautions;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public Precautions GetPrecautionsByIdAsync(int id)
        {
            var hotworksPermits = _databaseContext.Precautions.Where(p=>p.Id == id);

            return hotworksPermits.FirstOrDefault();
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<Precautions>();
            _databaseContext.Precautions.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
