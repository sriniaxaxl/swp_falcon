﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace SiteWorkPermits
{
    public class Helper
    {
        public static string WelcomeShow = "WelcomeShow";

        public static string ElapsedTime(DateTime dtEvent)
        {
            var pr = dtEvent.Date - DateTime.Now.Date;
            if (pr.Days > 0)
            {
                if (pr.Days < 364)
                {
                    if (dtEvent.Date.Month == DateTime.Now.Month)
                    {
                        if (pr.Days > 7)
                        {
                            return "After Week";
                        }
                        else if (pr.Days > 1)
                        {
                            return "This Week";
                        }
                        else if (Convert.ToDouble(pr.Days).Equals(1))
                        {
                            return "Tomorrow";
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        return dtEvent.ToString("MMM");
                    }
                }
                else
                {
                    var years = (pr.Days % 364);
                    var str = years > 1 ? " years after" : " year after";
                    return years + str;
                }
            }
            else
            {
                TimeSpan TD = dtEvent - DateTime.Now;
                if (Math.Abs(TD.Days) > 0)
                {
                    if (Math.Abs(pr.Days) < 364)
                    {
                        if (dtEvent.Date.Month == DateTime.Now.Month)
                        {
                            var days = Math.Abs(TD.Days);
                            if (days > 7)
                            {
                                return "Before Week";
                            }
                            else if (days > 1)
                            {
                                return "Last Week";
                            }
                            else if (Convert.ToDouble(days).Equals(1))
                            {
                                return "Yesterday";
                            }
                            else
                            {
                                return string.Empty;
                            }
                        }
                        else
                        {
                            return dtEvent.ToString("MMM");
                        }
                    }
                    else
                    {
                        var years = (Math.Abs(pr.Days) % 364);
                        var str = years > 1 ? " years before" : " year before";
                        return years + str;
                        //return (Math.Abs(pr.Days) % 364).ToString();
                    }
                }
                else
                {
                    return "Today";
                }
            }

        }


        public static string DateFormatSet(DateTime dt)
        {
            return dt.ToString("dd") + ", " + dt.ToString("MMM") + ", " + dt.ToString("yyyy");
        }

        public static string TimeFormatSet(DateTime dt)
        {
            return dt.ToString("HH:mm");
        }

        public async static Task Setval(string key, string val)
        {
            if (App.Current.Properties.Where(s => s.Key.Equals(key)).Count() == 0)
            {
                App.Current.Properties.Add(key, val);
            }
            else
            {
                App.Current.Properties[key] = val;
            }
            await App.Current.SavePropertiesAsync();
        }

        public static string Getval(string key)
        {
            if (Xamarin.Forms.Application.Current.Properties.Where(s => s.Key.Equals(key)).Count() > 0)
            {
                return (string)Xamarin.Forms.Application.Current.Properties[key];
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
