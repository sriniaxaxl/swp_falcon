﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class Test_Scope : ContentPage
    {
        public Test_Scope()
        {
            InitializeComponent();
            Panel.SectionView = new FirePreventionView();
        }
    }
}
