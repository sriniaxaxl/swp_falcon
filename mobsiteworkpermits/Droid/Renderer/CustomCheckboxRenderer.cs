using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using CustomControl;
using System.ComponentModel;
using Xamarin.Forms;
using static Android.Graphics.PorterDuff;
using Android.Renderer;
using Android.Graphics.Drawables;
using Android.Support.V4.Widget;
using Android.Content.Res;

[assembly: ExportRenderer(typeof(CustomCheckbox), typeof(CustomCheckboxRenderer))]
namespace Android.Renderer
{
    public class CustomCheckboxRenderer : ViewRenderer<CustomCheckbox, CheckBox>
    {
        CheckBox Cuscheckbox;
        CustomCheckbox CusFormCheckbox;
        protected override void OnElementChanged(ElementChangedEventArgs<CustomCheckbox> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;
            Cuscheckbox = new CheckBox(Context);
            int[][] states = {
                new int[] { Android.Resource.Attribute.StateEnabled}, // enabled
                new int[] {Android.Resource.Attribute.StateEnabled}, // disabled
                new int[] {Android.Resource.Attribute.StateChecked}, // unchecked
                new int[] { Android.Resource.Attribute.StatePressed }  // pressed
                };

            var checkBoxColor = Android.Graphics.Color.Gray;

            // Using ColorStateList to change the border color of the checkbox
            int[] colors = {
                        checkBoxColor,
                        checkBoxColor,
                        checkBoxColor,
                        checkBoxColor
                };
            var cusCheckboxList = new ColorStateList(states, colors);
            Cuscheckbox.ButtonTintList = cusCheckboxList;
            if (Control == null)
            {
                CusFormCheckbox = Element as CustomCheckbox;
                Cuscheckbox.Text = CusFormCheckbox.Text;
                Cuscheckbox.Checked = CusFormCheckbox.Checked;
                //var btndraw =(Drawable) Cuscheckbox.ButtonDrawable;
                Cuscheckbox.SetPadding(0, 0, 0, 0);
                Cuscheckbox.SetIncludeFontPadding(false);
                Cuscheckbox.SetTextColor(CusFormCheckbox.TextColor.ToAndroid());
                Cuscheckbox.CheckedChange += Cuscheckbox_CheckedChange;
                SetNativeControl(Cuscheckbox);
            }
        }

        private void Cuscheckbox_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            CusFormCheckbox.Checked = e.IsChecked;
            CheckedArgs args = new CheckedArgs();
            args.Checked = e.IsChecked;
            if (CusFormCheckbox.Checked_Changed != null)
            {
                CusFormCheckbox.Checked_Changed.Invoke(CusFormCheckbox, args);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            if (e.PropertyName == CustomCheckbox.CheckedProperty.PropertyName)
            {
                Cuscheckbox.Checked = CusFormCheckbox.Checked;
            }
        }
    }
}