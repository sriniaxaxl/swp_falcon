﻿using System;
namespace SiteWorkPermits.Platform
{
    public interface IAppInfo
    {
        string GetVersion();
        int GetBuild();
    }
}
