﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IImpairmentService
    {
        IEnumerable<Impairment> GetImpairments();

        Impairment GetImpairmentById(int id);

        bool AddImpairment(Impairment impairment);

        bool UpdateImpairment(Impairment impairment);

        bool RemoveImpairment(int id);

        void RemoveAllData();
    }
}
