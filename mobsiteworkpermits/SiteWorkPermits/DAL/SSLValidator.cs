﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace SiteWorkPermits.DAL
{
    public static class SSLValidator
    {
        // TODO: Run the /Utility/Lookup.PublicKeys application to extract the public key for the site's SSL cert

        // actual key Npd Dev / NPD Test
        static string PUBLIC_KEY = "30 82 01 0a 02 82 01 01 00 a3 04 bb 22 ab 98 3d 57 e8 26 72 9a b5 79 d4 29 e2 e1 e8 95 80 b1 b0 e3 5b 8e 2b 29 9a 64 df a1 5d ed b0 09 05 6d db 28 2e ce 62 a2 62 fe b4 88 da 12 eb 38 eb 21 9d c0 41 2b 01 52 7b 88 77 d3 1c 8f c7 ba b9 88 b5 6a 09 e7 73 e8 11 40 a7 d1 cc ca 62 8d 2d e5 8f 0b a6 50 d2 a8 50 c3 28 ea f5 ab 25 87 8a 9a 96 1c a9 67 b8 3f 0c d5 f7 f9 52 13 2f c2 1b d5 70 70 f0 8f c0 12 ca 06 cb 9a e1 d9 ca 33 7a 77 d6 f8 ec b9 f1 68 44 42 48 13 d2 c0 c2 a4 ae 5e 60 fe b6 a6 05 fc b4 dd 07 59 02 d4 59 18 98 63 f5 a5 63 e0 90 0c 7d 5d b2 06 7a f3 85 ea eb d4 03 ae 5e 84 3e 5f ff 15 ed 69 bc f9 39 36 72 75 cf 77 52 4d f3 c9 90 2c b9 3d e5 c9 23 53 3f 1f 24 98 21 5c 07 99 29 bd c6 3a ec e7 6e 86 3a 6b 97 74 63 33 bd 68 18 31 f0 78 8d 76 bf fc 9e 8e 5d 2a 86 a7 4d 90 dc 27 1a 39 02 03 01 00 01";

        //// actual key non prd mamc Dev/Test
        //static string PUBLIC_KEY_MAMC = "30 82 01 0a 02 82 01 01 00 aa 5d 39 1a 84 60 77 47 1f 76 35 3a db 01 a5 6a c9 14 87 01 3c be 03 da de 7d c6 74 93 03 50 d1 86 8e 4f 2d dd 6f 2f db 3d 36 aa 75 11 c6 69 c6 f6 9b d3 7c 93 a4 11 3d 6d b0 89 99 bd 37 01 75 18 72 3d bb c7 f3 20 fb e1 76 82 68 81 0b dd 68 b7 b1 21 84 79 c2 02 9e 86 d9 8e 40 f6 af 15 20 e4 25 c6 8b 9b ce 1b 01 fb 3c 6a 5a 88 b7 da 8d 62 c3 e7 6a c7 9c e2 a9 a1 c1 bd a7 de bc 84 b0 37 aa 02 6b 5c ed 73 64 a0 f8 f9 24 63 7e c6 57 97 2f 0b 04 11 f0 a5 82 c0 5b 57 57 85 80 de af 52 06 51 e7 f7 77 6c 4c 01 32 c5 3b 9e 43 7d 72 1d 4c 57 6b 24 03 0d 4c 40 08 59 e4 55 d9 37 03 06 e1 80 18 df d2 5f 8a 7d 65 01 3d 6a f6 e6 de 8a 2f 5c 78 50 71 bd 86 00 b3 7a 84 e4 a7 53 5f 7b e2 b1 45 71 39 75 8e 9c b1 d8 0b 39 4d 1a 49 2c af be a3 9f 97 e2 ae 75 6a 2f 89 bf 69 20 15 02 03 01 00 01";

        ////actual key prd
        //// static string PUBLIC_KEY = "3082010A0282010100A8F5704481F8FF2690EFAAC1D35434D5E2B0F0EB424193E3F9A13E5876E9758BED414EBB6A2B643334D37EDB39B0FD2F7BF6D04767D1AEFCC364DCE8FE9D05F53C6BE749072C60565D1246FAEAFECECD229226A4A85E3255C5EB5D0300BFE0E14CD6FC0C0BC3829065ACE323B163D85C8247D1B3A38EA7698B9B6324EB833913E49518B58ED4A1F3E0305CE0872A74D56E8064953E8631B2AFE1C05E52097E609AB76F75F11744E357DE0B1FE7A9C0E9A4993C2E069E5454E694F06D8826A354F63D3C4DE08950071A95D91CB951C812E8385B5DAA737CEEF19E82AC58696F7D1D909A3F863E8D45C14723D6085F452D8DAE1C220CF2095ACF73A1F773AF72BD0203010001";
        ////actual key prd
        //// static string PUBLIC_KEY_MAMC = "3082010A0282010100B8FE2F985F30313A8C5A184E01E379AF254ABB7E9BC25F81BD7F7E550DC44D747D6BFCDC18D91F447326FA5E266DDB8390D5F7FD57035D86F405E6AE87585827D2741C2897C539AC6435A3B50477408947036E32E3D0F6D441E652AFF9D22CE22239B575F202115359B17EC51D17DDF7515DF6C0A5D8B1E67E35064C5372D7ECE3D89728D5BB9CAA7CDFF6678BA0D72251FA91445A0B86033FAD26B4F5560165730AA64D0A269AF57A196B06F813A4197BFA64AA1D701733AD4F61035CA78090712215A13985A8C7357C6A4E77DA2C70A2A4998C06D1CCD8E53DDC0DB65D0C7BF35527C5343D644B9B4054A3FBFF297000A35CE76EB29C6071906B0B12EDE9450203010001";

        public static void Initialize()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
        }

        static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //var certPublicString = certificate?.GetPublicKeyString();
            //var keysMatch = PUBLIC_KEY_MAMC.Replace(" ", null).ToUpper() == certPublicString;
            //return keysMatch;


            var certPublicString = certificate?.GetPublicKeyString();
            var sss = "";


            ////nonprod dev
            //if (certificate.Subject == "CN=*.table.core.windows.net")
            //{
            //    var certPublicString = certificate?.GetPublicKeyString();
            //    var keysMatch = PUBLIC_KEY_MAMC.Replace(" ", null).ToUpper() == certPublicString;
            //    return keysMatch;
            //}
            //else if (certificate.Subject == "CN=*.azurewebsites.net")
            //{
            //    var certPublicString = certificate?.GetPublicKeyString();
            //    var keysMatch = PUBLIC_KEY.Replace(" ", null).ToUpper() == certPublicString;
            //    return keysMatch;
            //}


            ////  prod
            //if (certificate.Subject == "CN=*.table.core.windows.net")
            //{
            //    var certPublicString = certificate?.GetPublicKeyString();
            //    var keysMatch = PUBLIC_KEY_MAMC == certPublicString;
            //    return keysMatch;
            //}
            //else if (certificate.Subject == "CN=mysite-api.axaxl.com, OU=XL Services (Bermuda) Ltd, O=XL Services (Bermuda) Ltd, L=Hamilton, C=BM")
            //{
            //    var certPublicString = certificate?.GetPublicKeyString();
            //    var keysMatch = PUBLIC_KEY == certPublicString;
            //    return keysMatch;
            //}

            return true;
        }
    }
}
