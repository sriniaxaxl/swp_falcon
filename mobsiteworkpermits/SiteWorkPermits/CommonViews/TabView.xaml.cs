﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class TabView : ContentView
    {

        public static BindableProperty TitleProperty = BindableProperty.Create<TabView, string>(o => o.Title, string.Empty, BindingMode.TwoWay, null, HandleBindingPropertyChangedDelegate);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        static void HandleBindingPropertyChangedDelegate(BindableObject bindable, string oldValue, string newValue)
        {
            var control = (TabView)bindable;
            control.lblTitle.Text = newValue;
        }

        public static BindableProperty TabColorProperty = BindableProperty.Create<TabView, Color>(o => o.TabColor,Color.Transparent, BindingMode.TwoWay, null, TabColorPropertyChanged);

        public Color TabColor
        {
            get { return (Color)GetValue(TabColorProperty); }
            set { SetValue(TabColorProperty, value); }
        }

        static void TabColorPropertyChanged(BindableObject bindable, Color oldValue, Color newValue)
        {
            var control = (TabView)bindable;
            control.gdstripe.InnerBackground = newValue;
        }

        public TabView()
        {
            InitializeComponent();
        }
    }
}
