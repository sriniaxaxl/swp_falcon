﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SiteWorkPermits
{
    public partial class RaiseImpairmentView : ContentView
    {
        public event EventHandler RaiseImpairmentEvent;
        public event EventHandler CancelImpairmentEvent;
        public CustomControl.CusGrid GdDuplicate;
        public RaiseImpairmentView()
        {
            InitializeComponent();
            GdDuplicate = Btnduplicate;
        }

        private void RaiseImpairment_Tapped(object sender, System.EventArgs e)
        {
            if (RaiseImpairmentEvent != null)
            {
                RaiseImpairmentEvent.Invoke(this, new EventArgs());
            }
        }

        private void CancelImpairment_Tapped(object sender, System.EventArgs e)
        {
            if (CancelImpairmentEvent != null)
            {
                CancelImpairmentEvent.Invoke(this, new EventArgs());
            }
        }
    }
}
