﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
namespace SiteWorkPermits.Model
{
    public class Data
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string SiteName { get; set; }
        public string LPItemName { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string JWTToken { get; set; }
        public int HandheldId { get; set; }
    }

    public class Status
    {
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
    }

    public class UserLogin
    {
        public Data Data { get; set; }
        public Status Status { get; set; }
    }


    public class UserLogout
    {
        public string Data { get; set; }
        public Status Status { get; set; }
    }

    public class IncUniqueId {
        [Key]
        public int Id { get; set; }
        public int UniqueIncrementalId { get; set; }
    }
}
