﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class IncUniqueidService:IIncUniqueid
    {
        private readonly DatabaseContext _databaseContext;
        public IncUniqueidService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddIncUniqueId(IncUniqueId incUniqueId)
        {
            try
            {
                var uniqueId = _databaseContext.IncUniqueIds.Add(incUniqueId);
                _databaseContext.SaveChanges();
                var isAdded = uniqueId.State == EntityState.Unchanged;
                return isAdded;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<IncUniqueId> GetUniqueIds()
        {
            try
            {
                var IncUniqueId = _databaseContext.IncUniqueIds.ToList();
                return IncUniqueId;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
