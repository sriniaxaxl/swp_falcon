﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FB = Android.Support.Design.Widget.FloatingActionButton;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content.Res;
using System.ComponentModel;
using CustomControl;
using Droid.Renderer;

[assembly: ExportRenderer(typeof(CusFloatingButton), typeof(CustomFloatingButtonRender))]
namespace Droid.Renderer
{
    public class CustomFloatingButtonRender : Xamarin.Forms.Platform.Android.AppCompat.ViewRenderer<CusFloatingButton,FB>
    {
        
        public CustomFloatingButtonRender()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<CusFloatingButton> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;

            if (this.Control == null)
            {
                this.ViewGroup.SetClipChildren(false);
                this.ViewGroup.SetClipToPadding(false);
            }

            var fab = new FB(Context);
            Android.Support.V4.View.ViewCompat.SetBackgroundTintList(fab, ColorStateList.ValueOf(Element.ButtonColor.ToAndroid()));
            fab.UseCompatPadding = false;
            fab.Elevation = 5.0f;
            fab.SetBackgroundColor(Color.Transparent.ToAndroid());
            var elementImage = Element.Image;
            var imageFile = elementImage?.File;
            if (imageFile != null)
            {
                //fab.SetImageResource(SiteWorkPermits.Droid.Resource.Drawable.ic_add_white);
                //fab.SetImageDrawable(Context.Resources.GetDrawable(imageFile));
            }
            fab.Click += Fab_Click;
            SetNativeControl(fab);
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            //Control.BringToFront();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var fab = (FB)Control;
            if (e.PropertyName == nameof(Element.ButtonColor))
            {
                Android.Support.V4.View.ViewCompat.SetBackgroundTintList(fab, ColorStateList.ValueOf(Element.ButtonColor.ToAndroid()));
            }
            if (e.PropertyName == nameof(Element.Image))
            {
                var elementImage = Element.Image;
                var imageFile = elementImage?.File;
                if (imageFile != null)
                {
                    fab.SetImageDrawable(Context.Resources.GetDrawable(imageFile));
                }
            }
            base.OnElementPropertyChanged(sender, e);
        }


        private void Fab_Click(object sender, EventArgs e)
        {
            ((IButtonController)Element).SendClicked();
        }
    }
}
