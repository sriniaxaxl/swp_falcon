﻿using System;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;
using Android.Content.Res;
using Xamarin.Forms;

namespace SiteWorkPermits.Droid.Services
{
    [BroadcastReceiver]
    public class StartdbCheckServiceReciver : BroadcastReceiver
    {

        public override void OnReceive(Context context, Intent intent)
        {
            App.notificationId++;

            int ResBackIcon = Resource.Drawable.ic_launcher_round;
            int ResNewIcon = Resource.Drawable.ic_launcher_foreground;
            Android.Graphics.Color SmallIconColor = Android.Graphics.Color.Blue;
            Intent intent1 = new Intent(Forms.Context, typeof(MainActivity));
            intent1.SetFlags(ActivityFlags.SingleTop);
            intent1.PutExtra("OpenPage", "SomePage");
            intent1.PutExtra("HotWorkID", intent.GetStringExtra("hotworkID"));
            const int pendingIntentId = 0;
            PendingIntent pendingIntent = PendingIntent.GetActivity(Forms.Context,
                                                                    0, intent1, PendingIntentFlags.OneShot);

            string URGENT_CHANNEL = "HOT_WORKS";
            NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;
            String hwTitle = intent.GetStringExtra("hotworkTitle");
            String hwDescription = intent.GetStringExtra("hotworkDescription");
            Android.Net.Uri alarmSound = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
            //string chanName = GetString(Resource.String.noti_chan_urgent);
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                NotificationChannel chan = new NotificationChannel(URGENT_CHANNEL, URGENT_CHANNEL, NotificationImportance.High);
                chan.EnableVibration(true);
                chan.LockscreenVisibility = NotificationVisibility.Public;
                notificationManager.CreateNotificationChannel(chan);
                Notification.Builder builder = new Notification.Builder(context)
               .SetOngoing(false)
               .SetContentTitle(hwTitle)
               .SetContentText(hwDescription)
               .SetContentIntent(pendingIntent)
               .SetChannelId(URGENT_CHANNEL)                   
               .SetSmallIcon(ResNewIcon)
               .SetColor(SmallIconColor)
               .SetAutoCancel(true);
                builder.SetSound(alarmSound);
                Vibrator v = (Vibrator)context.GetSystemService(Context.VibratorService);
                v.Vibrate(1000);
                Notification notification = builder.Build();
                notificationManager.Notify(App.notificationId, notification);
                Xamarin.Forms.MessagingCenter.Send("SendUnsyncNotification", "SendUnsyncNotificationn");

            }
            else
            {
                Notification.Builder builder = new Notification.Builder(context)
                                                .SetOngoing(false)
                                                .SetContentTitle(hwTitle)
                                                .SetContentText(hwDescription)
                                                .SetContentIntent(pendingIntent)
                                                .SetAutoCancel(true);

                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                {
                    builder.SetSmallIcon(ResNewIcon);
                    builder.SetColor(SmallIconColor);
                }
                else
                {
                    builder.SetSmallIcon(ResBackIcon);
                }

                builder.SetSound(alarmSound);
                Vibrator v = (Vibrator)context.GetSystemService(Context.VibratorService);
                v.Vibrate(1000);
                Notification notification = builder.Build();
                notificationManager.Notify(App.notificationId, notification);
               // CustomControl.Validation.SetUnSyncAlarm();
            }
        }
    }
}
