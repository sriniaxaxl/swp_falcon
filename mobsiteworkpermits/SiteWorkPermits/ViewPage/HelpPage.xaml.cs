﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits.ViewPage
{
    public partial class HelpPage : CarouselPage
    {
        public HelpPage()
        {
            InitializeComponent();
            dashboardDesc.Text = "Dashboard.";
            hwdashboardDesc.Text = "Hot Works Dashboard.";
            hwPrintDesc.Text = "Print and Email.";
            impdashboardDesc.Text = "Impairment Dashboard.";
            impprintDesc.Text = "Print and Submit.";

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            App.curhomepage.IsPresented = true;
        }
    }
}
