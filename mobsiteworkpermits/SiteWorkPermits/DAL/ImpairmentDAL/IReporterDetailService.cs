﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IReporterDetailService
    {
        IEnumerable<ReporterDetail> GetReportDetails();

        ReporterDetail GetReporterDetailById(int id);

        bool AddReporterDetail(ReporterDetail reporterDetail);

        bool UpdateReporterDetail(ReporterDetail reporterDetailscope);

        bool RemoveReporterDetail(int id);

        void RemoveAllData();
    }
}
