﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CusPicker : Entry
    {
        public CusPicker() :base()
        {

        }
        public EventHandler<ValueArgs> ValueChanged;
        #region Properties  

        public static BindableProperty ItemSourceProperty = BindableProperty.Create<CusPicker, List<string>>(o => o.ItemSource,null,BindingMode.TwoWay,null);

        public List<string> ItemSource
        {
            get { return (List<string>)GetValue(ItemSourceProperty); }
            set { SetValue(ItemSourceProperty, value); }
        }
               
        static void ItemSourceChange(BindableObject bindable, List<string> value)
        { 
            var control = (CusPicker)bindable;
            control.ItemSource = value;
        }


        public static BindableProperty SelectedProperty = BindableProperty.Create<CusPicker, string>(o => o.Selected, null);
        public string Selected
        {
            get { return (string)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }

        public static BindableProperty TitleProperty = BindableProperty.Create<CusPicker, string>(o => o.Title, null);
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }


        public static BindableProperty PaddingProperty = BindableProperty.Create<CusPicker, Thickness>(o => o.Padding, new Thickness(5));
        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CusPicker, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CusPicker, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CusPicker, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CusPicker, float>(o => o.BorderRadius, 0);

        public float BorderRadius
        {
            get { return (float)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }
                            
        #endregion
    }
}
