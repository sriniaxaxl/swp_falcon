﻿using System;
using SiteWorkPermits.Platform;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;

namespace SiteWorkPermits.ViewModel
{
    public class GuideViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;      
        IPageDialogService _pageDialog;
        public GuideViewModel(INavigationService navigationService,IPageDialogService pageDialog)
        {
            _navigationService = navigationService;           
            _pageDialog = pageDialog;
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("HELP");
        }

        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
