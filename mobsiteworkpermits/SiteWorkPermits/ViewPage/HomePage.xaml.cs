﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Platform;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace SiteWorkPermits
{
    public partial class HomePage : Xamarin.Forms.MasterDetailPage
    {
        public HomePage()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex) {
                var ss = "";
            }
            App.curhomepage = this;
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            IsGestureEnabled = false;
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "ShowHambergerMenu", (hambergerMenu) =>
            {
                IsPresented = true;
            });


            MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "HideHambergerMenu", (hambergerMenu) =>
            {
                IsPresented = false;
            });

            //DependencyService.Get<ISetAlertHotworks>().SetStartAlertNotification("MySite Notification", "Hot Works Permit " + 123 + " has started.", 123456, DateTime.Now.AddSeconds(10));

            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:           
            analyticsService.LogScreen("DASHBOARD");
        }

        private void MasterDetailPage_IsPresentedChanged(object sender, EventArgs e)
        {
            //MessagingCenter.Subscribe<Xamarin.Forms.Application>(this, "ShowHambergerMenu", (hambergerMenu) =>
            //{
            //    IsPresented = true;
            //});
        }
    }
}
