﻿using System;
using System.Collections.Generic;

namespace SiteWorkPermits.Model.SyncImpairements
{
    public class ReporterDetail
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public class ImpairmentPrecautionList
    {
        public int ImpairmentPrecautionMasterId { get; set; }
        public string OtherDescription { get; set; }
    }

    public class SyncImpairements
    {
        public int Id { get; set; }
        public int? ImpairmentMobileId { get; set; }
        public bool? IsParent { get; set; }
        public int? ParentImpairmentId { get; set; }
        public string WorkFlowStatus { get; set; }
        public int ImpairmentTypeId { get; set; }
        public int ImpairmentClassId { get; set; }
        public int ShutDownReasonId { get; set; }
        public string ImpairmentDescription { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string GUID { get; set; }
        public bool? IsDeleted { get; set; }
        public ReporterDetail ReporterDetail { get; set; }
        public List<ImpairmentPrecautionList> ImpairmentPrecautionList { get; set; }
        public List<int> ImpairmentMeasureMasterIdList { get; set; }
        public DateTime? CloseDateTime { get; set; }
    }
}
