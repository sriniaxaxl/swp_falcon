﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SiteWorkPermits.Platform
{
    public interface IPicture
    {
        Task<bool> SavePictureToDisk(string filename, byte[] imageData);
    }
}
