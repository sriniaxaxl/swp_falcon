﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public class ShutDownReasonService:IShutDownReasonService
    {
        private readonly DatabaseContext _databaseContext;
        public ShutDownReasonService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddShutDownReason(ShutDownReason shutDownReason)
        {
            try
            {
                var status = _databaseContext.ShutDownReason.Add(shutDownReason);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<ShutDownReason> GetShutDownReason()
        {
            try
            {
                var shutDownReasons = _databaseContext.ShutDownReason.ToList();
                return shutDownReasons;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ShutDownReason GetShutDownReasonById(int id)
        {
            try
            {
                var shutDownReasons = _databaseContext.ShutDownReason.Where(p => p.Id == id);
                return shutDownReasons.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            var itemsToDelete = _databaseContext.Set<ShutDownReason>();
            _databaseContext.ShutDownReason.RemoveRange(itemsToDelete);
            _databaseContext.SaveChanges();
        }
    }
}
