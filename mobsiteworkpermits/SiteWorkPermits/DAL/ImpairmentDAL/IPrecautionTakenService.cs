﻿using System;
using System.Collections.Generic;
using SiteWorkPermits.Model;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public interface IPrecautionTakenService
    {
        IEnumerable<PrecautionTaken> GetPrecautionTaken();

        PrecautionTaken GetPrecautionTakenById(int id);

        bool AddPrecautionTaken(PrecautionTaken precautionTaken);

        void RemoveAllData();
    }
}
