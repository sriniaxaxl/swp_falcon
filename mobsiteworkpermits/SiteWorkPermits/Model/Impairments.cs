﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteWorkPermits.Model
{

    #region Tab 1 Region Details
    public class ReporterDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReporterDetailId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Impairment Impairment { get; set; }

        [ForeignKey("Impairment")]
        public int ImpairmentId { get; set; }
    }
    #endregion

    #region Tab 3 Precaution 
    // precaution master table
    public class PrecautionTaken
    {
       [Key]
        public int ImpairmentPrecautionId { get; set; }
        public string Description { get; set; }
        public ICollection<ImpairmentPrecautionTaken> ImpairmentPrecautionTakens { get; set; }
    }

    // impairments-Precautions table
    public class ImpairmentPrecautionTaken
    {
        [ForeignKey("PrecautionTaken")]
        public int ImpairmentPrecautionId { get; set; }
        [ForeignKey("Impairment")]
        public int ImpairmentId { get; set; }
        public Impairment Impairment { get; set; }
        public PrecautionTaken PrecautionTaken { get; set; }
        public string OtherDescription { get; set; }
    }

    #endregion

    #region Major Impairments

    public class ImpairmentMeasureMaster
    {       
        [Key]
        public int ImpairmentMeasureMasterId { get; set; }
        public string ImpairmentMeasureDescription { get; set; }
        public ICollection<ImpairmentImpairmentMeasureMaster> ImpairmentImpairmentMeasureMasters { get; set; }
    }

    // impairments-MajorImp table
    public class ImpairmentImpairmentMeasureMaster
    {
        //[Key]
        [ForeignKey("ImpairmentMeasureMaster")]
        public int ImpairmentMeasureMasterId { get; set; }
        //[Key]
        [ForeignKey("Impairment")]
        public int ImpairmentId { get; set; }
        public Impairment Impairment { get; set; }
        public ImpairmentMeasureMaster ImpairmentMeasureMaster { get; set; }
    }


    #endregion

    #region Impairments
    public class Impairment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ImpairmentId { get; set; }
        public int ImpairmentMobileId { get; set; }
        public int ImpairmentServerId { get; set; }
        public bool IsParent { get; set; }
        public int ParentImpairmentId { get; set; }
        public HWStatus WorkFlowStatus { get; set; }
        public bool IsDeleted { get; set; }

        //tab 1
        public ReporterDetail ReporterDetail { get; set; }

        //tab 2
        public int ImpairmentTypeId { get; set; } // class
        public int ImpairmentClassId { get; set; }// class
        public int ShutDownReasonId { get; set; }
        public string ImpairmentDescription { get; set; }
        public DateTime CloseDateTime { get; set; }

        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string CloseTime { get; set; }
        public string SiteId { get; set; }
        public bool IsEdited { get; set; }
        public bool IsNewlyCreated { get; set; }
        //public DateTime StartDateTime { get; set; }
        //public DateTime EndDateTime { get; set; }

        //private DateTime _StartTime = DateTime.Now;
        //public DateTime StartTime   
        //{
        //    get
        //    {
        //        return _StartTime;
        //    }
        //    set
        //    {
        //        _StartTime = value;
        //    }
        //}

        // tab 3
        public ICollection<ImpairmentPrecautionTaken> ImpairmentPrecautionTakens { get; set; }
        public ICollection<ImpairmentMeasureMaster> ImpairmentMeasureMasters { get; set; }
        public ICollection<ImpairmentImpairmentMeasureMaster> ImpairmentImpairmentMeasureMasters { get; set; }
    }
    #endregion

    public class ImpairmentType
    {
        [Key]
        public int Id { get; set; }
        public string TypeDescription { get; set; }
    }

    public class ImpairmentClassType
    {      
        [Key]
        public int Id { get; set; }
        public string ClassDescription { get; set; }
    }

    public class ShutDownReason
    {
        [Key]
        public int Id { get; set; }
        public string ClassDescription { get; set; }
    }

    public enum ImpairementStatus
    {
        NEW,
        ACTIVE,
        UPCOMING,
        DRAFT,
        CLOSED
    }
}
