using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System.ComponentModel;
using CustomControl;
using Android.Text;
using Android.Renderer;
using Android.Renderer.CommanRender;
using Android.Support.V4.Content;
using Android.Graphics;
using Android.Graphics.Drawables;

[assembly: ExportRenderer(typeof(CusEntry), typeof(CustomEntryRenderer))]
namespace Android.Renderer
{
    public class CustomEntryRenderer : EntryRenderer
    {
        #region Private fields and properties

        private BorderRenderer _renderer;
        private const GravityFlags DefaultGravity = GravityFlags.CenterVertical;
        #endregion

        #region Parent override
        Drawable Leftdrawable;
        Drawable rightDrawable;
        Drawable errorIcon;
        int size;
        CusEntry FormEntry;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {

            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
                return;

            FormEntry = Element as CusEntry;
            FormEntry.TextColor = Xamarin.Forms.Color.Black;
            FormEntry.PlaceholderColor = Xamarin.Forms.Color.FromHex("#808080");

            Control.Gravity = DefaultGravity;
            //if (FormEntry.IsMultiline == true)
            //Control.InputType = InputTypes.TextFlagMultiLine;
            //if (FormEntry.IsPassword != true)
            //Control.InputType = InputTypes.TextFlagNoSuggestions;
            UpdateBackground(FormEntry);
            UpdatePadding(FormEntry);
            UpdateTextAlighnment(FormEntry);
            //Control.FocusChange += (sender, res) =>
            //{
            //    if (!res.HasFocus)
            //    {                   
            //        if (FormEntry.Validation == CusEntry.ValidationType.Email)
            //        {
            //            var checkvalid = Validation.Emailvalid(FormEntry.Text);
            //            if (!checkvalid)
            //            {
            //                Control.Error = "Invalid EmailId";
            //            }
            //        }
            //        else if (FormEntry.Validation == CusEntry.ValidationType.Numeric)
            //        {
            //            var checkvalid = Validation.Numericvalid(FormEntry.Text);
            //            if (!checkvalid)
            //            {
            //                Control.Error = "Invalid Number";
            //            }
            //        }
            //        else if (FormEntry.Validation == CusEntry.ValidationType.MobileNumber)
            //        {
            //            var checkvalid = Validation.Mobilevalid(FormEntry.Text);
            //            if (!checkvalid)
            //            {
            //                Control.Error = "Invalid Mobile Number";
            //            }
            //        }
            //       else if (FormEntry.Validation == CusEntry.ValidationType.Length)
            //        {
            //           var checkvalid = Validation.Lengthvalid(FormEntry.Text,FormEntry.TextLength);
            //            if (!checkvalid)
            //            {
            //                Control.Error = "Length Exceed";
            //            }
            //        }
            //       else if (FormEntry.Validation == CusEntry.ValidationType.Password)
            //        {
            //           var checkvalid = Validation.PasswordValidate(FormEntry.Text);
            //            if (!checkvalid)
            //            {
            //                Control.Error = "Password not valid.";
            //            }
            //        }
            //    }
            //};
            //entryEx.TextChanged +=(sender, er) => 
            //{
            //    if(er.NewTextValue.Contains("d"))
            //    {
            //        int resID = Resources.GetIdentifier("user", "drawable", Forms.Context.PackageName);
            //        Drawable Icon = Resources.GetDrawable(resID);
            //        Bitmap rightbitmap = ((BitmapDrawable)Icon).Bitmap;
            //        var  rightimg = new BitmapDrawable(Forms.Context.Resources, Bitmap.CreateScaledBitmap(rightbitmap, size, size, true));
            //        Control.Error = "No valid Input";
            //        //Control.SetError("No valid Input",rightimg);                   
            //    }
            //};
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            var entryEx = Element as CusEntry;
            entryEx.TextColor = Xamarin.Forms.Color.Black;
            entryEx.PlaceholderColor = Xamarin.Forms.Color.FromHex("#808080");
            if (e.PropertyName == CusEntry.BorderWidthProperty.PropertyName ||
                e.PropertyName == CusEntry.BorderColorProperty.PropertyName ||
                e.PropertyName == CusEntry.BorderRadiusProperty.PropertyName ||
                e.PropertyName == CusEntry.BackgroundColorProperty.PropertyName)
            {
                UpdateBackground(entryEx);
            }
            else if (e.PropertyName == CusEntry.PaddingProperty.PropertyName)
            {
                UpdatePadding(entryEx);
            }
            else if (e.PropertyName == Entry.HorizontalTextAlignmentProperty.PropertyName)
            {
                UpdateTextAlighnment(entryEx);
            }
            else if (e.PropertyName == Entry.HeightProperty.PropertyName)
            {
                if (entryEx.HeightRequest > 0)
                {
                    size = Convert.ToInt32(Xamarin.Forms.Forms.Context.ToPixels(entryEx.HeightRequest) * 0.9);

                    if (entryEx.LeftImage != null)
                    {
                        int resID = Resources.GetIdentifier(entryEx.LeftImage, "drawable", Forms.Context.PackageName);
                        Drawable Icon = Resources.GetDrawable(resID);
                        Bitmap Leftbitmap = ((BitmapDrawable)Icon).Bitmap;
                        Leftdrawable = new BitmapDrawable(Forms.Context.Resources, Bitmap.CreateScaledBitmap(Leftbitmap, size, size, true));
                    }
                    if (entryEx.RightImage != null)
                    {
                        int resRightID = Resources.GetIdentifier(entryEx.RightImage, "drawable", Forms.Context.PackageName);
                        Drawable Icon = Resources.GetDrawable(resRightID);
                        Bitmap rightbitmap = ((BitmapDrawable)Icon).Bitmap;
                        rightDrawable = new BitmapDrawable(Forms.Context.Resources, Bitmap.CreateScaledBitmap(rightbitmap, size, size, true));
                    }

                    //Control.SetCompoundDrawablesRelativeWithIntrinsicBounds(Leftdrawable, null, rightDrawable, null);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (_renderer != null)
                {
                    _renderer.Dispose();
                    _renderer = null;
                }
            }
        }

        #endregion

        #region Utility methods

        private void UpdateBackground(CusEntry entryEx)
        {
            if (_renderer != null)
            {
                _renderer.Dispose();
                _renderer = null;
            }
            _renderer = new BorderRenderer();
            if (entryEx.InnerBackground != null)
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.InnerBackground, entryEx.BorderWidth, entryEx.BorderRadius);
            }
            else
            {
                Control.Background = _renderer.GetBorderBackground(entryEx.BorderColor, entryEx.BackgroundColor, entryEx.BorderWidth, entryEx.BorderRadius);
            }

        }

        private void UpdatePadding(CusEntry entryEx)
        {
            Control.SetPadding((int)Forms.Context.ToPixels(entryEx.Padding.Left), (int)Forms.Context.ToPixels(entryEx.Padding.Top), (int)Forms.Context.ToPixels(entryEx.Padding.Right), (int)Forms.Context.ToPixels(entryEx.Padding.Bottom));
        }

        private void UpdateTextAlighnment(CusEntry entryEx)
        {
            var gravity = DefaultGravity;
            switch (entryEx.HorizontalTextAlignment)
            {
                case Xamarin.Forms.TextAlignment.Start:
                    gravity |= GravityFlags.Start;
                    break;
                case Xamarin.Forms.TextAlignment.Center:
                    gravity |= GravityFlags.CenterHorizontal;
                    break;
                case Xamarin.Forms.TextAlignment.End:
                    gravity |= GravityFlags.End;
                    break;
            }
            switch (entryEx.VerticaltextAlignment)
            {
                case Xamarin.Forms.TextAlignment.Start:
                    gravity |= GravityFlags.Top;
                    break;
                case Xamarin.Forms.TextAlignment.Center:
                    gravity |= GravityFlags.CenterVertical;
                    break;
                case Xamarin.Forms.TextAlignment.End:
                    gravity |= GravityFlags.Bottom;
                    break;
            }

            Control.Gravity = gravity;
        }

        #endregion
    }
}