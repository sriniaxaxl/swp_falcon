﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using SiteWorkPermits.ViewModel;
using CustomControl;
using System.Diagnostics;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using SiteWorkPermits.Resx;

namespace SiteWorkPermits.ViewPage
{
    public partial class PrintPermitPage : ContentPage
    {
        public string note1 = AppResource.PRINT_LANDSCAPE_MODE_NOTE;
        public string note2 = AppResource.EMAIL_LABEL;
       
        public PrintPermitPage()
        {
            try
            {
                InitializeComponent();
                On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
                lblNote.Text = note1;
                lblNote1.Text = note2;
                Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
                //this.BackgroundImage = "loginbg.jpg";
                On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            }
            catch (Exception ex) {
                var dd = "";
            }
        }

        void ReadioSel_Tapped(object sender, System.EventArgs e)
        {
            var md = (PrintPermitViewModel)this.BindingContext;
            CusGrid Selgd = (CusGrid)sender;
            Label sellbl = (Label)Selgd.Children[1];
            if (sellbl.Text == "A4")
            {
                md.SelFormat = "A4";
                ImgA4.Source = "radio_on.png";
                ImgUs.Source = "radio_off.png";
            }
            else
            {
                md.SelFormat = "USLETTER";
                ImgA4.Source = "radio_off.png";
                ImgUs.Source = "radio_on.png";
            }

            Debug.WriteLine(md.SelFormat);
        }
    }
}
