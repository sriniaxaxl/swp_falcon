﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IPrecautionsService
    {
        IEnumerable<Precautions> GetPrecautionsAsync();

        Precautions GetPrecautionsByIdAsync(int id);     

        bool AddPrecautionsAsync(Precautions precautions);

        void RemoveAllData();       

    }
}
