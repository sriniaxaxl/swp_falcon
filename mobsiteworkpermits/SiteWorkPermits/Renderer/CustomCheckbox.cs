﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CustomCheckbox : View
    {

        public EventHandler<CheckedArgs> Checked_Changed;
        public CustomCheckbox() : base()
        {
            if (Device.OS == TargetPlatform.iOS)
            {
                HeightRequest = 20;
                WidthRequest = 20;
            }
        }

        public static BindableProperty CheckedProperty = BindableProperty.Create<CustomCheckbox, bool>(o => o.Checked, false);
        public bool Checked
        {
            get { return (bool)GetValue(CheckedProperty); }
            set { SetValue(CheckedProperty, value); }
        }

        public static BindableProperty TextProperty = BindableProperty.Create<CustomCheckbox, string>(o => o.Text, string.Empty);

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CustomCheckbox, Color>(o => o.TextColor, Color.Transparent);

        public Color TextColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty CheckedColorProperty = BindableProperty.Create<CustomCheckbox, Color>(o => o.CheckedColor, Color.Transparent);

        public Color CheckedColor
        {
            get { return (Color)GetValue(CheckedColorProperty); }
            set { SetValue(CheckedColorProperty, value); }
        }


        public static BindableProperty UnCheckedColorProperty = BindableProperty.Create<CustomCheckbox, Color>(o => o.UnCheckedColor, Color.Transparent);

        public Color UnCheckedColor
        {
            get { return (Color)GetValue(UnCheckedColorProperty); }
            set { SetValue(UnCheckedColorProperty, value); }
        }
    }

    public class CheckedArgs
    {
        public bool Checked { get; set; }
    }
}
