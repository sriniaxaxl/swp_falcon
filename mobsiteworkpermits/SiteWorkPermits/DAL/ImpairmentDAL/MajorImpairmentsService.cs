﻿using System;
using System.Collections.Generic;
using System.Linq;
using SiteWorkPermits.Model;
using Microsoft.EntityFrameworkCore;

namespace SiteWorkPermits.DAL.ImpairmentDAL
{
    public class MajorImpairmentsService:IMajorImpairmentsService
    {
        private readonly DatabaseContext _databaseContext;
        public MajorImpairmentsService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        public bool AddImpairmentMeasureMaster(ImpairmentMeasureMaster impairmentMeasureMaster)
        {
            try
            {
                var status = _databaseContext.ImpairmentMeasureMaster.Add(impairmentMeasureMaster);
                _databaseContext.SaveChanges();
                var isAdded = status.State == EntityState.Unchanged;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<ImpairmentMeasureMaster> GetImpairmentMeasureMaster()
        {
            try
            {
                var impairmentMeasureMasters = _databaseContext.ImpairmentMeasureMaster.ToList();
                return impairmentMeasureMasters;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ImpairmentMeasureMaster GetImpairmentMeasureMasterById(int id)
        {
            try
            {
                var impairments = _databaseContext.ImpairmentMeasureMaster.Where(p => p.ImpairmentMeasureMasterId == id);
                return impairments.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void RemoveAllData()
        {
            try
            {
                var itemsToDelete = _databaseContext.Set<ImpairmentMeasureMaster>();
                _databaseContext.ImpairmentMeasureMaster.RemoveRange(itemsToDelete);
                _databaseContext.SaveChanges();
            }
            catch (Exception ex)
            {

            }           
        }
    }
}
