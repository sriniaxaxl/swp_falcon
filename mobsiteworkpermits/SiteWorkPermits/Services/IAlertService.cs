﻿using System;
using System.Threading.Tasks;

namespace SiteWorkPermits
{
    public interface IAlertService
    {
        void DisplayAlert(string Title, string Message, string Accept, string Cancel);

        event EventHandler Valueselected;
    }
}
