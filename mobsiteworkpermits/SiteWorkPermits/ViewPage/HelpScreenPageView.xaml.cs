﻿using Prism.Navigation;
using SiteWorkPermits.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;

namespace SiteWorkPermits.ViewPage
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HelpScreenPageView : ContentPage, ISwipeCallBack
    {
        int status = 1;
        HelpScreenPageViewModel Model;

        public HelpScreenPageView ()
		{
			InitializeComponent ();

            SwipeListener swipeListener1 = new SwipeListener(img_1, this);
            SwipeListener swipeListener2= new SwipeListener(img_2, this);
            SwipeListener swipeListener3 = new SwipeListener(img_3, this);
            SwipeListener swipeListener4 = new SwipeListener(img_4, this);
            SwipeListener swipeListener5 = new SwipeListener(img_5, this);
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundImage = "loginbg.jpg";
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            if (Device.Idiom == TargetIdiom.Phone)
            {
                image1.Source = "dashboard.png";
                image2.Source = "hw_dashboard.png";
                image3.Source = "hw_print.png";
                image4.Source = "imp_dashboard.png";
                image5.Source = "imp_print.png";
            }
            else {
                image1.Source = "dashboard_ipad.png";
                image2.Source = "hw_dashboard_ipad.png";
                image3.Source = "hw_print_ipad.png";
                image4.Source = "imp_dashboard_ipad.png";
                image5.Source = "imp_print_ipad.png";
            }

            img_1.IsVisible = true;
            img_2.IsVisible = false;
            img_3.IsVisible = false;
            img_4.IsVisible = false;
            img_5.IsVisible = false;


            dot_img_1.Source = "green_dot.png";
            dot_img_2.Source = "dark_gray.png";
            dot_img_3.Source = "dark_gray.png";
            dot_img_4.Source = "dark_gray.png";
            dot_img_5.Source = "dark_gray.png";

            if (App.ScreenHeight > 0)
            {
                image1.HeightRequest = App.ScreenHeight * 0.8;
                image2.HeightRequest = App.ScreenHeight * 0.8;
                image3.HeightRequest = App.ScreenHeight * 0.8;
                image4.HeightRequest = App.ScreenHeight * 0.8;
                image5.HeightRequest = App.ScreenHeight * 0.8;
            }
            if (Helper.Getval(Helper.WelcomeShow).ToString() == "true")
            {
               
                Header.IsVisible = true;
                Skipview.IsVisible = false;
            }
            else
            {
                var ss = gridLayout.RowDefinitions[0];
                (ss as RowDefinition).Height = 0;
                Header.IsVisible = false;
                Skipview.IsVisible = true;
            }
        }

        public void onBottomSwipe(View view)
        {
            
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            Model = (HelpScreenPageViewModel)this.BindingContext;
            await Helper.Setval(Helper.WelcomeShow, "true");
        }

        async void SkipClicked(object sender, System.EventArgs e)
        {
            await Model._navigationService.NavigateAsync("LoginPage", null, null, false);
        }

        public void onLeftSwipe(View view)
        {
            if (status >=1 && status <5)
            {
                status++;
                if (status == 1) {
                    img_1.IsVisible = true;
                    img_2.IsVisible = false;
                    img_3.IsVisible = false;
                    img_4.IsVisible = false;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "green_dot.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 2) {
                    img_1.IsVisible = false;
                    img_2.IsVisible = true;
                    img_3.IsVisible = false;
                    img_4.IsVisible = false;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "green_dot.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 3) {
                    img_1.IsVisible = false;
                    img_2.IsVisible = false;
                    img_3.IsVisible = true;
                    img_4.IsVisible = false;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "green_dot.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 4) {
                    img_1.IsVisible = false;
                    img_2.IsVisible = false;
                    img_3.IsVisible = false;
                    img_4.IsVisible = true;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "green_dot.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 5)
                {
                    img_1.IsVisible = false;
                    img_2.IsVisible = false;
                    img_3.IsVisible = false;
                    img_4.IsVisible = false;
                    img_5.IsVisible = true;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "green_dot.png";
                }


            }
        }

        public void onNothingSwiped(View view)
        {
            
        }

        public void onRightSwipe(View view)
        {
            if (status <=5 && status >1)
            {
                status--;
                if (status == 1)
                {
                    img_1.IsVisible = true;
                    img_2.IsVisible = false;
                    img_3.IsVisible = false;
                    img_4.IsVisible = false;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "green_dot.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 2)
                {
                    img_1.IsVisible = false;
                    img_2.IsVisible = true;
                    img_3.IsVisible = false;
                    img_4.IsVisible = false;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "green_dot.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 3)
                {
                    img_1.IsVisible = false;
                    img_2.IsVisible = false;
                    img_3.IsVisible = true;
                    img_4.IsVisible = false;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "green_dot.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 4)
                {
                    img_1.IsVisible = false;
                    img_2.IsVisible = false;
                    img_3.IsVisible = false;
                    img_4.IsVisible = true;
                    img_5.IsVisible = false;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "green_dot.png";
                    dot_img_5.Source = "dark_gray.png";
                }
                else if (status == 5)
                {
                    img_1.IsVisible = false;
                    img_2.IsVisible = false;
                    img_3.IsVisible = false;
                    img_4.IsVisible = false;
                    img_5.IsVisible = true;
                    dot_img_1.Source = "dark_gray.png";
                    dot_img_2.Source = "dark_gray.png";
                    dot_img_3.Source = "dark_gray.png";
                    dot_img_4.Source = "dark_gray.png";
                    dot_img_5.Source = "green_dot.png";
                }

            }
        }

        public void onTopSwipe(View view)
        {
           
        }
    }
}