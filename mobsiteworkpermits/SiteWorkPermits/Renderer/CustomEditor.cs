﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomControl
{
    public class CustomEditor : Editor
    {

        public CustomEditor()
        {

        }

        #region Properties   

        public static BindableProperty PaddingProperty = BindableProperty.Create<CustomEditor, Thickness>(o => o.Padding, new Thickness(5));
        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public static BindableProperty BorderColorProperty = BindableProperty.Create<CustomEditor, Color>(o => o.BorderColor, Color.Transparent);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static BindableProperty BorderWidthProperty = BindableProperty.Create<CustomEditor, float>(o => o.BorderWidth, 0);

        public float BorderWidth
        {
            get { return (float)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }

        public static BindableProperty InnerBackgroundColorProperty = BindableProperty.Create<CustomEditor, Color>(o => o.InnerBackground, Color.Transparent);

        public Color InnerBackground
        {
            get { return (Color)GetValue(InnerBackgroundColorProperty); }
            set { SetValue(InnerBackgroundColorProperty, value); }
        }

        public static BindableProperty BorderRadiusProperty = BindableProperty.Create<CustomEditor, float>(o => o.BorderRadius, 0);

        public float BorderRadius
        {
            get { return (float)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        public static BindableProperty VerticaltextAlignmentProperty = BindableProperty.Create<CustomEditor, TextAlignment>(o => o.VerticaltextAlignment, TextAlignment.Center);

        public TextAlignment VerticaltextAlignment
        {
            get { return (TextAlignment)GetValue(VerticaltextAlignmentProperty); }
            set { SetValue(VerticaltextAlignmentProperty, value); }
        }

        public static BindableProperty IsMultilineProperty = BindableProperty.Create<CustomEditor, bool>(o => o.IsMultiline, false);

        public bool IsMultiline
        {
            get { return (bool)GetValue(IsMultilineProperty); }
            set { SetValue(IsMultilineProperty, value); }
        }


        public static BindableProperty PlaceholderProperty = BindableProperty.Create<CustomEditor, string>(o => o.Placeholder, string.Empty);

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public static BindableProperty PlaceholderColorProperty = BindableProperty.Create<CustomEditor, Color>(o => o.PlaceholderColor, Color.Transparent);
        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }        

        #endregion
    }
}
