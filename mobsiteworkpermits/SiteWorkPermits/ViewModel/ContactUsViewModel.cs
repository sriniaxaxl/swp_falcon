﻿using System;
using System.Text.RegularExpressions;
using SiteWorkPermits.Platform;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace SiteWorkPermits.ViewModel
{
    public class ContactUsViewModel : BindableBase, INavigatedAware
    {
        public INavigationService _navigationService;
        IPageDialogService _pageDialog;
        public DelegateCommand<string> OnNavigateCommand { get; set; }
        public ContactUsViewModel(INavigationService navigationService,
                                        IPageDialogService pageDialog)
        {
            _navigationService = navigationService;
            _pageDialog = pageDialog;
            var analyticsService = Xamarin.Forms.DependencyService.Get<IAnalyticsService>();
            //You can use any of the LogEvent Overloads, for example:  
            OnNavigateCommand = new DelegateCommand<string>(NavigateAsync);
            analyticsService.LogScreen("CONTACT US");
        }

        private async void NavigateAsync(string page)
        {
            // bool isDownloadedSuccess = false;
            if (page.Equals("Email"))
            {
                string shareurl = String.Empty;
                if (Device.RuntimePlatform == Device.iOS)
                {
                    var email = Regex.Replace("mysite@axaxl.com", @"[^\u0000-\u00FF]", string.Empty);
                    shareurl = "mailto:" + email + "?subject=";
                }
                else
                {
                    //for Android it is not necessary to code nor is it necessary to assign a destination email
                    shareurl = "mailto:mysite@axaxl.com";
                }
                Device.OpenUri(new Uri(shareurl));

            }
            else if (page.Equals("Help"))
            {
                await _navigationService.NavigateAsync(new Uri("HomePage/NavigationPage/GuidePage", System.UriKind.Relative));

                // await _navigationService.NavigateAsync("GuidePage"); 
            }
        }
        private bool _showSyncIcon;
        public bool ShowSyncIcon
        {
            get { return _showSyncIcon; }
            set { SetProperty(ref _showSyncIcon, value); }
        }

        private string _pageTitle;
        public string PageTitle
        {
            get { return _pageTitle; }
            set { SetProperty(ref _pageTitle, value); }
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.Count > 0)
            {
                PageTitle = (string)parameters["navigationParameter"];
            }
        }

        //public void OnNavigatedFrom(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}

        //public void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    //throw new NotImplementedException();
        //}
    }
}
