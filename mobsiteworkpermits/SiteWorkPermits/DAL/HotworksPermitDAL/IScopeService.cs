﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public interface IScopeService
    {
        IEnumerable<Scopes> GetScopeAsync();

        Scopes GetScopeByIdAsync(int id);

        bool AddScopeAsync(Scopes scope);

        bool UpdateScopeAsync(Scopes scope);

        bool RemoveScopeAsync(int id);

        void RemoveAllData();

    }
}
