﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SiteWorkPermits.Model;
using SiteWorkPermits.Platform;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;

namespace SiteWorkPermits.DAL.HotworksPermitDAL
{
    public class HighHazardAreasService:IHighHazardAreasService
    {
        private readonly DatabaseContext _databaseContext;
        public HighHazardAreasService()
        {
            _databaseContext = DataContextEntity.Instance();
        }

        //public  bool AddHighHazardAreasAsync(HighHazardAreas fighHazardAreas)
        //{
        //    try
        //    {
        //        var fighHazardArea =  _databaseContext.HighHazardAreas.Add(fighHazardAreas);
        //         _databaseContext.SaveChanges();
        //        var isAdded = fighHazardArea.State == EntityState.Unchanged;
        //        return isAdded;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
        //}

        //public  IEnumerable<HighHazardAreas> GetHighHazardAreasAsync()
        //{
        //    try
        //    {
        //        var fighHazardAreas =  _databaseContext.HighHazardAreas.ToList();
        //        return fighHazardAreas;
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

        //public HighHazardAreas GetHighHazardAreasByIdAsync(int id)
        //{
        //    try
        //    {
        //        var scope = _databaseContext.HighHazardAreas.Find(id);
        //        return scope;
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

        //public bool RemoveHighHazardAreasAsync(int id)
        //{
        //    throw new NotImplementedException();
        //}

        //public  bool UpdateHighHazardAreasAsync(HighHazardAreas fighHazardAreas)
        //{
        //    try
        //    {
        //        var scopeStatus = _databaseContext.Update(fighHazardAreas);
        //         _databaseContext.SaveChanges();
        //        var isUpdated = scopeStatus.State == EntityState.Unchanged;
        //        return isUpdated;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
        //}

        //public void RemoveAllData()
        //{
        //    var itemsToDelete = _databaseContext.Set<HighHazardAreas>();
        //    _databaseContext.HighHazardAreas.RemoveRange(itemsToDelete);
        //    _databaseContext.SaveChanges();
        //}
    }
}
