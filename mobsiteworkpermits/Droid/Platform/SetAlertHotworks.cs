﻿using System;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using SiteWorkPermits.Droid.Platform;
using SiteWorkPermits.Droid.Services;
using SiteWorkPermits.Platform;
using Java.Util;
using Xamarin.Forms;
using Android.Provider;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency(typeof(SetAlertHotworks))]
namespace SiteWorkPermits.Droid.Platform
{
    public class SetAlertHotworks:ISetAlertHotworks
    {        
        public void SetStartAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime startTime)
        {
            CancelAlertNotification(alertId);
            Calendar calendar = Calendar.Instance;
            calendar.Set(startTime.Year, startTime.Month - 1, startTime.Day, startTime.Hour, startTime.Minute, startTime.Second);
           
            var ctx =   Xamarin.Forms.Forms.Context;
            //long now = SystemClock.CurrentThreadTimeMillis();
            AlarmManager am = (AlarmManager)ctx.GetSystemService(Context.AlarmService);
            Intent intent = new Intent(ctx,typeof(StartdbCheckServiceReciver));
            intent.PutExtra("hotworkTitle", hotworkTitle);
            intent.PutExtra("hotworkDescription", hotworkDescription);
            if (alertId == 11991)
            {
            }
            else {
                var HotWorksID = alertId.ToString().Substring(0, alertId.ToString().Length - 3);
                intent.PutExtra("hotworkID", HotWorksID);
            }
            PendingIntent pi = PendingIntent.GetBroadcast(ctx, alertId, intent, PendingIntentFlags.UpdateCurrent);
            am.SetExactAndAllowWhileIdle(AlarmType.Rtc,calendar.TimeInMillis, pi); 
        }    

        public void SetEndAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            CancelAlertNotification(alertId);
            Calendar calendar = Calendar.Instance;
            calendar.Set(endTime.Year, endTime.Month - 1, endTime.Day, endTime.Hour, endTime.Minute, endTime.Second);
       
            var ctx = Xamarin.Forms.Forms.Context;
            long now = SystemClock.CurrentThreadTimeMillis();
            AlarmManager am = (AlarmManager)ctx.GetSystemService(Context.AlarmService);
            Intent intent = new Intent(ctx,typeof(StartdbCheckServiceReciver));
            intent.PutExtra("hotworkTitle", hotworkTitle);
            intent.PutExtra("hotworkDescription", hotworkDescription);
            var HotWorksID = alertId.ToString().Substring(0, alertId.ToString().Length - 3);
            intent.PutExtra("hotworkID", HotWorksID);
            PendingIntent pi = PendingIntent.GetBroadcast(ctx, alertId, intent, PendingIntentFlags.UpdateCurrent);
            am.SetExactAndAllowWhileIdle(AlarmType.Rtc, calendar.TimeInMillis, pi); 
        }


        public void SetGoingToEndAlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            CancelAlertNotification(alertId);
            Calendar calendar = Calendar.Instance;
            calendar.Set(endTime.Year, endTime.Month - 1, endTime.Day, endTime.Hour, endTime.Minute, endTime.Second);
       
            var ctx = Xamarin.Forms.Forms.Context;
            long now = SystemClock.CurrentThreadTimeMillis();
            AlarmManager am = (AlarmManager)ctx.GetSystemService(Context.AlarmService);
            Intent intent = new Intent(ctx,typeof(StartdbCheckServiceReciver));
            intent.PutExtra("hotworkTitle", hotworkTitle);
            intent.PutExtra("hotworkDescription", hotworkDescription);
            var HotWorksID = alertId.ToString().Substring(0, alertId.ToString().Length - 3);
            intent.PutExtra("hotworkID", HotWorksID);
            PendingIntent pi = PendingIntent.GetBroadcast(ctx, alertId, intent, PendingIntentFlags.UpdateCurrent);         
            am.SetExactAndAllowWhileIdle(AlarmType.Rtc, calendar.TimeInMillis, pi); 
        }

        public void SetFirewatcher1AlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            CancelAlertNotification(alertId);
            Calendar calendar = Calendar.Instance;
            calendar.Set(endTime.Year, endTime.Month - 1, endTime.Day, endTime.Hour, endTime.Minute, endTime.Second);
       
            var ctx = Xamarin.Forms.Forms.Context;
            long now = SystemClock.CurrentThreadTimeMillis();
            AlarmManager am = (AlarmManager)ctx.GetSystemService(Context.AlarmService);
            Intent intent = new Intent(ctx,typeof(StartdbCheckServiceReciver));
            intent.PutExtra("hotworkTitle", hotworkTitle);
            intent.PutExtra("hotworkDescription", hotworkDescription);
            var HotWorksID = alertId.ToString().Substring(0, alertId.ToString().Length - 3);
            intent.PutExtra("hotworkID", HotWorksID);
            PendingIntent pi = PendingIntent.GetBroadcast(ctx, alertId, intent, PendingIntentFlags.UpdateCurrent);
            am.SetExactAndAllowWhileIdle(AlarmType.Rtc, calendar.TimeInMillis, pi); 
        }

        public void SetFirewatcher2AlertNotification(string hotworkTitle, string hotworkDescription, int alertId, DateTime endTime)
        {
            CancelAlertNotification(alertId);
            Calendar calendar = Calendar.Instance;
            calendar.Set(endTime.Year, endTime.Month - 1, endTime.Day, endTime.Hour, endTime.Minute, endTime.Second);       
            var ctx = Xamarin.Forms.Forms.Context;
            long now = SystemClock.CurrentThreadTimeMillis();
            AlarmManager am = (AlarmManager)ctx.GetSystemService(Context.AlarmService);
            Intent intent = new Intent(ctx,typeof(StartdbCheckServiceReciver));
            intent.PutExtra("hotworkTitle", hotworkTitle);
            intent.PutExtra("hotworkDescription", hotworkDescription);
            var HotWorksID = alertId.ToString().Substring(0, alertId.ToString().Length - 3);
            intent.PutExtra("hotworkID", HotWorksID);
            PendingIntent pi = PendingIntent.GetBroadcast(ctx, alertId, intent, PendingIntentFlags.UpdateCurrent);
            am.SetExactAndAllowWhileIdle(AlarmType.Rtc, calendar.TimeInMillis, pi); 
        }

        public void CancelAlertNotification(int alertId)
        {
            var ctx = Xamarin.Forms.Forms.Context;
            long now = SystemClock.CurrentThreadTimeMillis();
            AlarmManager am = (AlarmManager)ctx.GetSystemService(Context.AlarmService);
            Intent intent = new Intent(ctx, typeof(StartdbCheckServiceReciver));
            PendingIntent pi = PendingIntent.GetBroadcast(ctx, alertId, intent, PendingIntentFlags.UpdateCurrent);
            am.Cancel(pi);
        }
    }
}
